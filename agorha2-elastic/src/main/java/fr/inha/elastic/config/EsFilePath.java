package fr.inha.elastic.config;

/**
 * Chemin des fichiers de mapping et settings pour Elasticsearch.
 * 
 * @author mhusser
 *
 */
public enum EsFilePath {
  SETTINGS("/mapping_es/index_settings_default.json"),
  COLLECTION("/mapping_es/mapping_collection.json"),
  PERSON("/mapping_es/mapping_person.json"),
  ARTWORK("/mapping_es/mapping_artwork.json"),
  REFERENCE("/mapping_es/mapping_reference.json"),
  EVENT("/mapping_es/mapping_event.json"),
  MEDIA("/mapping_es/mapping_media.json"),
  USER("/mapping_es/mapping_user.json"),
  DATABASE("/mapping_es/mapping_database.json"),
  SELECTION("/mapping_es/mapping_selection.json"),
  SAVED_SEARCH("/mapping_es/mapping_saved_search.json"),
  NEWS("/mapping_es/mapping_news.json");

  protected String fileName;

  private EsFilePath(String fileName) {
    this.fileName = fileName;
  }

  public String getFileName() {
    return fileName;
  }
}
