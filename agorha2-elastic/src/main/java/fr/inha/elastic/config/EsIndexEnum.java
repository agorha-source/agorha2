package fr.inha.elastic.config;

import com.sword.utils.elasticsearch.config.IIndex;
import java.util.Calendar;

/**
 * Nom des indexes pour Elasticsearch.
 * 
 * @author mhusser
 *
 */
public enum EsIndexEnum implements IIndex {
  COLLECTION("collection"),
  PERSON("person"),
  ARTWORK("artwork"),
  REFERENCE("reference"),
  EVENT("event"),
  NOTICE("notice"),
  CONTENT("content"),
  MEDIA("media"),
  USER("user"),
  DATABASE("database"),
  SELECTION("selection"),
  SAVED_SEARCH("saved_search"),
  NEWS("news"),

  // Index utilisés pour la création, avec ajout d'un alias du nom des types
  // ci-dessus
  CREATE_COLLECTION(COLLECTION.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis())),
  CREATE_PERSON(PERSON.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis())),
  CREATE_ARTWORK(ARTWORK.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis())),
  CREATE_REFERENCE(REFERENCE.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis())),
  CREATE_EVENT(EVENT.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis())),
  CREATE_MEDIA(MEDIA.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis())),
  CREATE_USER(USER.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis())),
  CREATE_DATABASE(DATABASE.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis())),
  CREATE_SELECTION(SELECTION.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis())),
  CREATE_NEWS(NEWS.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis())),
  CREATE_SAVED_SEARCH(SAVED_SEARCH.getName() + String.valueOf(Calendar.getInstance().getTimeInMillis()));

  protected String name;

  private EsIndexEnum(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return this.name;
  }
}
