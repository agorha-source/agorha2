package fr.inha.elastic.config;

import com.sword.utils.elasticsearch.config.IClusterKey;

/**
 * Noms des clusters pour ElasticSearch.
 * 
 * @author mhusser
 *
 */
public enum EsClusterEnum implements IClusterKey {
  AGORHA2;
}
