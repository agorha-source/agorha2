package fr.inha.elastic.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sword.utils.elasticsearch.config.IClusterKey;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import com.sword.utils.elasticsearch.intf.IServiceDedicated;
import com.sword.utils.elasticsearch.intf.IVersionableServiceDedicated;
import com.sword.utils.elasticsearch.services.EsServiceAdmin;
import com.sword.utils.elasticsearch.services.EsServiceGeneral;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.exception.ElasticException;
import fr.inha.elastic.model.Database;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.Notice;
import fr.inha.elastic.model.SavedSearch;
import fr.inha.elastic.model.Selection;
import fr.inha.elastic.model.User;
import org.elasticsearch.client.ClusterClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

/**
 * Placed in package ABOVE others else (ex. in .config) doesn't scan them.
 * 
 * @author mdutoo
 */
@Configuration
// add here dependencies' Configurations to be instanciated
@ComponentScan(basePackageClasses = { ElasticSearchConfiguration.class })
public class ElasticSearchConfiguration {

  private IClusterKey clusterKey = EsClusterEnum.AGORHA2;
  @Value("${agorha2.es.clusterName}")
  private String clusterName;
  @Value("${agorha2.es.hosts}")
  private String[] hosts;
  @Value("${agorha2.es.port}")
  private int port;
  // TODO vérifier la conf pour la prod
  @Value("${agorha2.es.number_of_shards}")
  private int numberOfShards;
  @Value("${agorha2.es.number_of_replicas}")
  private int numberOfReplicas;

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Bean
  public ObjectMapper esObjectMapper() {
    DefaultEsContext.instance().setMapperConfigurator(EsMapperConfigurator.getInstance());
    EsMapperConfigurator.getInstance().configureMapper(DefaultEsContext.instance().getObjectMapper());
    return DefaultEsContext.instance().getObjectMapper();
  }

  @Bean
  public EsClient esClient(ObjectMapper esObjectMapper) throws ElasticException {
    return new EsClient(clusterKey, clusterName, port, hosts);
  }

  @Bean
  public EsServiceAdmin esAdminService(EsClient esClient) {
    return esClient.getAdminService();
  }

  @Bean
  public EsServiceGeneral esService(EsClient esClient) {
    return esClient.getServiceGeneral();
  }

  @Bean
  public IVersionableServiceDedicated<Notice> esNoticeService(EsClient esClient) {
    return esClient.getServiceNotice();
  }

  @Bean
  public IVersionableServiceDedicated<Notice> esCollectionService(EsClient esClient) {
    return esClient.getServiceCollection();
  }

  @Bean
  public IVersionableServiceDedicated<Notice> esArtworkService(EsClient esClient) {
    return esClient.getServiceArtwork();
  }

  @Bean
  public IVersionableServiceDedicated<Notice> esReferenceService(EsClient esClient) {
    return esClient.getServiceReference();
  }

  @Bean
  public IVersionableServiceDedicated<Notice> esPersonService(EsClient esClient) {
    return esClient.getServicePerson();
  }

  @Bean
  public IVersionableServiceDedicated<Notice> esEventService(EsClient esClient) {
    return esClient.getServiceEvent();
  }

  @Bean
  public IServiceDedicated<Media> esMediaService(EsClient esClient) {
    return esClient.getServiceMedia();
  }

  @Bean
  public IServiceDedicated<User> esUserService(EsClient esClient) {
    return esClient.getServiceUser();
  }

  @Bean
  public IServiceDedicated<Database> esDatabaseService(EsClient esClient) {
    return esClient.getServiceDatabase();
  }

  @Bean
  public IServiceDedicated<Selection> esSelectionService(EsClient esClient) {
    return esClient.getServiceSelection();
  }

  @Bean
  public IServiceDedicated<SavedSearch> esSavedSearchService(EsClient esClient) {
    return esClient.getServiceSavedSearch();
  }

  @Bean
  public ClusterClient esClusterService(EsClient esClient) {
    return esClient.getEsRestClient().cluster();
  }

}