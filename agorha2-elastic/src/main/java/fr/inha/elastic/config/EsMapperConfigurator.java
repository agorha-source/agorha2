package fr.inha.elastic.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.sword.utils.elasticsearch.intf.IMapperConfigurator;

public class EsMapperConfigurator implements IMapperConfigurator {

  private static final EsMapperConfigurator instance = new EsMapperConfigurator();

  public static EsMapperConfigurator getInstance() {
    return instance;
  }

  private EsMapperConfigurator() {
    super();
  }

  public void configureMapper(ObjectMapper mapper) {
    // Permet de conserver les dates lisibles dans le source JSON envoyé à ES
    // (utilisation dans Kibana par ex)
    mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    // Ne pas écrire l'id de le Timezone
    mapper.configure(SerializationFeature.WRITE_DATES_WITH_ZONE_ID, false);
    // Ne pas inclure les valeurs vides dans les JSON
    mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
    // Prise en compte des objets LocalDateTime de Java 8 pour un sérialisation ISO
    mapper.registerModule(new JavaTimeModule());
    // Ignorer les propriétés non connues lors de la désérialisation
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
  }
}
