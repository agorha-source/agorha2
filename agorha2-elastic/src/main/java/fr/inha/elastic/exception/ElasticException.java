package fr.inha.elastic.exception;

/**
 * Exception pour le client ES.
 * 
 * @author mhusser
 *
 */
public class ElasticException extends Exception {

  private static final long serialVersionUID = 3433547135272033834L;

  public ElasticException(String message) {
    super(message);
  }

  public ElasticException(String message, Throwable cause) {
    super(message, cause);
  }
}
