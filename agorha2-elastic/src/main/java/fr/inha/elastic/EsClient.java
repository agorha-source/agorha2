package fr.inha.elastic;

import com.sword.utils.elasticsearch.config.IClusterKey;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.intf.IServiceDedicated;
import com.sword.utils.elasticsearch.intf.IVersionableServiceDedicated;
import com.sword.utils.elasticsearch.services.EsServiceAdmin;
import com.sword.utils.elasticsearch.services.EsServiceDedicated;
import com.sword.utils.elasticsearch.services.EsServiceGeneral;
import com.sword.utils.elasticsearch.services.EsVersionableServiceDedicated;
import fr.inha.elastic.config.EsFilePath;
import fr.inha.elastic.config.EsIndexEnum;
import fr.inha.elastic.config.EsMapperConfigurator;
import fr.inha.elastic.exception.ElasticException;
import fr.inha.elastic.model.Database;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.Notice;
import fr.inha.elastic.model.SavedSearch;
import fr.inha.elastic.model.Selection;
import fr.inha.elastic.model.User;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.client.RestHighLevelClient;

/**
 * Client pour la connexion et les actions avec ElasticSearch.
 * 
 * @author mhusser
 *
 */
public class EsClient {

  protected IClusterKey clusterKey;
  protected String clusterName;
  protected String[] hosts;
  protected int port;

  protected RestHighLevelClient esRestClient;
  protected EsServiceAdmin adminService;
  protected EsServiceGeneral serviceGeneral;
  protected EsVersionableServiceDedicated<Notice> serviceNotice;
  protected EsVersionableServiceDedicated<Notice> serviceCollection;
  protected EsVersionableServiceDedicated<Notice> serviceArtwork;
  protected EsVersionableServiceDedicated<Notice> serviceReference;
  protected EsVersionableServiceDedicated<Notice> servicePerson;
  protected EsVersionableServiceDedicated<Notice> serviceEvent;
  protected EsServiceDedicated<Media> serviceMedia;
  protected EsServiceDedicated<User> serviceUser;
  protected EsServiceDedicated<Database> serviceDatabase;
  protected EsServiceDedicated<Selection> serviceSelection;
  protected EsServiceDedicated<SavedSearch> serviceSavedSearch;
  protected BulkProcessor bulkProcessorNotice;

  private int bulkNumber = 10000;
  private int bulkMoSize = 5;
  private int bulkFlushInterval = 5;
  private int backoffTime = 100;
  private int backoffRetry = 3;
  private int concurrentRequest = 1;

  public EsClient(IClusterKey clusterKey, String clusterName, int port, String... hosts) throws ElasticException {
    this.clusterKey = clusterKey;
    this.clusterName = clusterName;
    this.hosts = hosts;
    this.port = port;

    DefaultEsContext.instance().addCluster(clusterKey, clusterName, port, false, null, null, hosts);
    DefaultEsContext.instance().setMapperConfigurator(EsMapperConfigurator.getInstance());
    EsMapperConfigurator.getInstance().configureMapper(DefaultEsContext.instance().getObjectMapper());

    esRestClient = DefaultEsContext.instance().getRestClient(clusterKey);
    serviceGeneral = new EsServiceGeneral(DefaultEsContext.instance(), clusterKey);
    adminService = new EsServiceAdmin(DefaultEsContext.instance(), clusterKey);

    initIndices();
  }

  public RestHighLevelClient getEsRestClient() {
    return this.esRestClient;
  }

  public EsServiceGeneral getServiceGeneral() {
    return serviceGeneral;
  }

  public EsServiceAdmin getAdminService() {
    return this.adminService;
  }

  public IVersionableServiceDedicated<Notice> getServiceNotice() {
    if (serviceNotice == null) {
      initServiceNotice();
    }
    return serviceNotice;
  }

  public IVersionableServiceDedicated<Notice> getServiceCollection() {
    if (serviceCollection == null) {
      initServiceCollection();
    }
    return serviceCollection;
  }

  public IVersionableServiceDedicated<Notice> getServiceArtwork() {
    if (serviceArtwork == null) {
      initServiceArtwork();
    }
    return serviceArtwork;
  }

  public IVersionableServiceDedicated<Notice> getServiceReference() {
    if (serviceReference == null) {
      initServiceReference();
    }
    return serviceReference;
  }

  public IVersionableServiceDedicated<Notice> getServicePerson() {
    if (servicePerson == null) {
      initServicePerson();
    }
    return servicePerson;
  }

  public IVersionableServiceDedicated<Notice> getServiceEvent() {
    if (serviceEvent == null) {
      initServiceEvent();
    }
    return serviceEvent;
  }

  public IServiceDedicated<Media> getServiceMedia() {
    if (serviceMedia == null) {
      initServiceMedia();
    }
    return serviceMedia;
  }

  public IServiceDedicated<User> getServiceUser() {
    if (serviceUser == null) {
      initServiceUser();
    }
    return serviceUser;
  }

  public IServiceDedicated<Database> getServiceDatabase() {
    initServiceDatabase();
    return serviceDatabase;
  }

  public IServiceDedicated<Selection> getServiceSelection() {
    initServiceSelection();
    return serviceSelection;
  }

  public IServiceDedicated<SavedSearch> getServiceSavedSearch() {
    initServiceSavedSearch();
    return serviceSavedSearch;
  }

  protected synchronized void initServiceNotice() {
    if (serviceNotice == null) {
      serviceNotice = new EsVersionableServiceDedicated<>(DefaultEsContext.instance(), clusterKey, EsIndexEnum.NOTICE,
          Notice.class);
    }
  }

  protected synchronized void initServiceCollection() {
    if (serviceCollection == null) {
      serviceCollection = new EsVersionableServiceDedicated<>(DefaultEsContext.instance(), clusterKey,
          EsIndexEnum.COLLECTION, Notice.class);
    }
  }

  protected synchronized void initServiceArtwork() {
    if (serviceArtwork == null) {
      serviceArtwork = new EsVersionableServiceDedicated<>(DefaultEsContext.instance(), clusterKey, EsIndexEnum.ARTWORK,
          Notice.class);
    }
  }

  protected synchronized void initServiceReference() {
    if (serviceReference == null) {
      serviceReference = new EsVersionableServiceDedicated<>(DefaultEsContext.instance(), clusterKey,
          EsIndexEnum.REFERENCE, Notice.class);
    }
  }

  protected synchronized void initServicePerson() {
    if (servicePerson == null) {
      servicePerson = new EsVersionableServiceDedicated<>(DefaultEsContext.instance(), clusterKey, EsIndexEnum.PERSON,
          Notice.class);
    }
  }

  protected synchronized void initServiceEvent() {
    if (serviceEvent == null) {
      serviceEvent = new EsVersionableServiceDedicated<>(DefaultEsContext.instance(), clusterKey, EsIndexEnum.EVENT,
          Notice.class);
    }
  }

  protected synchronized void initServiceMedia() {
    if (serviceMedia == null) {
      serviceMedia = new EsServiceDedicated<>(DefaultEsContext.instance(), clusterKey, EsIndexEnum.MEDIA, Media.class);
    }
  }

  protected void initServiceUser() {
    if (serviceUser == null) {
      serviceUser = new EsServiceDedicated<>(DefaultEsContext.instance(), clusterKey, EsIndexEnum.USER, User.class);
    }
  }

  protected void initServiceDatabase() {
    if (serviceDatabase == null) {
      serviceDatabase = new EsServiceDedicated<>(DefaultEsContext.instance(), clusterKey, EsIndexEnum.DATABASE,
          Database.class);
    }
  }

  protected void initServiceSelection() {
    if (serviceSelection == null) {
      serviceSelection = new EsServiceDedicated<>(DefaultEsContext.instance(), clusterKey, EsIndexEnum.SELECTION,
          Selection.class);
    }
  }

  protected void initServiceSavedSearch() {
    if (serviceSavedSearch == null) {
      serviceSavedSearch = new EsServiceDedicated<>(DefaultEsContext.instance(), clusterKey, EsIndexEnum.SAVED_SEARCH,
          SavedSearch.class);
    }
  }

  public BulkProcessor getBulkProcessorNotice() {
    return bulkProcessorNotice;
  }

  protected void initIndices() throws ElasticException {
    // Fichier avec les paramètres pour les indexes
    InputStream settingsDefaultInputStream;
    String settingsDefault;

    try {
      settingsDefaultInputStream = getClass().getResourceAsStream(EsFilePath.SETTINGS.getFileName());
      settingsDefault = IOUtils.toString(settingsDefaultInputStream, StandardCharsets.UTF_8.name());
    } catch (IOException ex) {
      throw new ElasticsearchException(
          "Impossible de lire le fichier de paramétrage des propriétés de l'index Elasticsearch", ex);
    }

    try {
      // Création des indexes
      createIndexIfNotExist(EsIndexEnum.CREATE_REFERENCE, EsIndexEnum.REFERENCE, EsFilePath.REFERENCE, settingsDefault);
      createIndexIfNotExist(EsIndexEnum.CREATE_ARTWORK, EsIndexEnum.ARTWORK, EsFilePath.ARTWORK, settingsDefault);
      createIndexIfNotExist(EsIndexEnum.CREATE_COLLECTION, EsIndexEnum.COLLECTION, EsFilePath.COLLECTION,
          settingsDefault);
      createIndexIfNotExist(EsIndexEnum.CREATE_EVENT, EsIndexEnum.EVENT, EsFilePath.EVENT, settingsDefault);
      createIndexIfNotExist(EsIndexEnum.CREATE_PERSON, EsIndexEnum.PERSON, EsFilePath.PERSON, settingsDefault);
      createIndexIfNotExist(EsIndexEnum.CREATE_MEDIA, EsIndexEnum.MEDIA, EsFilePath.MEDIA, settingsDefault);
      createIndexIfNotExist(EsIndexEnum.CREATE_USER, EsIndexEnum.USER, EsFilePath.USER, settingsDefault);
      createIndexIfNotExist(EsIndexEnum.CREATE_DATABASE, EsIndexEnum.DATABASE, EsFilePath.DATABASE, settingsDefault);
      createIndexIfNotExist(EsIndexEnum.CREATE_SELECTION, EsIndexEnum.SELECTION, EsFilePath.SELECTION, settingsDefault);
      createIndexIfNotExist(EsIndexEnum.CREATE_SAVED_SEARCH, EsIndexEnum.SAVED_SEARCH, EsFilePath.SAVED_SEARCH,
          settingsDefault);
      createIndexIfNotExist(EsIndexEnum.CREATE_NEWS, EsIndexEnum.NEWS, EsFilePath.NEWS, settingsDefault);

      // Création de l'alias pour la recherche
      createAliasIfNotExist(Arrays.asList(EsIndexEnum.CREATE_REFERENCE, EsIndexEnum.CREATE_ARTWORK,
          EsIndexEnum.CREATE_COLLECTION, EsIndexEnum.CREATE_EVENT, EsIndexEnum.CREATE_PERSON), EsIndexEnum.NOTICE);

      createAliasIfNotExist(Arrays.asList(EsIndexEnum.CREATE_REFERENCE, EsIndexEnum.CREATE_ARTWORK,
          EsIndexEnum.CREATE_COLLECTION, EsIndexEnum.CREATE_EVENT, EsIndexEnum.CREATE_PERSON, EsIndexEnum.CREATE_NEWS),
          EsIndexEnum.CONTENT);

      bulkProcessorNotice = getServiceNotice().buildBulkProcessor("noticeProcessor", bulkNumber, bulkMoSize, bulkFlushInterval,
          backoffTime, backoffRetry, concurrentRequest);

    } catch (IOException ex) {
      throw new ElasticException("Impossible de lire un ou plusieurs fichiers de définition des mappings Elasticsearch",
          ex);
    } catch (EsRequestException ex) {
      throw new ElasticException("Erreur lors de l'exécution de la requête Elasticsearch", ex);
    }
  }

  private void createAliasIfNotExist(List<EsIndexEnum> indexes, EsIndexEnum alias) throws EsRequestException {
    if (!adminService.indexExists(alias)) {
      for (EsIndexEnum index : indexes) {
        adminService.addIndexAlias(index, alias.getName());
      }
    }
  }

  protected void createIndexIfNotExist(EsIndexEnum index, EsIndexEnum alias, EsFilePath mapping, String settings)
      throws IOException, EsRequestException {
    if (!adminService.indexExists(alias)) {
      // On crée le nouvel index + l'alias staging
      adminService.createIndexWithSetting(index, settings);
      adminService.addIndexAlias(index, alias.getName());

      putMapping(index, mapping);
    }
  }

  protected void createIndexIfNotExist(EsIndexEnum index, String settings) throws EsRequestException {
    if (!adminService.indexExists(index)) {
      adminService.createIndexWithSetting(index, settings);
    }
  }

  protected void putMapping(EsIndexEnum index, EsFilePath filePath) throws EsRequestException, IOException {
    InputStream mappingInputStream = getClass().getResourceAsStream(filePath.getFileName());
    String mapping = IOUtils.toString(mappingInputStream, StandardCharsets.UTF_8.name());

    adminService.putMapping(index, mapping);
  }

}
