package fr.inha.elastic.model;

import java.util.HashSet;
import java.util.Set;

public class MediaInternal extends Auditable {

  private String uuid;
  private Integer uniqueKey;
  private Set<String> notice = new HashSet<>();
  private String linkedWithNotices;

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public Integer getUniqueKey() {
    return uniqueKey;
  }

  public void setUniqueKey(Integer uniqueKey) {
    this.uniqueKey = uniqueKey;
  }

  public Set<String> getNotice() {
    return notice;
  }

  public String getLinkedWithNotices() {
    return linkedWithNotices;
  }

  public void setLinkedWithNotices(String linkedWithNotices) {
    this.linkedWithNotices = linkedWithNotices;
  }

}
