package fr.inha.elastic.model;

public class InternationalLabel {

  public static final String DEFAULT_LANGUAGE = "fre";

  private String value;
  private String language;

  public InternationalLabel() {
    super();
  }

  public InternationalLabel(String value) {
    this.value = value;
    this.language = DEFAULT_LANGUAGE;
  }

  public InternationalLabel(String language, String value) {
    this.value = value;
    this.language = language;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

}
