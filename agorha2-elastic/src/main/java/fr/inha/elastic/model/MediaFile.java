package fr.inha.elastic.model;

import java.util.HashMap;
import java.util.Map;

public class MediaFile {

  private String uuid;
  private String alt;
  private MediaType mediaType;
  private String computedFacetMediaType;
  private String originFile;
  private String thumbnail;
  private String embeddedContent;
  private String mediumSizePicture;
  private String ocr;
  private String contentType;
  private Integer fileSize;
  private Map<String, String> internalMetadata = new HashMap<>();
  private Integer uniqueKey;

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getAlt() {
    return alt;
  }

  public void setAlt(String alt) {
    this.alt = alt;
  }

  public MediaType getMediaType() {
    return mediaType;
  }

  public void setMediaType(MediaType mediaType) {
    this.mediaType = mediaType;
  }

  public String getComputedFacetMediaType() {
    return computedFacetMediaType;
  }

  public void setComputedFacetMediaType(String computedFacetMediaType) {
    this.computedFacetMediaType = computedFacetMediaType;
  }

  public String getOriginFile() {
    return originFile;
  }

  public void setOriginFile(String originFile) {
    this.originFile = originFile;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  public String getEmbeddedContent() {
    return embeddedContent;
  }

  public void setEmbeddedContent(String embeddedContent) {
    this.embeddedContent = embeddedContent;
  }

  public String getMediumSizePicture() {
    return mediumSizePicture;
  }

  public void setMediumSizePicture(String mediumSizePicture) {
    this.mediumSizePicture = mediumSizePicture;
  }

  public String getOcr() {
    return ocr;
  }

  public void setOcr(String ocr) {
    this.ocr = ocr;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public Integer getFileSize() {
    return fileSize;
  }

  public void setFileSize(Integer fileSize) {
    this.fileSize = fileSize;
  }

  public Map<String, String> getInternalMetadata() {
    return internalMetadata;
  }

  public Integer getUniqueKey() {
    return uniqueKey;
  }

  public void setUniqueKey(Integer uniqueKey) {
    this.uniqueKey = uniqueKey;
  }

}
