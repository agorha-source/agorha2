package fr.inha.elastic.model;

public class User extends AbstractElasticData {

  private UserInternal internal;
  private Authorizations authorizations;

  public UserInternal getInternal() {
    return internal;
  }

  public void setInternal(UserInternal internal) {
    this.internal = internal;
  }

  public Authorizations getAuthorizations() {
    return authorizations;
  }

  public void setAuthorizations(Authorizations authorizations) {
    this.authorizations = authorizations;
  }

}
