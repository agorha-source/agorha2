package fr.inha.elastic.model;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class NoticeInternal extends Auditable {

  private String uuid;
  private String dcIdentifier;
  private String origin;
  private String originImportFile;
  private String originImportFileLine;
  private String sourceNoticeUuid;
  private int uniqueKey;
  private NoticeType noticeType;
  private String typeOai;
  private String typeDcmi;
  private String permalink;
  private String comment;
  private String verifiedBy;
  private String status;
  private LocalDateTime verificationDate;
  private LocalDateTime statusUpdatedDate;
  private Set<String> reference = new HashSet<>();
  private Set<String> referenceFromContent = new HashSet<>();
  private Map<String, Object> digest = new HashMap<>();

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getDcIdentifier() {
    return dcIdentifier;
  }

  public void setDcIdentifier(String dcIdentifier) {
    this.dcIdentifier = dcIdentifier;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public String getOriginImportFile() {
    return originImportFile;
  }

  public void setOriginImportFile(String originImportFile) {
    this.originImportFile = originImportFile;
  }

  public String getOriginImportFileLine() {
    return originImportFileLine;
  }

  public void setOriginImportFileLine(String originImportFileLine) {
    this.originImportFileLine = originImportFileLine;
  }

  public String getSourceNoticeUuid() {
    return sourceNoticeUuid;
  }

  public void setSourceNoticeUuid(String sourceNoticeUuid) {
    this.sourceNoticeUuid = sourceNoticeUuid;
  }

  public int getUniqueKey() {
    return uniqueKey;
  }

  public void setUniqueKey(int uniqueKey) {
    this.uniqueKey = uniqueKey;
  }

  public NoticeType getNoticeType() {
    return noticeType;
  }

  public void setNoticeType(NoticeType noticeType) {
    this.noticeType = noticeType;
  }

  public String getTypeOai() {
    return typeOai;
  }

  public void setTypeOai(String typeOai) {
    this.typeOai = typeOai;
  }

  public String getTypeDcmi() {
    return typeDcmi;
  }

  public void setTypeDcmi(String typeDcmi) {
    this.typeDcmi = typeDcmi;
  }

  public String getPermalink() {
    return permalink;
  }

  public void setPermalink(String permalink) {
    this.permalink = permalink;
  }

  public Map<String, Object> getDigest() {
    return digest;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public String getVerifiedBy() {
    return verifiedBy;
  }

  public void setVerifiedBy(String verifiedBy) {
    this.verifiedBy = verifiedBy;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public LocalDateTime getVerificationDate() {
    return verificationDate;
  }

  public void setVerificationDate(LocalDateTime verificationDate) {
    this.verificationDate = verificationDate;
  }

  public LocalDateTime getStatusUpdatedDate() {
    return statusUpdatedDate;
  }

  public void setStatusUpdatedDate(LocalDateTime statusUpdatedDate) {
    this.statusUpdatedDate = statusUpdatedDate;
  }

  public Set<String> getReference() {
    return reference;
  }

  public Set<String> getReferenceFromContent() {
    return referenceFromContent;
  }

}
