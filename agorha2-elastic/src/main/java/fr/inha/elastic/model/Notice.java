package fr.inha.elastic.model;

import java.util.HashMap;
import java.util.Map;

public class Notice extends AbstractElasticData {

  private NoticeInternal internal;

  private Map<String, Object> content = new HashMap<>();

  public NoticeInternal getInternal() {
    return internal;
  }

  public void setInternal(NoticeInternal internal) {
    this.internal = internal;
  }

  public Map<String, Object> getContent() {
    return content;
  }

}
