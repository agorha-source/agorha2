package fr.inha.elastic.model;

public enum NoticeType {
  EVENT("evenements", "Evénements"),
  REFERENCE("references", "Références"),
  ARTWORK("oeuvres", "Œuvres"),
  COLLECTION("collections", "Collections"),
  PERSON("personnes", "Personnes"),
  NEWS("", "");

  private String exportFileName;
  private String label;

  private NoticeType(String exportFileName, String label) {
    this.exportFileName = exportFileName;
    this.label = label;
  }

  public String getExportFileName() {
    return exportFileName;
  }

  public String getLabel() {
    return label;
  }

  public static NoticeType fromFileName(String fileName) {
    for (NoticeType noticeType : NoticeType.values()) {
      if (noticeType.getExportFileName().equals(fileName)) {
        return noticeType;
      }
    }
    return null;
  }

}
