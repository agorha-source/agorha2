package fr.inha.elastic.model;

import java.util.ArrayList;
import java.util.List;

public class Authorizations {

  private String role;
  private List<DatabaseAuthorization> database = new ArrayList<>();

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public List<DatabaseAuthorization> getDatabase() {
    return database;
  }

}
