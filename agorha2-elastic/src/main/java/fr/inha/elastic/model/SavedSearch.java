package fr.inha.elastic.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class SavedSearch extends AbstractElasticData {

  private LocalDateTime createdDate;
  private String name;
  private List<String> resume = new ArrayList<>();
  private String userId;
  private String url;

  public LocalDateTime getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(LocalDateTime createdDate) {
    this.createdDate = createdDate;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public List<String> getResume() {
    return resume;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

}
