package fr.inha.elastic.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sword.utils.elasticsearch.intf.IVersionable;
import org.elasticsearch.index.seqno.SequenceNumbers;

/**
 * Classe à étendre pour les classes de modèles destinées à ES.
 * 
 * @author mhusser
 *
 */
public abstract class AbstractElasticData implements IVersionable {

  protected String id;
  @JsonIgnore
  protected long version;
  @JsonIgnore
  protected long seqNo = SequenceNumbers.UNASSIGNED_SEQ_NO;
  @JsonIgnore
  protected long primaryTerm = SequenceNumbers.UNASSIGNED_PRIMARY_TERM;

  @Override
  public String getId() {
    return id;
  }

  @Override
  public void setId(String id) {
    this.id = id;
  }

  @Override
  public long getVersion() {
    return version;
  }

  @Override
  public void setVersion(long version) {
    this.version = version;
  }

  @Override
  public long getSeqNo() {
    return seqNo;
  }

  @Override
  public void setSeqNo(long seqNo) {
    this.seqNo = seqNo;
  }

  @Override
  public long getPrimaryTerm() {
    return primaryTerm;
  }

  @Override
  public void setPrimaryTerm(long primaryTerm) {
    this.primaryTerm = primaryTerm;
  }

}