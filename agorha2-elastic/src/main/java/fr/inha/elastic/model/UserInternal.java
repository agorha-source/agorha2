package fr.inha.elastic.model;

import java.time.LocalDateTime;

public class UserInternal extends Auditable {

  private String uuid;
  private String login;
  private String lastname;
  private String firstname;
  private UserStatus status;
  private String resetPasswordToken;
  private LocalDateTime resetPasswordExpiryDate;
  private String validateAccountToken;

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public UserStatus getStatus() {
    return status;
  }

  public void setStatus(UserStatus status) {
    this.status = status;
  }

  public String getResetPasswordToken() {
    return resetPasswordToken;
  }

  public void setResetPasswordToken(String resetPasswordToken) {
    this.resetPasswordToken = resetPasswordToken;
  }

  public LocalDateTime getResetPasswordExpiryDate() {
    return resetPasswordExpiryDate;
  }

  public void setResetPasswordExpiryDate(LocalDateTime resetPasswordExpiryDate) {
    this.resetPasswordExpiryDate = resetPasswordExpiryDate;
  }

  public String getValidateAccountToken() {
    return validateAccountToken;
  }

  public void setValidateAccountToken(String validateAccountToken) {
    this.validateAccountToken = validateAccountToken;
  }

}
