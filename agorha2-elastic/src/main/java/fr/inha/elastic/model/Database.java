package fr.inha.elastic.model;

public class Database extends AbstractElasticData {

  private DatabaseInternal internal;
  private DatabaseData content;

  public DatabaseInternal getInternal() {
    return internal;
  }

  public void setInternal(DatabaseInternal internal) {
    this.internal = internal;
  }

  public DatabaseData getContent() {
    return content;
  }

  public void setContent(DatabaseData content) {
    this.content = content;
  }

}
