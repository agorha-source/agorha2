package fr.inha.elastic.model;

public class Concept {

  private Thesaurus thesaurus;

  public Thesaurus getThesaurus() {
    return thesaurus;
  }

  public void setThesaurus(Thesaurus thesaurus) {
    this.thesaurus = thesaurus;
  }

}
