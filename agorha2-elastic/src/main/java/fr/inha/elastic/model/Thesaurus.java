package fr.inha.elastic.model;

import java.util.ArrayList;
import java.util.List;

public class Thesaurus {

  private String ref;
  private String conceptPath;
  private List<InternationalLabel> preflabels = new ArrayList<>();
  private List<InternationalLabel> altLabels = new ArrayList<>();
  private List<InternationalLabel> hiddenLabels = new ArrayList<>();

  public Thesaurus() {

  }

  public Thesaurus(String ref) {
    this.ref = ref;
  }

  public String getRef() {
    return ref;
  }

  public void setRef(String ref) {
    this.ref = ref;
  }

  public String getConceptPath() {
    return conceptPath;
  }

  public void setConceptPath(String conceptPath) {
    this.conceptPath = conceptPath;
  }

  public List<InternationalLabel> getPreflabels() {
    return preflabels;
  }

  public List<InternationalLabel> getAltLabels() {
    return altLabels;
  }

  public List<InternationalLabel> getHiddenLabels() {
    return hiddenLabels;
  }

}
