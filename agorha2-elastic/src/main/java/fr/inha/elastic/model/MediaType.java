package fr.inha.elastic.model;

public enum MediaType {

  LOCAL,
  LOCAL_IMAGE,
  LOCAL_VIDEO,
  YOUTUBE,
  FLICKR,
  SKETCHFAB,
  RMN,
  IIIF

}
