package fr.inha.elastic.model;

public enum NoticeStatus {
  CREATION_IN_PROGRESS(1, "En cours"),
  TO_PUBLISH(3, "A publier"),
  ARCHIVED(6, "Archivée"),
  PUBLISHED(7, "Publiée");

  private String value;
  private int id;

  NoticeStatus(int id, String value) {
    this.id = id;
    this.value = value;
  }

  public static NoticeStatus fromValue(String value) {
    for (NoticeStatus status : NoticeStatus.values()) {
      if (status.getValue().equals(value)) {
        return status;
      }
    }
    return null;
  }

  public static NoticeStatus fromId(int id) {
    for (NoticeStatus status : NoticeStatus.values()) {
      if (status.getId() == id) {
        return status;
      }
    }
    return null;
  }

  public int getId() {
    return id;
  }

  public String getValue() {
    return value;
  }

}
