package fr.inha.elastic.model;

public enum UserStatus {

  ACTIVE("Actif"),
  INACTIVE("Inactif");

  private String value;

  private UserStatus(String value) {
    this.value = value;

  }

  public String getValue() {
    return value;
  }

}
