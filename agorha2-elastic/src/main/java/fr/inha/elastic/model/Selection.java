package fr.inha.elastic.model;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.TreeSet;

public class Selection extends AbstractElasticData {

  private LocalDateTime createdDate;
  private LocalDateTime updatedDate;
  private String title;
  private Set<String> notices = new TreeSet<>();
  private String userId;

  public LocalDateTime getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(LocalDateTime createdDate) {
    this.createdDate = createdDate;
  }

  public LocalDateTime getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(LocalDateTime updatedDate) {
    this.updatedDate = updatedDate;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public Set<String> getNotices() {
    return notices;
  }

}
