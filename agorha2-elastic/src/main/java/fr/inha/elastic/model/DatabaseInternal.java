package fr.inha.elastic.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import java.util.ArrayList;
import java.util.List;

public class DatabaseInternal extends Auditable {

  private String uuid;
  private Integer uniqueKey;
  // Alias pour compatibilité avec les jobs Talend
  @JsonAlias(value = "status")
  private Integer agorhaStatus;
  private Integer cmsStatus;
  private String permalink;
  private List<NoticeType> relatedNoticeType = new ArrayList<>();

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public Integer getUniqueKey() {
    return uniqueKey;
  }

  public void setUniqueKey(Integer uniqueKey) {
    this.uniqueKey = uniqueKey;
  }

  public Integer getAgorhaStatus() {
    return agorhaStatus;
  }

  public void setAgorhaStatus(Integer agorhaStatus) {
    this.agorhaStatus = agorhaStatus;
  }

  public Integer getCmsStatus() {
    return cmsStatus;
  }

  public void setCmsStatus(Integer cmsStatus) {
    this.cmsStatus = cmsStatus;
  }

  public String getPermalink() {
    return permalink;
  }

  public void setPermalink(String permalink) {
    this.permalink = permalink;
  }

  public List<NoticeType> getRelatedNoticeType() {
    return relatedNoticeType;
  }

}
