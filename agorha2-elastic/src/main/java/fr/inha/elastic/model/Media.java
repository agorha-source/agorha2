package fr.inha.elastic.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Media extends AbstractElasticData {

  private MediaInternal internal = new MediaInternal();
  private String caption;
  private List<MediaFile> files = new ArrayList<>();
  private String fileProvenance;
  private Concept communicationRight;
  private Concept reproductionRight;
  private String credit;
  private String licence;
  private String comment;
  private List<Map<String, String>> database = new ArrayList<>();

  public MediaInternal getInternal() {
    return internal;
  }

  public void setInternal(MediaInternal internal) {
    this.internal = internal;
  }

  public String getCaption() {
    return caption;
  }

  public void setCaption(String caption) {
    this.caption = caption;
  }

  public String getFileProvenance() {
    return fileProvenance;
  }

  public void setFileProvenance(String fileProvenance) {
    this.fileProvenance = fileProvenance;
  }

  public Concept getCommunicationRight() {
    return communicationRight;
  }

  public void setCommunicationRight(Concept communicationRight) {
    this.communicationRight = communicationRight;
  }

  public Concept getReproductionRight() {
    return reproductionRight;
  }

  public void setReproductionRight(Concept reproductionRight) {
    this.reproductionRight = reproductionRight;
  }

  public String getCredit() {
    return credit;
  }

  public void setCredit(String credit) {
    this.credit = credit;
  }

  public String getLicence() {
    return licence;
  }

  public void setLicence(String licence) {
    this.licence = licence;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public List<MediaFile> getFiles() {
    return files;
  }

  public List<Map<String, String>> getDatabase() {
    return database;
  }

}
