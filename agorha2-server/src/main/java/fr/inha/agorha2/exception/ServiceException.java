package fr.inha.agorha2.exception;

public class ServiceException extends Exception {

  private static final long serialVersionUID = 1667238014912007813L;

  public ServiceException() {
    super();
  }

  public ServiceException(String s) {
    super(s);
  }

  public ServiceException(String s, Throwable throwable) {
    super(s, throwable);
  }

  public ServiceException(Throwable throwable) {
    super(throwable);
  }
}
