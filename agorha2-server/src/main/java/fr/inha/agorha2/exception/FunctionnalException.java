package fr.inha.agorha2.exception;

public class FunctionnalException extends Exception {

  private static final long serialVersionUID = -3392001900588482320L;

  public FunctionnalException() {
    super();
  }

  public FunctionnalException(String s) {
    super(s);
  }

  public FunctionnalException(String s, Throwable throwable) {
    super(s, throwable);
  }

  public FunctionnalException(Throwable throwable) {
    super(throwable);
  }
}
