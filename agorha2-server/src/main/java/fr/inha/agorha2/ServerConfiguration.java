package fr.inha.agorha2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import fr.inha.agorha2.aop.AopConfiguration;
import fr.inha.agorha2.api.Agorha2Api;
import fr.inha.agorha2.api.dto.QueryParamConverterProvider;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.RequestExceptionHandler;
import fr.inha.agorha2.cxf.RequestWebApplicationExceptionHandler;
import fr.inha.agorha2.security.SecurityConfiguration;
import fr.inha.elastic.config.ElasticSearchConfiguration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.Executor;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.servlet.Filter;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.ext.logging.LoggingFeature;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.openapi.OpenApiCustomizer;
import org.apache.cxf.jaxrs.openapi.OpenApiFeature;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.data.ldap.repository.config.EnableLdapRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Placed in package ABOVE others else (ex. in .config) doesn't scan them.
 * 
 * @author broussot
 */
@Configuration
@EnableCaching
@EnableAsync
@EnableLdapRepositories(basePackages = "fr.inha.agorha2.ldap.*")
@ComponentScan(basePackageClasses = { ServerConfiguration.class, ElasticSearchConfiguration.class,
    SecurityConfiguration.class, AopConfiguration.class })
public class ServerConfiguration {

  @Value("${cors.allowed.origins}")
  private String[] allowedOrigins;

  @Value("${agorha2.api.version}")
  private String apiVersion;

  @Value("${agorha2.rest.enableIndenting:false}")
  private boolean enableIndenting;

  @Value("${endpoint.adress}")
  private String endpointAdress;

  @Value(value = "${mail.server.host}")
  private String mailServerHost;

  @Value(value = "${mail.server.port}")
  private String mailServerPort;

  @Value(value = "${mail.server.username}")
  private String mailServerUsername;

  @Value(value = "${mail.server.password}")
  private String mailServerPassword;

  @Value(value = "${agorha2.messages.path}")
  private String messagesPath;

  @Bean
  public Server rsServer(List<CxfJaxrsDeployable> cxfJjaxrsServers,
      @Qualifier("frontJsonProvider") JacksonJsonProvider jsonProvider, Bus bus) {
    JAXRSServerFactoryBean endpoint = new JAXRSServerFactoryBean();
    endpoint.setBus(bus);
    endpoint.setServiceBeanObjects(cxfJjaxrsServers);
    ArrayList<Object> providers = new ArrayList<>();
    providers.add(new RequestWebApplicationExceptionHandler());
    providers.add(new RequestExceptionHandler());
    providers.add(new QueryParamConverterProvider());
    providers.add(jsonProvider);

    endpoint.setProviders(providers);
    endpoint.setAddress(endpointAdress);
    endpoint.getFeatures().add(createOpenApiFeature());
    endpoint.getFeatures().add(new LoggingFeature());
    return endpoint.create();
  }

  public OpenApiFeature createOpenApiFeature() {
    OpenApiFeature openApiFeature = new OpenApiFeature();
    openApiFeature.setPrettyPrint(true);
    openApiFeature.setUseContextBasedConfig(true);
    openApiFeature.setScan(false);
    OpenApiCustomizer customizer = new OpenApiCustomizer();
    customizer.setDynamicBasePath(true);
    openApiFeature.setCustomizer(customizer);
    openApiFeature.setTitle("Agorha2 API");
    openApiFeature.setContactName("Agorha2");
    openApiFeature.setVersion(apiVersion);
    Set<String> resourcePackages = new HashSet<>();
    resourcePackages.add(Agorha2Api.class.getPackageName());
    openApiFeature.setResourcePackages(resourcePackages);
    return openApiFeature;
  }

  @Bean
  public JacksonJsonProvider frontJsonProvider() {
    ObjectMapper frontApiMapper = new ObjectMapper();

    // more lenient parsing that accepts single element as array :
    frontApiMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    frontApiMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);

    // Ne pas inclure les valeurs vides dans les JSON
    frontApiMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

    frontApiMapper.configure(SerializationFeature.INDENT_OUTPUT, enableIndenting);
    frontApiMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true); // to load from default bootstrap file conf
    frontApiMapper.registerModule(new Jdk8Module()).registerModule(new JavaTimeModule());
    return new JacksonJsonProvider(frontApiMapper);
  }

  // Fix the CORS errors
  @Bean
  public FilterRegistrationBean<Filter> simpleCorsFilter() {
    CorsConfiguration config = new CorsConfiguration();
    config.setAllowCredentials(true);
    // *** URL below needs to match the Vue client URL and port ***
    config.setAllowedOrigins(Arrays.asList(allowedOrigins));
    config.setAllowedMethods(Collections.singletonList("*"));
    config.setAllowedHeaders(Collections.singletonList("*"));
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration("/**", config);
    FilterRegistrationBean<Filter> bean = new FilterRegistrationBean<>(new CorsFilter(source));
    bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
    return bean;
  }

  // To allow injection of properties as list
  @Bean
  public ConversionService conversionService() {
    return new DefaultConversionService();
  }

  @Bean
  public Session sessionMail() {
    Properties properties = new Properties();
    properties.put("mail.smtp.host", mailServerHost);
    properties.put("mail.smtp.port", mailServerPort);
    properties.put("mail.smtp.starttls.enable", "true");

    if (StringUtils.isEmpty(mailServerUsername) && StringUtils.isEmpty(mailServerPassword)) {
      properties.put("mail.smtp.auth", "false");
      return Session.getDefaultInstance(properties);
    } else {
      properties.put("mail.smtp.auth", "true");
      return Session.getDefaultInstance(properties, new javax.mail.Authenticator() {
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
          return new PasswordAuthentication(mailServerUsername, mailServerPassword);
        }
      });
    }
  }

  @Bean
  public MessageSource messageSource() {
    ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
    messageSource.setBasename(messagesPath);
    messageSource.setCacheSeconds(1800);
    return messageSource;
  }

  @Bean(name = "taskExecutor")
  public Executor taskExecutor() {
    final ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(5);
    executor.setMaxPoolSize(5);
    executor.setQueueCapacity(100);
    executor.setThreadNamePrefix("NoticeBatchUpdate-");
    executor.initialize();
    return executor;
  }

}