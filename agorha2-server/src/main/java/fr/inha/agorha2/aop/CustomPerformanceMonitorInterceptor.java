package fr.inha.agorha2.aop;

import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.springframework.aop.interceptor.AbstractMonitoringInterceptor;
import org.springframework.util.StopWatch;

/**
 * Permet de logger le temps d'execution d'une méthode.
 */
public class CustomPerformanceMonitorInterceptor extends AbstractMonitoringInterceptor {

  private static final long serialVersionUID = -6852369174767812530L;

  public CustomPerformanceMonitorInterceptor() {
  }

  public CustomPerformanceMonitorInterceptor(boolean useDynamicLogger) {
    setUseDynamicLogger(useDynamicLogger);
  }

  @Override
  protected Object invokeUnderTrace(MethodInvocation invocation, Log log) throws Throwable {

    String name = createInvocationTraceName(invocation);
    StopWatch stopWatch = new StopWatch(name);
    stopWatch.start(name);
    try {
      return invocation.proceed();
    } finally {
      stopWatch.stop();
      log.info("Performance Tracking -  Method " + name + " running time : " + stopWatch.getTotalTimeMillis() + " ms");

      if (stopWatch.getTotalTimeSeconds() > 10000) {
        log.warn("Performance Tracking - Method execution longer than 10 seconds !");
      }

    }
  }
}
