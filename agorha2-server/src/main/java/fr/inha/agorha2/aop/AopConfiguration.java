package fr.inha.agorha2.aop;

import org.aspectj.lang.annotation.Pointcut;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.interceptor.CustomizableTraceInterceptor;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy
public class AopConfiguration {

  @Pointcut("execution(public * (@org.springframework.stereotype.Service *).*(..)) "
      + "|| execution(public * (@org.springframework.stereotype.Component *).*(..))")
  public void serviceAnnotation() {
  }

  @Pointcut("execution(public * (@org.springframework.stereotype.Service *).*(..)) "
      + "|| execution(public * (@org.springframework.stereotype.Component *).*(..)) "
      + "|| execution(public * (com.sword.utils.elasticsearch.services..*).*(..))")
  public void performanceScope() {
  }

  /***
   * Ecrit un message dans les logs à chaque fois qu'on entre et sort d'une
   * méthode.
   * 
   * @return
   */
  @Bean
  public CustomizableTraceInterceptor customizableTraceInterceptor() {
    CustomizableTraceInterceptor customizableTraceInterceptor = new CustomizableTraceInterceptor();
    customizableTraceInterceptor.setUseDynamicLogger(true);
    // TODO ne pas garder les arguments, il ne faut pas que les identifiants de
    // connexion apparaissent dans les logs.
    customizableTraceInterceptor.setEnterMessage("Calling service $[methodName]() of class [$[targetClassShortName]]");
    customizableTraceInterceptor.setExitMessage("Leaving  $[methodName]() of class [$[targetClassShortName]] ");
    customizableTraceInterceptor.setUseDynamicLogger(false);
    return customizableTraceInterceptor;
  }

  /**
   * Ecrit dans le log de l'application le temps d'execution des méthodes.
   * 
   * @return
   */
  @Bean
  public CustomPerformanceMonitorInterceptor customPerformanceMonitorInterceptor() {
    return new CustomPerformanceMonitorInterceptor(false);
  }

  @Bean
  public Advisor performanceMonitorAdvisor() {
    AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
    pointcut.setExpression("fr.inha.agorha2.aop.AopConfiguration.performanceScope()");
    return new DefaultPointcutAdvisor(pointcut, customPerformanceMonitorInterceptor());
  }

  @Bean
  public Advisor customizableTraceAdvisor() {
    AspectJExpressionPointcut pointcut = new AspectJExpressionPointcut();
    pointcut.setExpression("fr.inha.agorha2.aop.AopConfiguration.serviceAnnotation()");
    return new DefaultPointcutAdvisor(pointcut, customizableTraceInterceptor());
  }

}
