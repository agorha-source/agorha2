package fr.inha.agorha2.cxf;

/**
 * Marks JAXRS impls to be deployed on CXF. NB. could also be done by
 * custom @Qualifier annotation but interface has better tooling support see
 * 
 * @author gsimonet
 *
 */
public interface CxfJaxrsDeployable {

}
