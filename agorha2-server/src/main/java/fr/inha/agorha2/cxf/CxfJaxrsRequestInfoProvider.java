package fr.inha.agorha2.cxf;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;
import org.apache.cxf.jaxrs.impl.HttpHeadersImpl;
import org.apache.cxf.jaxrs.impl.RequestImpl;
import org.apache.cxf.jaxrs.impl.UriInfoImpl;
import org.apache.cxf.message.Exchange;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.PhaseInterceptorChain;

public class CxfJaxrsRequestInfoProvider {

  // CXF-specific

  public Message getInMessage() {
    return (PhaseInterceptorChain.getCurrentMessage() == null) ? null
        : PhaseInterceptorChain.getCurrentMessage().getExchange().getInMessage();
  }

  public Exchange getExchange() {
    return (PhaseInterceptorChain.getCurrentMessage() == null) ? null
        : PhaseInterceptorChain.getCurrentMessage().getExchange();
  }

  public MediaType getNegotiatedResponseMediaType() {
    String responseContentType = (this.getExchange() != null)
        ? (String) this.getExchange().get(HttpHeaders.CONTENT_TYPE)
        : MediaType.APPLICATION_JSON; // default if called directly in tests
    // NB. exchange Content-Type has been negotiated according to Accept and set
    return MediaType.valueOf(responseContentType);
  }

  // JAXRS-generic

  public HttpHeaders getHttpHeaders() {
    return new HttpHeadersImpl(getInMessage());
  }

  public UriInfo getUriInfo() {
    return new UriInfoImpl(getInMessage());
  }

  public Request getJaxrsRequest() {
    return new RequestImpl(getInMessage());
  }

}
