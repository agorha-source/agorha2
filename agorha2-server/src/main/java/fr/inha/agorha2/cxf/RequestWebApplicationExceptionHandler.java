package fr.inha.agorha2.cxf;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestWebApplicationExceptionHandler implements ExceptionMapper<WebApplicationException> {
  private static final Logger log = LoggerFactory.getLogger(RequestWebApplicationExceptionHandler.class);

  @Override
  public Response toResponse(WebApplicationException e) {
    if (e.getResponse().getStatus() >= 400) {
      log.error("Erreur interne", e);
    }
    return Response.status(e.getResponse().getStatus()).header("Exception", e.getMessage())
        .entity(e.getCause() == null ? e.getMessage() : e.getCause().getMessage()).build();
  }
}
