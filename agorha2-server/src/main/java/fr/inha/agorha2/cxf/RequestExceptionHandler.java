package fr.inha.agorha2.cxf;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequestExceptionHandler implements ExceptionMapper<Exception> {
  private static final Logger log = LoggerFactory.getLogger(RequestExceptionHandler.class);

  @Override
  public Response toResponse(Exception e) {
    log.error("Erreur interne", e);
    return Response.status(Status.INTERNAL_SERVER_ERROR).header("Exception", e.getMessage())
        .entity(e.getCause() == null ? e.getMessage() : e.getCause().getMessage()).build();
  }
}
