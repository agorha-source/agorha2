package fr.inha.agorha2.security;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.ldap.odm.core.ObjectDirectoryMapper;
import org.springframework.ldap.odm.core.impl.DefaultObjectDirectoryMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired
  private List<LdapAuthenticationProvider> ldapAuthenticationProviders;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
    ldapAuthenticationProviders.forEach(auth::authenticationProvider);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.logout().logoutUrl("/api/users/logout").and()
        // HTTP Basic authentication
        .httpBasic().and().authorizeRequests()
        // Administrateurs ou responsables
        .antMatchers("/api/contribution/notices/status")
        .hasAnyAuthority(Authority.ADMIN.name(), Authority.DATABASE_MANAGER.name())
        // Adminisrateurs seulement
        .antMatchers("/api/administration/**").hasAnyAuthority(Authority.ADMIN.name())
        .antMatchers(HttpMethod.DELETE, "/api/contribution/notices/**").hasAnyAuthority(Authority.ADMIN.name())
        // Batch seulement
        .antMatchers("/api/batch/**").hasAnyAuthority(Authority.BATCH.name())
        // Espace de contribution
        .antMatchers("/api/contribution/**")
        .hasAnyAuthority(Authority.ADMIN.name(), Authority.CONTRIB.name(), Authority.DATABASE_MANAGER.name(),
            Authority.BATCH.name())
        // Utilisateur authentifié seulement
        .antMatchers("/api/notice/correctionProposal", "/api/users/current", "/api/users/selections/**",
            "/api/users/savedSearch/**")
        .authenticated().antMatchers(HttpMethod.DELETE, "/api/users").authenticated()
        .antMatchers(HttpMethod.PUT, "/api/users").authenticated()
        // Configuration générale
        .and().csrf().disable().exceptionHandling()
        .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
  }

  @Override
  @Bean
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public ObjectDirectoryMapper odm() {
    return new DefaultObjectDirectoryMapper();
  }

}