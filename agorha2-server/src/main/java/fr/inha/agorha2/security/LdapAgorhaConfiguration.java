package fr.inha.agorha2.security;

import fr.inha.agorha2.ldap.UserAgorhaRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.odm.core.ObjectDirectoryMapper;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.authentication.LdapAuthenticator;
import org.springframework.security.ldap.ppolicy.PasswordPolicyAwareContextSource;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;

@Configuration
public class LdapAgorhaConfiguration {

  // conf ldap agorha
  @Value("${ldap.server.url}")
  private String ldapUrl;
  @Value("${ldap.server.dnSearchBase}")
  private String dnSearchBase;
  @Value("${ldap.server.username}")
  private String ldapUsername;
  @Value("${ldap.server.password}")
  private String ldapPassword;

  @Bean
  public LdapAuthenticationProvider ldapAuthenticationProvider(Agorha2UserDetailsContextMapper userDetailsContextMapper,
      LdapContextSource ldapContextSource) {
    LdapAuthenticationProvider authenticationProvider = new LdapAuthenticationProvider(
        ldapAuthenticator(ldapContextSource), new DefaultLdapAuthoritiesPopulator(ldapContextSource, dnSearchBase));
    authenticationProvider.setUserDetailsContextMapper(userDetailsContextMapper);
    return authenticationProvider;
  }

  @Bean
  public LdapContextSource ldapContextSource() {
    PasswordPolicyAwareContextSource contextSource = new PasswordPolicyAwareContextSource(ldapUrl);
    contextSource.setUserDn(ldapUsername);
    contextSource.setPassword(ldapPassword);
    return contextSource;
  }

  @Bean
  public LdapAuthenticator ldapAuthenticator(LdapContextSource ldapContextSource) {
    BindAuthenticator authenticator = new BindAuthenticator(ldapContextSource);
    authenticator.setUserDnPatterns(new String[] { "CN={0}," + dnSearchBase });
    return authenticator;
  }

  @Bean
  public LdapTemplate ldapTemplate(LdapContextSource ldapContextSource) {
    return new LdapTemplate(ldapContextSource);
  }

  @Bean
  public UserAgorhaRepository userRepository(LdapTemplate ldapTemplate, ObjectDirectoryMapper odm) {
    return new UserAgorhaRepository(ldapTemplate, odm);
  }
}