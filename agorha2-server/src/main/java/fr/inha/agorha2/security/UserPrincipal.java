package fr.inha.agorha2.security;

import fr.inha.agorha2.ldap.LdapUser;
import fr.inha.elastic.model.User;
import java.util.Arrays;
import java.util.Collection;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserPrincipal implements UserDetails {

  private static final long serialVersionUID = -3590681150819131131L;

  private transient User user;
  private UserDetails ldapUserDetails;
  private transient LdapUser ldapUser;

  public UserPrincipal(User user, UserDetails ldapUserDetails) {
    this.user = user;
    this.ldapUserDetails = ldapUserDetails;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return Arrays.asList(new SimpleGrantedAuthority(user.getAuthorizations().getRole()));
  }

  @Override
  public String getPassword() {
    return ldapUserDetails.getPassword();
  }

  @Override
  public String getUsername() {
    return ldapUserDetails.getUsername();
  }

  @Override
  public boolean isAccountNonExpired() {
    return ldapUserDetails.isAccountNonExpired();
  }

  @Override
  public boolean isAccountNonLocked() {
    return ldapUserDetails.isAccountNonLocked();
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return ldapUserDetails.isCredentialsNonExpired();
  }

  @Override
  public boolean isEnabled() {
    return ldapUserDetails.isEnabled();
  }

  public User getUser() {
    return user;
  }

  public UserDetails getLdapUserDetails() {
    return ldapUserDetails;
  }

  public LdapUser getLdapUser() {
    return ldapUser;
  }

  public void setLdapUser(LdapUser ldapUser) {
    this.ldapUser = ldapUser;
  }

}
