package fr.inha.agorha2.security;

public enum Authority {
  ADMIN("Administrateur"),
  CONTRIB("Contributeur"),
  DATABASE_MANAGER("Responsable BDD"),
  BATCH("Utilisateur reprise"),
  DEFAULT("Visiteur");

  private String value;

  private Authority(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

}
