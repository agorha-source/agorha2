package fr.inha.agorha2.security;

import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.intf.IServiceDedicated;
import fr.inha.agorha2.ldap.LdapService;
import fr.inha.agorha2.ldap.LdapUser;
import fr.inha.agorha2.utils.UserUtils;
import fr.inha.elastic.model.User;
import java.util.Collection;
import java.util.List;
import javax.ws.rs.InternalServerErrorException;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.ldap.userdetails.LdapUserDetailsMapper;
import org.springframework.stereotype.Component;

@Component
public class Agorha2UserDetailsContextMapper extends LdapUserDetailsMapper {

  @Autowired
  private IServiceDedicated<User> userService;

  @Autowired
  private LdapService ldapService;

  @Override
  public UserDetails mapUserFromContext(DirContextOperations ctx, String username,
      Collection<? extends GrantedAuthority> authorities) {
    UserDetails ldapUserDetails = super.mapUserFromContext(ctx, username, authorities);
    LdapUser ldapUser = ldapService.get(username);
    User user = createEsUserIfAbsentOrGet(username, ldapUser);
    UserPrincipal userPrincipal = new UserPrincipal(user, ldapUserDetails);
    userPrincipal.setLdapUser(ldapUser);
    return userPrincipal;
  }

  private User createEsUserIfAbsentOrGet(String login, LdapUser ldapUser) {
    try {
      List<User> users = userService.list(QueryBuilders.matchQuery("internal.login", login), 1);
      if (users.isEmpty()) {
        User esUser = new User();
        esUser.setInternal(UserUtils.createInternal(ldapUser, login));
        esUser.setAuthorizations(UserUtils.createAuthorizations());
        return userService.createOrUpdateAndGet(esUser, true);
      } else {
        return users.get(0);
      }
    } catch (EsRequestException e) {
      throw new InternalServerErrorException(
          "Erreur lors de la création ou récupération de l'utilisateur " + login + " dans ES", e);
    }
  }
}
