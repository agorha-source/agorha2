package fr.inha.agorha2.security;

import fr.inha.agorha2.ldap.UserInhaRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.ldap.odm.core.ObjectDirectoryMapper;
import org.springframework.security.ldap.authentication.BindAuthenticator;
import org.springframework.security.ldap.authentication.LdapAuthenticationProvider;
import org.springframework.security.ldap.authentication.LdapAuthenticator;
import org.springframework.security.ldap.ppolicy.PasswordPolicyAwareContextSource;
import org.springframework.security.ldap.userdetails.DefaultLdapAuthoritiesPopulator;

@Configuration
@ConditionalOnProperty(value = "ldap.inha.enabled", matchIfMissing = true)
public class LdapInhaConfiguration {

  // conf ldap inha
  @Value("${ldap.inha.url}")
  private String ldapInhaUrl;
  @Value("${ldap.inha.dnSearchBase}")
  private String inhaDnSearchBase;
  @Value("${ldap.inha.username}")
  private String ldapInhaUsername;
  @Value("${ldap.inha.password}")
  private String ldapInhaPassword;

  @Bean
  public LdapAuthenticationProvider ldapInhaAuthenticationProvider(
      Agorha2UserDetailsContextMapper userDetailsContextMapper, LdapContextSource ldapInhaContextSource) {
    LdapAuthenticationProvider authenticationProvider = new LdapAuthenticationProvider(
        ldapInhaAuthenticator(ldapInhaContextSource),
        new DefaultLdapAuthoritiesPopulator(ldapInhaContextSource, inhaDnSearchBase));
    authenticationProvider.setUserDetailsContextMapper(userDetailsContextMapper);
    return authenticationProvider;
  }

  @Bean
  public LdapContextSource ldapInhaContextSource() {
    PasswordPolicyAwareContextSource contextSource = new PasswordPolicyAwareContextSource(ldapInhaUrl);
    contextSource.setUserDn(ldapInhaUsername);
    contextSource.setPassword(ldapInhaPassword);
    return contextSource;
  }

  @Bean
  public LdapAuthenticator ldapInhaAuthenticator(LdapContextSource ldapInhaContextSource) {
    BindAuthenticator authenticator = new BindAuthenticator(ldapInhaContextSource);
    authenticator.setUserDnPatterns(new String[] { "UID={0}," + inhaDnSearchBase });
    return authenticator;
  }

  @Bean
  public LdapTemplate ldapInhaTemplate(LdapContextSource ldapInhaContextSource) {
    return new LdapTemplate(ldapInhaContextSource);
  }

  @Bean
  public UserInhaRepository inhaUserRepository(LdapTemplate ldapInhaTemplate, ObjectDirectoryMapper odm) {
    return new UserInhaRepository(ldapInhaTemplate, odm);
  }
}