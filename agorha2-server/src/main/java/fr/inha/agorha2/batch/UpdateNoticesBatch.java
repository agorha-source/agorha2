package fr.inha.agorha2.batch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.NoticeService;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PostConstruct;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class UpdateNoticesBatch {
  private static final Logger log = LoggerFactory.getLogger(UpdateNoticesBatch.class);

  private static final String INPUT_DIR = "input";
  private static final String SUCCESS_DIR = "success";
  private static final String ERROR_DIR = "error";

  private ConcurrentHashMap<String, Boolean> prefixInProgress = new ConcurrentHashMap<>();

  @Value(value = "${agorha2.batch.workDir}")
  private String batchWorkDir;

  @Value(value = "${agorha2.batch.workFile}")
  private String batchWorkFile;

  @Value(value = "${agorha2.batch.maxFileLine:1000}")
  private int maxFileLine;

  private ObjectMapper mapper = DefaultEsContext.instance().getObjectMapper();

  @Autowired
  private NoticeService noticeService;

  private Path workDirPath;
  private Path inputDirPath;
  private Path successDirPath;
  private Path errorDirPath;

  @PostConstruct
  private void init() {
    workDirPath = Paths.get(batchWorkDir);
    inputDirPath = Paths.get(batchWorkDir, INPUT_DIR);
    successDirPath = Paths.get(batchWorkDir, SUCCESS_DIR);
    errorDirPath = Paths.get(batchWorkDir, ERROR_DIR);
  }

  public boolean isPrefixInProgress(String prefix) {
    if (prefixInProgress.get(prefix) == null) {
      return false;
    } else {
      return prefixInProgress.get(prefix);
    }
  }

  /**
   * Process files that matches the given prefix. For each noticeIds in the found
   * files, do a call to the {@link NoticeService#update(String, String)} to force
   * the recalculation of notice references.
   * 
   * @param filePrefix filePrefix to process
   * @throws IOException - in case of IO error while moving file to error dir
   */
  @Async
  public void process(String filePrefix) throws IOException {
    this.prefixInProgress.put(filePrefix, Boolean.TRUE);

    if (!inputDirPath.toFile().exists()) {
      throw new FileNotFoundException("Répertoire d'input non trouvé : " + inputDirPath);
    }
    String[] fileNames = inputDirPath.toFile().list(new PrefixFileFilter(filePrefix));
    log.info("Traitement du prefix {}, {} fichiers trouvés", filePrefix, fileNames.length);
    for (String fileName : fileNames) {
      File fileInProgress = inputDirPath.resolve(fileName).toFile();
      if (!fileInProgress.exists()) {
        log.warn("Fichier {} non trouvé", fileName);
        continue;
      }
      List<String> noticeUuids = null;
      try {
        noticeUuids = FileUtils.readLines(fileInProgress, StandardCharsets.UTF_8);
        int i = 0;
        for (String uuid : noticeUuids) {
          if (i % 100 == 0) {
            log.info("Traitement de la {}ème notice du fichier {} : {}", i, fileName, uuid);
          }
          try {
            noticeService.update(uuid, mapper.writeValueAsString(noticeService.get(uuid)));
            FileUtils.writeStringToFile(successDirPath.resolve(fileName).toFile(), uuid + System.lineSeparator(),
                StandardCharsets.UTF_8, true);
          } catch (Exception e) {
            FileUtils.writeStringToFile(errorDirPath.resolve(fileName).toFile(), uuid + System.lineSeparator(),
                StandardCharsets.UTF_8, true);
            log.error("Erreur lors de la mise à jour de la notice {}", uuid, e);
          }
          i++;
        }
        log.info("Fichier {} traité", fileInProgress);
      } catch (Exception e) {
        FileUtils.moveFile(fileInProgress, errorDirPath.resolve(fileName).toFile());
        log.error("Erreur lors de la mise à jour des notices du fichier {}", fileName, e);
      }
    }
    this.prefixInProgress.put(filePrefix, Boolean.FALSE);
  }

  public Set<String> readAndOrSplit() throws ServiceException, IOException {
    File inputWorkFile = inputDirPath.resolve(batchWorkFile).toFile();
    try {
      File inputFile = workDirPath.resolve(batchWorkFile).toFile();
      log.info("Traitement du fichier {}", inputFile);
      if (!inputFile.exists()) {
        log.info("Fichier général d'input déjà traité, lecture des fichiers préfixés");
        // Si on a pas le fichier initial en entrée, on lit les préfixes à partir des
        // fichier présents déjà préfixés (reprise des erreus)
        return readExistingPrefixes();
      }
      log.info("Nettoyage des répertoires de travail");
      cleanDirectories();
      FileUtils.moveFile(inputFile, inputWorkFile);
      List<String> lines = FileUtils.readLines(inputWorkFile, StandardCharsets.UTF_8);

      Set<String> prefixes = new HashSet<>();
      List<String> tempIds = new ArrayList<>();
      Integer prefix = 0;
      Map<Integer, Integer> prefixNbFile = new HashMap<>();
      prefix = incrementPrefix(prefix, prefixNbFile);
      log.info("Début découpage du fichier {}", inputWorkFile);
      for (String line : lines) {
        tempIds.add(line);
        if (tempIds.size() == maxFileLine) {
          writeFile(prefixes, tempIds, prefix, prefixNbFile);
          tempIds.clear();
          prefix = incrementPrefix(prefix, prefixNbFile);
        }
      }
      if (!tempIds.isEmpty()) {
        writeFile(prefixes, tempIds, prefix, prefixNbFile);
      }
      log.info("Déplacement du fichier {} vers {}", inputWorkFile, successDirPath);
      FileUtils.moveFile(inputWorkFile, successDirPath.resolve(batchWorkFile).toFile());
      return prefixes;
    } catch (IOException e) {
      FileUtils.moveFile(inputWorkFile, errorDirPath.resolve(batchWorkFile).toFile());
      throw new ServiceException("Erreur lors de la préparation des fichiers pour le traitement batch post reprise", e);
    }
  }

  private void writeFile(Set<String> prefixes, List<String> tempIds, Integer prefix, Map<Integer, Integer> prefixNbFile)
      throws IOException {
    String basePrefix = StringUtils.leftPad(prefix + "", 2, "0");
    String prefixString = basePrefix + "_" + prefixNbFile.get(prefix);
    File file = inputDirPath.resolve(prefixString + batchWorkFile).toFile();
    log.info("Ecriture du fichier {}", file);
    FileUtils.writeLines(file, tempIds);
    prefixes.add(basePrefix);
  }

  private Set<String> readExistingPrefixes() {
    Set<String> prefixes = new HashSet<>();
    for (String filename : inputDirPath.toFile().list()) {
      prefixes.add(filename.replace(filename.replaceFirst("[0-9]{2}", ""), ""));
    }
    return prefixes;
  }

  private void cleanDirectories() throws IOException {
    FileUtils.cleanDirectory(errorDirPath.toFile());
    FileUtils.cleanDirectory(successDirPath.toFile());
    FileUtils.cleanDirectory(inputDirPath.toFile());
  }

  private int incrementPrefix(Integer prefixAsNum, Map<Integer, Integer> prefixNbFile) {
    int newPrefix = prefixAsNum == 10 ? 1 : prefixAsNum + 1;
    if (prefixNbFile.get(newPrefix) != null) {
      prefixNbFile.put(newPrefix, prefixNbFile.get(newPrefix) + 1);
    } else {
      prefixNbFile.put(newPrefix, 1);
    }
    return newPrefix;
  }
}
