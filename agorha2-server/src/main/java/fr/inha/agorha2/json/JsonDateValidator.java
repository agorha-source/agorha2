package fr.inha.agorha2.json;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;
import java.util.Optional;
import org.everit.json.schema.FormatValidator;

public class JsonDateValidator implements FormatValidator {

  @Override
  public String formatName() {
    return "date-era";
  }

  @Override
  public Optional<String> validate(String subject) {

    DateTimeFormatter formatEra = DateTimeFormatter.ofPattern("yyyy-MM-dd GG", Locale.ENGLISH);

    try {
      formatEra.parse(subject);
      return Optional.empty();
    } catch (DateTimeParseException e) {
      return Optional.of(String.format("Invalid date format : [%s]", subject));
    }
  }

}
