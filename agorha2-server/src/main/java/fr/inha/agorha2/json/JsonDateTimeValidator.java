package fr.inha.agorha2.json;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;
import org.everit.json.schema.FormatValidator;

public class JsonDateTimeValidator implements FormatValidator {

  @Override
  public String formatName() {
    return "date-time";
  }

  @Override
  public Optional<String> validate(String subject) {

    try {
      DateTimeFormatter.ISO_DATE_TIME.parse(subject);
      return Optional.empty();
    } catch (DateTimeParseException e) {
      return Optional.of(String.format("Invalid date format : [%s]", subject));
    }
  }

}
