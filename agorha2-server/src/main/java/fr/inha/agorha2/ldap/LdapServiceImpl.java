package fr.inha.agorha2.ldap;

import fr.inha.agorha2.security.LdapInhaConfiguration;
import java.util.Optional;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnMissingBean(LdapInhaConfiguration.class)
public class LdapServiceImpl extends AbstractLdapService {

  @Override
  public boolean exists(String mail) {
    return userAgorhaRepository.existsById(LdapUtils.newLdapName("cn=" + mail + "," + searchBase));
  }

  @Override
  public LdapUser get(String mail) {
    Optional<LdapAgorhaUser> optionalAgorhaUser = userAgorhaRepository
        .findById(LdapUtils.newLdapName("cn=" + mail + "," + searchBase));
    if (optionalAgorhaUser.isPresent()) {
      return optionalAgorhaUser.get();
    }
    return null;
  }

}
