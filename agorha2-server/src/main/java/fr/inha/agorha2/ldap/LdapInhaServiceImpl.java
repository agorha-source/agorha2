package fr.inha.agorha2.ldap;

import fr.inha.agorha2.security.LdapInhaConfiguration;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.ldap.support.LdapUtils;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnBean(LdapInhaConfiguration.class)
public class LdapInhaServiceImpl extends AbstractLdapService {

  @Autowired
  private UserInhaRepository inhaUserRepository;

  @Override
  public boolean exists(String mail) {
    return userAgorhaRepository.existsById(LdapUtils.newLdapName("cn=" + mail + "," + searchBase))
        || inhaUserRepository.existsById(LdapUtils.newLdapName("uid=" + mail + "," + inhaSearchBase));
  }

  @Override
  public LdapUser get(String mail) {
    Optional<LdapAgorhaUser> optionalAgorhaUser = userAgorhaRepository
        .findById(LdapUtils.newLdapName("cn=" + mail + "," + searchBase));
    if (optionalAgorhaUser.isPresent()) {
      return optionalAgorhaUser.get();
    } else {
      Optional<LdapInhaUser> optionalInhaUser = inhaUserRepository
          .findById(LdapUtils.newLdapName("uid=" + mail + "," + inhaSearchBase));
      if (optionalInhaUser.isPresent()) {
        return optionalInhaUser.get();
      }
    }
    return null;
  }
}
