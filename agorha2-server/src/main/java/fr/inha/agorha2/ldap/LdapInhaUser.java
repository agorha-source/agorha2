package fr.inha.agorha2.ldap;

import javax.naming.ldap.LdapName;
import org.springframework.ldap.odm.annotations.Attribute;
import org.springframework.ldap.odm.annotations.Entry;
import org.springframework.ldap.odm.annotations.Id;

/**
 * Les attributs sont identiques à la classe {@link LdapAgorhaUser} car la
 * librairie ne supporte pas l'héritage pour les attributs.
 * 
 * @author broussot
 *
 */
@Entry(base = "ou=People,dc=inha,dc=fr", objectClasses = { "inhaPerson", "supannPerson", "inetOrgPerson", "eduPerson" })
public class LdapInhaUser implements LdapUser {

  @Id
  private LdapName id;
  private @Attribute(name = "cn") String username;
  private @Attribute(name = "userPassword") String password;
  private @Attribute(name = "mail") String mail;
  private @Attribute(name = "givenName") String name;
  private @Attribute(name = "sn") String firstName;
  private @Attribute(readonly = true) boolean isNew;

  /**
   * Spécifique au LDAP de l'INHA.
   */
  private @Attribute(name = "displayName") String displayName;

  public LdapName getId() {
    return id;
  }

  public void setId(LdapName id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  @Override
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public void setNew(boolean isNew) {
    this.isNew = isNew;
  }

  @Override
  public boolean isNew() {
    return isNew;
  }
}
