package fr.inha.agorha2.ldap;

import java.security.SecureRandom;
import java.util.Optional;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.Crypt;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.support.LdapUtils;

public abstract class AbstractLdapService implements LdapService {

  @Autowired
  protected UserAgorhaRepository userAgorhaRepository;

  @Autowired
  protected LdapTemplate ldapTemplate;

  @Value("${ldap.server.dnSearchBase}")
  protected String searchBase;

  @Value("${ldap.inha.dnSearchBase}")
  protected String inhaSearchBase;

  @Override
  public LdapAgorhaUser create(String password, String mail, String name, String firstName) {
    LdapAgorhaUser user = new LdapAgorhaUser();
    user.setId(LdapUtils.newLdapName("cn=" + mail + "," + searchBase));
    user.setUsername(mail);
    user.setPassword(crypt(password, generateSalt()));
    user.setMail(mail);
    user.setName(name);
    user.setFirstName(firstName);
    user.setNew(true);

    return userAgorhaRepository.save(user);
  }

  @Override
  public void delete(String mail) {
    LdapAgorhaUser user = new LdapAgorhaUser();
    user.setId(LdapUtils.newLdapName("cn=" + mail + "," + searchBase));

    userAgorhaRepository.delete(user);
  }

  @Override
  public LdapAgorhaUser getAgorhaUser(String mail) {
    Optional<LdapAgorhaUser> optionalAgorhaUser = userAgorhaRepository
        .findById(LdapUtils.newLdapName("cn=" + mail + "," + searchBase));
    if (optionalAgorhaUser.isPresent()) {
      return optionalAgorhaUser.get();
    }
    return null;
  }

  @Override
  public LdapUser save(LdapAgorhaUser ldapUser) {
    return userAgorhaRepository.save(ldapUser);
  }

  @Override
  public void update(LdapAgorhaUser ldapUser) {
    DirContextOperations ctx = ldapTemplate
        .lookupContext(LdapUtils.newLdapName("cn=" + ldapUser.getMail() + "," + searchBase));

    ctx.setAttributeValue("sn", ldapUser.getFirstName());
    ctx.setAttributeValue("givenName", ldapUser.getName());
    if (StringUtils.isNotEmpty(ldapUser.getPassword())) {
      ctx.setAttributeValue("userPassword", crypt(ldapUser.getPassword(), generateSalt()));
    }

    ldapTemplate.modifyAttributes(ctx);
  }

  /**
   * Chiffre un mot de passe en utilisant SHA-512, ajoute un nombre important de
   * rounds.
   * 
   * @param clearPassword mot de passe en clair
   * @param salt          sel utilisé pour le mot de passe
   * @return mot de passe chiffré dans le format attendu par le LDAP
   */
  private String crypt(String clearPassword, String salt) {
    return "{CRYPT}" + Crypt.crypt(clearPassword, "$6$rounds=80000$" + salt);
  }

  /**
   * Génère un sel utilisé dans le mot de passe pour renforcer la sécurité. On
   * limite à 16 la taille du sel parce que la chaine de caractère pour le sel ne
   * doit pas faire plus de 16 caractères. On transforme en base64 parce que tous
   * les caractères ne sont pas autorisés dans le sel.
   * 
   * @return
   */
  private String generateSalt() {
    SecureRandom random = new SecureRandom();
    byte[] bytes = new byte[20];
    random.nextBytes(bytes);
    return Base64.encodeBase64String(bytes).substring(0, 16);
  }

}
