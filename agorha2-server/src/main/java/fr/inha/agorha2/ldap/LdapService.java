package fr.inha.agorha2.ldap;

public interface LdapService {

  public LdapAgorhaUser create(String password, String mail, String name, String firstName);

  public void delete(String mail);

  public boolean exists(String mail);

  public LdapUser get(String mail);

  public LdapAgorhaUser getAgorhaUser(String mail);

  public LdapUser save(LdapAgorhaUser ldapUser);

  public void update(LdapAgorhaUser ldapUser);

}
