package fr.inha.agorha2.ldap;

import org.springframework.data.ldap.repository.support.SimpleLdapRepository;
import org.springframework.ldap.core.LdapOperations;
import org.springframework.ldap.odm.core.ObjectDirectoryMapper;

public class UserAgorhaRepository extends SimpleLdapRepository<LdapAgorhaUser> {

  public UserAgorhaRepository(LdapOperations ldapOperations, ObjectDirectoryMapper odm) {
    super(ldapOperations, odm, LdapAgorhaUser.class);
  }

}
