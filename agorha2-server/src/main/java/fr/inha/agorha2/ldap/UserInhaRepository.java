package fr.inha.agorha2.ldap;

import org.springframework.data.ldap.repository.support.SimpleLdapRepository;
import org.springframework.ldap.core.LdapOperations;
import org.springframework.ldap.odm.core.ObjectDirectoryMapper;

public class UserInhaRepository extends SimpleLdapRepository<LdapInhaUser> {

  public UserInhaRepository(LdapOperations ldapOperations, ObjectDirectoryMapper odm) {
    super(ldapOperations, odm, LdapInhaUser.class);
  }

}
