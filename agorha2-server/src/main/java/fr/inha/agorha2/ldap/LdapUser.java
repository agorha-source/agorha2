package fr.inha.agorha2.ldap;

import javax.naming.ldap.LdapName;
import org.springframework.data.domain.Persistable;

public interface LdapUser extends Persistable<LdapName> {

  public String getName();

  public String getFirstName();

  public String getMail();

}
