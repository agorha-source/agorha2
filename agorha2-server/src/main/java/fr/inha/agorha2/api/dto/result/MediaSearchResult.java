package fr.inha.agorha2.api.dto.result;

import java.util.ArrayList;
import java.util.List;

public class MediaSearchResult extends ApiSearchResult {

  private String caption;
  private String thumbnail;
  private List<String> urls = new ArrayList<>();

  public String getCaption() {
    return caption;
  }

  public void setCaption(String caption) {
    this.caption = caption;
  }

  public String getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(String thumbnail) {
    this.thumbnail = thumbnail;
  }

  public List<String> getUrls() {
    return urls;
  }

}
