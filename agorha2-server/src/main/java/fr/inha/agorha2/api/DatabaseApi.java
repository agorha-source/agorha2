package fr.inha.agorha2.api;

import fr.inha.agorha2.api.dto.request.FindDatabaseRequest;
import fr.inha.agorha2.api.dto.response.DatabaseListResponse;
import fr.inha.elastic.model.Database;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/database")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@OpenAPIDefinition(info = @Info(title = "Database API", version = "0.0.1-SNAPSHOT", license = @License(name = "Apache 2.0 License", url = "http://www.apache.org/licenses/LICENSE-2.0.html")))
@Tag(name = "Database")
public interface DatabaseApi {

  @GET
  @Path("/list")
  public DatabaseListResponse list(List<String> types);

  @GET
  @Path("/{id}")
  public Database get(Integer id);

  @POST
  @Path("/find")
  public Database find(FindDatabaseRequest request);

}
