package fr.inha.agorha2.api.dto.response;

import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.Notice;
import java.util.ArrayList;
import java.util.List;

public class MosaicCardResponse {

  private Notice notice;
  private List<Media> medias = new ArrayList<>();

  public Notice getNotice() {
    return notice;
  }

  public void setNotice(Notice notice) {
    this.notice = notice;
  }

  public List<Media> getMedias() {
    return medias;
  }

}
