package fr.inha.agorha2.api.dto;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import org.apache.commons.lang3.StringUtils;

/**
 * Allows implicit polymorphic deserializations for impls of SearchFilter.
 * 
 * @author mdutoo
 *
 */
public class SearchFilterDeserializer extends JsonDeserializer<SearchFilter> {

  @Override
  public SearchFilter deserialize(JsonParser p, DeserializationContext ctxt)
      throws IOException, JsonProcessingException {
    if (p.getCurrentToken() == JsonToken.START_OBJECT) {
      JsonNode searchFilterNode = p.readValueAsTree();

      JsonNode values = searchFilterNode.get("values");

      if (values != null) {
        JsonNode firstValue = values.get(0);
        if (firstValue != null) {
          if (firstValue.get("min") != null && StringUtils.isNumericSpace(firstValue.get("min").asText())
              || firstValue.get("max") != null && StringUtils.isNumericSpace(firstValue.get("max").asText())) {
            return p.getCodec().treeToValue(searchFilterNode, LongRangeFilter.class);

          } else if (firstValue.get("min") != null && firstValue.get("min").isTextual()
              || firstValue.get("max") != null && firstValue.get("max").isTextual()) {
            return p.getCodec().treeToValue(searchFilterNode, DateRangeFilter.class);
          }
        }
        return p.getCodec().treeToValue(searchFilterNode, MultiValueSearchFilter.class);
      }

      throw new IllegalStateException("Can't detect SearchFilter concrete class for " + searchFilterNode);
    }
    throw new IllegalStateException(
        "Attempting to deserialize SearchFilter " + "and expected START_OBJECT token but found " + p.getCurrentToken());
  }
}
