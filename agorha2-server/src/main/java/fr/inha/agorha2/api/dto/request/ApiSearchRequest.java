package fr.inha.agorha2.api.dto.request;

import fr.inha.agorha2.api.dto.Aggregation;
import java.util.HashMap;
import java.util.Map;

public class ApiSearchRequest {

  private String request;
  private String sort;
  private Map<String, Aggregation> aggregations = new HashMap<>();

  public ApiSearchRequest() {
  }

  public ApiSearchRequest(String request, String sort, Map<String, Aggregation> aggregations) {
    super();
    this.request = request;
    this.sort = sort;
    this.aggregations = aggregations;
  }

  public String getRequest() {
    return request;
  }

  public void setRequest(String request) {
    this.request = request;
  }

  public String getSort() {
    return sort;
  }

  public void setSort(String sort) {
    this.sort = sort;
  }

  public Map<String, Aggregation> getAggregations() {
    return aggregations;
  }

}
