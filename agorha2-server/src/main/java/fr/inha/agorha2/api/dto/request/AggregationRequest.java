package fr.inha.agorha2.api.dto.request;

import fr.inha.agorha2.api.dto.Aggregation;
import java.util.HashMap;
import java.util.Map;

public class AggregationRequest {
  private Map<String, Aggregation> aggregations = new HashMap<>();

  public Map<String, Aggregation> getAggregations() {
    return aggregations;
  }

}
