package fr.inha.agorha2.api;

import fr.inha.agorha2.api.dto.AuthenticatedUser;
import fr.inha.agorha2.api.dto.sort.SavedSearchSortType;
import fr.inha.elastic.model.SavedSearch;
import fr.inha.elastic.model.Selection;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.elasticsearch.search.sort.SortOrder;

@Path("/users")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@OpenAPIDefinition(info = @Info(title = "Users API", version = "0.0.1-SNAPSHOT", license = @License(name = "Apache 2.0 License", url = "http://www.apache.org/licenses/LICENSE-2.0.html")))
@Tag(name = "Utilisateur")
public interface UserApi {

  @GET
  @Path("/authenticate")
  public AuthenticatedUser login(String login, String password);

  @POST
  @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
  public Map<String, String> create(String name, String firstName, String mail, String password);

  @PUT
  @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
  public Map<String, String> update(String name, String firstName, String mail, String oldPassword, String newPassword);

  @DELETE
  public void delete();

  @PUT
  @Path("/validate/{id}")
  public void validate(String uuid, String token);

  @GET
  @Path("/resetPassword")
  public void resetPassword(String mail);

  @GET
  @Path("/resetPassword/{id}/isValid")
  public boolean resetPasswordIsValid(String uuid, String token);

  @PUT
  @Path("/updatePassword/{id}")
  public void updatePassword(String uuid, String newPassword, String token);

  @GET
  @Path("/isAuthenticated")
  public boolean isAuthenticated();

  @GET
  @Path("/current")
  public AuthenticatedUser getCurrent();

  @POST
  @Path("/selections")
  @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
  public Selection createSelection(String title, List<String> noticeIds);

  @PUT
  @Path("/selections/{id}/add")
  public Selection addToSelection(String id, List<String> noticeIds);

  @PUT
  @Path("/selections/{id}/remove")
  public Selection removeFromSelection(String id, List<String> noticeIds);

  @DELETE
  @Path("/selections/{id}")
  public void deleteSelection(String id);

  @GET
  @Path("/selections")
  public List<Selection> getSelections(String terms, Integer page, Integer pageSize, String sort, String sortType);

  @POST
  @Path("/savedSearches")
  @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
  public SavedSearch createSavedSearch(String name, List<String> resume, String url);

  @DELETE
  @Path("/savedSearches/{id}")
  public void deleteSavedSearch(String id);

  @GET
  @Path("/savedSearches")
  public List<SavedSearch> getSavedSearches(String terms, Integer page, Integer pageSize, SortOrder sort,
      SavedSearchSortType sortType);

}
