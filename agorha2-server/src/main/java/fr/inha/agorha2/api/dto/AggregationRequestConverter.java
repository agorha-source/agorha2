package fr.inha.agorha2.api.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inha.agorha2.api.dto.request.AggregationRequest;
import java.io.IOException;
import java.util.Base64;
import javax.ws.rs.ext.ParamConverter;
import org.apache.commons.lang3.StringUtils;

public class AggregationRequestConverter implements ParamConverter<AggregationRequest> {

  @Override
  public AggregationRequest fromString(String value) {
    if (StringUtils.isEmpty(value)) {
      return new AggregationRequest();
    }
    Base64.Decoder decoder = Base64.getDecoder();

    try {
      ObjectMapper objectMapper = new ObjectMapper();
      byte[] decodedValue = decoder.decode(value);

      return objectMapper.readValue(decodedValue, AggregationRequest.class);
    } catch (IllegalArgumentException | IOException e) {
      throw new RuntimeException("Erreur lors de la déserialisation de la requête", e);
    }
  }

  @Override
  public String toString(AggregationRequest value) {
    throw new UnsupportedOperationException();
  }

}
