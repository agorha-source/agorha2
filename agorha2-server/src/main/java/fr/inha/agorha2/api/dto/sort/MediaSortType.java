package fr.inha.agorha2.api.dto.sort;

public enum MediaSortType {
  UPDATED_DATE("internal.updatedDate"),
  TITLE("caption.keyword"),
  PERTINENCE(null);

  private String field;

  private MediaSortType(String field) {
    this.field = field;
  }

  public String getField() {
    return field;
  }

}
