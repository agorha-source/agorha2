package fr.inha.agorha2.api.dto.result;

import java.util.ArrayList;
import java.util.List;

public class FacetResult {

  private String id;
  private List<FacetResultEntry> entries = new ArrayList<>();
  private String facetScope;
  private Integer facetOrder;
  private boolean pathHierarchical;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<FacetResultEntry> getEntries() {
    return entries;
  }

  public String getFacetScope() {
    return facetScope;
  }

  public void setFacetScope(String facetScope) {
    this.facetScope = facetScope;
  }

  public Integer getFacetOrder() {
    return facetOrder;
  }

  public void setFacetOrder(Integer facetOrder) {
    this.facetOrder = facetOrder;
  }

  public boolean isPathHierarchical() {
    return pathHierarchical;
  }

  public void setPathHierarchical(boolean pathHierarchical) {
    this.pathHierarchical = pathHierarchical;
  }

}
