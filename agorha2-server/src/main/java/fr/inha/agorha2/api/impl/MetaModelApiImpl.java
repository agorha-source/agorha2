package fr.inha.agorha2.api.impl;

import fr.inha.agorha2.api.MetaModelApi;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.service.MetaModelService;
import fr.inha.elastic.model.NoticeType;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/metamodeles")
public class MetaModelApiImpl extends CxfJaxrsRequestInfoProvider implements MetaModelApi, CxfJaxrsDeployable {

  private static final Logger log = LoggerFactory.getLogger(MetaModelApiImpl.class);

  @Autowired
  private MetaModelService metaModelService;

  @Override
  @GET
  public String getVersion(@QueryParam(value = "version") String version,
      @QueryParam(value = "noticeType") String noticeType, @QueryParam(value = "default") Integer useDefault) {
    try {
      NoticeType.valueOf(noticeType);
    } catch (IllegalArgumentException e) {
      log.error("Erreur lors de la récupération d'un metamodele", e);
      throw new BadRequestException("Type de notice inconnu");
    }

    return metaModelService.get(version, NoticeType.valueOf(noticeType));
  }

}
