package fr.inha.agorha2.api.dto.sort;

public enum UserSortType {
  LASTNAME("internal.lastname.keyword"),
  FIRSTNAME("internal.firstname.keyword"),
  CREATION_DATE("internal.createdDate"),
  ROLE("authorizations.role"),
  STATUS("internal.status");

  private String field;

  private UserSortType(String field) {
    this.field = field;
  }

  public String getField() {
    return field;
  }

}
