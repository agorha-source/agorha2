package fr.inha.agorha2.api;

import fr.inha.agorha2.api.dto.response.UserSearchResponse;
import fr.inha.agorha2.api.dto.sort.UserSortType;
import fr.inha.elastic.model.Authorizations;
import fr.inha.elastic.model.NoticeType;
import fr.inha.elastic.model.User;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/administration")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@OpenAPIDefinition(info = @Info(title = "Administration API", version = "0.0.1-SNAPSHOT", license = @License(name = "Apache 2.0 License", url = "http://www.apache.org/licenses/LICENSE-2.0.html")))
@SecurityRequirement(name = "basicAuth")
@Tag(name = "Administration")
public interface AdministrationApi {

  @PATCH
  @Path("/users/{id}/authorizations")
  public Map<String, Object> updateAuthorizations(String id, Authorizations request);

  @GET
  @Path("/users/{id}")
  public User get(String id);

  @GET
  @Path("/users/search")
  public UserSearchResponse searchUser(Integer page, String terms, Integer pageSize, String sort,
      UserSortType fieldSort);

  @DELETE
  @Path("/users/{id}")
  public void deleteUser(String id);

  @POST
  @Path("/metamodeles/{noticeType}/update")
  public void updateMetamodel(NoticeType noticeType);

}
