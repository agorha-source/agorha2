package fr.inha.agorha2.api.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import fr.inha.agorha2.api.ContributionApi;
import fr.inha.agorha2.api.dto.Aggregation;
import fr.inha.agorha2.api.dto.MediaItem;
import fr.inha.agorha2.api.dto.request.AddNewsToSelectionRequest;
import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import fr.inha.agorha2.api.dto.request.AggregationRequest;
import fr.inha.agorha2.api.dto.request.MediaSearchRequest;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest;
import fr.inha.agorha2.api.dto.request.PaginatedSearchRequest;
import fr.inha.agorha2.api.dto.request.RemoveNewsFromSelectionRequest;
import fr.inha.agorha2.api.dto.request.UpdateNoticeRequest;
import fr.inha.agorha2.api.dto.response.MediaSearchResponse;
import fr.inha.agorha2.api.dto.sort.MediaSortType;
import fr.inha.agorha2.api.dto.sort.NoticeSortType;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.DatabaseService;
import fr.inha.agorha2.service.ExportService;
import fr.inha.agorha2.service.MediaService;
import fr.inha.agorha2.service.NoticeService;
import fr.inha.agorha2.service.SearchService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.elastic.model.Concept;
import fr.inha.elastic.model.Database;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.Notice;
import fr.inha.elastic.model.NoticeStatus;
import fr.inha.elastic.model.NoticeType;
import fr.inha.elastic.model.Thesaurus;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@Component
@Path("/contribution")
@SecurityRequirement(name = "basicAuth")
public class ContributionApiImpl extends CxfJaxrsRequestInfoProvider implements ContributionApi, CxfJaxrsDeployable {

  private static final String NOTICE_TYPE_UNKNOWN = "notice.type.unknown";

  @Autowired
  private MediaService mediaService;

  @Autowired
  private ExportService exportService;

  @Autowired
  private NoticeService noticeService;

  @Autowired
  private DatabaseService databaseService;

  @Autowired
  private SearchService searchService;

  @Autowired
  private MessageSource messageSource;

  @Value(value = "${agorha2.rest.file.extensions.local}")
  private List<String> localExtensions;

  @Value(value = "${agorha2.rest.file.extensions.manifest}")
  private List<String> manifestExtensions;

  @Value(value = "${agorha2.rest.file.extensions.thumbnail}")
  private List<String> thumbnailExtensions;

  @Override
  @POST
  @Path("/notices")
  @SecurityRequirement(name = "basicAuth")
  public Notice create(String noticeAsJson) {
    try {
      return noticeService.create(noticeAsJson);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la creation d'une notice", e);
    }
  }

  @DELETE
  @Path("/notices/{id}")
  @Override
  @SecurityRequirement(name = "basicAuth")
  public void delete(@PathParam(value = "id") String id) {
    try {
      noticeService.delete(id);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la suppression de la notice " + id, e);
    }
  }

  @PATCH
  @Path("/notices/status")
  @Override
  @SecurityRequirement(name = "basicAuth")
  public List<Map<String, String>> updateStatus(UpdateNoticeRequest request) {
    NoticeStatus noticeStatus;
    try {
      noticeStatus = NoticeStatus.valueOf(request.getStatus());
    } catch (IllegalArgumentException e) {
      throw new BadRequestException(
          messageSource.getMessage("notice.status.unknown", new Object[] { request.getStatus() }, null));
    }

    try {
      return noticeService.upddateStatus(request.getIds(), noticeStatus);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la mise à jour des status", e);
    }
  }

  @Override
  @PUT
  @Path("/notices/{id}")
  @SecurityRequirement(name = "basicAuth")
  public Notice update(@PathParam(value = "id") String id, String noticeAsJson) {
    try {
      return noticeService.update(id, noticeAsJson);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la modification d'une notice", e);
    }
  }

  @Override
  @POST
  @Path("/notices/{noticeId}/duplicate")
  @SecurityRequirement(name = "basicAuth")
  public Notice duplicate(@PathParam(value = "noticeId") String noticeId,
      @QueryParam(value = "databaseId") String databaseId) {
    try {
      return noticeService.duplicate(noticeId, databaseId);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la duplication d'une notice", e);
    }
  }

  @GET
  @Path("/notices/autocomplete")
  @Override
  public List<Map<String, String>> autocompleteNoticeTitle(@QueryParam(value = "terms") String terms,
      @QueryParam(value = "noticeType") String noticeType) {
    if (StringUtils.isEmpty(terms) || terms.length() < 2) {
      throw new BadRequestException(
          messageSource.getMessage("notice.title.autocomplete.min.char", new Object[] {}, null));
    }
    NoticeType noticeTypeEnum = null;
    if (StringUtils.isNotBlank(noticeType)) {
      try {
        noticeTypeEnum = NoticeType.valueOf(noticeType);
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(messageSource.getMessage(NOTICE_TYPE_UNKNOWN, new Object[] {}, null));
      }
    }
    try {
      return searchService.autocompleteNoticeTitle(terms, noticeTypeEnum);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(
          "Erreur lors de l'autocomplete sur le titre des notices, terms = " + terms + ", noticeType = " + noticeType,
          e);
    }
  }

  @Override
  @POST
  @Path("/news/addToSelection")
  public void addToSelection(AddNewsToSelectionRequest request) {
    if (!StringUtils.isNoneBlank(request.getNewsUrl(), request.getNewsTitle(), request.getSelectionId())) {
      throw new BadRequestException("Tout les paramètres sont obligatoire (newsUrl, newsTitle, selectionId)");
    }
    try {
      noticeService.addNews(request.getSelectionId(), request.getNewsUrl(), request.getNewsTitle());
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de l'association d'un article " + request.getNewsTitle()
          + " à une selection de notice " + request.getSelectionId(), e);
    }
  }

  @Override
  @PUT
  @Path("/news/removeFromSelection")
  public void removeFromSelection(RemoveNewsFromSelectionRequest request) {
    if (StringUtils.isBlank(request.getNewsUrl())) {
      throw new BadRequestException("Le paramètre newsUrl est obligatoire");
    }
    try {
      noticeService.removeNews(request.getSelectionId(), request.getNewsUrl());
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la désassociation d'un article" + request.getNewsUrl(), e);
    }

  }

  @POST
  @Path("/media")
  @Consumes({ MediaType.MULTIPART_FORM_DATA })
  @Override
  public Map<String, Object> createMedia(MultipartBody body) throws IOException {
    Media media = new Media();

    List<MediaItem> files = new ArrayList<>();
    MediaItem item = null;
    for (Attachment attachment : body.getAllAttachments()) {
      String attachmentName = attachment.getContentDisposition().getParameter("name");
      InputStream inputStream = attachment.getDataHandler().getDataSource().getInputStream();

      if (inputStream.available() > 0) {
        if (attachmentName.equals("file") || attachmentName.equals("url")) {
          if (item != null) {
            files.add(item);
          }
          item = new MediaItem();
          item.setName(attachment.getDataHandler().getName());
          item.setInputStream(inputStream);
        } else if (item != null && attachmentName.equals("mediaType")) {
          item.setMediaType(
              fr.inha.elastic.model.MediaType.valueOf(IOUtils.toString(inputStream, StandardCharsets.UTF_8)));
        } else if (item != null && attachmentName.equals("vignette")) {
          checkExtension(attachment.getDataHandler().getName(), thumbnailExtensions,
              "media.video.thumbnail.extension.invalid");
          item.setThumbnail(inputStream);
        } else if (item != null && attachmentName.equals("uniqueKeyFile")) {
          item.setUniqueKey(Integer.valueOf(IOUtils.toString(inputStream, StandardCharsets.UTF_8)));
        } else if (item != null && attachmentName.equals("uuid")) {
          item.setUuid(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
        } else {
          fillMediaData(media, attachmentName, inputStream, true);
        }
      }
    }
    if (item != null) {
      files.add(item);
    }

    if (StringUtils.isBlank(media.getCaption())) {
      throw new BadRequestException(messageSource.getMessage("media.title.mandatory", new Object[] {}, null));
    }

    if (files.isEmpty()) {
      throw new BadRequestException(messageSource.getMessage("media.files.mandatory", new Object[] {}, null));
    }
    checkExtensions(files);
    try {
      return mediaService.createMedia(files, media);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la creation d'un media", e);
    }
  }

  @PUT
  @Path("/media/{id}")
  @Consumes({ MediaType.MULTIPART_FORM_DATA })
  @Override
  public Map<String, String> updateMedia(@PathParam(value = "id") String uuid, MultipartBody body) throws IOException {
    Media media = new Media();

    List<MediaItem> files = new ArrayList<>();
    MediaItem item = null;
    for (Attachment attachment : body.getAllAttachments()) {
      String attachmentName = attachment.getContentDisposition().getParameter("name");
      InputStream inputStream = attachment.getDataHandler().getDataSource().getInputStream();

      if (inputStream.available() > 0) {
        if (attachmentName.equals("file") || attachmentName.equals("url")) {
          item = getOrCreateItem(item, files, attachmentName);
          item.setName(attachment.getDataHandler().getName());
          item.setInputStream(inputStream);
        } else if (attachmentName.equals("mediaType")) {
          item = getOrCreateItem(item, files, attachmentName);
          item.setMediaType(
              fr.inha.elastic.model.MediaType.valueOf(IOUtils.toString(inputStream, StandardCharsets.UTF_8)));
        } else if (item != null && attachmentName.equals("vignette")) {
          checkExtension(attachment.getDataHandler().getName(), thumbnailExtensions,
              "media.video.thumbnail.extension.invalid");
          item.setThumbnail(inputStream);
        } else if (attachmentName.equals("uuid")) {
          item = getOrCreateItem(item, files, attachmentName);
          item.setUuid(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
        } else {
          fillMediaData(media, attachmentName, inputStream, false);
        }
      }
    }
    if (item != null) {
      files.add(item);
    }

    if (StringUtils.isBlank(media.getCaption())) {
      throw new BadRequestException(messageSource.getMessage("media.title.mandatory", new Object[] {}, null));
    }

    if (files.isEmpty()) {
      throw new BadRequestException(messageSource.getMessage("media.files.mandatory", new Object[] {}, null));
    }

    try {
      return mediaService.updateMedia(uuid, files, media);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la creation d'un media", e);
    }
  }

  @Override
  @GET
  @Path("/media/search")
  @SecurityRequirement(name = "basicAuth")
  public MediaSearchResponse searchMedia(@QueryParam(value = "terms") String terms,
      @QueryParam(value = "sort") String sort, @QueryParam(value = "fieldSort") MediaSortType filedSort,
      @QueryParam(value = "page") Integer page, @QueryParam(value = "pageSize") Integer pageSize,
      @QueryParam(value = "facets") AggregationRequest aggregationRequest) {
    if (page == null) {
      page = PaginatedSearchRequest.DEFAULT_PAGE;
    }
    if (pageSize == null) {
      pageSize = PaginatedSearchRequest.DEFAULT_PAGE_SIZE;
    }
    if (page < 1 || pageSize > 10000) {
      throw new BadRequestException(messageSource.getMessage("search.paging.error", new Object[] {}, null));
    }
    try {
      Map<String, Aggregation> aggregations = null;
      if (aggregationRequest != null) {
        aggregations = aggregationRequest.getAggregations();
      }
      MediaSearchRequest request = new MediaSearchRequest(page, pageSize, terms, sort, filedSort, aggregations);
      return searchService.searchMedia(request);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de l'export Json", e);
    }
  }

  @Override
  @DELETE
  @Path("/media")
  @SecurityRequirement(name = "basicAuth")
  public long deleteMedia(@QueryParam(value = "id") List<String> uuids) {
    try {
      long deleted = mediaService.delete(uuids);
      if (uuids.size() > deleted) {
        throw new WebApplicationException(messageSource.getMessage("media.delete.partial", new Object[] {}, null),
            Status.OK);
      }
      return deleted;
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }

  }

  @Override
  @GET
  @Produces(MediaType.APPLICATION_OCTET_STREAM)
  @Path("/notice/exportJson")
  public Response exportJson(@QueryParam(value = "terms") String terms, @QueryParam(value = "sort") String sort,
      @QueryParam(value = "fieldSort") String fieldSort) {

    StreamingOutput stream = new StreamingOutput() {
      @Override
      public void write(OutputStream os) throws IOException, WebApplicationException {
        NoticeSortType sortField = null;
        if (StringUtils.isNoneEmpty(fieldSort)) {
          try {
            sortField = NoticeSortType.valueOf(fieldSort);
          } catch (IllegalArgumentException e) {
            throw new BadRequestException(
                messageSource.getMessage("search.sort.unknown", new Object[] { fieldSort }, null), e);
          }
        }
        Writer writer = new BufferedWriter(new OutputStreamWriter(os));
        try {
          NoticeSearchRequest apiSearchRequest = new NoticeSearchRequest(terms, sort, sortField, null);
          exportService.exportJson(writer, apiSearchRequest);
        } catch (ServiceException e) {
          throw new InternalServerErrorException("Erreur lors de l'export Json", e);
        }
      }
    };
    ResponseBuilder response = Response.ok(stream);
    response.header("Content-Disposition", "attachment;filename=exportNoticeJson.json");
    return response.build();
  }

  @Override
  @GET
  @Path("/notice/advExportCsv")
  public Response advExportCsv(@QueryParam(value = "filters") AdvancedSearchRequest request) {
    NoticeType noticeTypeEnum = null;
    if (StringUtils.isNotBlank(request.getNoticeType())) {
      try {
        noticeTypeEnum = NoticeType.valueOf(request.getNoticeType());
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(messageSource.getMessage(NOTICE_TYPE_UNKNOWN, new Object[] {}, null));
      }
    }
    return export(request, noticeTypeEnum);
  }

  @Override
  @GET
  @Path("/notice/simpleExportCsv")
  public Response exportCsv(@QueryParam(value = "terms") String terms, @QueryParam(value = "sort") String sort,
      @QueryParam(value = "fieldSort") String fieldSort, @QueryParam(value = "noticeType") String noticeType,
      @QueryParam(value = "facets") AggregationRequest aggregationRequest) {
    NoticeSortType sortField = null;
    if (StringUtils.isNoneEmpty(fieldSort)) {
      try {
        sortField = NoticeSortType.valueOf(fieldSort);
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(
            messageSource.getMessage("search.sort.unknown", new Object[] { fieldSort }, null));
      }
    }

    NoticeType noticeTypeEnum = null;
    if (StringUtils.isNotBlank(noticeType)) {
      try {
        noticeTypeEnum = NoticeType.valueOf(noticeType);
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(messageSource.getMessage(NOTICE_TYPE_UNKNOWN, new Object[] {}, null));
      }
    }
    NoticeSearchRequest request = new NoticeSearchRequest(terms, sort, sortField, aggregationRequest.getAggregations());
    return export(request, noticeTypeEnum);
  }

  @Override
  @POST
  @Path("/databases")
  public Database createDatabase(Database database) {
    if (StringUtils.isEmpty(database.getContent().getTitle())) {
      throw new BadRequestException("Le titre doit être renseigné");
    }
    try {
      return databaseService.create(database);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(
          "Erreur lors de la création de la base de données " + database.getContent().getTitle(), e);
    }
  }

  private void checkExtensions(List<MediaItem> files) {
    for (MediaItem mediaItem : files) {
      switch (mediaItem.getMediaType()) {
        case LOCAL:
          checkExtension(mediaItem.getName(), localExtensions, "media.file.extension.invalid");
          break;
        default:
          break;
      }
    }
  }

  private void checkExtension(String fileName, List<String> extensions, String errorKey) {
    String extension = FilenameUtils.getExtension(fileName);
    if (!extensions.contains(StringUtils.lowerCase(extension))) {
      throw new BadRequestException(messageSource.getMessage(errorKey, new Object[] { extension, extensions }, null));
    }
  }

  private MediaItem getOrCreateItem(MediaItem item, List<MediaItem> files, String attachmentName) {
    if (item == null) {
      item = new MediaItem();
      if (attachmentName.equals("mediaType")) {
        item.setInputStream(InputStream.nullInputStream());
      }
      return item;
    }
    if (attachmentName.equals("file") || attachmentName.equals("url")) {
      files.add(item);
      return new MediaItem();
    } else if (attachmentName.equals("mediaType")) {
      if (item.getMediaType() != null) {
        files.add(item);
        item = new MediaItem();
        item.setInputStream(InputStream.nullInputStream());
        return item;
      } else if (item.getInputStream() == null) {
        item = new MediaItem();
        item.setInputStream(InputStream.nullInputStream());
      }
    }
    return item;
  }

  private void fillMediaData(Media media, String attachmentName, InputStream inputStream, boolean creationMode)
      throws IOException {
    if (attachmentName.equals("database")) {
      String[] refs = IOUtils.toString(inputStream, StandardCharsets.UTF_8).split(",");
      for (String ref : refs) {
        HashMap<String, String> database = new HashMap<>();
        database.put("ref", ref);
        media.getDatabase().add(database);
      }
    }
    if (attachmentName.equals("createdDate") && creationMode) {
      media.getInternal().setCreatedDate(DateUtils.parseIsoDate(IOUtils.toString(inputStream, StandardCharsets.UTF_8)));
    }
    if (attachmentName.equals("createdBy") && creationMode) {
      media.getInternal().setCreatedBy(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
    }
    if (attachmentName.equals("updatedDate")) {
      media.getInternal().setUpdatedDate(DateUtils.parseIsoDate(IOUtils.toString(inputStream, StandardCharsets.UTF_8)));
    }
    if (attachmentName.equals("updatedBy")) {
      media.getInternal().setUpdatedBy(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
    }
    if (attachmentName.equals("uniqueKey")) {
      media.getInternal().setUniqueKey(Integer.parseInt(IOUtils.toString(inputStream, StandardCharsets.UTF_8)));
    }
    if (attachmentName.equals("comment")) {
      media.setComment(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
    }
    if (attachmentName.equals("credit")) {
      media.setCredit(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
    }
    if (attachmentName.equals("caption")) {
      media.setCaption(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
    }
    if (attachmentName.equals("licence")) {
      media.setLicence(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
    }
    if (attachmentName.equals("fileProvenance")) {
      media.setFileProvenance(IOUtils.toString(inputStream, StandardCharsets.UTF_8));
    }
    if (attachmentName.equals("communicationRight")) {
      Concept communicationRight = new Concept();
      communicationRight.setThesaurus(new Thesaurus(IOUtils.toString(inputStream, StandardCharsets.UTF_8)));
      media.setCommunicationRight(communicationRight);
    }
    if (attachmentName.equals("reproductionRight")) {
      Concept reproductionRight = new Concept();
      reproductionRight.setThesaurus(new Thesaurus(IOUtils.toString(inputStream, StandardCharsets.UTF_8)));
      media.setReproductionRight(reproductionRight);
    }
  }

  private Response export(NoticeSearchRequest request, NoticeType noticeTypeEnum) {
    File zip;
    try {
      zip = exportService.exportCsv(request, noticeTypeEnum);
      StreamingOutput output = innerOutput -> {
        try {
          Files.copy(zip.toPath(), innerOutput);
        } finally {
          Files.delete(zip.toPath());
        }
      };
      ResponseBuilder response = Response.ok().entity(output).type("application/zip");
      response.header("Content-Disposition", "attachment;filename=exportNoticeCsv.zip");
      return response.build();
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors d'export csv", e);
    }
  }
}
