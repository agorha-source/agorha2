package fr.inha.agorha2.api.impl;

import fr.inha.agorha2.api.DatabaseApi;
import fr.inha.agorha2.api.dto.request.FindDatabaseRequest;
import fr.inha.agorha2.api.dto.response.DatabaseListResponse;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.DatabaseService;
import fr.inha.elastic.model.Database;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/database")
public class DatabaseApiImpl extends CxfJaxrsRequestInfoProvider implements DatabaseApi, CxfJaxrsDeployable {

  @Autowired
  private DatabaseService databaseService;

  @Override
  @GET
  @Path("/list")
  public DatabaseListResponse list(@QueryParam(value = "type") List<String> noticeTypes) {
    try {
      return databaseService.list(noticeTypes);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération des bases de données", e);
    }
  }

  @Override
  @GET
  @Path("/{id}")
  public Database get(@PathParam(value = "id") Integer id) {
    try {
      return databaseService.get(id);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération de la base de donnée " + id, e);
    }
  }

  @Override
  @POST
  @Path("/find")
  public Database find(FindDatabaseRequest request) {
    try {
      return databaseService.find(request);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors l'autocomplétion sur les bases de données", e);
    }
  }
}
