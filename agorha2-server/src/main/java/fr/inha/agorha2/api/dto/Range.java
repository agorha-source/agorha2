package fr.inha.agorha2.api.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;

@JsonSubTypes({ // Below, we define the names and the binding classes.
    @JsonSubTypes.Type(value = DateRange.class, name = "DateRange"),
    @JsonSubTypes.Type(value = LongRange.class, name = "LongRange") })
public interface Range<T extends Comparable<T>> {
  public T getMin();

  public void setMin(T min);

  public T getMax();

  public void setMax(T max);
}
