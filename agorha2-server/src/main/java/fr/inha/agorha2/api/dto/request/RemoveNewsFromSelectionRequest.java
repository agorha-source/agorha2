package fr.inha.agorha2.api.dto.request;

public class RemoveNewsFromSelectionRequest {
  String newsUrl;
  String selectionId;

  public String getNewsUrl() {
    return newsUrl;
  }

  public void setNewsUrl(String newsUrl) {
    this.newsUrl = newsUrl;
  }

  public String getSelectionId() {
    return selectionId;
  }

  public void setSelectionId(String selectionId) {
    this.selectionId = selectionId;
  }
}
