package fr.inha.agorha2.api;

import fr.inha.agorha2.api.dto.request.AddNewsToSelectionRequest;
import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import fr.inha.agorha2.api.dto.request.AggregationRequest;
import fr.inha.agorha2.api.dto.request.RemoveNewsFromSelectionRequest;
import fr.inha.agorha2.api.dto.request.UpdateNoticeRequest;
import fr.inha.agorha2.api.dto.response.MediaSearchResponse;
import fr.inha.agorha2.api.dto.sort.MediaSortType;
import fr.inha.elastic.model.Database;
import fr.inha.elastic.model.Notice;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

/**
 * In the implementation, if any JAXRS annotation is overriden (ex. @Path), they
 * need to be all copied see
 * https://stackoverflow.com/questions/25916796/inheritance-with-jax-rs
 *
 * @author broussot
 */
@Path("/contribution")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@OpenAPIDefinition(info = @Info(title = "Contribution API", version = "0.0.1-SNAPSHOT", license = @License(name = "Apache 2.0 License", url = "http://www.apache.org/licenses/LICENSE-2.0.html")))
@Tag(name = "Contribution")
public interface ContributionApi {

  @POST
  @Path("/notices")
  public Notice create(String noticeAsJson);

  @DELETE
  @Path("/notices/{id}")
  public void delete(String id);

  @PATCH
  @Path("/notices/status")
  public List<Map<String, String>> updateStatus(UpdateNoticeRequest request);

  @PUT
  @Path("/notices/{id}")
  public Notice update(String id, String noticeAsJson);

  @POST
  @Path("/notices/{noticeId}/duplicate")
  public Notice duplicate(String noticeId, String databaseId);

  @GET
  @Path("/notices/autocomplete")
  public List<Map<String, String>> autocompleteNoticeTitle(String terms, String noticeType);

  @POST
  @Path("/news/addToSelection")
  public void addToSelection(AddNewsToSelectionRequest request);

  @PUT
  @Path("/news/removeFromSelection")
  public void removeFromSelection(RemoveNewsFromSelectionRequest request);

  @POST
  @Path("/media")
  @Consumes({ MediaType.MULTIPART_FORM_DATA })
  public Map<String, Object> createMedia(MultipartBody body) throws IOException;

  @PUT
  @Path("/media/{id}")
  @Consumes({ MediaType.MULTIPART_FORM_DATA })
  public Map<String, String> updateMedia(String uuid, MultipartBody body) throws IOException;

  @GET
  @Path("/media/search")
  public MediaSearchResponse searchMedia(String terms, String sort, MediaSortType filedSort, Integer page,
      Integer pageSize, AggregationRequest aggregationRequest);

  @DELETE
  @Path("/media")
  public long deleteMedia(List<String> uuids);

  @GET
  @Path("/notice/exportJson")
  public Response exportJson(String terms, String sort, String fieldSort);

  @GET
  @Path("/notice/simpleExportCsv")
  public Response exportCsv(String terms, String sort, String fieldSort, String noticeType,
      AggregationRequest aggregationRequest);

  @GET
  @Path("/notice/advExportCsv")
  public Response advExportCsv(AdvancedSearchRequest request);

  @POST
  @Path("/databases")
  public Database createDatabase(Database database);

}
