package fr.inha.agorha2.api.dto.response;

import fr.inha.agorha2.api.dto.result.FacetResult;
import fr.inha.elastic.model.Database;
import java.util.ArrayList;
import java.util.List;

public class DatabaseListResponse {
  private List<Database> databases = new ArrayList<>();
  private List<FacetResult> facets = new ArrayList<>();

  public List<Database> getDatabases() {
    return databases;
  }

  public List<FacetResult> getFacets() {
    return facets;
  }

}
