package fr.inha.agorha2.api.impl;

import fr.inha.agorha2.api.MediaApi;
import fr.inha.agorha2.api.dto.ImageFormat;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.MediaService;
import fr.inha.elastic.model.Media;
import java.util.Map;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/media")
public class MediaApiImpl extends CxfJaxrsRequestInfoProvider implements MediaApi, CxfJaxrsDeployable {

  private static final String ERROR_MESSAGE_ISSUE_GET_MEDIA = "Erreur lors de la récupération d'un media";
  @Autowired
  private MediaService mediaService;

  @Override
  @GET
  @Path("/{id}")
  public Media get(@PathParam(value = "id") String idMedia) {
    try {
      return mediaService.get(idMedia);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(ERROR_MESSAGE_ISSUE_GET_MEDIA, e);
    }
  }

  @Override
  @GET
  @Path("/{id}/picture")
  public Map<String, String> get(@PathParam(value = "id") String idMedia, @QueryParam(value = "fileId") String fileId,
      @QueryParam(value = "format") String format) {
    if (fileId == null) {
      throw new BadRequestException("Le champ fileId doit être renseigné");
    }

    ImageFormat imageFormat = ImageFormat.L;
    if (format != null) {
      try {
        imageFormat = ImageFormat.valueOf(format);
      } catch (IllegalArgumentException e) {
        // Do nothing
      }
    }

    try {
      return mediaService.getFile(idMedia, fileId, imageFormat);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(ERROR_MESSAGE_ISSUE_GET_MEDIA, e);
    }
  }

  @Override
  @GET
  @Path("/{id}/media/{fileId}")
  public Map<String, String> get(@PathParam(value = "id") String idMedia, @PathParam(value = "fileId") String fileId) {
    try {
      return mediaService.getFile(idMedia, fileId, ImageFormat.L);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(ERROR_MESSAGE_ISSUE_GET_MEDIA, e);
    }
  }

}
