package fr.inha.agorha2.api.dto;

import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import fr.inha.agorha2.api.dto.request.AggregationRequest;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

@Provider
public class QueryParamConverterProvider implements ParamConverterProvider {

  @SuppressWarnings("unchecked")
  @Override
  public <T> ParamConverter<T> getConverter(Class<T> rawType, Type genericType, Annotation[] annotations) {
    if (rawType.equals(AdvancedSearchRequest.class)) {
      return (ParamConverter<T>) new AdvancedSearchRequestConverter();
    } else if (rawType.equals(AggregationRequest.class)) {
      return (ParamConverter<T>) new AggregationRequestConverter();
    }
    return null;
  }
}
