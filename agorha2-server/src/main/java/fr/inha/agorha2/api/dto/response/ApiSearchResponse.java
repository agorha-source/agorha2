package fr.inha.agorha2.api.dto.response;

import fr.inha.agorha2.api.dto.result.ApiSearchResult;
import fr.inha.agorha2.api.dto.result.FacetResult;
import java.util.ArrayList;
import java.util.List;

public class ApiSearchResponse<T extends ApiSearchResult> {

  private Integer offset;
  private Integer limit;
  private Integer count = 0;
  private List<T> results = new ArrayList<>();
  private List<FacetResult> facets = new ArrayList<>();

  public Integer getOffset() {
    return offset;
  }

  public void setOffset(Integer offset) {
    this.offset = offset;
  }

  public Integer getLimit() {
    return limit;
  }

  public void setLimit(Integer limit) {
    this.limit = limit;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public List<T> getResults() {
    return results;
  }

  public List<FacetResult> getFacets() {
    return facets;
  }

}
