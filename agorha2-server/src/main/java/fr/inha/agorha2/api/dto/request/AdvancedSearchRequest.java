package fr.inha.agorha2.api.dto.request;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import fr.inha.agorha2.api.dto.SearchFilter;
import fr.inha.agorha2.api.dto.SearchFilterDeserializer;
import java.util.ArrayList;
import java.util.List;

public class AdvancedSearchRequest extends NoticeSearchRequest {

  private String noticeType;

  @JsonDeserialize(contentUsing = SearchFilterDeserializer.class)
  private List<SearchFilter> filters = new ArrayList<>();

  public AdvancedSearchRequest() {
    super();
  }

  public String getNoticeType() {
    return noticeType;
  }

  public void setNoticeType(String noticeType) {
    this.noticeType = noticeType;
  }

  public List<SearchFilter> getFilters() {
    return filters;
  }

}
