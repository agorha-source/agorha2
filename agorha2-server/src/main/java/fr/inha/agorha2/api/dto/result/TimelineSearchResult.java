package fr.inha.agorha2.api.dto.result;

import java.util.HashMap;
import java.util.Map;

public class TimelineSearchResult extends ApiSearchResult {

  private Map<String, Object> data = new HashMap<>();

  public Map<String, Object> getData() {
    return data;
  }

}
