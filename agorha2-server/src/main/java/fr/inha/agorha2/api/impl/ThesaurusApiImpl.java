package fr.inha.agorha2.api.impl;

import fr.inha.agorha2.api.ThesaurusApi;
import fr.inha.agorha2.bean.ThesaurusConcept;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.service.ThesaurusService;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@SecurityScheme(type = SecuritySchemeType.HTTP, scheme = "basic", name = "basicAuth")
@Path("/thesaurus")
public class ThesaurusApiImpl extends CxfJaxrsRequestInfoProvider implements ThesaurusApi, CxfJaxrsDeployable {

  @Autowired
  private ThesaurusService thesaurusService;

  @Override
  @GET
  @Path("/autocomplete")
  public List<ThesaurusConcept> autocomplete(@QueryParam(value = "terms") String terms,
      @QueryParam(value = "thesaurusKey") String thesaurusKey, @QueryParam(value = "branchLabel") String branchLabel,
      @QueryParam(value = "limit") Integer limit) {
    return thesaurusService.autocomplete(terms, thesaurusKey, branchLabel, limit);
  }

  @GET
  @Path("/list")
  @Override
  public List<ThesaurusConcept> list(@QueryParam(value = "thesaurusKey") String thesaurusKey) {
    return thesaurusService.list(thesaurusKey);
  }

  @Override
  @GET
  @Path("/prefLabel")
  public String prefLabel(@QueryParam(value = "conceptId") String conceptId) {
    return thesaurusService.getPrefLabel(conceptId);
  }

}
