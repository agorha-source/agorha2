package fr.inha.agorha2.api.impl;

import fr.inha.agorha2.api.AdministrationApi;
import fr.inha.agorha2.api.dto.request.PaginatedSearchRequest;
import fr.inha.agorha2.api.dto.request.UserSearchRequest;
import fr.inha.agorha2.api.dto.response.UserSearchResponse;
import fr.inha.agorha2.api.dto.sort.UserSortType;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.MappingService;
import fr.inha.agorha2.service.SearchService;
import fr.inha.agorha2.service.UserService;
import fr.inha.elastic.model.Authorizations;
import fr.inha.elastic.model.NoticeType;
import fr.inha.elastic.model.User;

import java.util.Map;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
@Path("/administration")
public class AdministrationApiImpl extends CxfJaxrsRequestInfoProvider
    implements AdministrationApi, CxfJaxrsDeployable {

  @Autowired
  private UserService userService;

  @Autowired
  private SearchService searchService;

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private MappingService mappingService;

  @Override
  @PATCH
  @Path("/users/{id}/authorizations")
  public Map<String, Object> updateAuthorizations(@PathParam(value = "id") String id, Authorizations authorizations) {
    try {
      return userService.update(id, authorizations);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la modification des authorisations de l'utilisateur " + id,
          e);
    }
  }

  @Override
  @GET
  @Path("/users/{id}")
  public User get(@PathParam(value = "id") String id) {
    try {
      return userService.get(id);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération de l'utilisateur " + id, e);
    }
  }

  @Override
  @GET
  @Path("/users/search")
  public UserSearchResponse searchUser(@QueryParam(value = "page") Integer page,
      @QueryParam(value = "terms") String terms, @QueryParam(value = "pageSize") Integer pageSize,
      @QueryParam(value = "sort") String sort, @QueryParam(value = "fieldSort") UserSortType fieldSort) {
    if (page == null) {
      page = PaginatedSearchRequest.DEFAULT_PAGE;
    }
    if (pageSize == null) {
      pageSize = PaginatedSearchRequest.DEFAULT_PAGE_SIZE;
    }
    if (page < 1 || pageSize > 20) {
      throw new BadRequestException(messageSource.getMessage("search.paging.error", new Object[] {}, null));
    }
    try {
      return searchService.searchUser(new UserSearchRequest(page, pageSize, sort, terms, null, fieldSort));
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la recherche d'utilisateurs", e);
    }
  }

  @Override
  @DELETE
  @Path("/users/{id}")
  public void deleteUser(@PathParam(value = "id") String id) {
    try {
      userService.delete(id);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @Override
  @POST
  @Path("/metamodeles/{noticeType}/update")
  public void updateMetamodel(@PathParam(value = "noticeType") NoticeType noticeType) {
    try {
      mappingService.updateMetamodel(noticeType);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

}
