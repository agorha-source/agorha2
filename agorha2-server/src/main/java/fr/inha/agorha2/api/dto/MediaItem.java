package fr.inha.agorha2.api.dto;

import fr.inha.elastic.model.MediaType;
import java.io.InputStream;

public class MediaItem {

  private InputStream inputStream;
  private InputStream thumbnail;
  private MediaType mediaType;
  private String name;
  private Integer uniqueKey;
  private String uuid;

  public InputStream getInputStream() {
    return inputStream;
  }

  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  public InputStream getThumbnail() {
    return thumbnail;
  }

  public void setThumbnail(InputStream thumbnail) {
    this.thumbnail = thumbnail;
  }

  public MediaType getMediaType() {
    return mediaType;
  }

  public void setMediaType(MediaType mediaType) {
    this.mediaType = mediaType;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getUniqueKey() {
    return uniqueKey;
  }

  public void setUniqueKey(Integer uniqueKey) {
    this.uniqueKey = uniqueKey;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

}
