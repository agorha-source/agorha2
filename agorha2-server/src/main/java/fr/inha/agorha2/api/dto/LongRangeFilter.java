package fr.inha.agorha2.api.dto;

import java.util.ArrayList;
import java.util.List;

public class LongRangeFilter extends RangeFilter<Long, LongRange> {

  private List<LongRange> values = new ArrayList<>();

  public LongRangeFilter() {
  }

  public LongRangeFilter(Operator op) {
    this.op = op;
  }

  public List<LongRange> getValues() {
    return values;
  }
}
