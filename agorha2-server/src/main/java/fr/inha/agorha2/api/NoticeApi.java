package fr.inha.agorha2.api;

import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import fr.inha.agorha2.api.dto.request.AggregationRequest;
import fr.inha.agorha2.api.dto.response.GraphApiResponse;
import fr.inha.agorha2.api.dto.response.MosaicCardResponse;
import fr.inha.agorha2.api.dto.response.NoticeSearchResponse;
import fr.inha.agorha2.api.dto.response.TimelineSearchResponse;
import fr.inha.agorha2.api.dto.result.NoticeSearchResult;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.Notice;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * In the implementation, if any JAXRS annotation is overriden (ex. @Path), they
 * need to be all copied see
 * https://stackoverflow.com/questions/25916796/inheritance-with-jax-rs
 *
 * @author broussot
 */
@Path("/notice")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@OpenAPIDefinition(info = @Info(title = "Notice API", version = "0.0.1-SNAPSHOT", license = @License(name = "Apache 2.0 License", url = "http://www.apache.org/licenses/LICENSE-2.0.html")))
@Tag(name = "Notice")
public interface NoticeApi {

  @GET
  @Path("/{id}")
  public Object get(String noticeId, String accept);

  @GET
  @Path("/consultation/{id}")
  public Notice getForConsultation(String noticeId);

  @POST
  @Path("/getMosaicCards")
  public List<MosaicCardResponse> getMosaicCards(List<String> ids);

  @GET
  public Notice getByDcIdentifier(String dcIdentifier);

  @GET
  @Path("/search")
  public NoticeSearchResponse search(Integer page, String terms, Integer pageSize, String sort, String fieldSort,
      String noticeType, String selectionId, Boolean withAttachment, AggregationRequest aggregations,
      List<String> fields);

  @GET
  @Path("/advSearch")
  NoticeSearchResponse search(AdvancedSearchRequest request);

  @GET
  @Path("/timeline")
  public TimelineSearchResponse timeline(Integer page, String terms, Integer pageSize, String noticeType,
      String selectionId, AggregationRequest aggregations, List<String> fields);

  @GET
  @Path("/advTimeline")
  public TimelineSearchResponse advTimeline(AdvancedSearchRequest request);

  @GET
  @Path("/graph")
  public GraphApiResponse graph(String selectionId, String noticeId);

  @GET
  @Path("/autocomplete")
  public List<NoticeSearchResult> autocomplete(String terms, String noticeType, String lang, Integer noticePageSize);

  @GET
  @Path("/{id}/media")
  public List<Media> getMedia(String noticeId, String dataMode);

  @GET
  @Path("/referenceby/{id}")
  public List<String> getReferenceBy(String id);

  @GET
  @Path("/advAutocomplete")
  public Set<String> advAutocomplete(String field, String query, String noticeType);

  @POST
  @Path("/correctionProposal")
  public Response correctionProposal(String noticeId, String text);

  @GET
  @Path("/exportCsv")
  public Response exportCsv(List<String> ids);

  @GET
  @Path("/selections/export/{id}")
  public Response exportSelection(String selectionId);

  @GET
  @Path("/exportOAI")
  public void exportOAI();

  @GET
  @Path("/exportjson")
  public Response exportJson(String noticeType, String databaseId, String token);

  @GET
  @Path("/geoPointField")
  public Map<String, List<String>> getGeoPointFields();

  @GET
  @Path("/redirect")
  public Response redirect(String id, String type);

}
