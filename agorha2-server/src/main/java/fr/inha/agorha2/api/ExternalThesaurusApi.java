package fr.inha.agorha2.api;

import fr.inha.agorha2.bean.ExternalThesaurusConcept;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.io.IOException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

@Path("/externalThesaurus")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@OpenAPIDefinition(info = @Info(title = "External Thesaurus API", version = "0.0.1-SNAPSHOT", license = @License(name = "Apache 2.0 License", url = "http://www.apache.org/licenses/LICENSE-2.0.html")))
@SecurityRequirement(name = "basicAuth")
@Tag(name = "ExternalThesaurus")
public interface ExternalThesaurusApi {

  @GET
  @Path("/autocomplete")
  List<ExternalThesaurusConcept> autocomplete(String terms, Integer limit)
      throws IOException, SAXException, ParserConfigurationException;

  @GET
  @Path("/prefLabel")
  public String prefLabel(String conceptId);
}
