package fr.inha.agorha2.api.dto.request;

import fr.inha.agorha2.api.dto.Aggregation;
import fr.inha.agorha2.api.dto.sort.MediaSortType;
import java.util.Map;

public class MediaSearchRequest extends PaginatedSearchRequest {
  private MediaSortType sortType;

  public MediaSearchRequest() {
    super();
  }

  public MediaSearchRequest(String terms, String sort, MediaSortType sortType, Map<String, Aggregation> aggregations) {
    super(terms, sort, aggregations);
    this.sortType = sortType;
  }

  public MediaSearchRequest(int page, int pageSize, String sort, String terms, MediaSortType sortType,
      Map<String, Aggregation> aggregations) {
    super(page, pageSize, terms, sort, aggregations);
    this.sortType = sortType;
  }

  public MediaSortType getSortType() {
    return sortType;
  }

  public void setSortType(MediaSortType sortType) {
    this.sortType = sortType;
  }

}
