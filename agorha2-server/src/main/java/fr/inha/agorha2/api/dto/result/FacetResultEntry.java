package fr.inha.agorha2.api.dto.result;

import java.util.ArrayList;
import java.util.List;

public class FacetResultEntry {

  private String value;
  private Long count;
  private List<FacetResultEntry> subEntries = new ArrayList<>();

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Long getCount() {
    return count;
  }

  public void setCount(Long count) {
    this.count = count;
  }

  public List<FacetResultEntry> getSubEntries() {
    return subEntries;
  }

}
