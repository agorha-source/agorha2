package fr.inha.agorha2.api.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import java.util.List;

@JsonSubTypes({ // Below, we define the names and the binding classes.
    @JsonSubTypes.Type(value = LongRangeFilter.class, name = "LongRangeFilter"),
    @JsonSubTypes.Type(value = DateRangeFilter.class, name = "DateRangeFilter") })
public abstract class RangeFilter<T extends Comparable<T>, Y extends Range<T>> extends SearchFilter {
  public abstract List<Y> getValues();
}
