package fr.inha.agorha2.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Aggregation {

  private String field;

  private List<String> filters = new ArrayList<>();
  private AggregationTermsOrder order;
  private Integer size = 100;
  private Integer minDocCount = 1;
  private boolean pathHierarchical;
  private String facetScope;
  private Integer facetOrder = Integer.MAX_VALUE;

  public boolean isPathHierarchical() {
    return pathHierarchical;
  }

  public void setPathHierarchical(boolean pathHierarchical) {
    this.pathHierarchical = pathHierarchical;
  }

  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

  public List<String> getFilters() {
    return filters;
  }

  public AggregationTermsOrder getOrder() {
    return order;
  }

  public void setOrder(AggregationTermsOrder order) {
    this.order = order;
  }

  public Integer getSize() {
    return size;
  }

  public void setSize(Integer size) {
    this.size = size;
  }

  public Integer getMinDocCount() {
    return minDocCount;
  }

  public void setMinDocCount(Integer minDocCount) {
    this.minDocCount = minDocCount;
  }

  public String getFacetScope() {
    return facetScope;
  }

  public void setFacetScope(String facetScope) {
    this.facetScope = facetScope;
  }

  public Integer getFacetOrder() {
    return facetOrder;
  }

  public void setFacetOrder(Integer facetOrder) {
    this.facetOrder = facetOrder;
  }

}
