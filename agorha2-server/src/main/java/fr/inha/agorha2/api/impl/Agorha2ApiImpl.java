package fr.inha.agorha2.api.impl;

import fr.inha.agorha2.api.Agorha2Api;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.service.ESHealthService;
import fr.inha.agorha2.service.MappingService;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import java.io.IOException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@SecurityScheme(type = SecuritySchemeType.HTTP, scheme = "basic", name = "basicAuth")
@Path("/")
public class Agorha2ApiImpl extends CxfJaxrsRequestInfoProvider implements Agorha2Api, CxfJaxrsDeployable {

  private static final Logger log = LoggerFactory.getLogger(Agorha2ApiImpl.class);

  @Autowired
  ESHealthService healthService;

  @Autowired
  MappingService mappingService;

  @Override
  @GET
  @Path("/health")
  public ClusterHealthResponse clusterHealth() {
    try {
      return healthService.clusterHealth();
    } catch (IOException e) {
      log.error("", e);
    }
    return null;
  }

  @Override
  @POST
  @Path("/mapping")
  public String generateMapping(String jsonSchema) {
    return mappingService.generateFromJson(jsonSchema);
  }
}
