package fr.inha.agorha2.api.dto;

public enum MediaRetrieveMode {
  FULL,
  THUMBNAILS;
}
