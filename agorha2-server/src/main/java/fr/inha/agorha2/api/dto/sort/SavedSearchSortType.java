package fr.inha.agorha2.api.dto.sort;

public enum SavedSearchSortType {
  CREATION_DATE("createdDate"),
  NAME("name");

  private String field;

  private SavedSearchSortType(String field) {
    this.field = field;
  }

  public String getField() {
    return field;
  }

}
