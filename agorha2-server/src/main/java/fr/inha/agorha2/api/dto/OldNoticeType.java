package fr.inha.agorha2.api.dto;

import fr.inha.elastic.model.NoticeType;

public enum OldNoticeType {
  DATABASE("001", null),
  PERSON("002", NoticeType.PERSON),
  BIEN("003", NoticeType.ARTWORK),
  EVENT("004", NoticeType.EVENT),
  BIBLIO("006", NoticeType.REFERENCE),
  REFARCHIVE("008", NoticeType.REFERENCE),
  COLLECTION("010", NoticeType.COLLECTION),
  EDIFICE("011", NoticeType.ARTWORK),
  MEDIA("012", null);

  private String code;
  private NoticeType type;

  private OldNoticeType(String code, NoticeType type) {
    this.code = code;
    this.type = type;
  }

  public static OldNoticeType getFromCode(String code) {
    for (OldNoticeType type : OldNoticeType.values()) {
      if (type.getCode().equals(code)) {
        return type;
      }
    }
    return null;
  }

  public String getCode() {
    return this.code;
  }

  public NoticeType getType() {
    return type;
  }

}
