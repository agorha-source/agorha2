package fr.inha.agorha2.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AggregationTermsOrder {

  public static final AggregationTermsOrder DEFAULT = new AggregationTermsOrder(AggregationSortType.COUNT,
      SortDirection.DESC);

  private AggregationSortType type;
  private SortDirection direction;

  public AggregationTermsOrder() {
    super();
  }

  private AggregationTermsOrder(AggregationSortType type, SortDirection direction) {
    this.type = type;
    this.direction = direction;
  }

  public AggregationSortType getType() {
    return type;
  }

  public void setType(AggregationSortType type) {
    this.type = type;
  }

  public SortDirection getDirection() {
    return direction;
  }

  public void setDirection(SortDirection direction) {
    this.direction = direction;
  }

  public enum AggregationSortType {
    COUNT,
    KEY
  }

  public enum SortDirection {
    ASC(true, "asc"),
    DESC(false, "desc");

    private boolean booleanValue;
    private String value;

    SortDirection(boolean booleanValue, String value) {
      this.booleanValue = booleanValue;
      this.value = value;
    }

    public boolean toBool() {
      return booleanValue;
    }

    public String getValue() {
      return value;
    }
  }
}
