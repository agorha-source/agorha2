package fr.inha.agorha2.api.dto.response;

import fr.inha.agorha2.api.dto.GraphLink;
import fr.inha.agorha2.api.dto.GraphNode;
import java.util.ArrayList;
import java.util.List;

public class GraphApiResponse {

  List<GraphNode> nodes = new ArrayList<>();
  List<GraphLink> links = new ArrayList<>();

  public List<GraphNode> getNodes() {
    return nodes;
  }

  public List<GraphLink> getLinks() {
    return links;
  }

}
