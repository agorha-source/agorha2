package fr.inha.agorha2.api.dto.sort;

public enum NoticeSortType {
  UPDATED_DATE("internal.digest.updatedDate"),
  TITLE("internal.digest.title");

  private String field;

  private NoticeSortType(String field) {
    this.field = field;
  }

  public String getField() {
    return field;
  }

}
