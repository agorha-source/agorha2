package fr.inha.agorha2.api.dto;

import java.util.ArrayList;
import java.util.List;

public class MultiValueSearchFilter extends SearchFilter {

  private List<String> values = new ArrayList<>();

  public MultiValueSearchFilter() {
  }

  public MultiValueSearchFilter(Operator op) {
    this.op = op;
  }

  public List<String> getValues() {
    return values;
  }

  public MultiValueSearchFilter setValues(List<String> values) {
    this.values = values;
    return this;
  }

  public MultiValueSearchFilter addValue(String v) {
    if (v != null) {
      this.values.add(v);
    }
    return this;
  }

}
