package fr.inha.agorha2.api.batch;

import fr.inha.agorha2.batch.UpdateNoticesBatch;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.ImportService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import java.io.IOException;
import java.util.Set;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/batch")
@SecurityRequirement(name = "basicAuth")
public class BatchApi extends CxfJaxrsRequestInfoProvider implements CxfJaxrsDeployable {

  @Autowired
  private UpdateNoticesBatch batch;

  @Autowired
  private ImportService importService;

  @GET
  @Path("/updateRefNotices")
  public void updateRefNotices() {

    Set<String> prefixes;
    try {
      prefixes = batch.readAndOrSplit();
      for (String prefix : prefixes) {
        if (!batch.isPrefixInProgress(prefix)) {
          batch.process(prefix);
        }
      }
    } catch (ServiceException | IOException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @GET
  @Path("/importMedia")
  public void importMedia() {
    try {
      importService.importMedia();
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @GET
  @Path("/importCsv")
  public void importCsv() {
    try {
      importService.importCsv();
    } catch (Exception e) {
      throw new InternalServerErrorException(e);
    }
  }

}
