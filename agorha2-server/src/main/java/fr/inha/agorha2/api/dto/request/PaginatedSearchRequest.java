package fr.inha.agorha2.api.dto.request;

import java.util.Map;

import fr.inha.agorha2.api.dto.Aggregation;

public class PaginatedSearchRequest extends ApiSearchRequest {

  // La taille maximum d'une page correspond au nombre maximum de résultats dans ES
  public static final int MAXIMUM_PAGE_SIZE = 10000;
  // La limite maximal qu'il est possible de demander via un paramètre d'URL
  public static final int LIMIT_PAGE_SIZE = 1000;
  public static final int DEFAULT_PAGE_SIZE = 20;
  public static final int DEFAULT_PAGE = 1;

  private Integer page = DEFAULT_PAGE;
  private Integer pageSize = DEFAULT_PAGE_SIZE;

  public PaginatedSearchRequest() {
    super();
  }

  public PaginatedSearchRequest(int page, int pageSize, String sort, String request,
      Map<String, Aggregation> aggregations) {
    super(request, sort, aggregations);
    this.page = page;
    this.pageSize = pageSize;
  }

  public PaginatedSearchRequest(String request, String sort, Map<String, Aggregation> aggregations) {
    super(request, sort, aggregations);
  }

  public Integer getPage() {
    return page;
  }

  public void setPage(Integer page) {
    this.page = page;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    this.pageSize = pageSize;
  }

}
