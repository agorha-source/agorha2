package fr.inha.agorha2.api.impl;

import fr.inha.agorha2.api.UserApi;
import fr.inha.agorha2.api.dto.AuthenticatedUser;
import fr.inha.agorha2.api.dto.sort.SavedSearchSortType;
import fr.inha.agorha2.api.dto.sort.SelectionSortType;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.security.UserPrincipal;
import fr.inha.agorha2.service.SavedSearchService;
import fr.inha.agorha2.service.SelectionService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.agorha2.service.UserService;
import fr.inha.agorha2.utils.UserUtils;
import fr.inha.elastic.model.SavedSearch;
import fr.inha.elastic.model.Selection;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import java.util.List;
import java.util.Map;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
@Path("/users")
public class UserApiImpl extends CxfJaxrsRequestInfoProvider implements UserApi, CxfJaxrsDeployable {

  private static final String MAIL_PATTERN = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";

  @Autowired
  private UserService userService;

  @Autowired
  private UserInfoService userInfoService;

  @Autowired
  private SelectionService selectionService;

  @Autowired
  private SavedSearchService savedSearchService;

  @Autowired
  private MessageSource messageSource;

  @Override
  @GET
  @Path("/authenticate")
  public AuthenticatedUser login(@QueryParam(value = "login") String login,
      @QueryParam(value = "password") @Schema(type = "string", format = "password") String password) {
    return userService.login(login, password);
  }

  @Override
  @POST
  @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
  public Map<String, String> create(@FormParam(value = "name") String name,
      @FormParam(value = "firstName") String firstName, @FormParam(value = "mail") String mail,
      @FormParam(value = "password") String password) {
    if (StringUtils.isEmpty(name)) {
      throw new BadRequestException("Le nom ne peut pas être vide");
    }
    if (StringUtils.isEmpty(firstName)) {
      throw new BadRequestException("Le prénom ne peut pas être vide");
    }
    if (StringUtils.isEmpty(password)) {
      throw new BadRequestException("Le mot de passe ne peut pas être vide");
    }
    if (mail == null || !mail.matches(MAIL_PATTERN)) {
      throw new BadRequestException(messageSource.getMessage("mail.format.invalid", new Object[] {}, null));
    }
    try {
      return userService.create(name, firstName, mail, password);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la création de l'utilisateur " + mail, e);
    }
  }

  @Override
  @PUT
  @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
  public Map<String, String> update(@FormParam(value = "name") String name,
      @FormParam(value = "firstName") String firstName, @FormParam(value = "mail") String mail,
      @FormParam(value = "oldPassword") String oldPassword, @FormParam(value = "newPassword") String newPassword) {
    if (StringUtils.isNotEmpty(mail) && !mail.matches(MAIL_PATTERN)) {
      throw new BadRequestException(messageSource.getMessage("mail.format.invalid", new Object[] {}, null));
    }
    try {
      String uuid = userInfoService.getPrincipal().getUser().getInternal().getUuid();
      return userService.update(uuid, name, firstName, mail, oldPassword, newPassword);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la création de l'utilisateur " + mail, e);
    }
  }

  @Override
  @DELETE
  public void delete() {
    try {
      String uuid = userInfoService.getPrincipal().getUser().getInternal().getUuid();
      userService.delete(uuid);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @Override
  @PUT
  @Path("/validate/{id}")
  public void validate(@PathParam(value = "id") String uuid, @QueryParam(value = "token") String token) {
    try {
      userService.validate(uuid, token);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la validation de l'utilisateur " + uuid, e);
    }
  }

  @Override
  @GET
  @Path("/resetPassword")
  public void resetPassword(@QueryParam(value = "mail") String mail) {
    if (mail != null && !mail.matches(MAIL_PATTERN)) {
      throw new BadRequestException(messageSource.getMessage("mail.format.invalid", new Object[] {}, null));
    }
    try {
      userService.resetPassword(mail);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }

  }

  @Override
  @GET
  @Path("/resetPassword/{id}/isValid")
  public boolean resetPasswordIsValid(@PathParam(value = "id") String uuid, @QueryParam(value = "token") String token) {
    return userService.resetPasswordIsValid(uuid, token);
  }

  @Override
  @PUT
  @Path("/updatePassword/{id}")
  public void updatePassword(@PathParam(value = "id") String uuid,
      @QueryParam(value = "newPassword") String newPassword, @QueryParam(value = "token") String token) {
    try {
      userService.updatePassword(uuid, newPassword, token);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }

  }

  @GET
  @Path("/isAuthenticated")
  @Override
  public boolean isAuthenticated() {
    return userInfoService.isAuthenticated();
  }

  @GET
  @Path("/current")
  @Override
  @SecurityRequirement(name = "basicAuth")
  public AuthenticatedUser getCurrent() {
    UserPrincipal principal = userInfoService.getPrincipal();
    if (principal == null) {
      throw new NotFoundException("Utilisateur non authentifié");
    }
    return UserUtils.getAuthenticatedUser(principal);
  }

  @POST
  @Path("/selections")
  @SecurityRequirement(name = "basicAuth")
  @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
  @Override
  public Selection createSelection(@FormParam(value = "title") String title,
      @FormParam(value = "noticeId") List<String> noticeIds) {
    try {
      return selectionService.create(title, noticeIds);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @PUT
  @Path("/selections/{id}/add")
  @SecurityRequirement(name = "basicAuth")
  @Override
  public Selection addToSelection(@PathParam(value = "id") String id, List<String> noticeIds) {
    try {
      return selectionService.add(id, noticeIds);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @PUT
  @Path("/selections/{id}/remove")
  @SecurityRequirement(name = "basicAuth")
  @Override
  public Selection removeFromSelection(@PathParam(value = "id") String id, List<String> noticeIds) {
    try {
      return selectionService.remove(id, noticeIds);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @DELETE
  @Path("/selections/{id}")
  @SecurityRequirement(name = "basicAuth")
  @Override
  public void deleteSelection(@PathParam(value = "id") String id) {
    try {
      selectionService.delete(id);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @GET
  @Path("/selections")
  @SecurityRequirement(name = "basicAuth")
  @Override
  public List<Selection> getSelections(@QueryParam(value = "terms") String terms,
      @QueryParam(value = "page") Integer page, @QueryParam(value = "pageSize") Integer pageSize,
      @QueryParam(value = "sort") String sort, @QueryParam(value = "sortType") String sortType) {

    SelectionSortType sortTypeEnum = null;
    if (StringUtils.isNoneEmpty(sortType)) {
      try {
        sortTypeEnum = SelectionSortType.valueOf(sortType);
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(messageSource.getMessage("search.sort.unknown", new Object[] { sortType }, null));
      }
    } else {
      sortTypeEnum = SelectionSortType.CREATION_DATE;
    }

    if (StringUtils.isEmpty(sort)) {
      sort = "desc";
    }

    if (page == null) {
      page = 1;
    }
    if (pageSize == null) {
      pageSize = 10000;
    }

    if (page < 1 || pageSize > 10000) {
      throw new BadRequestException(messageSource.getMessage("search.paging.error", new Object[] {}, null));
    }

    try {
      return selectionService.list(terms, page, pageSize, sort, sortTypeEnum);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @POST
  @Path("/savedSearches")
  @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
  @Override
  public SavedSearch createSavedSearch(@FormParam(value = "name") String name,
      @FormParam(value = "resume") List<String> resume, @FormParam(value = "url") String url) {
    try {
      return savedSearchService.create(name, resume, url);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @DELETE
  @Path("/savedSearches/{id}")
  @Override
  public void deleteSavedSearch(@PathParam(value = "id") String id) {
    try {
      savedSearchService.delete(id);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }
  }

  @GET
  @Path("/savedSearches")
  @Override
  public List<SavedSearch> getSavedSearches(@QueryParam(value = "terms") String terms,
      @QueryParam(value = "page") Integer page, @QueryParam(value = "pageSize") Integer pageSize,
      @QueryParam(value = "sort") SortOrder sort, @QueryParam(value = "sortType") SavedSearchSortType sortType) {
    try {
      return savedSearchService.list(terms, page, pageSize, sort, sortType);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(e);
    }

  }

}
