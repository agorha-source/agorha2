package fr.inha.agorha2.api.impl;

import fr.inha.agorha2.api.ExternalThesaurusApi;
import fr.inha.agorha2.bean.ExternalThesaurusConcept;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.service.ExternalThesaurusService;
import fr.inha.agorha2.service.impl.EsPrefLabelService;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import java.io.IOException;
import java.util.List;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

@Component
@SecurityScheme(type = SecuritySchemeType.HTTP, scheme = "basic", name = "basicAuth")
@Path("/externalThesaurus")
public class ExternalThesaurusApiImpl extends CxfJaxrsRequestInfoProvider
    implements ExternalThesaurusApi, CxfJaxrsDeployable {

  @Autowired
  private ExternalThesaurusService externalThesaurusService;

  @Autowired
  private EsPrefLabelService esPrefLabelService;

  @Autowired
  private MessageSource messageSource;

  @Override
  @GET
  @Path("/autocomplete")
  public List<ExternalThesaurusConcept> autocomplete(@QueryParam(value = "terms") String terms,
      @QueryParam(value = "limit") Integer limit) throws IOException, SAXException, ParserConfigurationException {
    if (StringUtils.isEmpty(terms)) {
      throw new BadRequestException(messageSource.getMessage("notice.thesaurus.terms.required", new Object[] {}, null));
    }
    return externalThesaurusService.autocomplete(terms, limit);
  }

  @Override
  @GET
  @Path("/prefLabel")
  public String prefLabel(@QueryParam(value = "conceptId") String conceptId) {
    return esPrefLabelService.getPrefLabel(conceptId);
  }

}
