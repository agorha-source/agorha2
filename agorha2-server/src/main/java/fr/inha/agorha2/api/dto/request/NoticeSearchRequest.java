package fr.inha.agorha2.api.dto.request;

import fr.inha.agorha2.api.dto.Aggregation;
import fr.inha.agorha2.api.dto.sort.NoticeSortType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NoticeSearchRequest extends PaginatedSearchRequest {
  private String selectionId;
  private NoticeSortType sortType;
  private List<String> fields = new ArrayList<>();
  private ContentTarget contentTarget;

  public NoticeSearchRequest() {
    super();
  }

  public NoticeSearchRequest(String terms, String sort, NoticeSortType sortType,
      Map<String, Aggregation> aggregations) {
    super(terms, sort, aggregations);
    this.sortType = sortType;
  }

  public NoticeSearchRequest(int page, int pageSize, String sort, String request, Map<String, Aggregation> aggregations,
      NoticeSortType sortType) {
    super(page, pageSize, sort, request, aggregations);
    this.sortType = sortType;
  }

  public NoticeSearchRequest(int page, int pageSize, String sort, String request, Map<String, Aggregation> aggregations,
      String selectionId, NoticeSortType sortType) {
    super(page, pageSize, sort, request, aggregations);
    this.selectionId = selectionId;
    this.sortType = sortType;
  }

  public String getSelectionId() {
    return selectionId;
  }

  public void setSelectionId(String selectionId) {
    this.selectionId = selectionId;
  }

  public NoticeSortType getSortType() {
    return sortType;
  }

  public void setSortType(NoticeSortType sortType) {
    this.sortType = sortType;
  }

  public List<String> getFields() {
    return fields;
  }

  public ContentTarget getContentTarget() {
    return contentTarget;
  }

  public void setContentTarget(ContentTarget contentTarget) {
    this.contentTarget = contentTarget;
  }

  public enum ContentTarget {
    NOTICE,
    ATTACHMENT,
    ALL;
  }
}
