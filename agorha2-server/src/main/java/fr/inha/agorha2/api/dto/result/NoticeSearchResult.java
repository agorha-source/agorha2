package fr.inha.agorha2.api.dto.result;

import fr.inha.elastic.model.Media;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NoticeSearchResult extends ApiSearchResult {

  private Map<String, Object> digest = new HashMap<>();
  private Map<String, Object> content = new HashMap<>();
  private List<Media> medias = new ArrayList<>();

  public Map<String, Object> getDigest() {
    return digest;
  }

  public Map<String, Object> getContent() {
    return content;
  }

  public List<Media> getMedias() {
    return medias;
  }

}
