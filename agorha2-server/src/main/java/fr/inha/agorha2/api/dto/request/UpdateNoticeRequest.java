package fr.inha.agorha2.api.dto.request;

import java.util.ArrayList;
import java.util.List;

public class UpdateNoticeRequest {

  private List<String> ids = new ArrayList<>();
  private String status;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public List<String> getIds() {
    return ids;
  }

}
