package fr.inha.agorha2.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;

public class DateRange implements Range<Date> {

  @JsonFormat(pattern = "yyyy-MM-dd GG", locale = "en")
  private Date min;
  @JsonFormat(pattern = "yyyy-MM-dd GG", locale = "en")
  private Date max;

  public DateRange() {
    super();
  }

  public Date getMin() {
    return min;
  }

  public void setMin(Date min) {
    this.min = min;
  }

  public Date getMax() {
    return max;
  }

  public void setMax(Date max) {
    this.max = max;
  }

}
