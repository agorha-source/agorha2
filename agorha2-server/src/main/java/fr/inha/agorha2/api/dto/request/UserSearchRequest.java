package fr.inha.agorha2.api.dto.request;

import fr.inha.agorha2.api.dto.Aggregation;
import fr.inha.agorha2.api.dto.sort.UserSortType;
import java.util.Map;

public class UserSearchRequest extends PaginatedSearchRequest {
  private UserSortType sortType;

  public UserSearchRequest() {
    super();
  }

  public UserSearchRequest(int page, int pageSize, String sort, String request, Map<String, Aggregation> aggregations,
      UserSortType sortType) {
    super(page, pageSize, sort, request, aggregations);
    this.sortType = sortType;
  }

  public UserSearchRequest(String request, String sort, Map<String, Aggregation> aggregations, UserSortType sortType) {
    super(request, sort, aggregations);
    this.sortType = sortType;
  }

  public UserSortType getSortType() {
    return sortType;
  }

  public void setSortType(UserSortType sortType) {
    this.sortType = sortType;
  }

}
