package fr.inha.agorha2.api;

import fr.inha.agorha2.bean.ThesaurusConcept;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/thesaurus")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@OpenAPIDefinition(info = @Info(title = "Thesaurus API", version = "0.0.1-SNAPSHOT", license = @License(name = "Apache 2.0 License", url = "http://www.apache.org/licenses/LICENSE-2.0.html")))
@SecurityRequirement(name = "basicAuth")
@Tag(name = "Thesaurus")
public interface ThesaurusApi {

  @GET
  @Path("/autocomplete")
  public List<ThesaurusConcept> autocomplete(String terms, String thesaurusKey, String branchLabel, Integer limit);

  @GET
  @Path("/list")
  public List<ThesaurusConcept> list(String thesaurusKey);

  @GET
  @Path("/prefLabel")
  public String prefLabel(String conceptId);
}
