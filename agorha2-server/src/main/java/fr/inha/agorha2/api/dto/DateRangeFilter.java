package fr.inha.agorha2.api.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateRangeFilter extends RangeFilter<Date, DateRange> {

  private List<DateRange> values = new ArrayList<>();

  public DateRangeFilter() {
  }

  public DateRangeFilter(Operator op) {
    this.op = op;
  }

  public List<DateRange> getValues() {
    return values;
  }

}
