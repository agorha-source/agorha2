package fr.inha.agorha2.api.dto;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import java.io.IOException;
import java.util.Base64;
import javax.ws.rs.ext.ParamConverter;

public class AdvancedSearchRequestConverter implements ParamConverter<AdvancedSearchRequest> {

  @Override
  public AdvancedSearchRequest fromString(String value) {
    Base64.Decoder decoder = Base64.getDecoder();

    try {
      ObjectMapper objectMapper = new ObjectMapper();
      byte[] decodedValue = decoder.decode(value);

      return objectMapper.readValue(decodedValue, AdvancedSearchRequest.class);
    } catch (IllegalArgumentException | IOException e) {
      throw new RuntimeException("Erreur lors de la déserialisation de la requête", e);
    }
  }

  @Override
  public String toString(AdvancedSearchRequest value) {
    throw new UnsupportedOperationException();
  }

}
