package fr.inha.agorha2.api.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import fr.inha.agorha2.api.NoticeApi;
import fr.inha.agorha2.api.dto.Aggregation;
import fr.inha.agorha2.api.dto.MediaRetrieveMode;
import fr.inha.agorha2.api.dto.OldNoticeType;
import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import fr.inha.agorha2.api.dto.request.AggregationRequest;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest.ContentTarget;
import fr.inha.agorha2.api.dto.request.PaginatedSearchRequest;
import fr.inha.agorha2.api.dto.response.GraphApiResponse;
import fr.inha.agorha2.api.dto.response.MosaicCardResponse;
import fr.inha.agorha2.api.dto.response.NoticeSearchResponse;
import fr.inha.agorha2.api.dto.response.TimelineSearchResponse;
import fr.inha.agorha2.api.dto.result.NoticeSearchResult;
import fr.inha.agorha2.api.dto.sort.NoticeSortType;
import fr.inha.agorha2.cxf.CxfJaxrsDeployable;
import fr.inha.agorha2.cxf.CxfJaxrsRequestInfoProvider;
import fr.inha.agorha2.exception.FunctionnalException;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.service.ExportService;
import fr.inha.agorha2.service.IdentifierServcice;
import fr.inha.agorha2.service.MailService;
import fr.inha.agorha2.service.NoticeGetMode;
import fr.inha.agorha2.service.NoticeService;
import fr.inha.agorha2.service.SearchService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.Notice;
import fr.inha.elastic.model.NoticeStatus;
import fr.inha.elastic.model.NoticeType;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@Component
@Path("/notice")
public class NoticeApiImpl extends CxfJaxrsRequestInfoProvider implements NoticeApi, CxfJaxrsDeployable {

  private static final String ERROR_MESSAGE_SEARCH_ISSUE = "Erreur lors de la recherche de notice";
  private static final String ERROR_MESSAGE_UNKNOWN_NOTICE_TYPE = "Type de notice inconnu";

  private static final Logger log = LoggerFactory.getLogger(NoticeApiImpl.class);

  @Autowired
  private NoticeService noticeService;

  @Autowired
  private ExportService exportService;

  @Autowired
  private SearchService searchService;

  @Autowired
  private IdentifierServcice identifierServcice;

  @Autowired
  private MailService mailService;

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private UserInfoService userInfoService;

  @Override
  @GET
  @Path("/{id}")
  @Produces(value = { "text/plain", "text/html", "application/ld+json", "application/json", "application/rdf+xml",
      "text/rdf+n3", "application/n-triples" })
  @SecurityRequirement(name = "basicAuth")
  public Response get(@PathParam(value = "id") String noticeId, @HeaderParam("Accept") String accept) {
    try {
      ResponseBuilder response = Response.ok();
      if ("application/ld+json".equals(accept)) {
        response.entity(noticeService.getJsonLd(noticeId)).type(MediaType.APPLICATION_JSON);
      } else if ("application/rdf+xml".equals(accept)) {
        response.entity(noticeService.getRdf(noticeId, "RDF/XML")).type(MediaType.TEXT_PLAIN);
      } else if ("text/rdf+n3".equals(accept)) {
        response.entity(noticeService.getRdf(noticeId, "N3")).type(MediaType.TEXT_PLAIN);
      } else if ("application/n-triples".equals(accept)) {
        response.entity(noticeService.getRdf(noticeId, "N-Triples")).type(MediaType.TEXT_PLAIN);
      } else {
        Notice notice = noticeService.get(noticeId);
        String status = notice.getInternal().getStatus();
        if (!status.equals(NoticeStatus.PUBLISHED.getValue()) && (!userInfoService.isAuthenticated()
            || userInfoService.hasAnyRole(Arrays.asList(Authority.DEFAULT.name())))) {
          throw new NotAuthorizedException(messageSource.getMessage("user.insufficient.rights", new Object[] {}, null));
        }
        response.entity(notice).type(MediaType.APPLICATION_JSON);
      }
      return response.build();

    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération d'une notice : " + noticeId, e);
    }
  }

  @Override
  @GET
  @Path("/consultation/{id}")
  @SecurityRequirement(name = "basicAuth")
  public Notice getForConsultation(@PathParam(value = "id") String noticeId) {
    try {
      Notice notice = noticeService.get(noticeId, NoticeGetMode.CONSULTATION);
      String status = notice.getInternal().getStatus();
      if (!status.equals(NoticeStatus.PUBLISHED.getValue()) && (!userInfoService.isAuthenticated()
          || userInfoService.hasAnyRole(Arrays.asList(Authority.DEFAULT.name())))) {
        throw new NotAuthorizedException(messageSource.getMessage("user.insufficient.rights", new Object[] {}, null));
      }
      return notice;
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération d'une notice : " + noticeId, e);
    }
  }

  @Override
  @POST
  @Path("/getMosaicCards")
  @SecurityRequirement(name = "basicAuth")
  public List<MosaicCardResponse> getMosaicCards(List<String> ids) {
    try {
      return noticeService.getMosaicCards(ids, NoticeGetMode.MOSAIC_CARDS);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération d'une notice : " + ids, e);
    }
  }

  @Override
  @GET
  @Path("/")
  @SecurityRequirement(name = "basicAuth")
  public Notice getByDcIdentifier(@QueryParam(value = "dcIdentifier") String dcIdentifier) {
    try {
      Notice notice = noticeService.get(dcIdentifier);
      String status = notice.getInternal().getStatus();
      if (!status.equals(NoticeStatus.PUBLISHED.getValue()) && (!userInfoService.isAuthenticated()
          || userInfoService.hasAnyRole(Arrays.asList(Authority.DEFAULT.name())))) {
        throw new NotAuthorizedException(messageSource.getMessage("user.insufficient.rights", new Object[] {}, null));
      }
      return notice;
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération d'une notice : " + dcIdentifier, e);
    }
  }

  @Override
  @GET
  @Path("/search")
  public NoticeSearchResponse search(@QueryParam(value = "page") Integer page,
      @QueryParam(value = "terms") String terms, @QueryParam(value = "pageSize") Integer pageSize,
      @QueryParam(value = "sort") String sort, @QueryParam(value = "fieldSort") String fieldSort,
      @QueryParam(value = "noticeType") String noticeType, @QueryParam(value = "selectionId") String selectionId,
      @QueryParam(value = "withAttachment") Boolean withAttachment,
      @QueryParam(value = "facets") AggregationRequest aggregationRequest,
      @QueryParam(value = "fields") List<String> fields) {
    NoticeSortType sortField = null;
    if (StringUtils.isNoneEmpty(fieldSort)) {
      try {
        sortField = NoticeSortType.valueOf(fieldSort);
      } catch (IllegalArgumentException e) {
        log.error(ERROR_MESSAGE_SEARCH_ISSUE, e);
        throw new BadRequestException(
            messageSource.getMessage("search.sort.unknown", new Object[] { fieldSort }, null));
      }
    }
    if (page == null) {
      page = PaginatedSearchRequest.DEFAULT_PAGE;
    }
    if (pageSize == null) {
      // Si on a une selection de renseigné sans pageSize, on ne limite pas le nombre
      // de résultat
      if (StringUtils.isNotEmpty(selectionId)) {
        pageSize = PaginatedSearchRequest.MAXIMUM_PAGE_SIZE;
        page = PaginatedSearchRequest.DEFAULT_PAGE;
      } else {
        pageSize = PaginatedSearchRequest.DEFAULT_PAGE_SIZE;
      }
    }
    if (page < 1 || (pageSize > PaginatedSearchRequest.LIMIT_PAGE_SIZE && StringUtils.isEmpty(selectionId))) {
      throw new BadRequestException(messageSource.getMessage("search.paging.error", new Object[] {}, null));
    }
    NoticeType noticeTypeEnum = null;
    if (StringUtils.isNotBlank(noticeType)) {
      try {
        noticeTypeEnum = NoticeType.valueOf(noticeType);
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(ERROR_MESSAGE_UNKNOWN_NOTICE_TYPE);
      }
    }
    try {
      Map<String, Aggregation> aggregations = null;
      if (aggregationRequest != null) {
        aggregations = aggregationRequest.getAggregations();
      }
      NoticeSearchRequest request = new NoticeSearchRequest(page, pageSize, sort, terms, aggregations, selectionId,
          sortField);
      if (BooleanUtils.isTrue(withAttachment)) {
        request.setContentTarget(ContentTarget.ALL);
      }
      request.getFields().addAll(fields);
      return searchService.search(request, noticeTypeEnum);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(ERROR_MESSAGE_SEARCH_ISSUE, e);
    }
  }

  @Override
  @GET
  @Path("/advSearch")
  @Consumes(MediaType.APPLICATION_JSON)
  public NoticeSearchResponse search(@QueryParam(value = "filters") AdvancedSearchRequest request) {
    Integer page = request.getPage();
    if (page == null) {
      page = PaginatedSearchRequest.DEFAULT_PAGE;
    }
    Integer pageSize = request.getPageSize();
    if (pageSize == null) {
      pageSize = PaginatedSearchRequest.DEFAULT_PAGE_SIZE;
    }
    if (page < 1 || pageSize > PaginatedSearchRequest.LIMIT_PAGE_SIZE) {
      throw new BadRequestException(messageSource.getMessage("search.paging.error", new Object[] {}, null));
    }
    request.setPage(page);
    request.setPageSize(pageSize);

    NoticeType noticeTypeEnum = null;
    if (StringUtils.isNotBlank(request.getNoticeType())) {
      try {
        noticeTypeEnum = NoticeType.valueOf(request.getNoticeType());
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(ERROR_MESSAGE_UNKNOWN_NOTICE_TYPE);
      }
    }
    try {
      return searchService.advancedSearch(request, noticeTypeEnum);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(ERROR_MESSAGE_SEARCH_ISSUE, e);
    }
  }

  @Override
  @GET
  @Path("/timeline")
  public TimelineSearchResponse timeline(@QueryParam(value = "page") Integer page,
      @QueryParam(value = "terms") String terms, @QueryParam(value = "pageSize") Integer pageSize,
      @QueryParam(value = "noticeType") String noticeType, @QueryParam(value = "selectionId") String selectionId,
      @QueryParam(value = "facets") AggregationRequest aggregationRequest,
      @QueryParam(value = "fields") List<String> fields) {
    if (page == null) {
      page = PaginatedSearchRequest.DEFAULT_PAGE;
    }
    if (pageSize == null) {
      // Si on a une selection de renseigné sans pageSize, on ne limite pas le nombre
      // de résultat
      if (StringUtils.isNotEmpty(selectionId)) {
        pageSize = PaginatedSearchRequest.MAXIMUM_PAGE_SIZE;
        page = PaginatedSearchRequest.DEFAULT_PAGE;
      } else {
        pageSize = PaginatedSearchRequest.DEFAULT_PAGE_SIZE;
      }
    }
    if (page < 1 || (pageSize > PaginatedSearchRequest.LIMIT_PAGE_SIZE && StringUtils.isEmpty(selectionId))) {
      throw new BadRequestException(messageSource.getMessage("search.paging.error", new Object[] {}, null));
    }
    NoticeType noticeTypeEnum = null;
    if (StringUtils.isNotBlank(noticeType)) {
      try {
        noticeTypeEnum = NoticeType.valueOf(noticeType);
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(ERROR_MESSAGE_UNKNOWN_NOTICE_TYPE);
      }
    }
    try {
      Map<String, Aggregation> aggregations = null;
      if (aggregationRequest != null) {
        aggregations = aggregationRequest.getAggregations();
      }
      NoticeSearchRequest request = new NoticeSearchRequest(page, pageSize, null, terms, aggregations, selectionId,
          null);
      request.getFields().addAll(fields);
      return searchService.searchTimeline(request, noticeTypeEnum);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(ERROR_MESSAGE_SEARCH_ISSUE, e);
    }
  }

  @Override
  @GET
  @Path("/advTimeline")
  @Consumes(MediaType.APPLICATION_JSON)
  public TimelineSearchResponse advTimeline(@QueryParam(value = "filters") AdvancedSearchRequest request) {
    Integer page = request.getPage();
    if (page == null) {
      page = PaginatedSearchRequest.DEFAULT_PAGE;
    }
    Integer pageSize = request.getPageSize();
    if (pageSize == null) {
      pageSize = PaginatedSearchRequest.DEFAULT_PAGE_SIZE;
    }
    if (page < 1 || pageSize > PaginatedSearchRequest.LIMIT_PAGE_SIZE) {
      throw new BadRequestException(messageSource.getMessage("search.paging.error", new Object[] {}, null));
    }
    request.setPage(page);
    request.setPageSize(pageSize);

    NoticeType noticeTypeEnum = null;
    if (StringUtils.isNotBlank(request.getNoticeType())) {
      try {
        noticeTypeEnum = NoticeType.valueOf(request.getNoticeType());
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(ERROR_MESSAGE_UNKNOWN_NOTICE_TYPE);
      }
    }
    try {
      return searchService.searchTimeline(request, noticeTypeEnum);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(ERROR_MESSAGE_SEARCH_ISSUE, e);
    }
  }

  @Override
  @GET
  @Path("/graph")
  public GraphApiResponse graph(@QueryParam(value = "selectionId") String selectionId,
      @QueryParam(value = "noticeId") String noticeId) {

    try {
      return searchService.searchGraph(selectionId, noticeId);
    } catch (ServiceException e) {
      throw new InternalServerErrorException(ERROR_MESSAGE_SEARCH_ISSUE, e);
    }
  }

  @GET
  @Path("/{id}/media")
  @Override
  public List<Media> getMedia(@PathParam(value = "id") String noticeId,
      @QueryParam(value = "mode") @Parameter(schema = @Schema(allowableValues = { "FULL",
          "THUMBNAILS" })) String mode) {

    MediaRetrieveMode mediaRetrieveMode = MediaRetrieveMode.FULL;
    if (StringUtils.isNotBlank(mode)) {
      try {
        mediaRetrieveMode = MediaRetrieveMode.valueOf(mode);
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(
            messageSource.getMessage("general.parameter.unknown.value", new Object[] { mode, "mode" }, null));
      }
    }
    try {
      return noticeService.getMedia(noticeId, mediaRetrieveMode);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération des thumbnails d'une notice : " + noticeId,
          e);
    }
  }

  @Override
  @GET
  @Path("/autocomplete")
  public List<NoticeSearchResult> autocomplete(@QueryParam(value = "terms") String terms,
      @QueryParam(value = "noticeType") String noticeType, @QueryParam(value = "lang") String lang,
      @QueryParam(value = "noticePageSize") Integer noticePageSize) {
    NoticeType noticeTypeEnum = null;
    if (StringUtils.isNotBlank(noticeType)) {
      try {
        noticeTypeEnum = NoticeType.valueOf(noticeType);
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(ERROR_MESSAGE_UNKNOWN_NOTICE_TYPE);
      }
    }
    try {
      return searchService.autocomplete(terms, noticeTypeEnum, lang, noticePageSize);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération de la prefPicture d'une notice", e);
    }
  }

  @Override
  @GET
  @Path("/referenceby/{id}")
  public List<String> getReferenceBy(@PathParam(value = "id") String noticeId) {
    try {
      return noticeService.getReferenceBy(noticeId);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération des références à la notice " + noticeId,
          e);
    }
  }

  @Override
  @GET
  @Path("/advAutocomplete")
  public Set<String> advAutocomplete(@QueryParam(value = "field") String field,
      @QueryParam(value = "query") String query, @QueryParam(value = "noticeType") String noticeType) {
    NoticeType noticeTypeEnum = null;
    if (StringUtils.isNotBlank(noticeType)) {
      try {
        noticeTypeEnum = NoticeType.valueOf(noticeType);
      } catch (IllegalArgumentException e) {
        throw new BadRequestException(ERROR_MESSAGE_UNKNOWN_NOTICE_TYPE);
      }
    }
    if (StringUtils.length(query) < 2) {
      throw new BadRequestException("La requête doit contenir au moins 2 caractères");
    }
    try {
      return searchService.advAutocomplete(field, query, noticeTypeEnum);
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la requête d'autocompletion sur le champ " + field
          + ", type de notice " + noticeType + ", valeur " + query, e);
    }
  }

  @Override
  @POST
  @Path("/correctionProposal")
  @Consumes({ MediaType.APPLICATION_FORM_URLENCODED })
  @Produces({ MediaType.TEXT_PLAIN })
  @SecurityRequirement(name = "basicAuth")
  public Response correctionProposal(@FormParam(value = "noticeId") String noticeId,
      @FormParam(value = "text") String text) {
    String userMail = userInfoService.getPrincipal().getLdapUser().getMail();
    mailService.sendCorrectionProposal(noticeId, text, userMail);
    return Response.ok().build();
  }

  @Override
  @GET
  @Path("/exportCsv")
  public Response exportCsv(@QueryParam(value = "ids") List<String> ids) {
    File zip;
    try {
      zip = exportService.exportCsv(ids);
      if (zip == null) {
        throw new WebApplicationException(Status.NO_CONTENT);
      }
      StreamingOutput output = stream -> {
        try {
          Files.copy(zip.toPath(), stream);
        } finally {
          Files.delete(zip.toPath());
        }
      };
      ResponseBuilder response = Response.ok().entity(output).type("application/zip");
      response.header("Content-Disposition", "attachment;filename=exportNoticeCsv.zip");
      return response.build();
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors d'export csv", e);
    }
  }

  @GET
  @Path("/selections/export/{id}")
  @Override
  public Response exportSelection(@PathParam(value = "id") String selectionId) {
    File zip;
    try {
      zip = exportService.exportSelection(selectionId);
      if (zip == null) {
        throw new WebApplicationException(Status.NO_CONTENT);
      }
      StreamingOutput output = new StreamingOutput() {
        @Override
        public void write(final OutputStream output) throws IOException, WebApplicationException {
          try {
            Files.copy(zip.toPath(), output);
          } finally {
            Files.delete(zip.toPath());
          }
        }
      };
      ResponseBuilder response = Response.ok().entity(output).type("application/zip");
      response.header("Content-Disposition", "attachment;filename=exportSelection.zip");
      return response.build();
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors d'export de la selection " + selectionId, e);
    }
  }

  @Override
  @GET
  @Path("/exportOAI")
  public void exportOAI() {
    try {
      exportService.exportOAI();
    } catch (ServiceException e) {
      throw new InternalServerErrorException("Erreur lors d'export OAI ", e);
    }
  }

  @GET
  @Path("/exportjson")
  @Override
  public Response exportJson(@QueryParam(value = "noticeType") String noticeType,
      @QueryParam(value = "database") String databaseId, @QueryParam(value = "token") String token) {
    if (StringUtils.isBlank(databaseId)) {
      throw new BadRequestException(messageSource.getMessage("database.mandatory", new Object[] {}, null));
    }
    StreamingOutput stream = new StreamingOutput() {
      @Override
      public void write(OutputStream os) throws IOException, WebApplicationException {
        NoticeType noticeTypeEnum = null;
        if (StringUtils.isNotBlank(noticeType)) {
          try {
            noticeTypeEnum = NoticeType.valueOf(noticeType);
          } catch (IllegalArgumentException e) {
            throw new BadRequestException(ERROR_MESSAGE_UNKNOWN_NOTICE_TYPE);
          }
        }
        Writer writer = new BufferedWriter(new OutputStreamWriter(os));
        try {
          exportService.exportJson(writer, noticeTypeEnum, databaseId, token);
        } catch (ServiceException e) {
          throw new InternalServerErrorException("Erreur lors de l'export Json", e);
        }
      }
    };
    ResponseBuilder response = Response.ok(stream);
    response.header("Content-Disposition", "attachment;filename=exportNoticeJson.json");
    return response.build();
  }

  @Override
  @GET
  @Path("/geoPointField")
  public Map<String, List<String>> getGeoPointFields() {
    return searchService.getGeoPointFields();
  }

  @Override
  @GET
  @Path("/redirect")
  public Response redirect(@QueryParam(value = "id") String id, @QueryParam(value = "type") String type) {

    OldNoticeType oldNoticeType = OldNoticeType.getFromCode(type);
    String permalink = "";
    switch (oldNoticeType) {
      case DATABASE:
        permalink = identifierServcice.getPermalink(id);
        break;
      case MEDIA:
        throw new NotFoundException();
      default:
        String uuid;
        try {
          uuid = noticeService.getUuid(id, oldNoticeType);
          permalink = identifierServcice.getPermalink(uuid);
        } catch (FunctionnalException | ServiceException e) {
          return Response.status(Status.NOT_FOUND).build();
        }
        break;
    }
    return Response.status(Response.Status.MOVED_PERMANENTLY).location(URI.create(permalink)).build();

  }

}
