package fr.inha.agorha2.api.dto;

public class LongRange implements Range<Long> {

  private Long min;
  private Long max;

  public LongRange() {
    super();
  }

  public Long getMin() {
    return min;
  }

  public void setMin(Long min) {
    this.min = min;
  }

  public Long getMax() {
    return max;
  }

  public void setMax(Long max) {
    this.max = max;
  }

}
