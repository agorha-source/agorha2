package fr.inha.agorha2.api.dto.request;

public class FindDatabaseRequest {

  private String title;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

}
