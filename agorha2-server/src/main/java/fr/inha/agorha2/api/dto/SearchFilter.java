package fr.inha.agorha2.api.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;

/**
 * BEWARE Jackson polymorphism configuration is NOT USED besides by Swagger,
 * because uses a custom deserializer BUT neither Swagger conf nor Jackson
 * polymorphism are able to show inheriting impls in API doc UI, so rather dummy
 * fields. *
 * 
 * @author mdutoo
 *
 */

@JsonSubTypes({ // Below, we define the names and the binding classes.
    @JsonSubTypes.Type(value = MultiValueSearchFilter.class, name = "MultiValueSearchFilter"),
    @JsonSubTypes.Type(value = LongRangeFilter.class, name = "LongRangeFilter"),
    @JsonSubTypes.Type(value = DateRangeFilter.class, name = "DateRangeFilter") })
public abstract class SearchFilter {
  protected Operator op;
  protected String field;
  private Float boost = 1f;

  public enum Operator {
    ANY,
    NONE
  }

  public Operator getOp() {
    return op;
  }

  public Float getBoost() {
    return boost;
  }

  public void setBoost(Float boost) {
    this.boost = boost;
  }

  public String getField() {
    return field;
  }

  public void setField(String field) {
    this.field = field;
  }

}
