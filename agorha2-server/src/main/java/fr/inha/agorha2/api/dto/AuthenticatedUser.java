package fr.inha.agorha2.api.dto;

import fr.inha.elastic.model.Authorizations;

public class AuthenticatedUser {

  private String name;
  private String firstName;
  private String mail;
  private Authorizations authorizations;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getMail() {
    return mail;
  }

  public void setMail(String mail) {
    this.mail = mail;
  }

  public Authorizations getAuthorizations() {
    return authorizations;
  }

  public void setAuthorizations(Authorizations authorizations) {
    this.authorizations = authorizations;
  }

}
