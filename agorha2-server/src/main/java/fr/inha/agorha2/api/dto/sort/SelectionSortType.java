package fr.inha.agorha2.api.dto.sort;

public enum SelectionSortType {
  UPDATED_DATE("updatedDate"),
  CREATION_DATE("createdDate"),
  TITLE("title"),
  SIZE(null);

  private String field;

  private SelectionSortType(String field) {
    this.field = field;
  }

  public String getField() {
    return field;
  }

}
