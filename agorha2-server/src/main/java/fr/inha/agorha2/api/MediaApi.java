package fr.inha.agorha2.api;

import fr.inha.elastic.model.Media;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * In the implementation, if any JAXRS annotation is overriden (ex. @Path), they
 * need to be all copied see
 * https://stackoverflow.com/questions/25916796/inheritance-with-jax-rs
 *
 * @author broussot
 */
@Path("/media")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
@OpenAPIDefinition(info = @Info(title = "Notice API", version = "0.0.1-SNAPSHOT", license = @License(name = "Apache 2.0 License", url = "http://www.apache.org/licenses/LICENSE-2.0.html")))
@Tag(name = "Media")
public interface MediaApi {

  @GET
  @Path("/{id}")
  public Media get(String idMedia);

  @GET
  @Path("/{id}/picture")
  public Map<String, String> get(String idMedia, String fileId, String format);

  @GET
  @Path("/{id}/media/{fileId}")
  public Map<String, String> get(String idMedia, String fileId);

}
