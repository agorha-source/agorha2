package fr.inha.agorha2.api.dto.result;

public class ApiSearchResult {

  private String id;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

}
