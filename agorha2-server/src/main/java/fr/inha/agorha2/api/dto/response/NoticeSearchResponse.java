package fr.inha.agorha2.api.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import fr.inha.agorha2.api.dto.result.ApiSearchResult;

@JsonInclude(value = JsonInclude.Include.ALWAYS)
public class NoticeSearchResponse extends ApiSearchResponse<ApiSearchResult> {

  private boolean selectionBelongToUser;

  public boolean isSelectionBelongToUser() {
    return selectionBelongToUser;
  }

  public void setSelectionBelongToUser(boolean selectionBelongToUser) {
    this.selectionBelongToUser = selectionBelongToUser;
  }

}
