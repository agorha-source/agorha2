package fr.inha.agorha2.bean;

import java.io.Serializable;

public class ExternalThesaurusConcept implements Serializable {

  private static final long serialVersionUID = 6522024823302149514L;

  private String uri;
  private String name;
  private String repository;
  private String description;
  private String broader;
  private String broaderName;
  private String thesaurus;

  public ExternalThesaurusConcept() {
    this.repository = "Ginco MCC";
    this.description = "No description available";
  }

  public ExternalThesaurusConcept(String uri, String name, String broader, String broaderName, String thesaurus) {
    this.uri = uri;
    this.name = name;
    this.repository = "Ginco MCC";
    this.description = "No description available";
    this.broader = broader;
    this.broaderName = broaderName;
    this.thesaurus = thesaurus;
  }

  public String getUri() {
    return uri;
  }

  public void setUri(String uri) {
    this.uri = uri;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getRepository() {
    return repository;
  }

  public void setRepository(String repository) {
    this.repository = repository;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getBroader() {
    return broader;
  }

  public void setBroader(String broader) {
    this.broader = broader;
  }

  public String getBroaderName() {
    return broaderName;
  }

  public void setBroaderName(String broaderName) {
    this.broaderName = broaderName;
  }

  public String getThesaurus() {
    return thesaurus;
  }

  public void setThesaurus(String thesaurus) {
    this.thesaurus = thesaurus;
  }

}
