package fr.inha.agorha2.bean;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ConceptSiecle implements Serializable {

  private static final long serialVersionUID = -9096889781253451101L;

  private String label;
  private String conceptId;
  private LocalDate startDate;
  private LocalDate endDate;

  private List<ConceptSiecle> subConcepts = new ArrayList<>();

  public String getLabel() {
    return label;
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public String getConceptId() {
    return conceptId;
  }

  public void setConceptId(String conceptId) {
    this.conceptId = conceptId;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }

  public List<ConceptSiecle> getSubConcepts() {
    return subConcepts;
  }

}
