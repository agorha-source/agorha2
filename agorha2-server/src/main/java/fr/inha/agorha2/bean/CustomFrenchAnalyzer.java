package fr.inha.agorha2.bean;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.fr.FrenchAnalyzer;
import org.apache.lucene.analysis.miscellaneous.ASCIIFoldingFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.ElisionFilter;
import org.springframework.stereotype.Component;

/**
 * Custom {@link Analyzer} configuré comme {@link FrenchAnalyzer} avec en plus
 * l'asciifolding.
 * 
 * @author broussot
 *
 */
@Component
public class CustomFrenchAnalyzer extends Analyzer {

  @Override
  protected TokenStreamComponents createComponents(String fieldName) {
    final Tokenizer source = new StandardTokenizer();
    TokenStream result = new ElisionFilter(source, FrenchAnalyzer.DEFAULT_ARTICLES);
    result = new LowerCaseFilter(result);
    result = new StopFilter(result, FrenchAnalyzer.getDefaultStopSet());
    result = new ASCIIFoldingFilter(result);
    return new TokenStreamComponents(source, result);
  }
}
