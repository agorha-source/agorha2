package fr.inha.agorha2.bean;

import java.io.Serializable;
import java.util.List;

public class ThesaurusConcept implements Serializable {

  private static final long serialVersionUID = 2551576675969948304L;

  private String identifier;
  private String lexicalValue;
  private String altLabel;
  private List<String> broaders;

  public ThesaurusConcept(String identifier, String lexicalValue) {
    this.identifier = identifier;
    this.lexicalValue = lexicalValue;
  }

  public ThesaurusConcept(String identifier, String lexicalValue, List<String> broaders) {
    this.identifier = identifier;
    this.lexicalValue = lexicalValue;
    this.broaders = broaders;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public String getLexicalValue() {
    return lexicalValue;
  }

  public void setLexicalValue(String lexicalValue) {
    this.lexicalValue = lexicalValue;
  }

  public String getAltLabel() {
    return altLabel;
  }

  public void setAltLabel(String altLabel) {
    this.altLabel = altLabel;
  }

  public List<String> getBroaders() {
    return broaders;
  }

  public void setBroaders(List<String> broaders) {
    this.broaders = broaders;
  }

}
