package fr.inha.agorha2.bean;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * Représentation arborescente du thesaurus siècle, contenant le label et les
 * attributs custom ginco start et end.
 * 
 * @author broussot
 *
 */
public class ThesaurusSiecle implements Serializable {

  private static final long serialVersionUID = -5733710404168312688L;

  private List<ConceptSiecle> siecles = new ArrayList<>();
  private Map<String, ImmutablePair<LocalDate, LocalDate>> mapSiecles = new HashMap<>();

  public List<ConceptSiecle> getSiecles() {
    return siecles;
  }

  public Map<String, ImmutablePair<LocalDate, LocalDate>> getMapSiecles() {
    return mapSiecles;
  }

}
