package fr.inha.agorha2.service;

import fr.inha.agorha2.bean.ExternalThesaurusConcept;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public interface ExternalThesaurusService {

  public List<ExternalThesaurusConcept> autocomplete(String terms, Integer limit)
      throws IOException, SAXException, ParserConfigurationException;

}
