package fr.inha.agorha2.service;

import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import fr.inha.agorha2.api.dto.request.MediaSearchRequest;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest;
import fr.inha.agorha2.api.dto.request.UserSearchRequest;
import fr.inha.agorha2.api.dto.response.GraphApiResponse;
import fr.inha.agorha2.api.dto.response.MediaSearchResponse;
import fr.inha.agorha2.api.dto.response.NoticeSearchResponse;
import fr.inha.agorha2.api.dto.response.TimelineSearchResponse;
import fr.inha.agorha2.api.dto.response.UserSearchResponse;
import fr.inha.agorha2.api.dto.result.NoticeSearchResult;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.model.NoticeType;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface SearchService {

  public NoticeSearchResponse search(NoticeSearchRequest request, NoticeType noticeType) throws ServiceException;

  public TimelineSearchResponse searchTimeline(NoticeSearchRequest request, NoticeType noticeTypeEnum)
      throws ServiceException;

  public GraphApiResponse searchGraph(String selectionId, String noticeId) throws ServiceException;

  public NoticeSearchResponse advancedSearch(AdvancedSearchRequest request, NoticeType noticeType)
      throws ServiceException;

  public List<NoticeSearchResult> autocomplete(String terms, NoticeType noticeType, String lang, Integer noticePageSize)
      throws ServiceException;

  public Set<String> advAutocomplete(String field, String query, NoticeType noticeType) throws ServiceException;

  public List<Map<String, String>> autocompleteNoticeTitle(String terms, NoticeType noticeTypeEnum)
      throws ServiceException;

  public MediaSearchResponse searchMedia(MediaSearchRequest request) throws ServiceException;

  public UserSearchResponse searchUser(UserSearchRequest request) throws ServiceException;

  public Map<String, List<String>> getGeoPointFields();

}
