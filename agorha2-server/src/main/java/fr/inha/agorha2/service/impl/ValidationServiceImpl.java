package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.inha.agorha2.service.MetaModelService;
import fr.inha.agorha2.service.ValidationService;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.model.NoticeType;
import java.io.IOException;
import java.util.List;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import org.apache.commons.lang3.StringUtils;
import org.everit.json.schema.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidationServiceImpl implements ValidationService {

  private static final String REQUIRED_FIELD = "required";

  private static final Logger log = LoggerFactory.getLogger(ValidationServiceImpl.class);

  @Autowired
  private MetaModelService metaModelService;
  @Autowired
  private ObjectMapper mapper;

  @Override
  public void validate(NoticeType noticeType, String json) {
    String jsonSchema = metaModelService.get(null, noticeType);

    try {
      JsonUtils.validateJson(jsonSchema, json);
    } catch (ValidationException e) {
      StringBuilder sb = new StringBuilder();
      sb.append(e.getMessage());
      boolean requiredFieldMissing = true;
      for (ValidationException exception : e.getCausingExceptions()) {
        if (!REQUIRED_FIELD.equals(exception.getKeyword())) {
          requiredFieldMissing = false;
        }
        log.error(exception.getMessage());
        sb.append(exception.getMessage());
      }
      if (requiredFieldMissing) {
        // Si toute les erreurs de validation viennent d'un champ manquant parmis les
        // champs obligatoires
        throw new BadRequestException("Vous devez renseigner tous les champs obligatoires pour publier cette notice");
      } else {
        throw new BadRequestException(sb.toString(), e);
      }
    }
    validateSourcing(json);
  }

  private void validateSourcing(String json) {
    JsonNode dataAsJson;
    try {
      dataAsJson = mapper.readTree(json);
      List<JsonNode> sourcings = dataAsJson.findValues("sourcing");
      for (JsonNode sourcing : sourcings) {
        ArrayNode sourcingNode = (ArrayNode) sourcing;
        for (JsonNode sourcingBlock : sourcingNode) {
          JsonNode comment = sourcingBlock.at("/comment");
          boolean contentExists = !comment.isMissingNode() && StringUtils.isNotEmpty(comment.asText());
          boolean sourceNotExists = sourcingBlock.at("/biblioRef").isMissingNode()
              && sourcingBlock.at("/database").isMissingNode() && sourcingBlock.at("/url").isMissingNode();
          if (contentExists && sourceNotExists) {
            throw new BadRequestException(
                "Au moins un des champ source doit être renseigné pour chaque block sourcing");
          }
        }
      }
    } catch (IOException e) {
      throw new InternalServerErrorException("Impossible de déserializer l'objet", e);
    }
  }

  public void validateDraft(NoticeType noticeType, String json) {
    String jsonSchema = metaModelService.get(null, noticeType);

    JsonNode schemaAsJson;
    try {
      schemaAsJson = mapper.readTree(jsonSchema);
      List<JsonNode> requiredParents = schemaAsJson.findParents(REQUIRED_FIELD);
      for (JsonNode jsonNode : requiredParents) {
        ((ObjectNode) jsonNode).remove(REQUIRED_FIELD);
      }
      jsonSchema = schemaAsJson.toString();
    } catch (IOException e) {
      throw new InternalServerErrorException("Impossible de déserializer l'objet", e);
    }

    try {
      JsonUtils.validateJson(jsonSchema, json);
    } catch (ValidationException e) {
      StringBuilder sb = new StringBuilder();
      sb.append(e.getMessage());
      for (ValidationException exception : e.getCausingExceptions()) {
        log.error(exception.getMessage());
        sb.append(exception.getMessage());
      }
      throw new BadRequestException(sb.toString(), e);
    }
  }

}
