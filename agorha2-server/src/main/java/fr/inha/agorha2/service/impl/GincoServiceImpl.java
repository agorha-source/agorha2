package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.inha.agorha2.bean.ConceptSiecle;
import fr.inha.agorha2.bean.ThesaurusConcept;
import fr.inha.agorha2.bean.ThesaurusSiecle;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.ThesaurusService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.agorha2.utils.DateUtils.PrefixType;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.ws.rs.InternalServerErrorException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

/**
 * Interface d'accès aux services Ginco. Permet l'appel aux services rest de
 * Ginco Admin.
 * 
 * @author broussot
 *
 */
@Service
@ConfigurationProperties(prefix = "thesaurus")
public class GincoServiceImpl implements ThesaurusService {

  public static final String THESAURUS_SIECLES_KEY = "TH_SIECLES";
  public static final String THESAURUS_LIEU_KEY = "TH_LIEU_CONCEPTS";
  public static final String THESAURUS_LANGUAGE_KEY = "LANGUE_ISO";
  public static final String THESAURUS_PREFIXE_KEY = "TH_PREFIXE";

  private static final String GINCO_FRENCH_LANGUAGE = "fr-FR";
  private static final String LEXICAL_VALUE = "lexicalValue";
  private static final int SEARCH_RESULT_TYPE_CONCEPT = 1;
  private static final int SEARCH_RESULT_TYPE_TERME = 2;
  private static final int SEARCH_RESULT_STATUS_VALIDE = 1;

  private static final String GET_CONCEPT_ATTRIBUTE_URL = "/customattributesservice/getConceptAttribute";

  private static final String PREFIX_CONCEPTS = "CONCEPTS_";

  private static final String GET_TREE_CONTENT_URL = "/baseservice/getTreeContent";

  private static final String GET_CONCEPT_ATTRIBUTES_URL = "/customattributesservice/getAllConceptAttributeTypes";

  private static final String GET_CONCEPT_URL = "/thesaurusconceptservice/getConcept";
  private static final String SEARCH_URL = "/searcherservice/search";
  private static final Logger log = LoggerFactory.getLogger(GincoServiceImpl.class);

  @Autowired
  private ObjectMapper mapper;

  @Value(value = "${ginco.ws.url}")
  private String gincoWSUrl;
  @Value(value = "${ginco.ws.username}")
  private String gincoWSUsername;
  @Value(value = "${ginco.ws.password}")
  private String gincoWSPassword;
  @Value(value = "${thesaurus.baseArkUrl}")
  private String thesaurusBaseArkUrl;

  private Map<String, String> thesaurusMap;

  @Override
  public String getThesaurusId(String thesaurusKey) {
    return thesaurusMap.get(thesaurusKey);
  }

  @Override
  public JsonNode getConcept(String conceptId) {

    String urlAsString = gincoWSUrl + GET_CONCEPT_URL + "?id=" + URLEncoder.encode(conceptId, StandardCharsets.UTF_8);

    try {
      URL url = new URL(urlAsString);

      return getResponseAsJson(url);
    } catch (IOException | ServiceException e) {
      log.error("Erreur lors de la récupération du concept {}, cause : {}", conceptId, e.getMessage());
      return null;
    }
  }

  @Override
  public String getConceptPath(JsonNode concept) {

    List<String> labels = new ArrayList<>();
    labels.add("");
    concept.at("/prefLabelsPath").elements()
        .forEachRemaining(element -> labels.add(StringEscapeUtils.unescapeXml(element.asText())));

    return String.join("/", labels);
  }

  @Override
  public String getGeoPoint(String conceptId) {

    try {
      String thesaurusSiecleId = thesaurusMap.get(THESAURUS_LIEU_KEY);
      URL urlAttributes = new URL(gincoWSUrl + GET_CONCEPT_ATTRIBUTES_URL + "?thesaurusId="
          + URLEncoder.encode(thesaurusSiecleId, StandardCharsets.UTF_8));

      JsonNode attributesSiecle = getResponseAsJson(urlAttributes);
      int idLatitude = 0;
      int idLongitude = 0;
      for (JsonNode attribute : attributesSiecle.at("/data")) {
        String code = attribute.get("code").asText();
        if ("latitude".equals(code)) {
          idLatitude = attribute.get("identifier").asInt();
        } else if ("longitude".equals(code)) {
          idLongitude = attribute.get("identifier").asInt();
        }
      }

      URL urlConceptAttribut = new URL(gincoWSUrl + GET_CONCEPT_ATTRIBUTE_URL + "?entityId="
          + URLEncoder.encode(conceptId, StandardCharsets.UTF_8) + "&page=1&start=0&limit=25");

      JsonNode attributes = getResponseAsJson(urlConceptAttribut);
      String latitude = "";
      String longitude = "";
      for (JsonNode attribute : attributes.at("/data")) {
        if (attribute.get("typeid").asInt() == idLatitude) {
          latitude = attribute.get(LEXICAL_VALUE).asText();
        } else if (attribute.get("typeid").asInt() == idLongitude) {
          longitude = attribute.get(LEXICAL_VALUE).asText();
        }
      }
      if (StringUtils.isNotEmpty(latitude) && StringUtils.isNotEmpty(longitude) && NumberUtils.isParsable(latitude)
          && NumberUtils.isParsable(longitude)) {
        return String.join(",", latitude, longitude);
      } else {
        return null;
      }
    } catch (IOException | ServiceException e) {
      log.error("Erreur lors de la récupération du geoPoint du concept {}, cause : {}", conceptId, e.getMessage());
      return "";
    }
  }

  @Override
  public String getPrefLabel(String conceptId) {
    String urlAsString = gincoWSUrl + GET_CONCEPT_URL + "?id="
        + URLEncoder.encode(getUrlFromUuid(conceptId), StandardCharsets.UTF_8);

    try {
      URL url = new URL(urlAsString);

      JsonNode concept = getResponseAsJson(url);

      StringBuilder lexicalValue = new StringBuilder();
      concept.at("/terms").elements().forEachRemaining(element -> {
        if (element.get("prefered").asBoolean() && element.at("/language").asText().equals(GINCO_FRENCH_LANGUAGE)) {
          lexicalValue.append(element.get(LEXICAL_VALUE).asText());
        }
      });

      return unescapeSolrSpecialChar(lexicalValue.toString());
    } catch (IOException | ServiceException e) {
      log.error("Erreur lors de la récupération du prefLabel du concept {}, cause : {}", conceptId, e);
      return "";
    }
  }

  @Override
  public String getAltLabel(String conceptId) {
    String urlAsString = gincoWSUrl + GET_CONCEPT_URL + "?id=" + URLEncoder.encode(conceptId, StandardCharsets.UTF_8);

    try {
      URL url = new URL(urlAsString);

      JsonNode concept = getResponseAsJson(url);

      StringBuilder lexicalValue = new StringBuilder();
      concept.at("/terms").elements().forEachRemaining(element -> {
        if (!element.get("prefered").asBoolean() && element.at("/language").asText().equals(GINCO_FRENCH_LANGUAGE)) {
          lexicalValue.append(element.get(LEXICAL_VALUE).asText());
        }
      });

      return lexicalValue.toString();
    } catch (IOException | ServiceException e) {
      log.error("Erreur lors de la récupération du prefLabel du concept {}, cause : {}", conceptId, e);
      return "";
    }
  }

  @Override
  @Cacheable(value = "thesaurusSiecle", key = "#root.methodName")
  public ThesaurusSiecle getThesaurusSiecle() {
    try {
      String thesaurusSiecleId = thesaurusMap.get(THESAURUS_SIECLES_KEY);
      URL urlAttributes = new URL(gincoWSUrl + GET_CONCEPT_ATTRIBUTES_URL + "?thesaurusId="
          + URLEncoder.encode(thesaurusSiecleId, StandardCharsets.UTF_8));

      JsonNode attributesSiecle = getResponseAsJson(urlAttributes);
      int idstart = 0;
      int idend = 0;
      for (JsonNode attribute : attributesSiecle.at("/data")) {
        String code = attribute.get("code").asText();
        if ("earliest".equals(code)) {
          idstart = attribute.get("identifier").asInt();
        } else if ("latest".equals(code)) {
          idend = attribute.get("identifier").asInt();
        }
      }

      URL urlTree = new URL(gincoWSUrl + GET_TREE_CONTENT_URL + "?id="
          + URLEncoder.encode(PREFIX_CONCEPTS + thesaurusSiecleId, StandardCharsets.UTF_8) + "&node="
          + URLEncoder.encode(PREFIX_CONCEPTS + thesaurusSiecleId, StandardCharsets.UTF_8));

      JsonNode tree = getResponseAsJson(urlTree);

      ThesaurusSiecle thesaurusSiecle = new ThesaurusSiecle();
      thesaurusSiecle.getSiecles().addAll(getSieclesFromTree(idstart, idend, tree));
      thesaurusSiecle.getMapSiecles().putAll(getMapSiecles(thesaurusSiecle.getSiecles()));

      return thesaurusSiecle;
    } catch (IOException | ServiceException | ParseException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération du thésaurus siècle", e);
    }
  }

  @Override
  @Cacheable(value = "thesaurusPrefixe", key = "#root.methodName")
  public Map<String, PrefixType> getThesaurusPrefixe() {
    try {
      String thesaurusPrefixeId = thesaurusMap.get(THESAURUS_PREFIXE_KEY);

      URL urlTree = new URL(gincoWSUrl + GET_TREE_CONTENT_URL + "?id="
          + URLEncoder.encode(PREFIX_CONCEPTS + thesaurusPrefixeId, StandardCharsets.UTF_8) + "&node="
          + URLEncoder.encode(PREFIX_CONCEPTS + thesaurusPrefixeId, StandardCharsets.UTF_8));

      JsonNode tree = getResponseAsJson(urlTree);

      Map<String, PrefixType> thesaurusPrefix = new HashMap<>();
      for (JsonNode concept : tree) {
        String id = concept.get("id").asText();
        String conceptId = id.replaceFirst(".*\\*", "");
        thesaurusPrefix.put(conceptId, PrefixType.fromValue(concept.get("title").asText()));
      }

      return thesaurusPrefix;
    } catch (IOException | ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération du thésaurus siècle", e);
    }
  }

  @Override
  public List<ThesaurusConcept> autocomplete(String terms, String thesaurusKey, String branchLabel, Integer limit) {
    String urlAsString = gincoWSUrl + SEARCH_URL;

    ObjectNode payload = mapper.createObjectNode();
    try {
      if (StringUtils.isNotEmpty(terms)) {
        String escapedTerms = escapeSolrSpecialChar(terms);
        payload.put("query", escapedTerms + "* " + escapedTerms);
      } else {
        payload.put("query", "");
      }
      String thesaurusId = thesaurusMap.get(thesaurusKey);
      if (StringUtils.isNoneEmpty(thesaurusId)) {
        payload.put("thesaurus", thesaurusId);
      }
      payload.put("limit", limit);
      payload.put("start", 0);
      payload.put("type", SEARCH_RESULT_TYPE_TERME);
      payload.put("status", SEARCH_RESULT_STATUS_VALIDE);

      URL url = new URL(urlAsString);
      JsonNode response = getResponseAsJson(url, "POST", Map.of("Content-Type", "application/json"),
          payload.toString());

      List<ThesaurusConcept> results = new ArrayList<>();
      response.at("/data").forEach(result -> {
        String conceptId = result.get("conceptId").asText();
        JsonNode concept = getConcept(conceptId);
        List<String> broaders = new ArrayList<>();
        concept.get("prefLabelsPath").elements()
            .forEachRemaining(element -> broaders.add(StringEscapeUtils.unescapeXml(element.asText())));
        if (StringUtils.isEmpty(branchLabel) || broaders.contains(branchLabel)) {
          ThesaurusConcept thesaurusConcept = new ThesaurusConcept(conceptId,
              StringEscapeUtils.unescapeXml(result.get(LEXICAL_VALUE).asText()), broaders);
          results.add(thesaurusConcept);
        }
      });
      return results;
    } catch (IOException | ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de la l'intérrogation de la recherche ginco : " + urlAsString
          + " payload : " + payload.toString(), e);
    }
  }

  @Override
  @Cacheable(value = "thesaurusCache")
  public List<ThesaurusConcept> list(String thesaurusKey) {
    List<ThesaurusConcept> result = new ArrayList<>();
    String thesaurusId = thesaurusMap.get(thesaurusKey);
    try {
      URL urlTree = new URL(gincoWSUrl + GET_TREE_CONTENT_URL + "?id="
          + URLEncoder.encode(PREFIX_CONCEPTS + thesaurusId, StandardCharsets.UTF_8) + "&node="
          + URLEncoder.encode(PREFIX_CONCEPTS + thesaurusId, StandardCharsets.UTF_8));

      JsonNode tree = getResponseAsJson(urlTree);
      for (JsonNode conceptNode : tree) {
        String id = conceptNode.get("id").asText();
        String identifier = id.replaceFirst(".*\\*", "");
        String lexicalValue = conceptNode.get("title").asText();
        result.add(new ThesaurusConcept(identifier, lexicalValue));
      }
    } catch (IOException | ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de l'interrogation du thesaurus " + thesaurusId, e);
    }
    return result;
  }

  @Override
  @Cacheable(value = "thesaurusMapCache")
  public Map<String, ThesaurusConcept> thesaurusByLabel(String thesaurusKey) {
    Map<String, ThesaurusConcept> result = new HashMap<>();
    String thesaurusId = thesaurusMap.get(thesaurusKey);
    try {
      URL urlTree = new URL(gincoWSUrl + GET_TREE_CONTENT_URL + "?id="
          + URLEncoder.encode(PREFIX_CONCEPTS + thesaurusId, StandardCharsets.UTF_8) + "&node="
          + URLEncoder.encode(PREFIX_CONCEPTS + thesaurusId, StandardCharsets.UTF_8));

      JsonNode tree = getResponseAsJson(urlTree);
      for (JsonNode conceptNode : tree) {
        String id = conceptNode.get("id").asText();
        String identifier = id.replaceFirst(".*\\*", "");
        String lexicalValue = conceptNode.get("title").asText();
        ThesaurusConcept concept = new ThesaurusConcept(identifier, lexicalValue);
        concept.setAltLabel(getAltLabel(id));
        result.put(lexicalValue, concept);
      }
    } catch (IOException | ServiceException e) {
      throw new InternalServerErrorException("Erreur lors de l'interrogation du thesaurus " + thesaurusId, e);
    }
    return result;
  }

  @Override
  public String getUrlFromUuid(String uuid) {
    if (uuid.startsWith(thesaurusBaseArkUrl)) {
      return uuid;
    } else {
      return thesaurusBaseArkUrl + "/" + uuid;
    }
  }

  private List<ConceptSiecle> getSieclesFromTree(int idstart, int idend, JsonNode tree)
      throws IOException, ServiceException, ParseException {
    List<ConceptSiecle> siecles = new ArrayList<>();
    for (JsonNode concept : tree) {
      ConceptSiecle siecle = new ConceptSiecle();
      String id = concept.get("id").asText();
      String conceptId = id.replaceFirst(".*\\*", "");
      siecle.setLabel(concept.get("title").asText());
      siecle.setConceptId(conceptId);
      URL urlConceptAttribut = new URL(gincoWSUrl + GET_CONCEPT_ATTRIBUTE_URL + "?entityId="
          + URLEncoder.encode(conceptId, StandardCharsets.UTF_8) + "&page=1&start=0&limit=25");

      JsonNode attributes = getResponseAsJson(urlConceptAttribut);
      for (JsonNode attribute : attributes.at("/data")) {
        String date = attribute.get(LEXICAL_VALUE).asText();
        if (attribute.get("typeid").asInt() == idstart) {
          siecle.setStartDate(DateUtils.parseDate(date));
        } else if (attribute.get("typeid").asInt() == idend) {
          siecle.setEndDate(DateUtils.parseDate(date));
        }
      }
      URL urlSubTree = new URL(gincoWSUrl + GET_TREE_CONTENT_URL + "?id="
          + URLEncoder.encode(id, StandardCharsets.UTF_8) + "&node=" + URLEncoder.encode(id, StandardCharsets.UTF_8));
      JsonNode subTree = getResponseAsJson(urlSubTree);

      siecle.getSubConcepts().addAll(getSieclesFromTree(idstart, idend, subTree));
      siecles.add(siecle);
    }
    return siecles;
  }

  private JsonNode getResponseAsJson(URL url) throws IOException, ServiceException {
    return getResponseAsJson(url, "GET", null, null);
  }

  private JsonNode getResponseAsJson(URL url, String method, Map<String, String> headers, String payload)
      throws IOException, ServiceException {
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod(method);
    conn.setRequestProperty("Accept", "application/json");
    conn.setRequestProperty("Authorization", "Basic " + Base64.getEncoder()
        .encodeToString(String.join(":", gincoWSUsername, gincoWSPassword).getBytes(StandardCharsets.UTF_8)));

    if (headers != null) {
      for (Entry<String, String> header : headers.entrySet()) {
        conn.setRequestProperty(header.getKey(), header.getValue());
      }
    }
    if (StringUtils.isNoneEmpty(payload)) {
      conn.setDoOutput(true);
      try (OutputStream os = conn.getOutputStream()) {
        byte[] input = payload.getBytes(StandardCharsets.UTF_8);
        os.write(input, 0, input.length);
      }
    }
    StringBuilder sb = new StringBuilder();
    try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {

      String output;
      while ((output = br.readLine()) != null) {
        sb.append(output);
      }
    } finally {
      conn.disconnect();
    }
    String string = sb.toString();
    if (string.contains("\"success\":false")) {
      throw new ServiceException("Erreur lors de l'appel au service Ginco : " + string + ", url :" + url.toString());
    }

    return mapper.readTree(string);
  }

  private Map<String, ImmutablePair<LocalDate, LocalDate>> getMapSiecles(List<ConceptSiecle> siecles) {
    Map<String, ImmutablePair<LocalDate, LocalDate>> map = new HashMap<>();
    for (ConceptSiecle conceptSiecle : siecles) {
      map.put(conceptSiecle.getConceptId(),
          new ImmutablePair<LocalDate, LocalDate>(conceptSiecle.getStartDate(), conceptSiecle.getEndDate()));
      map.putAll(getMapSiecles(conceptSiecle.getSubConcepts()));
    }
    return map;
  }

  private String escapeSolrSpecialChar(String terms) {
    return terms.replace("'", "&apos;");
  }

  private String unescapeSolrSpecialChar(String terms) {
    return StringEscapeUtils.unescapeXml(terms);
  }

  public Map<String, String> getThesaurusMap() {
    return thesaurusMap;
  }

  public void setThesaurusMap(Map<String, String> thesaurusMap) {
    this.thesaurusMap = thesaurusMap;
  }

}
