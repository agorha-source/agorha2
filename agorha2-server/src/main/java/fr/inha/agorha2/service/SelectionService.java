package fr.inha.agorha2.service;

import fr.inha.agorha2.api.dto.sort.SelectionSortType;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.model.Selection;
import java.util.List;
import javax.validation.constraints.NotNull;

public interface SelectionService {

  Selection get(String id) throws ServiceException;

  Selection create(String title, List<String> noticeIds) throws ServiceException;

  Selection add(String id, List<String> noticeIds) throws ServiceException;

  Selection remove(String id, List<String> noticeIds) throws ServiceException;

  Selection remove(String id, List<String> noticeIds, boolean forceRemove) throws ServiceException;

  void delete(String id) throws ServiceException;

  List<Selection> list(String terms, Integer page, Integer pageSize, String sort, @NotNull SelectionSortType fieldSort)
      throws ServiceException;

  void deleteByUser(String uuid) throws ServiceException;

}
