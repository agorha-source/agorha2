package fr.inha.agorha2.service;

import fr.inha.agorha2.api.dto.request.ApiSearchRequest;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.model.NoticeType;
import java.io.File;
import java.io.Writer;
import java.util.List;

public interface ExportService {

  /**
   * Exporte au format json à partir d'une {@link ApiSearchRequest}.<br>
   * Les données sont écrites dans le writer au fûr et à mesure du parcours des
   * résultats.
   * 
   * Le format de sortie est un fichier avec une liste de notice au format json.
   * 
   * @param writer  output ou sont écrites les notices exportées
   * @param request requête à partir de laquelle exportée les notice
   * @throws ServiceException si une erreur se produit lors de l'export
   */
  public void exportJson(Writer writer, NoticeSearchRequest request) throws ServiceException;

  public void exportJson(Writer writer, NoticeType noticeType, String databaseId, String scrollId)
      throws ServiceException;

  /**
   * Exporte au format csv à partir d'une {@link ApiSearchRequest} et d'un type de
   * notice.<br>
   * Les données sont écrites dans un fichier zip contenant un fichier csv par
   * type de notice.
   * 
   * Si noticeTypeFilter est null, on exporte tout les types de notice.
   * 
   * @param request          requête à partir de laquelle exportée les notice
   * @param noticeTypeFilter type de notice
   * @return {@link File} pointant sur le fichier zip généré
   * @throws ServiceException si une erreur se produit lors de l'export
   */
  public File exportCsv(NoticeSearchRequest request, NoticeType noticeTypeFilter) throws ServiceException;

  /**
   * Exporte au format csv à partir d'une liste d'identifiants de notice. Les
   * données sont écrites dans un fichier zip contenant un fichier csv par type de
   * notice.
   * 
   * @param ids liste des ids à exporter
   * @return {@link File} pointant sur le fichier zip généré, null si aucune
   *         donnée à exporter
   * @throws ServiceException si une erreur se produit lors de l'export
   */
  public File exportCsv(List<String> ids) throws ServiceException;

  public File exportSelection(String id) throws ServiceException;

  public void exportOAI() throws ServiceException;

}
