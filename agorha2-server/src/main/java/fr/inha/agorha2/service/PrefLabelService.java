package fr.inha.agorha2.service;

public interface PrefLabelService {

  public String getPrefLabel(String conceptId);

}
