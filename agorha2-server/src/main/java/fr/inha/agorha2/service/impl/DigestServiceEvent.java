package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.DigestService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.model.NoticeType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class DigestServiceEvent extends AbstractDigestService implements DigestService {

  private static final String EVENT_FIELD_PATH = "/content/identificationInformation/event";

  @Override
  public NoticeType getType() {
    return NoticeType.EVENT;
  }

  @Override
  public void build(JsonNode notice) throws ServiceException {
    ObjectNode digestObject = getDigestObject(notice);

    buildCommonFields(notice, digestObject);

    digestObject.put("title",
        removeHtmlTags(notice.at(JsonUtils.getPathToField(notice, EVENT_FIELD_PATH, "/label/value", "")).asText()));
    JsonNode eventType = notice.at("/content/identificationInformation/eventType/thesaurus/0/prefLabels/0/value");
    if (eventType.isMissingNode()) {
      eventType = notice.at(
          JsonUtils.getPathToField(notice, EVENT_FIELD_PATH, "/transactionMode/thesaurus", "/0/prefLabels/0/value"));
    }
    digestObject.set("eventType", getObjectNode(eventType));

    String pathToDate = JsonUtils.getPathToField(notice, EVENT_FIELD_PATH, "/date", "");
    if (StringUtils.isNotEmpty(pathToDate)) {
      digestObject.set("startDate", constructDate(pathToDate + "/start", notice));
      digestObject.set("endDate", constructDate(pathToDate + "/end", notice));
    }

    digestObject.set("place", getObjectNode(
        notice.at(JsonUtils.getPathToField(notice, EVENT_FIELD_PATH, "/place/thesaurus", "/0/prefLabels/0/value"))));

    digestObject.put("noticeType", getType().toString());

    digestObject.put("displayLabelLink", buildDisplayLabelLink(digestObject));
  }

  private String buildDisplayLabelLink(JsonNode digestObject) {
    StringBuilder sb = new StringBuilder();
    addValueToDisplayLabel(digestObject.get("title"), sb);

    String datesAsString = DateUtils.buildDatesFormattedFromDigest(digestObject.get("startDate"),
        digestObject.get("endDate"));
    if (StringUtils.isNoneBlank(datesAsString)) {
      sb.append(" (");
      sb.append(datesAsString);
      sb.append(")");
    }

    addValueToDisplayLabel(digestObject.get("place"), sb);

    return sb.toString();
  }

}
