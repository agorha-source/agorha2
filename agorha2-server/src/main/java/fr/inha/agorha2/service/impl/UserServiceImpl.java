package fr.inha.agorha2.service.impl;

import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.intf.IServiceDedicated;
import fr.inha.agorha2.api.dto.AuthenticatedUser;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.ldap.LdapAgorhaUser;
import fr.inha.agorha2.ldap.LdapService;
import fr.inha.agorha2.ldap.LdapUser;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.security.UserPrincipal;
import fr.inha.agorha2.service.MailService;
import fr.inha.agorha2.service.SelectionService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.agorha2.service.UserService;
import fr.inha.agorha2.utils.UserUtils;
import fr.inha.elastic.model.Authorizations;
import fr.inha.elastic.model.User;
import fr.inha.elastic.model.UserStatus;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  private static final String ERROR_GET_USER = "Erreur lors de la récupération de l'utilisateur ";

  private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

  @Value(value = "${mail.accountCreation.validationUrl}")
  private String accountCreationValidationUrl;

  @Value(value = "${mail.resetPassword.url}")
  private String resetPasswordUrl;

  @Autowired
  private LdapService ldapService;

  @Autowired
  private IServiceDedicated<User> userService;

  @Autowired
  private UserInfoService userInfoService;

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private MailService mailService;

  @Autowired
  private SelectionService selectionService;

  @Override
  public AuthenticatedUser login(String login, String password) {
    try {
      logger.debug("Tentative de connexion de l'utilisateur {}", login);
      List<User> user = userService.list(QueryBuilders.matchQuery("internal.login", login), 1);
      if (!user.isEmpty()) {
        logger.debug("Status de l'utilisateur {}", user.get(0).getInternal().getStatus());
        if (user.get(0).getInternal().getStatus().equals(UserStatus.INACTIVE)) {
          throw new NotAuthorizedException(
              "Les informations de connexion que vous avez renseignées ne sont pas correctes.");
        }
      }
      UsernamePasswordAuthenticationToken authenticationTokenRequest = new UsernamePasswordAuthenticationToken(login,
          password);
      Authentication authentication = this.authenticationManager.authenticate(authenticationTokenRequest);
      SecurityContext securityContext = SecurityContextHolder.getContext();
      securityContext.setAuthentication(authentication);

      UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();

      logger.debug("Utilisateur {} connecté", login);
      return UserUtils.getAuthenticatedUser(principal);

    } catch (BadCredentialsException e) {
      logger.error("Erreur lors de l'authentification de l'utilisateur " + login, e);
      throw new NotAuthorizedException(
          "Les informations de connexion que vous avez renseignées ne sont pas correctes.");
    } catch (EsRequestException e) {
      throw new InternalServerErrorException("Erreur lors de la récupération des info de l'utilisateur " + login, e);
    }
  }

  /**
   * Implémentation qui créé l'utilisateur à la fois dans un annuaire ldap et dans
   * un index Elasticsearch.
   * 
   * <p>
   * Les informations de bases comme le nom, prénom, mail son stockés dans le
   * ldap. <br>
   * Les informations relatives aux rôles et autorisations de bases de données son
   * stockés dans ES.
   */
  @Override
  public Map<String, String> create(String name, String firstName, String mail, String password)
      throws ServiceException {

    if (ldapService.exists(mail)) {
      throw new BadRequestException(messageSource.getMessage("user.mail.already.exists", new Object[] { mail }, null));
    }

    Map<String, String> response = new HashMap<>();
    try {
      LdapUser ldapUser = ldapService.create(password, mail, name, firstName);

      User esUser = new User();
      esUser.setInternal(UserUtils.createInternal(ldapUser, mail));
      esUser.setAuthorizations(UserUtils.createAuthorizations());
      String token = UUID.randomUUID().toString();
      esUser.getInternal().setValidateAccountToken(token);
      esUser.getInternal().setStatus(UserStatus.INACTIVE);
      esUser = userService.createOrUpdateAndGet(esUser, true);

      mailService.sendAccountCreation(mail, createValidationLink(esUser.getInternal().getUuid(), token));

      response.put("uuid", esUser.getInternal().getUuid());
      response.put("name", ldapUser.getName());
      response.put("firstName", ldapUser.getFirstName());
      response.put("mail", ldapUser.getMail());
    } catch (Exception e) {
      // rollback si erreur lors de la création
      ldapService.delete(mail);
      throw new ServiceException("Erreur lors de la création de l'utilisateur " + mail, e);
    }
    return response;
  }

  /**
   * Implémentation mettant à jours les autorisations dans l'index ES.
   */
  @Override
  public Map<String, Object> update(String id, Authorizations authorizations) throws ServiceException {
    User user = null;
    try {
      user = userService.get(id);
    } catch (EsRequestException e) {
      throw new ServiceException(ERROR_GET_USER + id + " dans ES", e);
    }
    if (user == null) {
      throw new NotFoundException(messageSource.getMessage("user.unknown", new Object[] { id }, null));
    }
    user.setId(id);
    user.setAuthorizations(authorizations);
    updateInternal(user);
    try {
      user = userService.createOrUpdateAndGet(user, true);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de la mise à jour de l'utilisateur " + id + " dans ES", e);
    }

    mailService.sendUpdateAuthorizations(user.getInternal().getLogin(),
        Authority.valueOf(authorizations.getRole()).getValue());

    Map<String, Object> response = new HashMap<>();
    response.put("id", user.getId());
    response.put("authorizations", user.getAuthorizations());
    return response;
  }

  @Override
  public Map<String, String> update(String uuid, String name, String firstName, String mail, String oldPassword,
      String newPassword) throws ServiceException {

    Map<String, String> response = new HashMap<>();
    try {
      User esUser = get(uuid);

      String login = esUser.getInternal().getLogin();
      LdapAgorhaUser ldapUser = ldapService.getAgorhaUser(login);
      if (ldapUser == null) {
        throw new InternalServerErrorException(
            "Utilisateur " + login + " non trouvé dans ldap, désynchronisation avec ES");
      }

      // Si on change le mot de passe, on vérifie que l'ancien soit correct
      if (StringUtils.isNotEmpty(newPassword)) {
        checkCredentials(oldPassword, login, "user.oldPassword.invalid");
      }

      boolean mailModified = false;
      String oldlogin = esUser.getInternal().getLogin();

      if (!esUser.getInternal().getLogin().equals(mail)) {
        // Si on change le mail, on vérifie que le mot de passe soit correct
        checkCredentials(oldPassword, login, "user.password.invalid");
        if (ldapService.exists(mail)) {
          throw new BadRequestException(
              messageSource.getMessage("user.mail.already.exists", new Object[] { mail }, null));
        }
        // Mise à jour du mail dans ES
        esUser.getInternal().setLogin(mail);
        updateInternal(esUser);
        esUser = userService.createOrUpdateAndGet(esUser, true);
        try {
          // Création d'un nouveau compte ldap avec le nouveau mail et les anciennes info
          ldapUser = ldapService.create(oldPassword, mail, ldapUser.getName(), ldapUser.getFirstName());
        } catch (Exception e) {
          // rollback si erreur lors de la création dans ldap
          esUser.getInternal().setLogin(oldlogin);
          updateInternal(esUser);
          userService.createOrUpdateAndGet(esUser, true);
          throw new ServiceException("Erreur lors de la modification de l'utilisateur dans le ldap" + oldlogin, e);
        }
        mailModified = true;
      }

      ldapUser.setFirstName(firstName);
      ldapUser.setName(name);
      if (StringUtils.isNotEmpty(newPassword)) {
        ldapUser.setPassword(newPassword);
      } else {
        ldapUser.setPassword(null);
      }
      ldapUser.setNew(false);
      ldapService.update(ldapUser);

      if (mailModified) {
        ldapService.delete(oldlogin);
      }

      response.put("uuid", esUser.getInternal().getUuid());
      response.put("name", ldapUser.getName());
      response.put("firstName", ldapUser.getFirstName());
      response.put("mail", ldapUser.getMail());
    } catch (WebApplicationException e) {
      throw e;
    } catch (Exception e) {
      throw new ServiceException("Erreur lors de la modification de l'utilisateur " + mail, e);
    }
    return response;
  }

  private void checkCredentials(String oldPassword, String login, String errorMessageKey) {
    UsernamePasswordAuthenticationToken authenticationTokenRequest = new UsernamePasswordAuthenticationToken(login,
        oldPassword);
    try {
      this.authenticationManager.authenticate(authenticationTokenRequest);
    } catch (BadCredentialsException e) {
      throw new BadRequestException(messageSource.getMessage(errorMessageKey, new Object[] {}, null));
    }
  }

  @Override
  public User get(String uuid) {
    try {
      List<User> users = userService.list(QueryBuilders.matchQuery("internal.uuid", uuid), 1);
      if (users.size() != 1) {
        throw new NotFoundException(messageSource.getMessage("user.unknown", new Object[] { uuid }, null));
      }
      return users.get(0);
    } catch (EsRequestException e) {
      throw new InternalServerErrorException(ERROR_GET_USER + uuid, e);
    }
  }

  @Override
  public void delete(String uuid) throws ServiceException {
    User user = get(uuid);

    // Si l'utilisateur à supprimer n'est pas l'utilisateur courrant ou un
    // administrateur
    if (!user.getInternal().getLogin().equals(userInfoService.getCurrentUsername())
        && !userInfoService.hasAnyRole(Arrays.asList(Authority.ADMIN.name()))) {
      throw new NotAuthorizedException(messageSource.getMessage("user.insufficient.rights", new Object[] {}, null));
    }
    try {
      logger.info("Début suppression de l'utilisateur {}", uuid);

      userService.delete(user.getId(), RefreshPolicy.IMMEDIATE);
      ldapService.delete(user.getInternal().getLogin());

      logger.info("Utilisateur {} supprimé", uuid);

      // TODO supprimer les recherches enregistrées quand elles seront gérées
      String role = user.getAuthorizations().getRole();
      if (!role.equals(Authority.CONTRIB.name()) && !role.equals(Authority.DATABASE_MANAGER.name())) {
        selectionService.deleteByUser(uuid);
      }
    } catch (EsRequestException e) {
      throw new InternalServerErrorException("Erreur lors de la suppression de l'utilisateur " + uuid, e);
    }

  }

  @Override
  public void validate(String uuid, String token) throws ServiceException {
    try {
      User user = get(uuid);
      if (user == null) {
        throw new NotFoundException(messageSource.getMessage("user.unknown", new Object[] { uuid }, null));
      }
      if (!token.equals(user.getInternal().getValidateAccountToken())) {
        throw new BadRequestException(
            messageSource.getMessage("user.activation.link.invalid", new Object[] { uuid }, null));
      }

      user.getInternal().setValidateAccountToken(StringUtils.EMPTY);
      user.getInternal().setStatus(UserStatus.ACTIVE);

      userService.createOrUpdate(user, RefreshPolicy.IMMEDIATE);

    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de l'activation de l'utilisateur " + uuid, e);
    }
  }

  @Override
  public void resetPassword(String mail) throws ServiceException {
    try {
      List<User> users = userService.list(QueryBuilders.matchQuery("internal.login", mail), 1);
      if (users.isEmpty()) {
        return;
      }
      String token = UUID.randomUUID().toString();
      User user = users.get(0);
      mailService.sendResetPassword(mail, createResetPasswordLink(user.getInternal().getUuid(), token));

      user.getInternal().setResetPasswordExpiryDate(LocalDateTime.now().plusHours(24));
      user.getInternal().setResetPasswordToken(token);
      userService.createOrUpdate(user, RefreshPolicy.IMMEDIATE);
    } catch (EsRequestException e) {
      throw new ServiceException(ERROR_GET_USER + mail, e);
    }

  }

  @Override
  public boolean resetPasswordIsValid(String uuid, String token) {
    return resetPasswordIsValid(get(uuid), token);
  }

  private boolean resetPasswordIsValid(User user, String token) {
    if (!token.equals(user.getInternal().getResetPasswordToken())) {
      throw new BadRequestException(messageSource.getMessage("user.resetPassword.link.invalid", new Object[] {}, null));
    } else if (LocalDateTime.now().compareTo(user.getInternal().getResetPasswordExpiryDate()) > 1) {
      throw new BadRequestException(messageSource.getMessage("user.resetPassword.link.expired", new Object[] {}, null));
    }
    return true;
  }

  @Override
  public void updatePassword(String uuid, String newPassword, String token) throws ServiceException {
    User esUser = get(uuid);
    resetPasswordIsValid(esUser, token);

    String login = esUser.getInternal().getLogin();
    LdapAgorhaUser ldapUser = ldapService.getAgorhaUser(login);
    if (ldapUser == null) {
      throw new InternalServerErrorException(
          "Utilisateur " + login + " non trouvé dans ldap, désynchronisation avec ES");
    }
    ;
    ldapUser.setPassword(newPassword);
    ldapService.save(ldapUser);
    esUser.getInternal().setResetPasswordToken(StringUtils.EMPTY);
    esUser.getInternal().setResetPasswordExpiryDate(null);
    try {
      userService.createOrUpdate(esUser, RefreshPolicy.IMMEDIATE);
    } catch (EsRequestException e) {
      throw new InternalServerErrorException(
          "Erreur lors de la mise à jour du mot de passe pour l'utilisateur " + login, e);
    }

  }

  private String createResetPasswordLink(String userId, String token) {
    return resetPasswordUrl.replace("{userId}", userId).replace("{token}", token);
  }

  private String createValidationLink(String userId, String token) {
    return accountCreationValidationUrl.replace("{userId}", userId).replace("{token}", token);
  }

  private void updateInternal(User user) {
    user.getInternal().setUpdatedDate(LocalDateTime.now());
    user.getInternal().setUpdatedBy(userInfoService.getCurrentUsername());
  }

}
