package fr.inha.agorha2.service.impl;

import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import fr.inha.agorha2.api.dto.sort.SelectionSortType;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.SelectionService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.model.Selection;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.sort.ScriptSortBuilder.ScriptSortType;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class SelectionServiceImpl implements SelectionService {

  private static final Integer SELECTION_DEFAULT_PAGE = 1;
  private static final Integer SELECTION_DEFAULT_PAGE_SIZE = 5;

  @Autowired
  private EsClient esClient;

  @Autowired
  private UserInfoService userInfoService;

  @Autowired
  private MessageSource messageSource;

  @Override
  public Selection get(String id) throws ServiceException {
    try {
      Selection selection = esClient.getServiceSelection().get(id);
      if (selection == null) {
        throw new NotFoundException("Sélection inconnue");
      }
      selection.setId(id);
      return selection;
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de la récupération de la sélection " + id, e);
    }
  }

  @Override
  public Selection create(String title, List<String> noticeIds) throws ServiceException {

    Selection selection = new Selection();
    selection.setCreatedDate(LocalDateTime.now());
    selection.setUpdatedDate(LocalDateTime.now());
    selection.setUserId(userInfoService.getPrincipal().getUser().getInternal().getUuid());

    selection.setTitle(title);
    selection.getNotices().addAll(noticeIds);

    try {
      List<Selection> list = esClient.getServiceSelection()
          .list(QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("userId", selection.getUserId()))
              .must(QueryBuilders.matchQuery("title", selection.getTitle())), 1);
      if (!list.isEmpty()) {
        throw new BadRequestException(
            messageSource.getMessage("selection.title.already.exists", new Object[] { selection.getTitle() }, null));
      }
      return esClient.getServiceSelection().createOrUpdateAndGet(selection, true);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de la création de la sélection " + title, e);
    }
  }

  @Override
  public Selection add(String id, List<String> noticeIds) throws ServiceException {
    Selection selection = get(id);

    String currentUserId = userInfoService.getPrincipal().getUser().getInternal().getUuid();
    if (!selection.getUserId().equals(currentUserId)) {
      throw new NotAuthorizedException(messageSource.getMessage("user.insufficient.rights", new Object[] {}, null));
    }
    selection.getNotices().addAll(noticeIds);
    selection.setUpdatedDate(LocalDateTime.now());
    try {
      return esClient.getServiceSelection().createOrUpdateAndGet(selection, true);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de la mise à jour de la sélection " + id, e);
    }
  }

  @Override
  public Selection remove(String id, List<String> noticeIds) throws ServiceException {
    return remove(id, noticeIds, false);
  }

  @Override
  public Selection remove(String id, List<String> noticeIds, boolean forceRemove) throws ServiceException {
    Selection selection = get(id);

    String currentUserId = userInfoService.getPrincipal().getUser().getInternal().getUuid();
    if (!selection.getUserId().equals(currentUserId) && !forceRemove) {
      throw new NotAuthorizedException(messageSource.getMessage("user.insufficient.rights", new Object[] {}, null));
    }
    selection.getNotices().removeAll(noticeIds);
    selection.setUpdatedDate(LocalDateTime.now());
    try {
      return esClient.getServiceSelection().createOrUpdateAndGet(selection, true);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de la mise à jour de la sélection " + id, e);
    }
  }

  @Override
  public void delete(String id) throws ServiceException {
    try {
      Selection selection = get(id);
      if (!selection.getUserId().equals(userInfoService.getPrincipal().getUser().getInternal().getUuid())) {
        throw new NotAuthorizedException(messageSource.getMessage("user.insufficient.rights", new Object[] {}, null));
      }
      esClient.getServiceSelection().delete(id, RefreshPolicy.IMMEDIATE);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de la suppression de la sélection " + id, e);
    }
  }

  @Override
  public List<Selection> list(String terms, Integer page, Integer pageSize, String sort,
      @NotNull SelectionSortType sortType) throws ServiceException {
    String userUuid = userInfoService.getPrincipal().getUser().getInternal().getUuid();
    if (page == null) {
      page = SELECTION_DEFAULT_PAGE;
    }
    if (pageSize == null) {
      pageSize = SELECTION_DEFAULT_PAGE_SIZE;
    }
    try {
      BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
      MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("userId", userUuid);
      boolQuery.must(matchQuery);

      if (StringUtils.isNotEmpty(terms)) {
        boolQuery.must(QueryBuilders.simpleQueryStringQuery(terms).field("title.autocomplete"));
      }

      List<SortBuilder<?>> sortBuilders = new ArrayList<>();
      if (sortType.equals(SelectionSortType.SIZE)) {
        sortBuilders.add(SortBuilders.scriptSort(new Script("doc['notices'].length"), ScriptSortType.NUMBER)
            .order(SortOrder.fromString(sort)));
      } else {
        sortBuilders.add(EsQueryService.createFieldSortBuilder(sort, sortType.getField()));
      }
      return esClient.getServiceSelection().list(boolQuery, pageSize, page - 1, sortBuilders);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors du listing des sélections pour l'utilisateur" + userUuid, e);
    }
  }

  @Override
  public void deleteByUser(String uuid) throws ServiceException {
    try {
      List<Selection> selections = esClient.getServiceSelection().listAll(QueryBuilders.matchQuery("userId", uuid));
      for (Selection selection : selections) {
        delete(selection.getId());
      }
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de la suppression des selections liées à l'utilisateur " + uuid, e);
    }

  }

}
