package fr.inha.agorha2.service;

import fr.inha.elastic.model.NoticeType;

public interface ValidationService {

  public void validate(NoticeType noticeType, String json);

  public void validateDraft(NoticeType noticeType, String json);

}
