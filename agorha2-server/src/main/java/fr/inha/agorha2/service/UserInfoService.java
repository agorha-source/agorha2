package fr.inha.agorha2.service;

import fr.inha.agorha2.security.UserPrincipal;
import fr.inha.elastic.model.DatabaseAuthorization;
import java.util.List;

/**
 * Utilisé pour récupérer les informations de l'utilisateur courant.
 * 
 * @author broussot
 *
 */
public interface UserInfoService {

  public String getCurrentUsername();

  public boolean hasAnyRole(List<String> authorities);

  public List<DatabaseAuthorization> getAuthorizations();

  public boolean isAuthenticated();

  /**
   * Get the current {@link UserPrincipal}.
   * 
   * @return current user, null if none is authenticated
   */
  public UserPrincipal getPrincipal();

}
