package fr.inha.agorha2.service;

import fr.inha.agorha2.api.dto.sort.SavedSearchSortType;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.model.SavedSearch;
import java.util.List;
import org.elasticsearch.search.sort.SortOrder;

public interface SavedSearchService {

  SavedSearch get(String id) throws ServiceException;

  SavedSearch create(String name, List<String> resume, String url) throws ServiceException;

  void delete(String id) throws ServiceException;

  List<SavedSearch> list(String terms, Integer page, Integer pageSize, SortOrder sort, SavedSearchSortType sortType)
      throws ServiceException;

}
