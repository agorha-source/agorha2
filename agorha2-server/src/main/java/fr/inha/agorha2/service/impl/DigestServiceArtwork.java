package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.DigestService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.model.NoticeType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class DigestServiceArtwork extends AbstractDigestService implements DigestService {

  private static final String CREATION_FIELD_PATH = "/content/creationInformation/creation";

  @Override
  public NoticeType getType() {
    return NoticeType.ARTWORK;
  }

  @Override
  public void build(JsonNode notice) throws ServiceException {
    ObjectNode digestObject = getDigestObject(notice);

    buildCommonFields(notice, digestObject);

    digestObject.put("title",
        removeHtmlTags(notice.at("/content/identificationInformation/title/0/label/value").asText()));
    digestObject.set("artworkType",
        getObjectNode(notice.at("/content/identificationInformation/type/thesaurus/0/prefLabels/0/value")));

    digestObject.set("authorRole", getObjectNode(notice
        .at(JsonUtils.getPathToField(notice, CREATION_FIELD_PATH, "/personRole", "/thesaurus/0/prefLabels/0/value"))));

    String pathToDate = JsonUtils.getPathToField(notice, CREATION_FIELD_PATH, "/date", "");
    if (StringUtils.isNotEmpty(pathToDate)) {
      digestObject.set("datationExecutionStart", constructDate(pathToDate + "/start", notice));
      digestObject.set("datationExecutionEnd", constructDate(pathToDate + "/end", notice));
    }

    digestObject.set("place", getObjectNode(notice.at(JsonUtils.getPathToField(notice,
        "/content/localizationInformation/localization", "/place", "/thesaurus/prefLabels/0/value"))));

    String pathToIdentifier = JsonUtils.getPathToField(notice, "/content/localizationInformation/localization",
        "/institutionIdentifier", "");
    if (StringUtils.isNotEmpty(pathToIdentifier)) {
      digestObject.set("shelfMark", getObjectNode(notice.at(pathToIdentifier + "/value")));
      digestObject.set("shelfMarkType",
          getObjectNode(notice.at(pathToIdentifier + "Type/thesaurus/prefLabels/0/value")));
    }

    // Récupération des info de la personne
    String refPerson = notice.at(JsonUtils.getPathToField(notice, CREATION_FIELD_PATH, "/person/ref", "")).asText();
    if (!refPerson.isEmpty()) {
      PersonInfo personInfo = buildPersonDataFromUuid(refPerson);

      if (personInfo != null) {
        digestObject.set("usualName", personInfo.getName());
        digestObject.set("usualFirstName", personInfo.getFirstName());
        digestObject.set("startDate", personInfo.getStart());
        digestObject.set("endDate", personInfo.getEnd());
      }
    }
    digestObject.put("noticeType", getType().toString());

    digestObject.put("displayLabelLink", buildDisplayLabelLink(digestObject));
  }

  private String buildDisplayLabelLink(JsonNode digestObject) {
    StringBuilder sb = new StringBuilder();
    if (digestObject.get("usualName") != null) {
      sb.append(constructPersonTitle(digestObject.get("usualName"), digestObject.get("usualFirstName")));

      String datesAsString = DateUtils.buildDatesFormattedFromDigest(digestObject.get("startDate"),
          digestObject.get("endDate"));
      if (datesAsString != null) {
        sb.append(" ");
        sb.append(datesAsString);
      }

      addValueToDisplayLabel(digestObject.get("authorRole"), sb);
    }

    addValueToDisplayLabel(digestObject.get("title"), sb);
    addValueToDisplayLabel(digestObject.get("artworkType"), sb);

    String datesAsString = DateUtils.buildDatesFormattedFromDigest(digestObject.get("datationExecutionStart"),
        digestObject.get("datationExecutionEnd"));
    if (StringUtils.isNoneBlank(datesAsString)) {
      sb.append(" (");
      sb.append(datesAsString);
      sb.append(")");
    }

    return sb.toString();
  }
}
