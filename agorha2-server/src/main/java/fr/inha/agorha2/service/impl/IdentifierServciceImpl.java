package fr.inha.agorha2.service.impl;

import fr.inha.agorha2.service.IdentifierServcice;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class IdentifierServciceImpl implements IdentifierServcice {

  @Value("${resources.fullBaseUrl}")
  private String fullBaseUrl;

  @Override
  public String getArkIdentifier() {
    // TODO voir spec pour génération identifiant ark
    return UUID.randomUUID().toString();
  }

  @Override
  public String getPermalink(String uuid) {
    return fullBaseUrl + uuid;
  }

}
