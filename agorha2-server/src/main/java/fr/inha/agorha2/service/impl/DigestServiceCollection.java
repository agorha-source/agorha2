package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.DigestService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.model.NoticeType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class DigestServiceCollection extends AbstractDigestService implements DigestService {

  @Override
  public NoticeType getType() {
    return NoticeType.COLLECTION;
  }

  @Override
  public void build(JsonNode notice) throws ServiceException {
    ObjectNode digestObject = getDigestObject(notice);

    buildCommonFields(notice, digestObject);

    digestObject.put("title",
        removeHtmlTags(notice.at("/content/identificationInformation/identification/0/title/value").asText()));
    digestObject.set("collectionType", getObjectNode(notice.at(JsonUtils.getPathToField(notice,
        "/content/identificationInformation/identification", "/type/thesaurus", "/0/prefLabels/0/value"))));

    // Récupération des info de la personne
    String refPerson = notice
        .at(JsonUtils.getPathToField(notice, "/content/identificationInformation/relatedPerson", "/person/ref", ""))
        .asText();
    if (!refPerson.isEmpty()) {
      PersonInfo personInfo = buildPersonDataFromUuid(refPerson);

      if (personInfo != null) {
        digestObject.set("usualName", personInfo.getName());
        digestObject.set("usualFirstName", personInfo.getFirstName());
        digestObject.set("personStartDate", personInfo.getStart());
        digestObject.set("personEndDate", personInfo.getEnd());
      }
    }

    String pathToDate = JsonUtils.getPathToField(notice, "/content/identificationInformation/datePlace", "/date", "");
    if (StringUtils.isNotEmpty(pathToDate)) {
      digestObject.set("startDate", constructDate(pathToDate + "/start", notice));
      digestObject.set("endDate", constructDate(pathToDate + "/end", notice));
    }

    digestObject.put("noticeType", getType().toString());

    digestObject.put("displayLabelLink", buildDisplayLabelLink(digestObject));
  }

  private String buildDisplayLabelLink(JsonNode digestObject) {
    StringBuilder sb = new StringBuilder();
    addValueToDisplayLabel(digestObject.get("title"), sb);

    String datesAsString = DateUtils.buildDatesFormattedFromDigest(digestObject.get("startDate"),
        digestObject.get("endDate"));
    if (StringUtils.isNoneBlank(datesAsString)) {
      sb.append(" (");
      sb.append(datesAsString);
      sb.append(")");
    }

    return sb.toString();
  }
}
