package fr.inha.agorha2.service;

public enum NoticeDataSourceMode {
  BATCH,
  CONTRIBUTION,
  IMPORT;
}
