package fr.inha.agorha2.service;

import fr.inha.agorha2.exception.ServiceException;

public interface ImportService {

  public void importCsv() throws ServiceException;

  public void importMedia() throws ServiceException;

}
