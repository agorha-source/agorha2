package fr.inha.agorha2.service;

import fr.inha.agorha2.api.dto.AuthenticatedUser;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.model.Authorizations;
import fr.inha.elastic.model.User;
import java.util.Map;

public interface UserService {

  /**
   * Création d'un utilisateur.
   * 
   * @param name      nom de l'utilisateur
   * @param firstName prénom de l'utilisateur
   * @param mail      adresse mail de l'utilisateur
   * @param password  mot de passe de l'utilisateur
   * @return les informations de l'utilisateur nouvellement créé
   * @throws ServiceException si une erreur se produit
   */
  Map<String, String> create(String name, String firstName, String mail, String password) throws ServiceException;

  /**
   * Mise à jour des autorisations d'un utilisateur.
   * 
   * @param id             id de l'utilisateur
   * @param authorizations nouvelles autorisations
   * @return les informations des autorisations mises à jours
   * @throws ServiceException si une erreur se produit
   */
  Map<String, Object> update(String id, Authorizations authorizations) throws ServiceException;

  Map<String, String> update(String uuid, String name, String firstName, String mail, String oldPassword,
      String newPassword) throws ServiceException;

  User get(String uuid) throws ServiceException;

  AuthenticatedUser login(String login, String password);

  void delete(String uuid) throws ServiceException;

  void validate(String uuid, String token) throws ServiceException;

  void resetPassword(String mail) throws ServiceException;

  boolean resetPasswordIsValid(String uuid, String token);

  void updatePassword(String uuid, String newPassword, String token) throws ServiceException;

}
