package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.jsonldjava.core.JsonLdOptions;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.search.EsSearchParameters;

import de.digitalcollections.iiif.model.ImageContent;
import de.digitalcollections.iiif.model.image.ImageService;
import de.digitalcollections.iiif.model.jackson.IiifObjectMapper;
import de.digitalcollections.iiif.model.sharedcanvas.Canvas;
import de.digitalcollections.iiif.model.sharedcanvas.Manifest;
import de.digitalcollections.iiif.model.sharedcanvas.Sequence;

import fr.inha.agorha2.api.dto.Aggregation;
import fr.inha.agorha2.api.dto.ImageFormat;
import fr.inha.agorha2.api.dto.MediaRetrieveMode;
import fr.inha.agorha2.api.dto.OldNoticeType;
import fr.inha.agorha2.api.dto.response.MosaicCardResponse;
import fr.inha.agorha2.bean.ConceptSiecle;
import fr.inha.agorha2.bean.ThesaurusSiecle;
import fr.inha.agorha2.exception.FunctionnalException;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.service.DatabaseService;
import fr.inha.agorha2.service.DigestService;
import fr.inha.agorha2.service.IdentifierServcice;
import fr.inha.agorha2.service.MediaService;
import fr.inha.agorha2.service.MetaModelService;
import fr.inha.agorha2.service.NoticeDataSourceMode;
import fr.inha.agorha2.service.NoticeGetMode;
import fr.inha.agorha2.service.NoticeService;
import fr.inha.agorha2.service.SelectionService;
import fr.inha.agorha2.service.ThesaurusService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.agorha2.service.ValidationService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.agorha2.utils.DateUtils.PrefixType;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.agorha2.utils.MediaUtils;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.config.EsIndexEnum;
import fr.inha.elastic.model.Concept;
import fr.inha.elastic.model.Database;
import fr.inha.elastic.model.DatabaseAuthorization;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.MediaFile;
import fr.inha.elastic.model.MediaType;
import fr.inha.elastic.model.Notice;
import fr.inha.elastic.model.NoticeStatus;
import fr.inha.elastic.model.NoticeType;
import fr.inha.elastic.model.Selection;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.SimpleQueryStringBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class NoticeServiceImpl implements NoticeService {

  private static final String ELASTICSEARCH_INTERNAL_ERROR = "elasticsearch.internal.error";
  private static final String INTERNAL_SOURCE_NOTICE_UUID = "internal.sourceNoticeUuid";
  private static final String CONCEPT_PATH_FIELD = "conceptPath";
  private static final String GEO_POINT_FIELD = "geoPoint";
  private static final String INTERNAL_UUID_FIELD = "internal.uuid";
  private static final String INTERNAL_NOTICE_TYPE_FIELD = "internal.noticeType";
  private static final String INTERNAL_UNIQUE_KEY_FIELD = "internal.uniqueKey";
  private static final String INTERNAL_PATH = "/internal";
  private static final String ASSOCIATED_MEDIA_PATH = "/content/mediaInformation/associatedMedia";
  private static final String PREF_PICTURE_PATH = "/content/mediaInformation/prefPicture";
  private static final String DATABASE_LABEL_PATH = "/content/recordManagementInformation/databaseLabel";
  private static final String INTERNAL_UUID_PATH = "/internal/uuid";
  private static final String INTERNAL_STATUS_PATH = "/internal/status";
  private static final String INTERNAL_REFERENCES_PATH = "/internal/reference";
  private static final String INTERNAL_REFERENCES_FROM_CONTENT_PATH = "/internal/referenceFromContent";
  private static final String INTERNAL_DIGEST_DISPLAY_LABEL_LINK_PATH = "/internal/digest/displayLabelLink";
  private static final Logger log = LoggerFactory.getLogger(NoticeServiceImpl.class);
  private static final Logger logTalend = LoggerFactory.getLogger("talend-error");

  private ObjectMapper mapper = DefaultEsContext.instance().getObjectMapper();

  @Value("${media.iiif.manifests.path}")
  private String manifestStorePathAsName;

  @Value(value = "${media.iiif.manifests.baseUrl}")
  private String iiifManifestsBaseUrl;

  @Value(value = "${agorha2.batch.workDir}")
  private String batchWorkDir;

  @Value(value = "${agorha2.batch.workFile}")
  private String batchWorkFile;

  @Value(value = "${media.logoUrl}")
  private String logoUrl;

  @Value(value = "${agorha2.export.joai.path}")
  private String exportJoaiPath;

  @Value(value = "${media.baseApiUrl}")
  private String mediaBaseApiUrl;

  @Autowired
  private EsClient esClient;

  @Autowired
  private ValidationService validationService;

  @Autowired
  private MetaModelService metaModelService;

  @Autowired
  private IdentifierServcice identifierService;

  @Autowired
  private List<DigestService> digestServices;

  @Autowired
  private MediaService mediaService;

  @Autowired
  private ThesaurusService thesaurusService;

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private UserInfoService userInfoService;

  @Autowired
  private SelectionService selectionService;

  @Autowired
  private EsQueryService esQueryService;

  @Autowired
  private DatabaseService databaseService;

  @Value(value = "${media.iiif.images.apiUrl}")
  private String iiifImagesApiUrl;

  @Override
  public Notice create(String noticeAsJson) throws ServiceException {
    boolean isBatchUser = userInfoService.hasAnyRole(Arrays.asList(Authority.BATCH.name()));
    if (isBatchUser) {
      return create(noticeAsJson, NoticeDataSourceMode.BATCH);
    } else {
      return create(noticeAsJson, NoticeDataSourceMode.CONTRIBUTION);
    }
  }

  @Override
  public Notice create(String noticeAsJson, NoticeDataSourceMode dataSourceMode) throws ServiceException {
    JsonNode noticeNode;
    try {
      noticeNode = mapper.readTree(noticeAsJson);
      JsonUtils.removeEmptyNodes(noticeNode);
    } catch (IOException e) {
      throw new ServiceException("Impossible de déserializer l'objet", e);
    }

    NoticeType noticeType = getNoticeType(noticeNode);

    validationService.validateDraft(noticeType, noticeAsJson);

    verifyUniqueKey(noticeNode);

    String uuid = identifierService.getArkIdentifier();
    ((ObjectNode) noticeNode.at(INTERNAL_PATH)).put("uuid", uuid);
    ((ObjectNode) noticeNode.at(INTERNAL_PATH)).put("permalink", identifierService.getPermalink(uuid));

    // Pour le traitement Talend, on ne change pas les info d'audit ni le statut
    if (!dataSourceMode.equals(NoticeDataSourceMode.BATCH)) {
      updateAuditForCreation((ObjectNode) noticeNode.at(INTERNAL_PATH));
      // En mode contribution, on utilise le statut "En Cours"
      if (dataSourceMode.equals(NoticeDataSourceMode.CONTRIBUTION)) {
        ((ObjectNode) noticeNode.at(INTERNAL_PATH)).put("status", NoticeStatus.CREATION_IN_PROGRESS.getValue());
      }
    } else if (noticeNode.at(INTERNAL_STATUS_PATH).isMissingNode()) {
      log.warn("La notice {} n'a pas de status, attribution du statuts \"En cours\"", uuid);
      ((ObjectNode) noticeNode.at(INTERNAL_PATH)).put("status", NoticeStatus.CREATION_IN_PROGRESS.getValue());
    }

    updateData(noticeNode, noticeType, dataSourceMode);

    Notice notice;
    try {
      notice = mapper.treeToValue(noticeNode, Notice.class);
    } catch (JsonProcessingException e) {
      throw new ServiceException("Erreur lors de la création de la notice", e);
    }
    return save(notice, noticeType, null, null, dataSourceMode);
  }

  @Override
  public void delete(String id) throws ServiceException {
    Notice notice = get(id);

    List<String> referenceBy = getReferenceBy(notice.getInternal().getUuid());
    if (!referenceBy.isEmpty()) {
      throw new ForbiddenException(
          messageSource.getMessage("notice.delete.references.not.empty", new Object[] {}, null));
    }

    try {
      deleteNotice(notice);
      deleteNoticeInSelection(id);
      deleteNoticeInMedia(id);
      deleteNoticeInLinkedNotice(id);
      deleteNoticeInJoai(id);
    } catch (EsRequestException | IOException e) {
      throw new ServiceException("Erreur de lors de la suppression de la notice " + id, e);
    }
  }

  @Override
  public List<Map<String, String>> upddateStatus(List<String> uuids, NoticeStatus targetStatus)
      throws ServiceException {

    boolean isAdmin = userInfoService.hasAnyRole(Arrays.asList(Authority.ADMIN.name()));
    boolean isDatabaseManager = userInfoService.hasAnyRole(Arrays.asList(Authority.DATABASE_MANAGER.name()));

    if (targetStatus.equals(NoticeStatus.PUBLISHED) && !isAdmin) {
      throw new NotAuthorizedException(messageSource.getMessage("user.insufficient.rights", new Object[] {}, null));
    }

    List<DatabaseAuthorization> authorizations = userInfoService.getAuthorizations();

    List<Map<String, String>> errors = new ArrayList<>();

    for (String uuid : uuids) {
      try {
        Notice notice = get(uuid);
        String currentStatus = notice.getInternal().getStatus();
        Notice originalNotice = null;
        String sourceNoticeUuid = notice.getInternal().getSourceNoticeUuid();
        boolean mustDeleteOriginal = targetStatus.equals(NoticeStatus.PUBLISHED) && sourceNoticeUuid != null;
        if (mustDeleteOriginal) {
          originalNotice = get(sourceNoticeUuid);
          notice.getInternal().getReference().addAll(originalNotice.getInternal().getReference());
          notice.getInternal().getReferenceFromContent().addAll(originalNotice.getInternal().getReferenceFromContent());
        }
        JsonNode noticeNode = mapper.valueToTree(notice);

        log.info("Update Status {} - Vérification des droits et du status", uuid);
        // vérification des droits sur les bases de données
        verifyDatabaseAuthorization(uuid, targetStatus, isDatabaseManager, authorizations, currentStatus, noticeNode,
            errors);
        verifyNoticeStatus(uuid, currentStatus, targetStatus, errors, isAdmin, isDatabaseManager);

        ObjectNode internal = (ObjectNode) noticeNode.at(INTERNAL_PATH);
        internal.put("status", targetStatus.getValue());
        internal.put("statusUpdatedDate", LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        JsonNode digestNode = internal.get("digest");
        if (digestNode != null) {
          ((ObjectNode) digestNode).put("status", targetStatus.getValue());
        }

        // Dans le cas d'une publication
        if (targetStatus.equals(NoticeStatus.PUBLISHED)) {
          log.info("Update Status {} - Validation de la notice pour publication", uuid);
          // Vérification des champs obligatoire
          validationService.validate(notice.getInternal().getNoticeType(), noticeNode.toString());

          // Contrôle des BDD liées qui doivent être publiées
          verifyLinkedDatabaseStatus(errors, uuid, noticeNode);

          // Lors d'une publication, si la notice à été créé à partir d'une autre notice
          // publiée, on utilise l'uuid de la notice publiée (permet la continuité des
          // identifiants ark et des relations entre notices)
          if (sourceNoticeUuid != null) {
            log.info(
                "Update Status {} - Notice issue d'une notice déjà publiée, utilisation de l'uuid de la notice déjà publiée {}",
                uuid, sourceNoticeUuid);
            internal.put("uuid", sourceNoticeUuid);
            internal.put("permalink", identifierService.getPermalink(sourceNoticeUuid));
          }
          updateRefs(noticeNode, NoticeDataSourceMode.CONTRIBUTION);
        } else if (NoticeStatus.PUBLISHED.getValue().equals(currentStatus)
            && !targetStatus.equals(NoticeStatus.PUBLISHED)) {
          // Cas d'une dépublication
          // Suppression des liens existants vers cette notice dans les notices liées
          for (String ref : notice.getInternal().getReferenceFromContent()) {
            removeFromLinkedNotice(ref, uuid, NoticeDataSourceMode.CONTRIBUTION);
          }
          deleteNoticeInJoai(uuid);
        }

        notice = mapper.treeToValue(noticeNode, Notice.class);
        if (mustDeleteOriginal) {
          notice.getInternal().setSourceNoticeUuid(null);
        }
        save(notice, notice.getInternal().getNoticeType(), null, null, NoticeDataSourceMode.CONTRIBUTION);

        // Lors d'une publication, si la notice à été créé à partir d'une autre notice,
        // on supprime l'ancienne notice publiée car elle va être remplacée par la
        // notice nouvellement publiée
        if (mustDeleteOriginal) {
          log.info("Update Status {} - Suppression de la notice originale déjà publiée {}", uuid,
              originalNotice.getInternal().getUuid());
          deleteNotice(originalNotice);
        }
      } catch (FunctionnalException e) {
        log.error(e.getMessage());
      } catch (Exception e) {
        HashMap<String, String> errorDetail = new HashMap<>();
        errorDetail.put("uuid", uuid);
        errorDetail.put("msg", e.getMessage());
        errors.add(errorDetail);
      }
    }
    return errors;
  }

  @Override
  public Notice update(String uuid, String noticeAsJson) throws ServiceException {
    boolean isBatchUser = userInfoService.hasAnyRole(Arrays.asList(Authority.BATCH.name()));
    if (isBatchUser) {
      return update(uuid, noticeAsJson, NoticeDataSourceMode.BATCH);
    } else {
      return update(uuid, noticeAsJson, NoticeDataSourceMode.CONTRIBUTION);
    }

  }

  @Override
  public Notice update(String uuid, String noticeAsJson, NoticeDataSourceMode dataSourceMode) throws ServiceException {
    log.info("Mise à jour de la notice {}", uuid);
    JsonNode noticeNode;
    try {
      noticeNode = mapper.readTree(noticeAsJson);
      JsonUtils.removeEmptyNodes(noticeNode);
    } catch (IOException e) {
      throw new ServiceException("Impossible de déserializer l'objet", e);
    }

	NoticeType noticeType = getNoticeType(noticeNode);

    // Si une notice à déjà été créée suite à une modification d'une notice publiée,
    // on bloque la mise à jour de la notice publiée
	Notice createdFrom = getCreatedFrom(uuid, noticeType);
    if (createdFrom != null && !NoticeDataSourceMode.BATCH.equals(dataSourceMode)) {
      throw new BadRequestException(messageSource.getMessage("notice.update.wip.impossible",
          new Object[] { createdFrom.getInternal().getUuid() }, null));
    }

    // Récupération de la version actuelle de la notice
    Notice existingNotice = get(uuid);

    Long seqNo = null;
    Long primaryTerm = null;
    String status = noticeNode.at(INTERNAL_STATUS_PATH).asText();
    ((ObjectNode) noticeNode).set("internal", mapper.valueToTree(existingNotice.getInternal()));
    ObjectNode internal = (ObjectNode) noticeNode.at(INTERNAL_PATH);

    if (StringUtils.isBlank(existingNotice.getInternal().getStatus())) {
      log.warn("La notice {} n'a pas de status, attribution du statuts \"En cours\"",
          existingNotice.getInternal().getUuid());
      existingNotice.getInternal().setStatus(NoticeStatus.CREATION_IN_PROGRESS.getValue());
      internal.put("status", NoticeStatus.CREATION_IN_PROGRESS.getValue());
      status = NoticeStatus.CREATION_IN_PROGRESS.getValue();
    }

    if (existingNotice.getInternal().getStatus().equals(NoticeStatus.ARCHIVED.getValue())) {
      // On ne peut pas mettre à jour une notice archivée
      throw new BadRequestException(
          messageSource.getMessage("notice.update.archived.impossible", new Object[] {}, null));
    } else if (existingNotice.getInternal().getStatus().equals(NoticeStatus.PUBLISHED.getValue())
        && dataSourceMode.equals(NoticeDataSourceMode.CONTRIBUTION)) {
      // Si la notice est publiée, on en créé une copie avec un nouvel uuid
      ((ObjectNode) noticeNode).set("id", null);
      internal.put("status", NoticeStatus.CREATION_IN_PROGRESS.getValue());
      String newUUid = UUID.randomUUID().toString();
      internal.put("uuid", newUUid);
      internal.put("permalink", identifierService.getPermalink(newUUid));
      internal.put("sourceNoticeUuid", uuid);
    } else {
      // Si la notice n'est ni publiée ni archivée on utilise l'id de la version
      // existante pour qu'elle soit mise à jour
      ((ObjectNode) noticeNode).put("id", existingNotice.getId());

      // Si on est utilisateur batch, ou que l'on est en mode import en masse on prend
      // le statut demandé, pas celui existant
      if (!dataSourceMode.equals(NoticeDataSourceMode.CONTRIBUTION)) {
        internal.put("status", status);
      }
      internal.put("permalink", identifierService.getPermalink(uuid));
      seqNo = existingNotice.getSeqNo();
      primaryTerm = existingNotice.getPrimaryTerm();
    }


    validationService.validateDraft(noticeType, noticeAsJson);

    updateAuditForUpdate(internal);

    updateData(noticeNode, noticeType, dataSourceMode);

    Notice notice;
    try {
      notice = mapper.treeToValue(noticeNode, Notice.class);
    } catch (JsonProcessingException e) {
      throw new ServiceException("Erreur lors de la création de la notice", e);
    }

    return save(notice, noticeType, seqNo, primaryTerm, dataSourceMode);
  }

  @Override
  public Notice duplicate(String noticeId, String databaseId) throws ServiceException {
    Notice notice = get(noticeId);
    notice.setPrimaryTerm(0L);
    notice.setSeqNo(0L);
    notice.setVersion(0L);
    notice.setId(null);

    LocalDateTime now = LocalDateTime.now();
    String currentUsername = userInfoService.getCurrentUsername();
    notice.getInternal().setCreatedBy(currentUsername);
    notice.getInternal().getDigest().put("createdby", currentUsername);
    notice.getInternal().setCreatedDate(now);

    notice.getInternal().setUpdatedBy(currentUsername);
    notice.getInternal().setUpdatedDate(now);
    notice.getInternal().getDigest().put("updatedDate", now);

    notice.getInternal().setStatus(NoticeStatus.CREATION_IN_PROGRESS.getValue());
    notice.getInternal().getDigest().put("status", NoticeStatus.CREATION_IN_PROGRESS.getValue());

    String uuid = UUID.randomUUID().toString();
    notice.getInternal().setUuid(uuid);
    notice.getInternal().setPermalink(identifierService.getPermalink(uuid));

    // On ne conserve pas le lien vers la notice originale si la notice dupliquée
    // est issue d'une modification de notice publiée
    notice.getInternal().setSourceNoticeUuid(null);

    ArrayNode databasesNode = mapper.createArrayNode();
    ObjectNode databaseNode = mapper.createObjectNode();

    Database database = databaseService.get(Integer.valueOf(databaseId));

    databaseNode.put("ref", database.getInternal().getUuid());
    databaseNode.put("value", database.getContent().getTitle());
    databasesNode.add(databaseNode);

    JsonNode noticeNode = mapper.valueToTree(notice);
    ((ObjectNode) noticeNode.at("/content/recordManagementInformation")).set("databaseLabel", databasesNode);

    List<JsonNode> sourcingParents = noticeNode.findParents("sourcing");

    // APDT-903 la BDD original dans le sourcing doit être remplacé par la nouvelle
    // BDD en mode duplication
    for (JsonNode sourcingParent : sourcingParents) {
      sourcingParent.findValues("database").stream().map(node -> (ObjectNode) node).forEach(node -> {
        node.put("ref", database.getInternal().getUuid());
        node.put("value", database.getContent().getTitle());
      });
    }

    ArrayNode digestDatabases = (ArrayNode) noticeNode.at("/internal/digest/databases");
    digestDatabases.removeAll();
    digestDatabases.add(database.getContent().getTitle());

    updateMediaInfo(noticeNode);
    try {
      notice = mapper.treeToValue(noticeNode, Notice.class);
    } catch (JsonProcessingException e) {
      throw new ServiceException(e);
    }

    return save(notice, notice.getInternal().getNoticeType(), null, null, NoticeDataSourceMode.CONTRIBUTION);
  }

  @Override
  public Notice get(String noticeId) throws ServiceException {
    return get(noticeId, NoticeGetMode.DEFAULT);
  }

  @Override
  public Notice get(String noticeId, NoticeGetMode mode) throws ServiceException {
    List<String> includes = null;
    List<String> excludes = null;

    if (mode.equals(NoticeGetMode.CONSULTATION)) {
      // On n'affiche pas les informations des media en consultation sauf le
      // commentaire (content.mediaInformation.associatedMedia.comment)
      excludes = Arrays.asList("content.mediaInformation.prefPicture.*",
          "content.mediaInformation.associatedMedia.sourcing", "content.mediaInformation.associatedMedia.ref",
          "content.mediaInformation.associatedMedia.ocr", "content.mediaInformation.associatedMedia.digest",
          "content.mediaInformation.associatedMedia.internalMetadata");
    }

    List<Notice> notice = null;
    try {
      notice = esClient.getServiceNotice().listAll(
          QueryBuilders.multiMatchQuery(noticeId, INTERNAL_UUID_FIELD, "internal.dcIdentifier"), includes, excludes);
    } catch (EsRequestException e) {
      throw new ServiceException(messageSource.getMessage(ELASTICSEARCH_INTERNAL_ERROR, new Object[] {}, null), e);
    }
    if (CollectionUtils.isEmpty(notice)) {
      throw new NotFoundException("Identifiant de notice inconnu : " + noticeId);
    } else if (notice.size() != 1) {
      throw new NotFoundException("Plusieurs notice trouvé pour l'identifiant : " + noticeId);
    }
    return notice.get(0);
  }

  @Override
  public List<MosaicCardResponse> getMosaicCards(List<String> noticeIds, NoticeGetMode mode) throws ServiceException {
    List<MosaicCardResponse> results = new ArrayList<>();

    if (mode.equals(NoticeGetMode.MOSAIC_CARDS)) {
      try {
        EsSearchParameters aggregationParameters = new EsSearchParameters();
        aggregationParameters.setIndexes(List.of(EsIndexEnum.NOTICE));
        aggregationParameters.setQuery(QueryBuilders.termsQuery(INTERNAL_UUID_FIELD, noticeIds));
        aggregationParameters.setAggregations(new ArrayList<>());
        Aggregation aggregation = new Aggregation();
        aggregation.setField(INTERNAL_NOTICE_TYPE_FIELD);
        ImmutablePair<String, Aggregation> agg = ImmutablePair.of("Notices par type", aggregation);
        // Aggregation sur les termes
        aggregationParameters.getAggregations().add(esQueryService.constructAggregation(agg));

        List<EsSearchParameters> multiSearchParameters = new ArrayList<>();
        multiSearchParameters.add(aggregationParameters);

        NoticeType[] noticeTypes = new NoticeType[] { NoticeType.ARTWORK, NoticeType.COLLECTION, NoticeType.EVENT,
            NoticeType.PERSON, NoticeType.REFERENCE };
        List<String> includes = Arrays.asList("internal.*", "content.mediaInformation.prefPicture",
            "content.mediaInformation.associatedMedia.ref");
        for (NoticeType type : noticeTypes) {
          EsSearchParameters searchParameters = new EsSearchParameters();
          searchParameters.setIndexes(List.of(EsIndexEnum.NOTICE));
          BoolQueryBuilder query = QueryBuilders.boolQuery()
              .must(QueryBuilders.termsQuery(INTERNAL_UUID_FIELD, noticeIds))
              .must(QueryBuilders.termQuery(INTERNAL_NOTICE_TYPE_FIELD, type));
          searchParameters.setQuery(query);
          searchParameters.setSize(50);
          searchParameters.setSourceInclude(includes);
          multiSearchParameters.add(searchParameters);
        }
        // Multi-recherche, les résultats sont renvoyés dans l'ordre demandé
        // (aggregation en première)
        MultiSearchResponse.Item[] items = esClient.getServiceGeneral().multiSearch(multiSearchParameters);

        // 1 seul résultat = aucun résultat sur les recherches par type de notice
        if (items.length > 1) {
          SearchResponse searchResponse = items[0].getResponse();
          MultiBucketsAggregation bucketsAggregation = (MultiBucketsAggregation) searchResponse.getAggregations()
              .getAsMap().get(agg.getKey());
          List<? extends MultiBucketsAggregation.Bucket> buckets = bucketsAggregation.getBuckets();
          int total = buckets.stream().mapToInt(a -> (int) a.getDocCount()).sum();

          // Calcul du nombre de résultats max par type de notice, en gardant la
          // représentativité des résultats totaux
          // Calcul basé sur 50 notices maximum (peut-être un peu plus dû à la fonction
          // Math.ceil() qui assure qu'au moins une notice de chaque type présent dans le
          // résultat global soit renvoyée)
          Map<NoticeType, Integer> nbMaxResultsPerType = new EnumMap<>(NoticeType.class);
          for (MultiBucketsAggregation.Bucket bucket : buckets) {
            nbMaxResultsPerType.put(NoticeType.valueOf(bucket.getKeyAsString()),
                (int) Math.ceil(50.0 * bucket.getDocCount() / total));
          }

          // Map pour le nombre de notices de chaque type déjà lues
          Map<NoticeType, Integer> nbResultsPerType = new EnumMap<>(NoticeType.class);
          nbResultsPerType.put(NoticeType.ARTWORK, 0);
          nbResultsPerType.put(NoticeType.COLLECTION, 0);
          nbResultsPerType.put(NoticeType.EVENT, 0);
          nbResultsPerType.put(NoticeType.PERSON, 0);
          nbResultsPerType.put(NoticeType.REFERENCE, 0);

          for (int i = 1; i < items.length; i++) {
            SearchHits hits = items[i].getResponse().getHits();
            for (SearchHit hit : hits) {
              MosaicCardResponse result = new MosaicCardResponse();
              JsonNode noticeNode = mapper.readTree(hit.getSourceAsString());
              Notice notice = mapper.treeToValue(noticeNode, Notice.class);

              NoticeType noticeType = notice.getInternal().getNoticeType();
              Integer nbResult = nbResultsPerType.get(noticeType);
              // Contrôle du nombre de notices déjà lues pour un type donné
              if (nbResult >= nbMaxResultsPerType.get(noticeType)) {
                continue;
              }
              nbResultsPerType.put(noticeType, ++nbResult);

              result.setNotice(notice);
              List<String> mediaIds = JsonUtils.getMediaIds(noticeNode, ASSOCIATED_MEDIA_PATH);
              List<Media> medias = getMedia(mediaIds, noticeNode.at(PREF_PICTURE_PATH), MediaRetrieveMode.THUMBNAILS);
              if (!medias.isEmpty()) {
                result.getMedias().add(medias.get(0));
              }
              results.add(result);
            }
          }
        }
      } catch (NullPointerException | EsRequestException e) {
        throw new ServiceException(messageSource.getMessage(ELASTICSEARCH_INTERNAL_ERROR, new Object[] {}, null), e);
      } catch (JsonProcessingException e) {
        throw new ServiceException("Erreur lors du parsing des notices", e);
      }
    }
    return results;
  }

  @Override
  public String getJsonLd(String noticeId) throws ServiceException {
    Notice notice = get(noticeId);
    String status = notice.getInternal().getStatus();
    if (!status.equals(NoticeStatus.PUBLISHED.getValue()) && (!userInfoService.isAuthenticated()
        || userInfoService.hasAnyRole(Arrays.asList(Authority.DEFAULT.name())))) {
      throw new NotAuthorizedException(messageSource.getMessage("user.insufficient.rights", new Object[] {}, null));
    }
    try {
      JsonNode noticeJson = mapper.valueToTree(notice);
      ObjectNode jsonLdNode = JsonUtils.convertNoticeToJsonLd(
          mapper.readTree(metaModelService.get(null, notice.getInternal().getNoticeType())), noticeJson);

      createPermalinkFromUUid(jsonLdNode,
          noticeJson.at("/content/mediaInformation/associatedMedia").findValuesAsText("ref"));

      Map<String, String> context = new HashMap<>();
      context.put("schema", "http://schema.org/");
      context.put("dc", "http://purl.org/dc/terms/");
      context.put("crm", "http://www.cidoc-crm.org/cidoc-crm/");
      context.put("rdfs", "https://www.w3.org/TR/rdf-schema/");
      context.put("prov", "http://www.w3.org/ns/prov#");

      Map<String, Object> compact = JsonLdProcessor.compact(
          com.github.jsonldjava.utils.JsonUtils.fromString(jsonLdNode.toString()), context, new JsonLdOptions());

      return com.github.jsonldjava.utils.JsonUtils.toPrettyString(compact);
    } catch (IllegalArgumentException | IOException e) {
      throw new ServiceException("Erreur lors de la transformation en json-ld", e);
    }
  }

  @Override
  public String getRdf(String noticeId, String format) throws ServiceException {
    String jsonLd = getJsonLd(noticeId);
    Model model = ModelFactory.createDefaultModel();
    model.read(new StringReader(jsonLd), null, RDFLanguages.JSONLD.getLabel());

    StringWriter writer = new StringWriter();
    RDFDataMgr.write(writer, model, RDFLanguages.nameToLang(format));
    return writer.toString();
  }

  @Override
  public Map<String, String> getPrefPicture(String noticeId) throws ServiceException {
    Notice notice = get(noticeId);
    JsonNode noticeNode = mapper.valueToTree(notice);
    JsonNode prefPicture = noticeNode.at("/content/mediaInformation/prefPicture");
    if (prefPicture.isMissingNode()) {
      return new HashMap<>();
    }
    return mediaService.getFile(prefPicture.get("idMedia").asText(), prefPicture.get("idFile").asText(), ImageFormat.S);
  }

  @Override
  public List<Media> getMedia(String noticeId, MediaRetrieveMode mode) throws ServiceException {
    Notice notice = get(noticeId);
    JsonNode noticeNode = mapper.valueToTree(notice);
    List<String> mediaIds = JsonUtils.getMediaIds(noticeNode, ASSOCIATED_MEDIA_PATH);
    if (!mediaIds.isEmpty()) {
      List<Media> medias = mediaService.getMedia(mediaIds, mode);
      JsonNode prefPictureNode = noticeNode.at(PREF_PICTURE_PATH);
      if (!medias.isEmpty()) {
        putPrefPictureFirst(prefPictureNode, medias);
      }
      return medias;
    }
    return new ArrayList<>();

  }

  @Override
  public List<Media> getMedia(List<String> mediaIds, JsonNode prefPictureNode, MediaRetrieveMode mode)
      throws ServiceException {
    if (!mediaIds.isEmpty()) {
      List<Media> medias = mediaService.getMedia(mediaIds, mode);
      if (!medias.isEmpty()) {
        putPrefPictureFirst(prefPictureNode, medias);
      }
      return medias;
    }
    return new ArrayList<>();

  }

  @Override
  public List<String> getReferenceBy(String noticeId) throws ServiceException {
    try {
      EsSearchParameters parameters = new EsSearchParameters();
      parameters.setIndexes(Arrays.asList(EsIndexEnum.NOTICE));
      // Les notices référençant la notice en paramètre doivent avec son id dans le
      // champ reference et dans un champ ref de son contenu
      BoolQueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("internal.reference", noticeId))
          .must(QueryBuilders.queryStringQuery(noticeId).field("content.*.ref"));

      // Filtre sur le status publié pour les utilisateurs non admin
      esQueryService.filterStatus(query);

      parameters.setQuery(query);
      parameters.setSourceInclude(Arrays.asList(INTERNAL_UUID_FIELD));
      SearchResponse response = esClient.getServiceGeneral().search(parameters);

      List<String> uuids = new ArrayList<>();
      for (SearchHit hit : response.getHits()) {
        JsonNode source = mapper.readTree(hit.getSourceAsString());
        uuids.add(source.at(INTERNAL_UUID_PATH).asText());
      }

      return uuids;
    } catch (EsRequestException | IOException e) {
      throw new ServiceException(e);
    }
  }

  @Override
  public void addNews(String selectionId, String newsUrl, String newsTitle) throws ServiceException {
    Selection selection = selectionService.get(selectionId);
    for (String noticeUuid : selection.getNotices()) {
      Notice notice = get(noticeUuid);
      JsonNode noticeNode = mapper.valueToTree(notice);
      JsonNode recordManagementInformation = noticeNode.at("/content/recordManagementInformation");
      if (recordManagementInformation.isMissingNode()) {
        recordManagementInformation = mapper.createObjectNode();
        ((ObjectNode) noticeNode.at("/content")).set("recordManagementInformation", recordManagementInformation);
      }
      JsonNode newsLabel = recordManagementInformation.at("/newsLabel");
      if (newsLabel.isMissingNode()) {
        newsLabel = mapper.createArrayNode();
        ((ObjectNode) recordManagementInformation).set("newsLabel", newsLabel);
      }

      for (Iterator<JsonNode> iterator = newsLabel.iterator(); iterator.hasNext();) {
        JsonNode node = iterator.next();
        if (newsUrl.equals(node.at("/ref").asText())) {
          iterator.remove();
        }
      }

      ObjectNode news = mapper.createObjectNode();
      news.put("ref", newsUrl);
      news.put("value", newsTitle);
      ((ArrayNode) newsLabel).add(news);

      try {
        notice = mapper.treeToValue(noticeNode, Notice.class);
      } catch (JsonProcessingException e) {
        throw new ServiceException(e);
      }

      save(notice, notice.getInternal().getNoticeType(), notice.getSeqNo(), notice.getPrimaryTerm(),
          NoticeDataSourceMode.CONTRIBUTION);
    }
  }

  @Override
  public void removeNews(String selectionId, String newsUrl) throws ServiceException {
    Selection selection;
    if (StringUtils.isBlank(selectionId)) {
      selection = getSelectionFromNews(newsUrl);
    } else {
      selection = selectionService.get(selectionId);
    }
    for (String noticeUuid : selection.getNotices()) {
      Notice notice = get(noticeUuid);
      JsonNode noticeNode = mapper.valueToTree(notice);
      ArrayNode newsLabel = (ArrayNode) noticeNode.at("/content/recordManagementInformation/newsLabel");

      for (Iterator<JsonNode> iterator = newsLabel.iterator(); iterator.hasNext();) {
        JsonNode node = iterator.next();
        if (newsUrl.equals(node.at("/ref").asText())) {
          iterator.remove();
        }
      }

      try {
        notice = mapper.treeToValue(noticeNode, Notice.class);
      } catch (JsonProcessingException e) {
        throw new ServiceException(e);
      }

      save(notice, notice.getInternal().getNoticeType(), notice.getSeqNo(), notice.getPrimaryTerm(),
          NoticeDataSourceMode.CONTRIBUTION);
    }

  }

  @SuppressWarnings("unchecked")
  @Override
  public String getUuid(String uniqueKey, OldNoticeType oldNoticeType) throws FunctionnalException, ServiceException {
    try {
      if (oldNoticeType.getType() != null) {
        int uniqueKeyAsInt = Integer.parseInt(uniqueKey);
        if (oldNoticeType.equals(OldNoticeType.BIEN)) {
          uniqueKeyAsInt += 1000000;
        } else if (oldNoticeType.equals(OldNoticeType.EDIFICE)) {
          uniqueKeyAsInt += 2000000;
        } else if (oldNoticeType.equals(OldNoticeType.BIBLIO)) {
          uniqueKeyAsInt += 3000000;
        } else if (oldNoticeType.equals(OldNoticeType.REFARCHIVE)) {
          uniqueKeyAsInt += 4000000;
        }
        NoticeType noticeType = oldNoticeType.getType();
        SearchResponse searchResponse = esClient.getServiceGeneral()
            .search(esQueryService.createSearchParametersForUniqueKey(uniqueKeyAsInt, noticeType));
        if (searchResponse.getHits().getTotalHits().value > 1) {
          log.warn("Plusieurs notices trouvées pour la uniqueKey {} et le code {}", uniqueKey, oldNoticeType.getCode());
        } else if (searchResponse.getHits().getTotalHits().value == 1) {
          return ((Map<String, String>) searchResponse.getHits().getAt(0).getSourceAsMap().get("internal")).get("uuid");
        }
      }
      throw new FunctionnalException(
          "Pas de notice trouvé pour la uniqueKey " + uniqueKey + " et le code " + oldNoticeType.getCode());
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur interne", e);
    }
  }

  /**
   * Permet de convertir les uuid des notices et des médias liés en permalink dans
   * une notice au format jsonLD. Se base sur les champ "@id" du jsonLD.
   * 
   * @param jsonLdNode jsonld dans lequel effectuer les remplacements
   */
  private void createPermalinkFromUUid(ObjectNode jsonLdNode, List<String> mediaIds) {
    for (JsonNode node : jsonLdNode.findParents("@id")) {
      try {
        String id = node.get("@id").asText();
        if (id.startsWith("/ark:/54721") && id.contains("thumbnail")) {
          ((ObjectNode) node).put("@id", identifierService.getPermalink(id.replace("/ark:/54721/", "")));
        } else {
          UUID.fromString(id);
          if (mediaIds.contains(id)) {
            ((ObjectNode) node).put("@id", URI.create(mediaBaseApiUrl + "/").resolve(id).toString());
          } else {
            ((ObjectNode) node).put("@id", identifierService.getPermalink(id));
          }
        }
      } catch (IllegalArgumentException e) {
        // Rien à faire si pas un UUID
      }
    }
  }

  /**
   * Si le champ prefPicture existe dans le json de la notice, place le media
   * associé en première position, et le fichier associé en première position dans
   * le media.
   * 
   * @param prefPictureNode json de la notice
   * @param medias          liste des medias de la notice
   */
  private void putPrefPictureFirst(JsonNode prefPictureNode, List<Media> medias) {
    if (prefPictureNode.isMissingNode()) {
      return;
    }
    String idMedia = prefPictureNode.get("idMedia").asText();
    String idFile = prefPictureNode.get("idFile").asText();
    Media prefMedia = medias.get(0);
    for (Iterator<Media> iterator = medias.iterator(); iterator.hasNext();) {
      Media media = iterator.next();
      if (media.getInternal().getUuid().equals(idMedia)) {
        prefMedia = MediaUtils.copy(media);
        MediaFile prefFile = prefMedia.getFiles().get(0);
        if (prefMedia.getFiles().size() == 1) {
          iterator.remove();
        }
        prefMedia.getFiles().clear();
        for (Iterator<MediaFile> iteratorFiles = media.getFiles().iterator(); iteratorFiles.hasNext();) {
          MediaFile mediaFile = iteratorFiles.next();
          if (mediaFile.getUuid().equals(idFile)) {
            prefFile = mediaFile;
            iteratorFiles.remove();
          }
        }
        prefMedia.getFiles().add(prefFile);
      }
    }
    medias.add(0, prefMedia);

  }

  private Selection getSelectionFromNews(String newsUrl) throws ServiceException {
    Selection selection;
    EsSearchParameters parameters = new EsSearchParameters();
    parameters.setIndexes(Arrays.asList(EsIndexEnum.NEWS));
    parameters.setQuery(QueryBuilders.matchQuery("internal.digest.permalink", newsUrl));
    parameters.setSourceInclude(Arrays.asList("content.selection"));
    try {
      SearchResponse response = esClient.getServiceGeneral().search(parameters);
      if (response.getHits().getTotalHits().value != 1L) {
        throw new ServiceException("Impossible de traité les données de l'article " + newsUrl);
      }
      JsonNode source = mapper.readTree(response.getHits().getHits()[0].getSourceAsString());
      if (source.at("/content/selection/ref").isMissingNode()) {
        throw new NotFoundException("L'article ou l'actualité n'est pas asssocié à une selection");
      }
      selection = selectionService.get(source.at("/content/selection/ref").asText());
    } catch (EsRequestException | IOException e) {
      throw new ServiceException(e);
    }
    return selection;
  }

  private void deleteNotice(Notice notice) throws EsRequestException, ServiceException {
    NoticeType noticeType = notice.getInternal().getNoticeType();
    String noticeId = notice.getId();
    switch (noticeType) {
      case REFERENCE:
        esClient.getServiceReference().delete(noticeId, RefreshPolicy.IMMEDIATE);
        break;
      case ARTWORK:
        esClient.getServiceArtwork().delete(noticeId, RefreshPolicy.IMMEDIATE);
        break;
      case COLLECTION:
        esClient.getServiceCollection().delete(noticeId, RefreshPolicy.IMMEDIATE);
        break;
      case EVENT:
        esClient.getServiceEvent().delete(noticeId, RefreshPolicy.IMMEDIATE);
        break;
      case PERSON:
        esClient.getServicePerson().delete(noticeId, RefreshPolicy.IMMEDIATE);
        break;
      default:
        throw new ServiceException("Type de notice inconnu");
    }
  }

  private Notice getCreatedFrom(String noticeId, NoticeType noticeType) throws ServiceException {
    List<Notice> notice = null;
    try {
      switch (noticeType) {
        case ARTWORK:
          notice = esClient.getServiceArtwork().list(QueryBuilders.matchQuery(INTERNAL_SOURCE_NOTICE_UUID, noticeId),
              1);
          break;
        case COLLECTION:
          notice = esClient.getServiceCollection().list(QueryBuilders.matchQuery(INTERNAL_SOURCE_NOTICE_UUID, noticeId),
              1);
          break;
        case EVENT:
          notice = esClient.getServiceEvent().list(QueryBuilders.matchQuery(INTERNAL_SOURCE_NOTICE_UUID, noticeId), 1);
          break;
        case PERSON:
          notice = esClient.getServicePerson().list(QueryBuilders.matchQuery(INTERNAL_SOURCE_NOTICE_UUID, noticeId), 1);
          break;
        case REFERENCE:
          notice = esClient.getServiceReference().list(QueryBuilders.matchQuery(INTERNAL_SOURCE_NOTICE_UUID, noticeId),
              1);
          break;
        default:
          break;
      }
    } catch (EsRequestException e) {
      throw new ServiceException(messageSource.getMessage(ELASTICSEARCH_INTERNAL_ERROR, new Object[] {}, null), e);
    }
    if (!CollectionUtils.isEmpty(notice)) {
      return notice.get(0);
    }
    return null;
  }

  private void verifyUniqueKey(JsonNode noticeNode) throws ServiceException {
    try {
      String uniqueKey = noticeNode.at("/internal/uniqueKey").asText();
      String noticeType = noticeNode.at("/internal/noticeType").asText();
      if (StringUtils.isNotBlank(uniqueKey)) {
        SimpleQueryStringBuilder filterType = QueryBuilders.simpleQueryStringQuery(noticeType);
        filterType.field(INTERNAL_NOTICE_TYPE_FIELD);
        List<Notice> list = esClient.getServiceNotice().list(QueryBuilders.boolQuery().filter(filterType)
            .must(QueryBuilders.termQuery(INTERNAL_UNIQUE_KEY_FIELD, uniqueKey)), 1);
        if (!list.isEmpty()) {
          throw new BadRequestException(
              messageSource.getMessage("notice.uniqueKey.already.exists", new Object[] { uniqueKey }, null));
        }
      }
    } catch (EsRequestException e) {
      throw new ServiceException(messageSource.getMessage(ELASTICSEARCH_INTERNAL_ERROR, new Object[] {}, null), e);
    }
  }

  private void verifyLinkedDatabaseStatus(List<Map<String, String>> errors, String uuid, JsonNode noticeNode)
      throws EsRequestException, IOException, FunctionnalException {
    JsonNode databaseNodes = noticeNode.at(DATABASE_LABEL_PATH);

    List<String> databasesUuid = new ArrayList<>();
    databaseNodes.findValues("ref").forEach(ref -> databasesUuid.add(ref.asText()));

    if (databasesUuid.isEmpty()) {
      return;
    }
    EsSearchParameters parameters = new EsSearchParameters();
    parameters.setQuery(QueryBuilders.termsQuery(INTERNAL_UUID_FIELD, databasesUuid));
    parameters.setIndexes(Arrays.asList(EsIndexEnum.DATABASE));
    parameters.setSourceInclude(Arrays.asList(INTERNAL_UUID_FIELD, "internal.agorhaStatus"));
    SearchResponse searchResponse = esClient.getServiceGeneral().search(parameters);

    boolean atLeastOneIsPublished = false;
    for (SearchHit hit : searchResponse.getHits()) {
      JsonNode source = mapper.readTree(hit.getSourceAsString());
      if (NoticeStatus.PUBLISHED.getId() == source.at("/internal/agorhaStatus").asInt()) {
        atLeastOneIsPublished = true;
        break;
      }
    }
    if (!atLeastOneIsPublished) {
      String message = messageSource.getMessage("notice.publication.link.to.not.published.database", new Object[] {},
          null);
      HashMap<String, String> errorDetail = new HashMap<>();
      errorDetail.put("uuid", uuid);
      errorDetail.put("msg", message);
      errors.add(errorDetail);
      throw new FunctionnalException(message);
    }
  }

  private void deleteNoticeInSelection(String noticeId) throws ServiceException {
    try {
      EsSearchParameters parameters = new EsSearchParameters();
      parameters.setIndexes(Arrays.asList(EsIndexEnum.SELECTION));
      BoolQueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("notices", noticeId));
      parameters.setQuery(query);
      parameters.setSourceInclude(Arrays.asList("id"));
      SearchResponse response = esClient.getServiceGeneral().search(parameters);

      for (SearchHit hit : response.getHits()) {
        selectionService.remove((String) hit.getSourceAsMap().get("id"), Arrays.asList(noticeId), true);
      }
    } catch (EsRequestException e) {
      throw new ServiceException(e);
    }

  }

  void deleteNoticeInMedia(String noticeId) throws ServiceException {
    try {
      BoolQueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("internal.notice", noticeId));
      List<Media> medias = esClient.getServiceMedia().list(query, 100);

      for (Media media : medias) {
        media.getInternal().getNotice().remove(noticeId);
        if (media.getInternal().getNotice().isEmpty()) {
          media.getInternal().setLinkedWithNotices("Non");
        }
        esClient.getServiceMedia().createOrUpdate(media, RefreshPolicy.IMMEDIATE);
      }

    } catch (EsRequestException e) {
      throw new ServiceException(e);
    }
  }

  private void deleteNoticeInLinkedNotice(String noticeId) throws ServiceException {
    try {
      BoolQueryBuilder query = QueryBuilders.boolQuery().must(QueryBuilders.matchQuery("internal.reference", noticeId));
      List<Notice> notices = esClient.getServiceNotice().list(query, 100);

      for (Notice notice : notices) {
        notice.getInternal().getReference().remove(noticeId);
        esClient.getServiceNotice().createOrUpdate(notice, RefreshPolicy.IMMEDIATE);
      }

    } catch (EsRequestException e) {
      throw new ServiceException(e);
    }

  }

  private void deleteNoticeInJoai(String id) throws IOException {
    File xmlFile = Path.of(exportJoaiPath, id + ".xml").toFile();
    boolean isDeleted = Files.deleteIfExists(xmlFile.toPath());
    if (!isDeleted) {
      log.warn("Fichier xml de la notice {} pour export OAI non présent, suppression impossible", id);
    }
  }

  private NoticeType getNoticeType(JsonNode noticeNode) {
    String noticeTypeAsText = noticeNode.at("/internal/noticeType").asText();
    NoticeType noticeType;
    try {
      noticeType = NoticeType.valueOf(noticeTypeAsText);
    } catch (IllegalArgumentException e) {
      throw new BadRequestException("Type de notice inconnu", e);
    }
    return noticeType;
  }

  private void updateData(JsonNode noticeNode, NoticeType noticeType, NoticeDataSourceMode dataSourceMode)
      throws ServiceException {

    // Mise à jour des digest avant les références, car nécessaire pour les notices liées
    for (DigestService digestService : digestServices) {
      if (digestService.getType().equals(noticeType)) {
        digestService.build(noticeNode);
      }
    }

    // Mise à jour des anciennes réfèrences et remplissage du champ "references"
    updateRefs(noticeNode, dataSourceMode);

    // Mise à jour des thesaurus pour ajouter le conceptPath
    updateThesauri(noticeNode);

    updateDateFacet(noticeNode);

    // Pour les personnes, on veut que la facette général "Personnes" remonte la
    // notice elle-même, on ajoute donc la notice elle-même dans le champ calculé
    if (noticeType.equals(NoticeType.PERSON)) {
      String personType = noticeNode.at("/internal/digest/personType").asText();
      JsonNode facetPerson = noticeNode.at("/content/computedFacetPerson");
      if (facetPerson.isMissingNode()) {
        facetPerson = mapper.createArrayNode();
        ((ObjectNode) noticeNode.get("content")).set("computedFacetPerson", facetPerson);
      }
      Set<String> values = new HashSet<>();
      ArrayNode facetPersonArray = (ArrayNode) facetPerson;
      values.add("/" + personType + "/" + buildDigestPersonForFacet(noticeNode.at("/internal/digest")));
      facetPersonArray.removeAll();
      values.forEach(facetPersonArray::add);
    }

    // APDT 1204
    // On désassocie tout les medias actuellement associés à la notice
    // Les medias devant rester associés sont recalculés dans updateMediaInfo
    deleteNoticeInMedia(noticeNode.at(INTERNAL_UUID_PATH).asText());

    updateMediaInfo(noticeNode);
  }

  private void updateThesauri(JsonNode noticeNode) {
    List<JsonNode> thesaurus = noticeNode.findValues("thesaurus");
    for (JsonNode node : thesaurus) {
      if (node.isArray()) {
        for (Iterator<JsonNode> iterator = node.elements(); iterator.hasNext();) {
          JsonNode subNode = iterator.next();
          updateThesaurus(subNode);
        }
      } else {
        updateThesaurus(node);
      }
    }
  }

  private void updateThesaurus(JsonNode subNode) {
    JsonNode ref = subNode.get("ref");
    if (ref != null) {
      JsonNode concept = thesaurusService.getConcept(ref.asText());
      if (concept != null) {
        String conceptPath = thesaurusService.getConceptPath(concept);
        ((ObjectNode) subNode).put(CONCEPT_PATH_FIELD, conceptPath);
        ArrayNode prefLabels = mapper.createArrayNode();
        ObjectNode prefLabel = mapper.createObjectNode();
        prefLabel.put("value", thesaurusService.getPrefLabel(ref.asText()));
        prefLabel.put("language", "fre");
        prefLabels.add(prefLabel);
        ((ObjectNode) subNode).set("prefLabels", prefLabels);
        if (concept.get("thesaurusId").asText()
            .equals(thesaurusService.getThesaurusId(GincoServiceImpl.THESAURUS_LIEU_KEY))) {
          ((ObjectNode) subNode).put(GEO_POINT_FIELD, thesaurusService.getGeoPoint(concept.get("identifier").asText()));
        }
      }
    }
  }

  private void updateDateFacet(JsonNode noticeNode) {
    List<JsonNode> dates = noticeNode.findParents("start");

    for (JsonNode date : dates) {
      ArrayNode pathStartComputed = getPathComputed(date.at("/start"));
      ((ObjectNode) date).set("startDatePathComputed", pathStartComputed);

      ArrayNode pathEndComputed = getPathComputed(date.at("/end"));
      ((ObjectNode) date).set("endDatePathComputed", pathEndComputed);

      updateDateComputed(date);
    }

  }

  void updateDateComputed(JsonNode nodeDate) {
    JsonNode startEarliest = nodeDate.at("/start/earliest/date");
    JsonNode startLatest = nodeDate.at("/start/latest/date");
    JsonNode endEarliest = nodeDate.at("/end/earliest/date");
    JsonNode endLatest = nodeDate.at("/end/latest/date");
    JsonNode startSiecle = nodeDate.at("/start/siecle/thesaurus/ref");
    JsonNode endSiecle = nodeDate.at("/end/siecle/thesaurus/ref");
    String startPrefix = nodeDate.at("/start/prefix/thesaurus/ref").asText();
    String endPrefix = nodeDate.at("/end/prefix/thesaurus/ref").asText();
    PrefixType startPrefixType = thesaurusService.getThesaurusPrefixe().get(startPrefix);
    PrefixType endPrefixType = thesaurusService.getThesaurusPrefixe().get(endPrefix);

    LocalDate startComputed = null;
    LocalDate endComputed = null;

    // Pour la date de début
    if (!startEarliest.isMissingNode()) {
      // champ earliest renseigné
      startComputed = DateUtils.parseDate(startEarliest.asText(), DateUtils.ConvertionMode.START);
      startComputed = DateUtils.updateYearsForStart(startPrefixType, startComputed, false);
    } else if (!startSiecle.isMissingNode()) {
      // champ siecle renseigné
      startComputed = thesaurusService.getThesaurusSiecle().getMapSiecles().get(startSiecle.asText()).getLeft();
      startComputed = DateUtils.updateYearsForStart(startPrefixType, startComputed, true);
    }

    // Pour la date de fin
    if (!endLatest.isMissingNode()) {
      // champ latest du block end renseigné
      endComputed = DateUtils.parseDate(endLatest.asText(), DateUtils.ConvertionMode.END);
      endComputed = DateUtils.updateYearsForEnd(endPrefixType, endComputed, false);
    } else if (!endEarliest.isMissingNode()) {
      // champ earliest du block end renseigné
      endComputed = DateUtils.parseDate(endEarliest.asText(), DateUtils.ConvertionMode.END);
      endComputed = DateUtils.updateYearsForEnd(endPrefixType, endComputed, false);
    } else if (!endSiecle.isMissingNode()) {
      // champ siecle du block end renseigné
      endComputed = thesaurusService.getThesaurusSiecle().getMapSiecles().get(endSiecle.asText()).getRight()
          .with(TemporalAdjusters.lastDayOfYear());
      endComputed = DateUtils.updateYearsForEnd(endPrefixType, endComputed, true);
    } else if (!startLatest.isMissingNode()) {
      // champ latest du block start renseigné
      endComputed = DateUtils.parseDate(startLatest.asText(), DateUtils.ConvertionMode.END);
      endComputed = DateUtils.updateYearsForEnd(startPrefixType, endComputed, false);
    } else if (!startEarliest.isMissingNode()) {
      // champ earliest du block start renseigné
      endComputed = DateUtils.parseDate(startEarliest.asText(), DateUtils.ConvertionMode.END);
      endComputed = DateUtils.updateYearsForEnd(startPrefixType, endComputed, false);
    } else if (!startSiecle.isMissingNode()) {
      // champ siecle du block start renseigné
      endComputed = thesaurusService.getThesaurusSiecle().getMapSiecles().get(startSiecle.asText()).getRight()
          .with(TemporalAdjusters.lastDayOfYear());
      endComputed = DateUtils.updateYearsForEnd(startPrefixType, endComputed, true);
    }

    if (startComputed != null) {
      ((ObjectNode) nodeDate).put("startDateComputed", DateUtils.formatEraDate(startComputed));
    }
    if (endComputed != null) {
      ((ObjectNode) nodeDate).put("endDateComputed", DateUtils.formatEraDate(endComputed));
    }

  }

  private ArrayNode getPathComputed(JsonNode nodeDate) {
    ArrayNode pathComputed = mapper.createArrayNode();
    if (nodeDate.at("/siecle").isMissingNode()) {
      JsonNode earliest = nodeDate.at("/earliest/date");
      ThesaurusSiecle thesaurusSiecle = thesaurusService.getThesaurusSiecle();
      if (!earliest.isMissingNode()) {
        pathComputed.add(getPathSiecle(thesaurusSiecle, DateUtils.parseDate(earliest.asText())));
      }

      JsonNode latest = nodeDate.at("/latest/date");
      if (!latest.isMissingNode()) {
        pathComputed.add(getPathSiecle(thesaurusSiecle, DateUtils.parseDate(latest.asText())));
      }

    } else {
      pathComputed.add(nodeDate.at("/siecle/thesaurus/conceptPath").asText());
    }
    return pathComputed;
  }

  private String getPathSiecle(ThesaurusSiecle thesaurusSiecle, LocalDate year) {
    StringBuilder path = new StringBuilder("/");
    for (ConceptSiecle siecle : thesaurusSiecle.getSiecles()) {
      if (siecle.getStartDate().compareTo(year) <= 0 && siecle.getEndDate().compareTo(year) >= 0) {
        path.append(siecle.getLabel());
        path.append(getPathSiecle(siecle, year));
        break;
      }
    }
    String yearAsString = DateUtils.formatYear(year);
    return path.append("/").append(yearAsString).toString();
  }

  private Object getPathSiecle(ConceptSiecle siecle, LocalDate year) {
    StringBuilder path = new StringBuilder("/");
    for (ConceptSiecle subSiecle : siecle.getSubConcepts()) {
      if (subSiecle.getStartDate().compareTo(year) <= 0 && subSiecle.getEndDate().compareTo(year) >= 0) {
        path.append(subSiecle.getLabel());
        if (!subSiecle.getSubConcepts().isEmpty()) {
          path.append(getPathSiecle(subSiecle, year));
        }
        break;
      }
    }
    return path.toString();
  }

  private void updateMediaInfo(JsonNode noticeNode) throws ServiceException {
    JsonNode associatedMedia = noticeNode.at(ASSOCIATED_MEDIA_PATH);
    if (!associatedMedia.isMissingNode()) {
      boolean withImage = false;
      boolean withDocument = false;
      MediaFile firstMediaFile = null;
      String firstMediaUuid = null;
      Iterator<JsonNode> elements = associatedMedia.elements();
      List<Media> allMedia = new ArrayList<>();
      for (Iterator<JsonNode> iterator = elements; iterator.hasNext();) {
        JsonNode mediaJson = iterator.next();
        JsonNode refNode = mediaJson.at("/ref");
        if (!refNode.isMissingNode()) {
          String ref = refNode.asText();
          Media media = null;
          try {
            media = mediaService.get(ref);
            allMedia.add(media);
            Map<String, String> map = new HashMap<>();
            map.put("digest", digestFromMedia(media));
            map.put("internalMetadata", metadataFromMedia(media));
            map.put("ocr", ocrFromMedia(media));
            map.put("ref", ref);

            replaceMedia(associatedMedia, ref, map);
            firstMediaUuid = firstMediaUuid == null ? ref : firstMediaUuid;
            for (MediaFile file : media.getFiles()) {
              if (file.getMediaType().equals(MediaType.LOCAL) && file.getContentType().equals("application/pdf")) {
                withDocument = true;
              } else {
                withImage = true;
              }
              firstMediaFile = firstMediaFile == null ? file : firstMediaFile;
              if (!noticeNode.at("/content/mediaInformation/prefPicture/idFile").isMissingNode()
                  && noticeNode.at("/content/mediaInformation/prefPicture/idFile").asText().equals(file.getUuid())) {
                firstMediaFile = file;
                firstMediaUuid = ref;
              }
            }

            // MAJ des références aux notices dans les media
            media.getInternal().getNotice().add(noticeNode.at(INTERNAL_UUID_PATH).asText());

            // MAJ des références aux databases dans les media
            updateMediaDatabase(noticeNode, media);

            media.getInternal().setLinkedWithNotices("Oui");
            esClient.getServiceMedia().createOrUpdate(media, RefreshPolicy.IMMEDIATE);
          } catch (NotFoundException | EsRequestException e) {
            log.warn(e.getMessage());
          }
        }
      }
      if (firstMediaUuid != null) {
        updatePrefMedia(noticeNode, firstMediaUuid, firstMediaFile);
      }
      updateAttachmentFacet(noticeNode, withImage, withDocument);
      createManifest(allMedia, noticeNode.at(INTERNAL_UUID_PATH).asText(),
          noticeNode.at("/internal/digest/title").asText());
    } else {
      // APDT-1195
      // Suppression du prefPicture quand plus de media
      // (c.a.d. associatedMedia.isMissingNode())
      JsonNode mediaInformation = noticeNode.at("/content/mediaInformation");
      if (!mediaInformation.isMissingNode()) {
        ((ObjectNode) mediaInformation).remove("prefPicture");
      }
    }
  }

  private void updateMediaDatabase(JsonNode noticeNode, Media media) {
    List<Map<String, String>> databases = media.getDatabase();
    noticeNode.at(DATABASE_LABEL_PATH).forEach(database -> {
      String refDatabase = database.get("ref").asText();
      String valueDatabase = database.get("value").asText();
      if (databases.stream().filter(databaseObject -> refDatabase.equals(databaseObject.get("ref"))).count() == 0) {
        databases.add(Map.of("ref", refDatabase, "value", valueDatabase));
      }
    });
  }

  private void updatePrefMedia(JsonNode noticeNode, String mediaUuid, MediaFile mediaFile) {
    ObjectNode prefPicture = mapper.createObjectNode();
    prefPicture.put("idMedia", mediaUuid);
    prefPicture.put("idFile", mediaFile.getUuid());
    prefPicture.put("extension", FilenameUtils.getExtension(mediaFile.getThumbnail()));
    prefPicture.put("thumbnail", mediaFile.getThumbnail());
    prefPicture.put("mediaType", mediaFile.getMediaType().name());
    ((ObjectNode) noticeNode.at("/content/mediaInformation")).set("prefPicture", prefPicture);
  }

  private void updateAttachmentFacet(JsonNode noticeNode, boolean withImage, boolean withDocument) {
    JsonNode computedFacetAttachment = mapper.createArrayNode();

    if (withImage) {
      ((ArrayNode) computedFacetAttachment).add("Avec image");
    }
    if (withDocument) {
      ((ArrayNode) computedFacetAttachment).add("Avec document");
    }

    if (computedFacetAttachment.size() > 0) {
      ((ObjectNode) noticeNode.at("/content")).set("computedFacetAttachment", computedFacetAttachment);
    }
  }

  private void replaceMedia(JsonNode associatedMedia, String ref, Map<String, String> media) {
    for (Iterator<JsonNode> iterator = associatedMedia.elements(); iterator.hasNext();) {
      ObjectNode mediaNode = (ObjectNode) iterator.next();
      if (mediaNode.at("/ref").asText().equals(ref)) {
        for (Entry<String, String> entry : media.entrySet()) {
          mediaNode.put(entry.getKey(), entry.getValue());
        }
      }
    }
  }

  private String ocrFromMedia(Media media) {
    return String.join(" ",
        media.getFiles().stream().map(MediaFile::getOcr).filter(Objects::nonNull).collect(Collectors.toList()));
  }

  private String metadataFromMedia(Media media) {
    List<String> values = new ArrayList<>();
    media.getFiles().stream().map(MediaFile::getInternalMetadata).collect(Collectors.toList())
        .forEach(map -> values.addAll(map.values()));
    return String.join(" ", values);
  }

  private String digestFromMedia(Media media) {
    List<String> list = new ArrayList<>();
    list.add(media.getComment());
    list.add(media.getCredit());
    list.add(media.getCaption());
    list.add(media.getLicence());
    list.add(media.getFileProvenance());

    Concept communicationRight = media.getCommunicationRight();
    if (communicationRight != null && !communicationRight.getThesaurus().getPreflabels().isEmpty()
        && communicationRight.getThesaurus().getPreflabels().get(0) != null) {
      list.add(communicationRight.getThesaurus().getPreflabels().get(0).getValue());
    }
    Concept reproductionRight = media.getReproductionRight();
    if (reproductionRight != null && !reproductionRight.getThesaurus().getPreflabels().isEmpty()
        && reproductionRight.getThesaurus().getPreflabels().get(0) != null) {
      list.add(reproductionRight.getThesaurus().getPreflabels().get(0).getValue());
    }
    return String.join(" ", list.stream().filter(Objects::nonNull).collect(Collectors.toList()));
  }

  private void updateAuditForCreation(ObjectNode internal) {
    String currentUsername = userInfoService.getCurrentUsername();
    String now = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    internal.put("createdBy", currentUsername);
    internal.put("createdDate", now);
    internal.put("updatedBy", currentUsername);
    internal.put("updatedDate", now);

  }

  private void updateAuditForUpdate(ObjectNode internal) {
    String currentUsername = userInfoService.getCurrentUsername();
    String now = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    internal.put("updatedBy", currentUsername);
    internal.put("updatedDate", now);

  }

  private void updateRefs(JsonNode noticeNode, NoticeDataSourceMode dataSourceMode)
      throws ServiceException {
    Set<String> references = new HashSet<>();
    noticeNode.at(INTERNAL_REFERENCES_PATH).elements()
        .forEachRemaining(reference -> references.add(reference.asText()));
    Set<String> originalReferenceFromContent = new HashSet<>();
    noticeNode.at(INTERNAL_REFERENCES_FROM_CONTENT_PATH).elements()
        .forEachRemaining(reference -> originalReferenceFromContent.add(reference.asText()));
    // On supprime les références contenu dans le champ "content" car elles seront
    // traités séparément
    references.removeAll(originalReferenceFromContent);

    JsonNode mediaInformation = noticeNode.at(ASSOCIATED_MEDIA_PATH);
    Set<String> refsMedia = mediaInformation.findValues("ref").stream().map(JsonNode::asText)
        .collect(Collectors.toSet());

    JsonNode databaseLabel = noticeNode.at(DATABASE_LABEL_PATH);
    Set<String> refsDB = databaseLabel.findValues("ref").stream().map(JsonNode::asText).collect(Collectors.toSet());
    noticeNode.findParents("database").stream().map(node -> node.at("/database/ref")).map(JsonNode::asText)
        .forEach(refsDB::add);

    Set<String> referenceFromContent = new HashSet<>();
    boolean hasRefError = false;
    List<JsonNode> refParents = noticeNode.findParents("ref");
    for (JsonNode refParent : refParents) {
      String refValue = refParent.get("ref").asText();
      log.info("Updating references : found reference {}", refValue);
      if (refValue.matches("[A-Z]+/[\\d]+")) {
        String uniqueKey = refValue.replaceFirst("[A-Z]+/", "");
        String refType = refValue.replaceFirst("/[\\d]+", "");
        try {
          if (refType.equals("MEDIA")) {
            refValue = getNewRefMediaFromOld(uniqueKey);
            ((ObjectNode) refParent).put("ref", refValue);
          } else if (refType.equals("DATABASE")) {
            updateNewRefDatabaseFromOld(uniqueKey, refParent);
          } else {
            NoticeType noticeTypeEnum = NoticeType.valueOf(refType);
            refValue = getNewRefFromOld(uniqueKey, noticeTypeEnum);
            ((ObjectNode) refParent).put("ref", refValue);
          }
          log.info("Updating references: uniqueKey {} replaced by new reference {}", uniqueKey, refValue);
        } catch (FunctionnalException | IllegalArgumentException e) {
          hasRefError = true;
        }
      }
      try {
        UUID.fromString(refValue);
        // On ignore les références aux media et aux bdd
        if (!refsMedia.contains(refValue) && !refsDB.contains(refValue)) {
          log.info("Updating references : adding uuid {} to references", refValue);
          // Mise à jour du champ value avec le digest de la notice liée et remplissage
          // des notices liées

          updateNoticeRef(refValue, (ObjectNode) refParent, noticeNode.at(INTERNAL_UUID_PATH).asText(),
              noticeNode.at(INTERNAL_STATUS_PATH).asText(), dataSourceMode);
          referenceFromContent.add(refValue);
        }
      } catch (IllegalArgumentException e) {
        // On ignore les ref qui ne sont pas des uuid, ce sont des ref thesaurus ou des
        // anciennes ref non trouvées
      }
    }

    // Mise à jour des digest des notices listées comme notices liées car le lien n'est pas
    // forcément créé dans les deux sens lorsqu'une notice n'est pas publié
    Set<String> referencesToRemove = new HashSet<>();
    Collection<String> linkedNoticeUUIDs = CollectionUtils
        .union(getReferenceBy(noticeNode.at(INTERNAL_UUID_PATH).asText()), references);
    for (String reference : linkedNoticeUUIDs) {
      updateLinkedNotice(reference, noticeNode.at(INTERNAL_UUID_PATH).asText(),
          noticeNode.at(INTERNAL_STATUS_PATH).asText(), noticeNode.at(INTERNAL_DIGEST_DISPLAY_LABEL_LINK_PATH).asText(),
          referencesToRemove, dataSourceMode);
    }

    // Mise à jours des notices qui ne sont plus référencées par la notice en cours
    // de traitement
    if (noticeNode.at(INTERNAL_STATUS_PATH).asText().equals(NoticeStatus.PUBLISHED.getValue())) {
      for (String reference : CollectionUtils.removeAll(originalReferenceFromContent, referenceFromContent)) {
        removeFromLinkedNotice(reference, noticeNode.at(INTERNAL_UUID_PATH).asText(), dataSourceMode);
      }
    }

    // Suppression des ref qui n'existent pas dans les index
    references.removeAll(referencesToRemove);

    // Ajout des références trouvées dans le contenu
    references.addAll(referenceFromContent);

    if (hasRefError) {
      logTalend.error(noticeNode.at(INTERNAL_UUID_PATH).asText());
    }

    ArrayNode referencesNode = mapper.createArrayNode();
    references.stream().forEach(referencesNode::add);
    ((ObjectNode) noticeNode.at(INTERNAL_PATH)).set("reference", referencesNode);

    ArrayNode referencesFromContentNode = mapper.createArrayNode();
    referenceFromContent.stream().forEach(referencesFromContentNode::add);
    ((ObjectNode) noticeNode.at(INTERNAL_PATH)).set("referenceFromContent", referencesFromContentNode);
  }

  private void removeFromLinkedNotice(String reference, String fromNoticeId, NoticeDataSourceMode dataSourceMode) {
    Notice notice;
    try {
      notice = get(reference);

      String noticeAsString = mapper.writeValueAsString(notice);
      JsonNode noticeNode = mapper.readTree(noticeAsString);

      // Mise à jour du champ internal.reference de la notice liée, suppression de
      // l'uuid de la notice en cours de traitement
      JsonNode referenceNode = noticeNode.at(INTERNAL_REFERENCES_PATH);
      if (!referenceNode.isMissingNode()) {
        ArrayNode filteredReferenceNode = mapper.createArrayNode();
        for (Iterator<JsonNode> iterator = referenceNode.elements(); iterator.hasNext();) {
          JsonNode refNode = iterator.next();
          if (!refNode.asText().equals(fromNoticeId)) {
            filteredReferenceNode.add(refNode);
          }
        }
        ((ObjectNode) noticeNode.get("internal")).set("reference", filteredReferenceNode);
        notice = mapper.treeToValue(noticeNode, Notice.class);
        save(notice, notice.getInternal().getNoticeType(), notice.getSeqNo(), notice.getPrimaryTerm(), dataSourceMode);
      }
    } catch (Exception e) {
      log.error("Mise à jour de la notice {}, notice notice liée {} en erreur", fromNoticeId, reference, e);
    }

  }

  private void updateLinkedNotice(String refValue, String fromNoticeId, String fromNoticeStatus,
      String fromNoticeDisplayLabelLink, Set<String> referencesToRemove, NoticeDataSourceMode dataSourceMode) {
    try {
      Notice linkedNotice;
      try {
        linkedNotice = get(refValue);
      } catch (NotFoundException e) {
        // Notice liée non trouvé, il faut la supprimé de la liste des références
        referencesToRemove.add(refValue);
        return;
      }

      String referencedNoticeStatus = linkedNotice.getInternal().getStatus();

      // Si la notice liée n'est pas publié, il faut la supprimer des notices liées de
      // la notice publiée
      if (fromNoticeStatus.equals(NoticeStatus.PUBLISHED.getValue())
          && !referencedNoticeStatus.equals(NoticeStatus.PUBLISHED.getValue())) {
        referencesToRemove.add(refValue);
      }

      // On ne met à jour les notices liées que si celle en cours de
      // traitement est publiée ou que la notice cible n'est pas publiée
      if (fromNoticeStatus.equals(NoticeStatus.PUBLISHED.getValue())
          || !NoticeStatus.PUBLISHED.getValue().equals(referencedNoticeStatus)) {

        String noticeAsString = mapper.writeValueAsString(linkedNotice);
        JsonNode noticeNode = mapper.readTree(noticeAsString);

        Optional<JsonNode> refParent = noticeNode.findParents("ref").stream()
            .filter(refNode -> fromNoticeId.equals(refNode.get("ref").asText())).findFirst();
        if (refParent.isPresent()) {
          ((ObjectNode) refParent.get()).put("value", fromNoticeDisplayLabelLink);
          linkedNotice = mapper.treeToValue(noticeNode, Notice.class);
          save(linkedNotice, linkedNotice.getInternal().getNoticeType(), linkedNotice.getSeqNo(),
              linkedNotice.getPrimaryTerm(), dataSourceMode);
        }
      }
    } catch (Exception e) {
      log.error("Mise à jour de la notice {}, notice liée {} en erreur", fromNoticeId, refValue);
    }
  }

  private void updateNewRefDatabaseFromOld(String oldRef, JsonNode refParent)
      throws ServiceException, FunctionnalException {
    List<Database> database = null;
    try {
      database = esClient.getServiceDatabase().list(QueryBuilders.termQuery(INTERNAL_UNIQUE_KEY_FIELD, oldRef), 2);
    } catch (EsRequestException e) {
      throw new ServiceException(messageSource.getMessage(ELASTICSEARCH_INTERNAL_ERROR, new Object[] {}, null), e);
    }
    if (CollectionUtils.isEmpty(database)) {
      throw new FunctionnalException("Cle unique inconnu : " + oldRef);
    } else if (database.size() != 1) {
      throw new FunctionnalException("Plusieurs database trouvées pour la clé unique : " + oldRef);
    }
    ((ObjectNode) refParent).put("ref", database.get(0).getInternal().getUuid());

    ((ObjectNode) refParent).put("value", database.get(0).getContent().getTitle());
  }

  private String getNewRefMediaFromOld(String oldRef) throws ServiceException, FunctionnalException {
    List<Media> media = null;
    try {
      media = esClient.getServiceMedia().list(QueryBuilders.termQuery(INTERNAL_UNIQUE_KEY_FIELD, oldRef), 2);
    } catch (EsRequestException e) {
      throw new ServiceException(messageSource.getMessage(ELASTICSEARCH_INTERNAL_ERROR, new Object[] {}, null), e);
    }
    if (CollectionUtils.isEmpty(media)) {
      throw new FunctionnalException("Cle unique inconnu : " + oldRef);
    } else if (media.size() != 1) {
      throw new FunctionnalException("Plusieurs media trouvés pour la clé unique : " + oldRef);
    }
    return media.get(0).getInternal().getUuid();
  }

  private String getNewRefFromOld(String oldRef, NoticeType noticeType) throws ServiceException, FunctionnalException {
    List<Notice> notice = null;
    try {
      int oldRefAsInt = Integer.parseInt(oldRef);
      switch (noticeType) {
        case ARTWORK:
          // On recherche les UK avec leurs variantes déboublonnées (MUS_BIEN,
          // MUS_EDIFICE)
          int oldRefForMusBien = oldRefAsInt + 1000000;
          int oldRefForMusEdifice = oldRefAsInt + 2000000;
          notice = esClient.getServiceArtwork().list(
              QueryBuilders.termsQuery(INTERNAL_UNIQUE_KEY_FIELD, oldRef, oldRefForMusBien, oldRefForMusEdifice), 2);
          if (notice.size() > 1) {
            log.error("Plusieurs notice ARTWORK trouvées pour la clé unique {} et ses variantes {} et {}", oldRef,
                oldRefForMusBien, oldRefForMusEdifice);
          }
          break;
        case COLLECTION:
          notice = esClient.getServiceCollection().list(QueryBuilders.termQuery(INTERNAL_UNIQUE_KEY_FIELD, oldRef), 2);
          break;
        case EVENT:
          notice = esClient.getServiceEvent().list(QueryBuilders.termQuery(INTERNAL_UNIQUE_KEY_FIELD, oldRef), 2);
          break;
        case PERSON:
          notice = esClient.getServicePerson().list(QueryBuilders.termQuery(INTERNAL_UNIQUE_KEY_FIELD, oldRef), 2);
          break;
        case REFERENCE:
          // On recherche les UK avec leurs variantes déboublonnées (MUS_BIBLIO,
          // REF_ARCHIVE)
          int oldRefForMusBiblio = oldRefAsInt + 3000000;
          int oldRefForRefArchive = oldRefAsInt + 4000000;
          notice = esClient.getServiceReference().list(
              QueryBuilders.termsQuery(INTERNAL_UNIQUE_KEY_FIELD, oldRef, oldRefForMusBiblio, oldRefForRefArchive), 2);
          if (notice.size() > 1) {
            log.error("Plusieurs notice REFERNCE trouvées pour la clé unique {} et ses variantes {} et {}", oldRef,
                oldRefForMusBiblio, oldRefForRefArchive);
          }
          break;
        default:
          break;
      }
    } catch (EsRequestException e) {
      throw new ServiceException(messageSource.getMessage(ELASTICSEARCH_INTERNAL_ERROR, new Object[] {}, null), e);
    }
    if (CollectionUtils.isEmpty(notice)) {
      throw new FunctionnalException("Cle unique inconnu : " + oldRef);
    } else if (notice.size() != 1) {
      throw new FunctionnalException("Plusieurs notice trouvées pour la clé unique : " + oldRef);
    }
    return notice.get(0).getInternal().getUuid();
  }

  private void updateNoticeRef(String refValue, ObjectNode ref, String fromNoticeId, String fromNoticeStatus,
      NoticeDataSourceMode dataSourceMode) {
    Notice notice;
    try {
      notice = get(refValue);

      String noticeAsString = mapper.writeValueAsString(notice);
      JsonNode noticeNode = mapper.readTree(noticeAsString);

      JsonNode digestNode = noticeNode.at("/internal/digest");
      ref.put("value", noticeNode.at(INTERNAL_DIGEST_DISPLAY_LABEL_LINK_PATH).asText());
      JsonNode personType = noticeNode.at("/internal/digest/personType");
      if (!personType.isMissingNode()) {
        ref.put(CONCEPT_PATH_FIELD, "/" + personType.asText() + "/" + buildDigestPersonForFacet(digestNode));
      }

      String referencedNoticeStatus = noticeNode.at(INTERNAL_STATUS_PATH).asText();
      // Mise à jour du champ internal.reference de la notice liée, ajout de l'uuid de
      // la notice en cours de traitement
      if (fromNoticeStatus.equals(NoticeStatus.PUBLISHED.getValue())
          || !NoticeStatus.PUBLISHED.getValue().equals(referencedNoticeStatus)) {
        JsonNode referenceNode = noticeNode.at(INTERNAL_REFERENCES_PATH);
        if (referenceNode.isMissingNode()) {
          referenceNode = mapper.createArrayNode();
          ((ObjectNode) noticeNode.get("internal")).set("reference", referenceNode);
        }
        ((ArrayNode) referenceNode).add(fromNoticeId);
        notice = mapper.treeToValue(noticeNode, Notice.class);
        save(notice, notice.getInternal().getNoticeType(), notice.getSeqNo(), notice.getPrimaryTerm(), dataSourceMode);
      }
    } catch (Exception e) {
      log.error("Mise à jour de la notice {}, notice notice liée {} en erreur", fromNoticeId, refValue, e);
    }
  }

  private String buildDigestPersonForFacet(JsonNode digest) {
    StringBuilder sb = new StringBuilder();
    JsonNode name = digest.at("/usualName");
    if (!name.isMissingNode()) {
      sb.append(name.asText());

    }

    String firstName = digest.at("/usualFirstName").asText();
    if (StringUtils.isNoneBlank(firstName)) {
      sb.append(", ");
      sb.append(firstName);
    }

    return sb.toString();
  }

  Notice save(Notice notice, NoticeType noticeType, Long seqNo, Long primaryTerm, NoticeDataSourceMode dataSourceMode)
      throws ServiceException {

    log.info("Sauvegarde de la notice {}", notice.getInternal().getUuid());

    // Utilisation du bulk dans le cas des traitements de reprise
    if (dataSourceMode.equals(NoticeDataSourceMode.BATCH)) {
      BulkProcessor buildBulkProcessor = esClient.getBulkProcessorNotice();
      IndexRequest request;
      try {
        request = new IndexRequest(EsIndexEnum.valueOf(noticeType.name()).getName())
            .source(mapper.writeValueAsString(notice), XContentType.JSON).setRefreshPolicy(RefreshPolicy.NONE);
        if (seqNo != null && primaryTerm != null) {
          request.setIfSeqNo(seqNo).setIfPrimaryTerm(primaryTerm);
        }
        if (notice.getId() != null) {
          request.id(notice.getId());
        }
      } catch (JsonProcessingException e) {
        throw new ServiceException("Erreur de lors de l'écriture de la notice en json", e);
      }
      buildBulkProcessor.add(request);

      // pour avoir un retour moins verbeux en mode batch on ne renvoit rien
      return null;
    } else {
      try {
        switch (noticeType) {
          case REFERENCE:
            return esClient.getServiceReference().createOrUpdateAndGet(notice, true, seqNo, primaryTerm);
          case ARTWORK:
            return esClient.getServiceArtwork().createOrUpdateAndGet(notice, true, seqNo, primaryTerm);
          case COLLECTION:
            return esClient.getServiceCollection().createOrUpdateAndGet(notice, true, seqNo, primaryTerm);
          case EVENT:
            return esClient.getServiceEvent().createOrUpdateAndGet(notice, true, seqNo, primaryTerm);
          case PERSON:
            return esClient.getServicePerson().createOrUpdateAndGet(notice, true, seqNo, primaryTerm);
          default:
            throw new ServiceException("Type de notice inconnu");
        }
      } catch (EsRequestException e) {
        throw new ServiceException("Erreur de lors de l'interrogation d'ES", e);
      }
    }
  }

  private void verifyDatabaseAuthorization(String uuid, NoticeStatus targetStatus, boolean isDatabaseManager,
      List<DatabaseAuthorization> authorizations, String currentStatus, JsonNode noticeNode,
      List<Map<String, String>> errors) throws FunctionnalException {

    if ((targetStatus.equals(NoticeStatus.CREATION_IN_PROGRESS)
        && currentStatus.equals(NoticeStatus.TO_PUBLISH.getValue())
        || targetStatus.equals(NoticeStatus.TO_PUBLISH)
            && currentStatus.equals(NoticeStatus.CREATION_IN_PROGRESS.getValue()))
        && isDatabaseManager) {
      JsonNode databaseNodes = noticeNode.at(DATABASE_LABEL_PATH);
      List<String> databasesUuid = new ArrayList<>();
      databaseNodes.findValues("ref").forEach(ref -> databasesUuid.add(ref.asText()));
      boolean notAuthorized = true;
      for (DatabaseAuthorization databaseAuthorization : authorizations) {
        if (databasesUuid.contains(databaseAuthorization.getDatabaseId())) {
          notAuthorized = false;
          break;
        }
      }
      if (notAuthorized) {
        String message = messageSource.getMessage("user.insufficient.rights", new Object[] {}, null);
        HashMap<String, String> errorDetail = new HashMap<>();
        errorDetail.put("uuid", uuid);
        errorDetail.put("msg", message);
        errors.add(errorDetail);
        throw new FunctionnalException(message);
      }
    }
  }

  void verifyNoticeStatus(String uuid, String currentStatus, NoticeStatus targetStatus,
      List<Map<String, String>> errors, boolean isAdmin, boolean isDatabaseManager) throws FunctionnalException {
    if (currentStatus.equals(NoticeStatus.ARCHIVED.getValue())) {
      HashMap<String, String> errorDetail = new HashMap<>();
      errorDetail.put("uuid", uuid);
      errorDetail.put("msg",
          messageSource.getMessage("notice.status.update.archived.impossible", new Object[] {}, null));
      errors.add(errorDetail);
    }

    if ((isAdmin
        // Publiée vers archivée ou à publier ou en cours
        && !(currentStatus.equals(NoticeStatus.PUBLISHED.getValue())
            && (targetStatus.equals(NoticeStatus.TO_PUBLISH) || targetStatus.equals(NoticeStatus.CREATION_IN_PROGRESS)
                || targetStatus.equals(NoticeStatus.ARCHIVED))
            // En cours ou à publier vers publiée
            || (currentStatus.equals(NoticeStatus.CREATION_IN_PROGRESS.getValue())
                || currentStatus.equals(NoticeStatus.TO_PUBLISH.getValue()))
                && targetStatus.equals(NoticeStatus.PUBLISHED)
            // A publier vers en cours
            || currentStatus.equals(NoticeStatus.CREATION_IN_PROGRESS.getValue())
                && targetStatus.equals(NoticeStatus.TO_PUBLISH)
            // En cours vers à publier
            || currentStatus.equals(NoticeStatus.TO_PUBLISH.getValue())
                && targetStatus.equals(NoticeStatus.CREATION_IN_PROGRESS)))
        // Responsable BDD
        || (isDatabaseManager
            // A publier vers en cours
            && !(currentStatus.equals(NoticeStatus.CREATION_IN_PROGRESS.getValue())
                && targetStatus.equals(NoticeStatus.TO_PUBLISH)
                // En cours vers à publier
                || currentStatus.equals(NoticeStatus.TO_PUBLISH.getValue())
                    && targetStatus.equals(NoticeStatus.CREATION_IN_PROGRESS)))) {
      String message = messageSource.getMessage("notice.status.update.impossible",
          new Object[] { currentStatus, targetStatus.getValue() }, null);
      HashMap<String, String> errorDetail = new HashMap<>();
      errorDetail.put("uuid", uuid);
      errorDetail.put("msg", message);
      errors.add(errorDetail);
      throw new FunctionnalException(message);
    }
  }

  private void createManifest(List<Media> allMedia, String noticeUuid, String label) {

    Manifest manifest = new Manifest(iiifManifestsBaseUrl + noticeUuid);
    manifest.addAttribution(messageSource.getMessage("media.manifest.attribution", new Object[] {}, null));
    manifest.addLogo(logoUrl);
    manifest.addLabel(label);
    Sequence sequence = new Sequence("");
    for (Media media : allMedia) {
      for (MediaFile mediaFile : media.getFiles()) {
        if (mediaFile.getMediaType().equals(MediaType.LOCAL_IMAGE)) {
          String imageId = media.getInternal().getUuid() + "_" + mediaFile.getUuid();
          manifest.addThumbnail(new ImageContent(iiifImagesApiUrl + imageId + "_S"));

          Canvas canvas = new Canvas(iiifManifestsBaseUrl + imageId + "/canvas.json");
          canvas.addLabel(media.getCaption());
          Map<String, String> metadata = mediaFile.getInternalMetadata();
          for (Entry<String, String> entry : metadata.entrySet()) {
            canvas.addMetadata(entry.getKey(), entry.getValue());
          }

          ImageService service = new ImageService(iiifImagesApiUrl + imageId);
          service.addProfile("http://iiif.io/api/image/2/level2.json");

          ImageContent imageContent = new ImageContent(service);
          imageContent.setHeight(Integer.valueOf(metadata.get("Height")));
          imageContent.setWidth(Integer.valueOf(metadata.get("Width")));
          imageContent.setFormat(metadata.get("Content-Type"));

          canvas.addImage(imageContent);

          sequence.addCanvas(canvas);
        }
      }
    }
    manifest.addSequence(sequence);
    File manifestFilePath = Paths.get(manifestStorePathAsName).resolve(Paths.get(noticeUuid, "manifest.json")).toFile();

    try {
      FileUtils.writeByteArrayToFile(manifestFilePath, new IiifObjectMapper().writeValueAsBytes(manifest));
    } catch (IOException e) {
      log.error("Erreur lors de l'écriture du manifest IIIF pour la notice " + noticeUuid, e);
    }
  }

}
