package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.DigestService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.model.NoticeType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class DigestServicePerson extends AbstractDigestService implements DigestService {

  @Override
  public NoticeType getType() {
    return NoticeType.PERSON;
  }

  @Override
  public void build(JsonNode notice) throws ServiceException {
    ObjectNode digestObject = getDigestObject(notice);

    buildCommonFields(notice, digestObject);

    JsonNode personType = getObjectNode(notice.at(JsonUtils.getPathToField(notice,
        "/content/identificationInformation/personType", "/thesaurus", "/prefLabels/0/value")));
    digestObject.set("personType", personType);

    JsonNode city = getObjectNode(notice.at(JsonUtils.getPathToField(notice, "/content/addressInformation/address",
        "/place/thesaurus/prefLabels/0/value", "")));
    digestObject.set("city", city);

    PersonInfo personInfo = buildPersonDataFromContent(notice.at("/content"));
    JsonNode usualName = getObjectNode(personInfo.getName());
    JsonNode usualFirstName = getObjectNode(personInfo.getFirstName());
    digestObject.set("usualName", usualName);
    digestObject.set("usualFirstName", usualFirstName);

    JsonNode startDate = getObjectNode(personInfo.getStart());
    JsonNode endDate = getObjectNode(personInfo.getEnd());
    digestObject.set("startDate", startDate);
    digestObject.set("endDate", endDate);

    String title = constructPersonTitle(usualName, usualFirstName);
    digestObject.put("title", removeHtmlTags(title));

    digestObject.set("activityType", getObjectNode(notice.at(JsonUtils.getPathToField(notice,
        "/content/activityInformation/activity", "/type/thesaurus", "/0/prefLabels/0/value"))));
    digestObject.set("place", getObjectNode(notice.at(JsonUtils.getPathToField(notice,
        "/content/activityInformation/activity", "/place/thesaurus", "/0/prefLabels/0/value"))));

    digestObject.put("noticeType", getType().toString());

    digestObject.put("displayLabelLink", buildDisplayLabelLink(digestObject));
  }

  private String buildDisplayLabelLink(JsonNode digestObject) {
    StringBuilder sb = new StringBuilder();
    addValueToDisplayLabel(digestObject.get("title"), sb);

    String datesAsString = DateUtils.buildDatesFormattedFromDigest(digestObject.get("startDate"),
        digestObject.get("endDate"));
    if (StringUtils.isNoneBlank(datesAsString)) {
      sb.append(" (");
      sb.append(datesAsString);
      sb.append(")");
    }

    addValueToDisplayLabel(digestObject.get("city"), sb);

    return sb.toString();
  }

}
