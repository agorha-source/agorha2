package fr.inha.agorha2.service;

import com.fasterxml.jackson.databind.JsonNode;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.model.NoticeType;

public interface DigestService {

  public NoticeType getType();

  /**
   * Method designed to build a map object representing the digest of a notice
   * depending on the {@link #getType()} method.
   * 
   * @param notice Notice as jsonNode
   * @throws ServiceException if anything goes wrong while creation of digest
   */
  public void build(JsonNode notice) throws ServiceException;

}
