package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonObject;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.search.EsSearchParameters;
import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest;
import fr.inha.agorha2.bean.ThesaurusConcept;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.ExportService;
import fr.inha.agorha2.service.MetaModelService;
import fr.inha.agorha2.service.SelectionService;
import fr.inha.agorha2.service.ThesaurusService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.config.EsIndexEnum;
import fr.inha.elastic.model.NoticeStatus;
import fr.inha.elastic.model.NoticeType;
import fr.inha.elastic.model.Selection;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ExportServiceImpl implements ExportService {

  private static final Logger log = LoggerFactory.getLogger(ExportServiceImpl.class);

  private static final String TITLE_DC_ELEMENT_NAME = "title";
  private static final String SUBJECT_DC_ELEMENT_NAME = "subject";
  private static final String SOURCE_DC_ELEMENT_NAME = "source";
  private static final String LANGUAGE_DC_ELEMENT_NAME = "language";
  private static final String CREATOR_DC_ELEMENT_NAME = "creator";
  private static final String COVERAGE_DC_ELEMENT_NAME = "coverage";
  private static final String INTERNAL_NOTICE_TYPE_JSONPATH = "/internal/noticeType";
  private static final int SCROLL_BATCH_SIZE = 1000;
  private static final String PREFIX_FILE_NAME_EXPORT = "Export_";
  private static final String[] COMMON_OAI_TYPE = new String[] { "internal.noticeType", "internal.typeOai",
      "internal.typeDcmi" };
  private static final String[] COMMON_OAI_SOURCE = new String[] { "AGORHA",
      "Institut national d'histoire de l'art (France)" };
  @Autowired
  private EsClient esClient;
  @Autowired
  private EsQueryService esQueryService;
  @Autowired
  private ObjectMapper mapper;
  @Autowired
  private MetaModelService metaModelService;
  @Autowired
  private SelectionService selectionService;
  @Autowired
  private ThesaurusService thesaurusService;

  @Value(value = "${agorha2.export.temporay_path}")
  private String exportPath;

  @Value(value = "${agorha2.export.joai.path}")
  private String exportJoaiPath;

  @Value(value = "${agorha2.export.joai.lastExecutionFilename}")
  private String lastExecutionFilename;

  @Override
  public void exportJson(Writer writer, NoticeSearchRequest request) throws ServiceException {
    try {

      EsSearchParameters searchParameters = esQueryService.createSearchParameters(request, null);
      searchParameters.setSize(SCROLL_BATCH_SIZE);
      searchParameters.setFetchSource(true);

      SearchResponse searchResponse = esClient.getServiceGeneral().initScroll(searchParameters);

      String scrollId = searchResponse.getScrollId();
      SearchHit[] searchHits = searchResponse.getHits().getHits();

      int index = 0;
      writer.write("[");
      do {
        for (SearchHit hit : searchHits) {
          if (index != 0) {
            writer.write(",");
          }
          String source = hit.getSourceAsString();
          writer.write(source);
          index += 1;
        }

        searchResponse = esClient.getServiceGeneral().nextScroll(scrollId);
        scrollId = searchResponse.getScrollId();
        searchHits = searchResponse.getHits().getHits();
        writer.flush();
      } while (searchHits != null && searchHits.length > 0);
      writer.write("]");
      writer.flush();
    } catch (EsRequestException | IOException e) {
      throw new ServiceException("Erreur lors de l'interogation d'ES", e);
    }

  }

  @Override
  public void exportJson(Writer writer, NoticeType noticeType, String databaseId, String scrollId)
      throws ServiceException {
    try {
      EsSearchParameters searchParameters = esQueryService.createExportJsonSearchParameters(noticeType, databaseId);
      searchParameters.setSize(SCROLL_BATCH_SIZE);
      searchParameters.setFetchSource(true);

      SearchResponse searchResponse;
      SearchHit[] searchHits;
      if (StringUtils.isNoneBlank(scrollId)) {
        searchResponse = esClient.getServiceGeneral().nextScroll(scrollId);
        searchHits = searchResponse.getHits().getHits();
      } else {
        searchResponse = esClient.getServiceGeneral().initScroll(searchParameters);
        searchHits = searchResponse.getHits().getHits();
      }

      ObjectNode response = mapper.createObjectNode();
      if (searchHits != null && searchHits.length > 0) {
        if (searchHits.length == SCROLL_BATCH_SIZE) {
          // si on a pas le nombre de résultats demandé, on est sur le dernier lot de
          // notice, on ne renvoie plus le token pour prévenir l'utilisateur qu'il a
          // atteint la fin
          response.put("token", searchResponse.getScrollId());
        }
        ArrayNode notices = mapper.createArrayNode();
        for (SearchHit hit : searchHits) {
          String source = hit.getSourceAsString();
          notices.add(mapper.readTree(source));
        }
        response.set("notices", notices);
      }
      writer.write(mapper.writeValueAsString(response));
      writer.flush();

    } catch (EsRequestException | IOException e) {
      throw new ServiceException("Erreur lors de l'interogation d'ES", e);
    }

  }

  @Override
  public File exportCsv(NoticeSearchRequest request, NoticeType noticeTypeFilter) throws ServiceException {
    try {
      String processUuid = UUID.randomUUID().toString();
      File temporaryPath = new File(exportPath, processUuid);
      EsSearchParameters searchParameters = null;
      if (request instanceof AdvancedSearchRequest) {
        searchParameters = esQueryService.createAdvancedSearchParameters((AdvancedSearchRequest) request,
            noticeTypeFilter);
      } else {
        searchParameters = esQueryService.createSearchParameters(request, noticeTypeFilter);
      }

      esQueryService.addFacetsFilters(request.getAggregations(), (BoolQueryBuilder) searchParameters.getQuery());
      searchParameters.setSize(SCROLL_BATCH_SIZE);
      searchParameters.setFetchSource(true);

      SearchResponse searchResponse = esClient.getServiceGeneral().initScroll(searchParameters);

      String scrollId = searchResponse.getScrollId();
      SearchHit[] searchHits = searchResponse.getHits().getHits();
      EnumMap<NoticeType, Map<String, Integer>> headersPerBlock = new EnumMap<>(NoticeType.class);
      EnumMap<NoticeType, Map<String, List<String>>> fieldsPerBlock = new EnumMap<>(NoticeType.class);

      // On fait une première passe sur les résultats pour détecter les headers
      // nécessaires en fonction des données
      do {
        for (SearchHit hit : searchHits) {
          detectHeaders(headersPerBlock, fieldsPerBlock, hit);
        }

        searchResponse = esClient.getServiceGeneral().nextScroll(scrollId);
        scrollId = searchResponse.getScrollId();
        searchHits = searchResponse.getHits().getHits();
      } while (searchHits != null && searchHits.length > 0);

      for (NoticeType noticeType : NoticeType.values()) {
        writeHeaders(headersPerBlock.get(noticeType), fieldsPerBlock.get(noticeType), noticeType, temporaryPath);
      }

      // Deuxième passe pour récupérer les données en fonction des headers détéctés
      searchResponse = esClient.getServiceGeneral().initScroll(searchParameters);
      scrollId = searchResponse.getScrollId();
      searchHits = searchResponse.getHits().getHits();
      do {
        for (SearchHit hit : searchHits) {
          readHit(temporaryPath, headersPerBlock, fieldsPerBlock, hit);
        }
        searchResponse = esClient.getServiceGeneral().nextScroll(scrollId);
        scrollId = searchResponse.getScrollId();
        searchHits = searchResponse.getHits().getHits();
      } while (searchHits != null && searchHits.length > 0);

      Set<NoticeType> toExport = headersPerBlock.keySet();
      if (toExport.isEmpty()) {
        return null;
      }
      return createZip(toExport, temporaryPath);
    } catch (EsRequestException | IOException e) {
      throw new ServiceException("Erreur lors de l'interogation d'ES", e);
    }

  }

  @Override
  public File exportCsv(List<String> ids) throws ServiceException {
    try {
      String processUuid = UUID.randomUUID().toString();
      File temporaryPath = new File(exportPath, processUuid);
      EsSearchParameters searchParameters = new EsSearchParameters();
      searchParameters.setQuery(QueryBuilders.termsQuery("internal.uuid", ids));

      searchParameters.setSize(ids.size());
      searchParameters.setFetchSource(true);
      searchParameters.setIndexes(Arrays.asList(EsIndexEnum.NOTICE));
      SearchResponse searchResponse = esClient.getServiceGeneral().search(searchParameters);

      SearchHit[] searchHits = searchResponse.getHits().getHits();
      EnumMap<NoticeType, Map<String, Integer>> headersPerBlock = new EnumMap<>(NoticeType.class);
      EnumMap<NoticeType, Map<String, List<String>>> fieldsPerBlock = new EnumMap<>(NoticeType.class);

      for (SearchHit hit : searchHits) {
        detectHeaders(headersPerBlock, fieldsPerBlock, hit);
      }

      for (NoticeType noticeType : NoticeType.values()) {
        writeHeaders(headersPerBlock.get(noticeType), fieldsPerBlock.get(noticeType), noticeType, temporaryPath);
      }

      for (SearchHit hit : searchHits) {
        readHit(temporaryPath, headersPerBlock, fieldsPerBlock, hit);
      }
      Set<NoticeType> toExport = headersPerBlock.keySet();
      if (toExport.isEmpty()) {
        return null;
      }
      return createZip(toExport, temporaryPath);
    } catch (EsRequestException | IOException e) {
      throw new ServiceException("Erreur lors de l'interogation d'ES", e);
    }

  }

  @Override
  public File exportSelection(String id) throws ServiceException {
    Selection selection = selectionService.get(id);
    return exportCsv(new ArrayList<>(selection.getNotices()));
  }

  @Override
  public void exportOAI() throws ServiceException {
    try {
      File lastExecutionFile = Path.of(exportJoaiPath, lastExecutionFilename).toFile();
      LocalDateTime from = LocalDateTime.ofEpochSecond(0, 0, ZoneOffset.UTC);
      if (lastExecutionFile.exists()) {
        String dateAsString = FileUtils.readFileToString(lastExecutionFile, StandardCharsets.UTF_8);
        if (!dateAsString.isBlank()) {
          from = DateUtils.parseIsoDate(dateAsString);
        }
      }
      log.info("Dernière execution le {}", from);

      EsSearchParameters searchParameters = new EsSearchParameters();
      searchParameters
          .setQuery(QueryBuilders.boolQuery().should(QueryBuilders.rangeQuery("internal.updatedDate").gt(from))
              .should(QueryBuilders.rangeQuery("internal.statusUpdatedDate").gt(from)).minimumShouldMatch(1).filter(
                  QueryBuilders.simpleQueryStringQuery(NoticeStatus.PUBLISHED.getValue()).field("internal.status")));
      searchParameters.setIndexes(Arrays.asList(EsIndexEnum.NOTICE));
      searchParameters.setSize(SCROLL_BATCH_SIZE);
      searchParameters.setFetchSource(true);

      SearchResponse searchResponse;
      searchResponse = esClient.getServiceGeneral().initScroll(searchParameters);

      String scrollId = searchResponse.getScrollId();
      SearchHit[] searchHits = searchResponse.getHits().getHits();

      log.info("Début export OAI");
      int nbDocs = 0;
      do {
        for (SearchHit hit : searchHits) {
          nbDocs = nbDocs + exportNoticeToDublinCore(hit);
        }

        searchResponse = esClient.getServiceGeneral().nextScroll(scrollId);
        scrollId = searchResponse.getScrollId();
        searchHits = searchResponse.getHits().getHits();
      } while (searchHits != null && searchHits.length > 0);

      log.info("{} notices exportées", nbDocs);
      FileUtils.write(lastExecutionFile, DateUtils.formatIsoDate(LocalDateTime.now()), StandardCharsets.UTF_8);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de l'intérogation d'ES", e);
    } catch (IOException e) {
      throw new ServiceException("Erreur lors de la lecture du fichier de dernière execution", e);
    }
  }

  private int exportNoticeToDublinCore(SearchHit hit) throws ServiceException {
    String uuid = "";
    try {
      JsonNode noticeNode = mapper.readTree(hit.getSourceAsString());
      uuid = noticeNode.at("/internal/uuid").asText();
      NoticeType noticeType = NoticeType.valueOf(noticeNode.at(INTERNAL_NOTICE_TYPE_JSONPATH).asText());

      Namespace dcNamespace = Namespace.getNamespace("dc", "http://purl.org/dc/elements/1.1/");
      Document document = createXmlDocument(dcNamespace);
      Element rootElement = document.getRootElement();

      // Commun
      addCommonElements(noticeNode, dcNamespace, rootElement);

      switch (noticeType) {
        case ARTWORK:
          addArtworkElements(noticeNode, dcNamespace, rootElement);
          break;
        case COLLECTION:
          addCollectionElements(noticeNode, dcNamespace, rootElement);
          break;
        case EVENT:
          addEventElements(noticeNode, dcNamespace, rootElement);
          break;
        case PERSON:
          addPersonElements(noticeNode, dcNamespace, rootElement);
          break;
        case REFERENCE:
          addReferenceElements(noticeNode, dcNamespace, rootElement);
          break;
        default:
          break;
      }
      writeXmlFile(uuid, document);
    } catch (IOException e) {
      throw new ServiceException("Erreur lors du parsing de la notice", e);
    } catch (Exception e) {
      log.error("Erreur inatendue lors de l'export de la notice {}", uuid, e);
      return 0;
    }
    return 1;

  }

  private void addCommonElements(JsonNode noticeNode, Namespace dcNamespace, Element rootElement) {
    addElementsToRootIfExists("identifier", noticeNode, dcNamespace, rootElement, "internal.permalink");
    addElementsToRootIfExists("contributor", noticeNode, dcNamespace, rootElement,
        "content.recordManagementInformation.recordContributor.value");
    addElementsToRootIfExists("relation", noticeNode, dcNamespace, rootElement,
        "content.recordManagementInformation.databaseLabel.value",
        "vignette : &&content.mediaInformation.prefPicture.thumbnail");
    addElementsToRootIfExists("rights", noticeNode, dcNamespace, rootElement,
        "content.recordManagementInformation.recordSource.value",
        "content.recordManagementInformation.recordLicence.value");
    addElementsToRootIfExists(TITLE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "internal.digest.displayLabelLink");
  }

  private void addReferenceElements(JsonNode noticeNode, Namespace dcNamespace, Element rootElement) {
    addElementsToRootIfExists(COVERAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.subjectInformation.subject.date",
        "content.subjectInformation.subject.rameau.thesaurus.prefLabels.value",
        "content.identificationInformation.thesis.discipline.thesaurus.prefLabels.value");
    addElementsToRootIfExists(CREATOR_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.author.name.value",
        "content.identificationInformation.author.role.thesaurus.prefLabels.value",
        "content.identificationInformation.contributor.name.value",
        "content.identificationInformation.contributor.role.thesaurus.prefLabels.value");
    addElementsToRootIfExists("date", noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.edition.date", "content.identificationInformation.thesis.defenseYear");
    addElementsToRootIfExists(LANGUAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.reference.language.thesaurus.prefLabels.value");
    addElementsToRootIfExists("publisher", noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.edition.editingInstitution.value",
        "content.identificationInformation.edition.editingPlace.thesaurus.prefLabels.value");
    addElementsToRootIfExists(SOURCE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement, ArrayUtils.addAll(
        COMMON_OAI_SOURCE,
        "content.localizationInformation.localization.institution.value&&content.localizationInformation.localization.shelfMark.value"));
    addElementsToRootIfExists("type", noticeNode, dcNamespace, rootElement, ArrayUtils.addAll(COMMON_OAI_TYPE,
        "internal.digest.referenceType", "content.identificationInformation.thesis.type.thesaurus.prefLabels.value"));

  }

  private void addPersonElements(JsonNode noticeNode, Namespace dcNamespace, Element rootElement) {
    addElementsToRootIfExists(COVERAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.name.nationality.thesaurus.prefLabels.value",
        "content.identificationInformation.life.birth.place.thesaurus.prefLabels.value",
        "content.identificationInformation.life.death.place.thesaurus.prefLabels.value",
        "content.activityInformation.activity.place.thesaurus.prefLabels.value",
        "content.activityInformation.activity.artSchool.thesaurus.prefLabels.value",
        "content.addressInformation.address.place.thesaurus.prefLabels.value",
        "content.activityInformation.activity.period.thesaurus.prefLabels.value");
    addElementsToRootIfExists("date", noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.life.birth.date", "content.identificationInformation.life.death.date",
        "content.identificationInformation.institution.date", "content.activityInformation.activity.date");
    addElementsToRootIfExists(LANGUAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement, "fre");
    addElementsToRootIfExists(SOURCE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement, ArrayUtils.addAll(
        COMMON_OAI_SOURCE,
        "content.identificationInformation.institution.code&&content.identificationInformation.institution.codeType.thesaurus.prefLabels.value"));
    addElementsToRootIfExists(SUBJECT_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.name.gender.thesaurus.prefLabels.value",
        "content.activityInformation.activity.type.thesaurus.prefLabels.value",
        "content.activityInformation.activity.institution.value",
        "content.activityInformation.activity.place.thesaurus.prefLabels.value");
    addElementsToRootIfExists("type", noticeNode, dcNamespace, rootElement,
        ArrayUtils.add(COMMON_OAI_TYPE, "internal.digest.personType"));
  }

  private void addEventElements(JsonNode noticeNode, Namespace dcNamespace, Element rootElement) {
    addElementsToRootIfExists(COVERAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.event.place.thesaurus.prefLabels.value");
    addElementsToRootIfExists(CREATOR_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.event.institution.value");
    addElementsToRootIfExists("date", noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.event.date");
    addElementsToRootIfExists(LANGUAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement, "fre");
    addElementsToRootIfExists(SOURCE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement, COMMON_OAI_SOURCE);
    addElementsToRootIfExists(SUBJECT_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.concernedArtwork.category.thesaurus.prefLabels.value",
        "content.identificationInformation.concernedPerson.person.value",
        "content.identificationInformation.concernedPerson.role.thesaurus.prefLabels.value");
    addElementsToRootIfExists("type", noticeNode, dcNamespace, rootElement,
        ArrayUtils.add(COMMON_OAI_TYPE, "internal.digest.eventType"));
  }

  private void addCollectionElements(JsonNode noticeNode, Namespace dcNamespace, Element rootElement) {
    addElementsToRootIfExists(COVERAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.datePlace.place.thesaurus.prefLabels.value");
    addElementsToRootIfExists(CREATOR_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.relatedPerson.person.value",
        "content.identificationInformation.relatedPerson.role.thesaurus.prefLabels.value");
    addElementsToRootIfExists("date", noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.datePlace.date");
    addElementsToRootIfExists(LANGUAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement, "fre");
    addElementsToRootIfExists(SOURCE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement, COMMON_OAI_SOURCE);
    addElementsToRootIfExists(SUBJECT_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.identificationInformation.description.objectType.thesaurus.prefLabels.value",
        "content.identificationInformation.description.status.thesaurus.prefLabels.value");
    addElementsToRootIfExists("type", noticeNode, dcNamespace, rootElement,
        ArrayUtils.add(COMMON_OAI_TYPE, "internal.digest.collectionType"));
  }

  private void addArtworkElements(JsonNode noticeNode, Namespace dcNamespace, Element rootElement) {
    addElementsToRootIfExists(COVERAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.creationInformation.creation.place.thesaurus.prefLabels.value",
        "content.creationInformation.creation.period.thesaurus.prefLabels.value",
        "content.historicalInformation.discovery.place.thesaurus.prefLabels.value",
        "content.historicalInformation.discovery.date");
    addElementsToRootIfExists(CREATOR_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.creationInformation.creation.person.value", "content.creationInformation.creation.person.alternative");
    addElementsToRootIfExists("date", noticeNode, dcNamespace, rootElement, "content.creationInformation.creation.date",
        "content.creationInformation.creation.dateType.thesaurus.prefLabels.value");
    addElementsToRootIfExists("format", noticeNode, dcNamespace, rootElement,
        "content.descriptionInformation.materiality.support.thesaurus.prefLabels.value",
        "content.descriptionInformation.materiality.material.thesaurus.prefLabels.value",
        "content.descriptionInformation.materiality.technical.thesaurus.prefLabels.value",
        "content.descriptionInformation.materiality.descriptionType.thesaurus.prefLabels.value");
    List<String> languagePaths = JsonUtils.getAllSubPath(
        "content.manuscriptPrintedInformation.bookContent.language.thesaurus.prefLabels.value", noticeNode, "");
    if (languagePaths.isEmpty()) {
      addElementsToRootIfExists(LANGUAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement, "fre");
    } else {
      for (String languagePath : languagePaths) {
        ThesaurusConcept thesaurusConcept = thesaurusService.thesaurusByLabel(GincoServiceImpl.THESAURUS_LANGUAGE_KEY)
            .get(noticeNode.at(languagePath).asText());
        if (thesaurusConcept != null) {
          addElementsToRootIfExists(LANGUAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
              thesaurusConcept.getAltLabel());
        } else {
          addElementsToRootIfExists(LANGUAGE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement, "fre");
        }
      }
    }
    addElementsToRootIfExists(SOURCE_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        ArrayUtils.add(COMMON_OAI_SOURCE,
            String.join("&&", "content.localizationInformation.localization.place.thesaurus.prefLabels.value",
                "content.localizationInformation.localization.institutionIdentifier.value",
                "content.localizationInformation.localization.institutionIdentifierType.thesaurus.prefLabels.value")));
    addElementsToRootIfExists(SUBJECT_DC_ELEMENT_NAME, noticeNode, dcNamespace, rootElement,
        "content.descriptionInformation.subject.garnier.thesaurus.prefLabels.value",
        "content.descriptionInformation.subject.hornbostelSachs.thesaurus.prefLabels.value",
        "content.descriptionInformation.subject.iconclass.thesaurus.prefLabels.value",
        "content.descriptionInformation.subject.rameau.thesaurus.prefLabels.value",
        "content.manuscriptPrintedInformation.printedSubject.garnier.thesaurus.prefLabels.value",
        "content.manuscriptPrintedInformation.printedSubject.technical.thesaurus.prefLabels.value");
    addElementsToRootIfExists("type", noticeNode, dcNamespace, rootElement,
        ArrayUtils.add(COMMON_OAI_TYPE, "internal.digest.artworkType"));
  }

  private void writeXmlFile(String uuid, Document document) throws IOException {
    XMLOutputter outputer = new XMLOutputter(Format.getPrettyFormat());
    File xmlFile = Path.of(exportJoaiPath, uuid + ".xml").toFile();
    FileUtils.forceMkdirParent(xmlFile);

    try (FileOutputStream output = new FileOutputStream(xmlFile)) {
      outputer.output(document, output);
    }
  }

  private Document createXmlDocument(Namespace dcNamespace) {
    Namespace dcOaiNamespace = Namespace.getNamespace("oai_dc", "http://www.openarchives.org/OAI/2.0/oai_dc/");
    Namespace xsi = Namespace.getNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");

    Element rootElement = new Element("dc", dcOaiNamespace);
    rootElement.addNamespaceDeclaration(dcNamespace);
    rootElement.addNamespaceDeclaration(xsi);
    rootElement.setAttribute("schemaLocation",
        "http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd", xsi);

    Document document = new Document();
    document.setRootElement(rootElement);
    return document;
  }

  private void addElementsToRootIfExists(String elementName, JsonNode noticeNode, Namespace dcNamespace,
      Element rootElement, String... jsonPaths) {
    for (String jsonPath : jsonPaths) {
      Element element = new Element(elementName, dcNamespace);
      List<String> content = new ArrayList<>();
      String[] paths = jsonPath.split("&&");
      String delimiter = ", ";

      int nbStaticContent = 0;
      for (String path : paths) {
        if (!path.contains("content.") && !path.contains("internal.")) {
          content.add(path);
          nbStaticContent++;
          delimiter = "";
          continue;
        }
        element = addElementToRootFromAllSubPath(elementName, noticeNode, dcNamespace, rootElement, element, content,
            path);
      }
      if (!content.isEmpty() && content.size() != nbStaticContent) {
        element.addContent(String.join(delimiter, content));
        rootElement.addContent(element);
      }
    }
  }

  private Element addElementToRootFromAllSubPath(String elementName, JsonNode noticeNode, Namespace dcNamespace,
      Element rootElement, Element element, List<String> content, String path) {
    List<String> allSubPath = JsonUtils.getAllSubPath(path, noticeNode, "");
    for (String subPath : allSubPath) {
      JsonNode node = noticeNode.at(subPath);
      if (!node.isMissingNode()) {
        String value = "";
        if (node.get("start") != null) {
          // Cas d'une date
          value = DateUtils.buildDatesFormattedFromContent(node.at("/start"), node.at("/end"), true, true);
        } else {
          value = node.asText();
        }
        if (allSubPath.size() > 1) {
          if (!content.isEmpty()) {
            // On a déjà un élément dans la liste, on veut concatener plusieurs champs, on
            // ne garde que la première occurence des champs suivants
            content.add(value);
            break;
          } else {
            element.addContent(value);
            rootElement.addContent(element);
            element = new Element(elementName, dcNamespace);
            content.clear();
          }
        } else {
          content.add(value);
        }
      }
    }
    return element;
  }

  /**
   * Détecte et sauvegarde les headers et les champs potentiels qui seront
   * nécessaire pour la création du csv à partir des données d'un
   * {@link SearchHit}.
   * 
   * Les champs sont calculés grâce à la fonction
   * {@link JsonUtils#getFieldPerBlock(String, ObjectMapper)} à partir du
   * métamodèle du type de notice du hit.
   * 
   * Les headers sont calculés à partir des champs potentiels et des données.
   * 
   * @param headersPerBlock map dans laquelle on sauvegarde les headers par type
   *                        de notice et par block de données
   * @param fieldsPerBlock  map dans laquelle on sauvegarde les champs potentiels
   *                        par type de notice et par block de données
   * @param hit             hit dans lequel sont lues les données
   * @throws IOException si une erreur de parsing se produit
   */
  private void detectHeaders(EnumMap<NoticeType, Map<String, Integer>> headersPerBlock,
      EnumMap<NoticeType, Map<String, List<String>>> fieldsPerBlock, SearchHit hit) throws IOException {
    String source = hit.getSourceAsString();
    JsonNode notice = mapper.readTree(source);
    NoticeType noticeType = NoticeType.valueOf(notice.at(INTERNAL_NOTICE_TYPE_JSONPATH).asText());
    Map<String, Integer> header = headersPerBlock.get(noticeType);
    if (header == null) {
      header = new LinkedHashMap<>();
      headersPerBlock.put(noticeType, header);
      fieldsPerBlock.put(noticeType, JsonUtils.getFieldPerBlock(metaModelService.get(null, noticeType)));
    }
    populateHeader(header, fieldsPerBlock.get(noticeType).keySet(), notice.get("content"));
  }

  /**
   * Renseigne les headers nécessaire à un export, et le nombre de répétition
   * maximum d'un block.
   * 
   * @param header   map pour stocker le block de données et son nombre de
   *                 répétition.
   * @param blocks   blocks de données à utiliser dans les headers
   * @param jsonNode données à partir de laquelle on calcul les headers
   *                 nécessaires
   */
  private void populateHeader(Map<String, Integer> header, Set<String> blocks, JsonNode jsonNode) {
    for (String blockName : blocks) {
      JsonNode block = jsonNode.at("/" + blockName.replace(".", "/"));
      int size = -1;
      if (block.isArray()) {
        size = block.size();
      }
      if (header.get(blockName) == null || header.get(blockName) < size) {
        header.put(blockName, size);
      }
    }
  }

  /**
   * Ecrit les headers dans le fichier csv pour un type de notice donné. Chaque
   * header contient l'intitulé d'un block de données, ainsi que tout les champs
   * qu'il contient, concaténés avec le séparateur "§".
   * 
   * @param headers       intitulés des blocks à écrire en tant que header, et
   *                      leur nombre de répétition
   * @param fields        mapping entre le sintitulés des blocks et les champs
   *                      qu'ils contiennent
   * @param noticeType    type de notice
   * @param temporaryPath chemin où écrire les fichiers
   * @throws IOException si une erreur se produit lors du traitement des
   *                     différents fichiers
   */
  private void writeHeaders(Map<String, Integer> headers, Map<String, List<String>> fields, NoticeType noticeType,
      File temporaryPath) throws IOException {
    if (headers != null) {
      List<String> headersList = headers.entrySet().stream().map(entry -> {
        List<String> blocks = new ArrayList<>();
        Integer nbBlocks = entry.getValue();
        List<String> fieldsForBlock = fields.get(entry.getKey());
        if (nbBlocks > 0) {
          for (int i = 1; i <= nbBlocks; i++) {
            blocks.add(entry.getKey() + i + "§" + String.join("§", fieldsForBlock));
          }
        } else {
          if (CollectionUtils.isNotEmpty(fieldsForBlock)) {
            blocks.add(entry.getKey() + "§" + String.join("§", fieldsForBlock));
          } else {
            blocks.add(entry.getKey());
          }
        }
        return String.join(";", blocks);
      }).collect(Collectors.toList());
      headersList.add(0, "status");
      headersList.add(0, "uuid");
      FileUtils.write(new File(temporaryPath, PREFIX_FILE_NAME_EXPORT + noticeType.getExportFileName() + ".csv"),
          String.join(";", headersList) + "\n", "UTF-8", true);
    }
  }

  /**
   * Lecture d'un résultat de recherche pour écriture de ses données dans le bon
   * fichier csv.
   * 
   * @param temporaryPath   chemin où écrire le fichier csv
   * @param headersPerBlock map dans laquelle sont sauvegardés les headers par
   *                        type de notice et par block de données
   * @param fieldsPerBlock  map dans laquelle sont sauvegardés les champs
   *                        potentiels par type de notice et par block de données
   * @param hit             hit à lire
   * @throws IOException si un erreur se produit lors de l'écriture du fichier csv
   */
  private void readHit(File temporaryPath, EnumMap<NoticeType, Map<String, Integer>> headersPerBlock,
      EnumMap<NoticeType, Map<String, List<String>>> fieldsPerBlock, SearchHit hit) throws IOException {
    String source = hit.getSourceAsString();
    JsonNode notice = mapper.readTree(source);

    List<String> values = new ArrayList<>();

    values.add(notice.at("/internal/uuid").asText());

    NoticeStatus status = NoticeStatus.fromValue(notice.at("/internal/status").asText());
    if (status == null) {
      // Le status est inconnu, on ignore la notice en cours
      return;
    }
    values.add(status.getValue());

    NoticeType noticeType = NoticeType.valueOf(notice.at(INTERNAL_NOTICE_TYPE_JSONPATH).asText());
    Map<String, List<String>> fields = fieldsPerBlock.get(noticeType);
    JsonNode content = notice.at("/content");
    for (Entry<String, List<String>> entry : fields.entrySet()) {
      JsonNode block = content.at("/" + entry.getKey().replace(".", "/"));
      int nbBlock = headersPerBlock.get(noticeType).get(entry.getKey());
      if (nbBlock > 0) {
        for (int i = 0; i < nbBlock; i++) {
          readBlock(values, entry.getValue(), block, i);
        }
      } else {
        readBlock(values, entry.getValue(), block, -1);
      }
    }
    FileUtils.write(new File(temporaryPath, PREFIX_FILE_NAME_EXPORT + noticeType.getExportFileName() + ".csv"),
        String.join(";", values) + "\n", "UTF-8", true);
  }

  /**
   * Lecture d'un block de données et écriture de ses données dans valuesToWrite.
   * 
   * @param valuesToWrite liste de String dans laquelle sont écrits toutes les
   *                      valeurs du block
   * @param fieldValues   liste des champs dans lesquels lire les données
   * @param block         block dans lequel lire les données
   * @param index         index où lire les données si le block est un tableau
   */
  private void readBlock(List<String> valuesToWrite, List<String> fieldValues, JsonNode block, int index) {
    StringBuilder sb = new StringBuilder();
    if (!block.isValueNode()) {
      JsonNode element = null;
      if (block.isArray()) {
        element = block.get(index);
      } else if (block.isObject()) {
        element = block;
      }
      if (element != null) {
        int nbValues = 0;
        for (String field : fieldValues) {
          readData(sb, element, ++nbValues, field);
        }
        valuesToWrite.add(escapeSpecialCharacters(sb.toString()));
      } else {
        valuesToWrite.add(StringUtils.repeat("§", fieldValues.size()));
      }
    } else {
      valuesToWrite.add(escapeSpecialCharacters(block.asText()));
    }
  }

  /**
   * Lecture d'un champ de données.<br>
   * Les données lues sont ajoutées à la suite dans le StringBuiler, séparés par
   * le caractère "§".
   * 
   * @param sb       StringBuilder où écrire les données lues
   * @param element  {@link JsonObject} dans lequel lire la donnée
   * @param nbValues nombre de valeurs déjà lues dans l'element
   * @param field    nom du champ dans lequel lire les données
   */
  private void readData(StringBuilder sb, JsonNode element, int nbValues, String field) {
    if (nbValues > 0) {
      sb.append("§");
    }
    if (field.contains("[]")) {
      int index = 0;
      List<String> multiValues = new ArrayList<>();
      String fieldPath = field.replace(".", "/");
      do {
        JsonNode fieldObject = element.at("/" + fieldPath.replace("[]", index + ""));
        multiValues.add(fieldObject.asText());
        index++;
      } while (!element.at("/" + fieldPath.replace("[]", index + "")).isMissingNode());
      sb.append(String.join("¤", multiValues));
    } else {
      JsonNode fieldObject = element.at("/" + field.replace(".", "/"));
      // Cas d'une date
      if (field.endsWith("date.display")) {
        fieldObject = element.at("/" + field.replace(".display", "").replace(".", "/"));
        JsonNode optionalStart = fieldObject.at("/start");
        if (!optionalStart.isMissingNode()) {
          sb.append(DateUtils.buildDatesFormattedFromContent(optionalStart, fieldObject.at("/end"), false));
        }
      }
      sb.append(fieldObject.asText());
    }
  }

  /**
   * Création du fichier zip à partir des fichiers csv par type de notice.
   * 
   * @param set           types de notice à inclure dans lz zip
   * @param temporaryPath chemin où trouver les fichiers csv, et où écrire le zip
   * @return {@link File} pointant sur le fichier zip nouvellement créer
   * @throws IOException si une erreur se produit lors du traitement des
   *                     différents fichiers
   */
  private File createZip(Set<NoticeType> set, File temporaryPath) throws IOException {
    File zip = new File(temporaryPath, "export" + ".zip");
    try (FileOutputStream fos = new FileOutputStream(zip); ZipOutputStream zipOut = new ZipOutputStream(fos);) {

      for (NoticeType noticeType : set) {
        File file = new File(temporaryPath, PREFIX_FILE_NAME_EXPORT + noticeType.getExportFileName() + ".csv");
        try (FileInputStream fis = new FileInputStream(file)) {
          ZipEntry zipEntry = new ZipEntry(file.getName());
          zipOut.putNextEntry(zipEntry);

          byte[] bytes = new byte[1024];
          int length;
          while ((length = fis.read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
          }
        }
        Files.delete(file.toPath());
      }
    }
    return zip;
  }

  public String escapeSpecialCharacters(String data) {
    String escapedData = data.replaceAll("\\R", " ");
    if (data.contains(",") || data.contains("\"") || data.contains("'") || data.contains(";")) {
      data = data.replace("\"", "\"\"");
      escapedData = "\"" + data + "\"";
    }
    return escapedData;
  }

}
