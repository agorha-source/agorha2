package fr.inha.agorha2.service;

import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.model.NoticeType;

public interface MappingService {

  public String generateFromJson(String jsonSchema);

  public void updateMetamodel(NoticeType noticeType) throws ServiceException;

}
