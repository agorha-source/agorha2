package fr.inha.agorha2.service.impl;

import fr.inha.agorha2.bean.ExternalThesaurusConcept;
import fr.inha.agorha2.service.ExternalThesaurusService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Interface d'accès aux services Ginco du MCC (thésaurus Externe). Permet
 * l'appel aux services rest de Ginco Admin.
 * 
 * @author jcortinovis
 *
 */
@Service
public class GincoMCCServiceImpl implements ExternalThesaurusService {

  private static final String SEARCH_URL = "/thesaurus/sparql";

  private static final String REQUEST_PREFIX_SKOS = "PREFIX skos: <http://www.w3.org/2004/02/skos/core#> \r\n";
  private static final String REQUEST_PREFIX_CD = "PREFIX dc: <http://purl.org/dc/elements/1.1/> \r\n";
  private static final String REQUEST_PROPERTIES = "SELECT distinct ?broaderName ?broader ?uri ?name ?thesaurus WHERE { \r\n";
  private static final String REQUEST_URI_NAME = "    ?uri skos:prefLabel ?name .\r\n";
  private static final String REQUEST_THESAURUS = "    ?uri skos:inScheme/dc:title ?thesaurus .\r\n";
  private static final String REQUEST_BROADER = "    ?broader skos:narrower ?uri .\r\n";
  private static final String REQUEST_BROADER_NAME = "    ?broader skos:prefLabel ?broaderName .\r\n";
  private static final String REQUEST_FILTER = "    FILTER (lang(?name)='fr-fr')\r\n";
  private static final String REQUEST_REGEX_START = "    FILTER regex(?name , \"";
  private static final String REQUEST_REGEX_END = "\", \"i\") \r\n";

  private static final int RESULT_LIMIT = 10;

  @Value(value = "${ginco.mcc}")
  private String gincoWSUrl;

  @Override
  public List<ExternalThesaurusConcept> autocomplete(String terms, Integer limit)
      throws IOException, ParserConfigurationException, SAXException {
    List<ExternalThesaurusConcept> results = new ArrayList<>();
    String urlAsString = gincoWSUrl + SEARCH_URL + "?query="
        + URLEncoder.encode(requestComposer(terms, limit), StandardCharsets.UTF_8);

    Document document = getResponseAsXMLDocument(urlAsString);
    document.getDocumentElement().normalize();

    NodeList nodeResults = document.getDocumentElement().getElementsByTagName("results");

    Element elements = (Element) nodeResults.item(0);
    nodeResults = elements.getElementsByTagName("result");

    for (int i = 0; i < nodeResults.getLength(); i++) {
      elements = (Element) nodeResults.item(i);
      NodeList bindings = elements.getElementsByTagName("binding");

      ExternalThesaurusConcept externalThesaurus = new ExternalThesaurusConcept();

      for (int j = 0; j < bindings.getLength(); j++) {
        Element subBinding = (Element) bindings.item(j);
        String bindingName = subBinding.getAttribute("name");

        if (bindingName.equals("uri")) {
          externalThesaurus.setUri(subBinding.getElementsByTagName("uri").item(0).getTextContent());
        } else if (bindingName.equals("name")) {
          externalThesaurus.setName(subBinding.getElementsByTagName("literal").item(0).getTextContent());
        } else if (bindingName.equals("broader")) {
          externalThesaurus.setBroader(subBinding.getElementsByTagName("uri").item(0).getTextContent());
        } else if (bindingName.equals("broaderName")) {
          externalThesaurus.setBroaderName(subBinding.getElementsByTagName("literal").item(0).getTextContent());
        } else if (bindingName.equals("thesaurus")) {
          externalThesaurus.setThesaurus(subBinding.getElementsByTagName("literal").item(0).getTextContent());
        }
      }
      results.add(externalThesaurus);
    }

    return results;
  }

  private Document getResponseAsXMLDocument(String urlAsString)
      throws IOException, ParserConfigurationException, SAXException {
    URL url = new URL(urlAsString);
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod("GET");
    conn.setRequestProperty("Accept", "application/json");

    StringBuilder sb = new StringBuilder();
    try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
      String output;
      while ((output = br.readLine()) != null) {
        sb.append(output);
      }
    } finally {
      conn.disconnect();
    }

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    return builder.parse(new InputSource(new StringReader(sb.toString())));
  }

  public String requestComposer(String terms, Integer limit) {
    String request = REQUEST_PREFIX_SKOS + REQUEST_PREFIX_CD + REQUEST_PROPERTIES + REQUEST_URI_NAME + REQUEST_THESAURUS
        + REQUEST_BROADER + REQUEST_BROADER_NAME + REQUEST_FILTER;
    if (!terms.isEmpty()) {
      request = request.concat(REQUEST_REGEX_START + terms + REQUEST_REGEX_END);
    }
    limit = limit != null ? limit : RESULT_LIMIT;
    request = request.concat("} LIMIT " + limit.toString());

    return request;
  }

}
