package fr.inha.agorha2.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.BadRequestException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;

import fr.inha.agorha2.api.dto.MediaItem;
import fr.inha.agorha2.exception.FunctionnalException;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.ImportService;
import fr.inha.agorha2.service.MediaDataSourceMode;
import fr.inha.agorha2.service.MediaService;
import fr.inha.agorha2.service.MetaModelService;
import fr.inha.agorha2.service.NoticeDataSourceMode;
import fr.inha.agorha2.service.NoticeService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.agorha2.utils.FileUtils;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.MediaType;
import fr.inha.elastic.model.NoticeStatus;
import fr.inha.elastic.model.NoticeType;

@Service
public class ImportServiceImpl implements ImportService {

  private static final String JSON_EXTENSION = ".json";

  private static final String SUCCESS_PATH = "success";

  private static final String LOGFILE_DATE_PATTERN = "yyyyMMddHHmmss";

  private static final Logger logImportCsv = LoggerFactory.getLogger("import-csv");
  private static final Logger logImportMedia = LoggerFactory.getLogger("import-media");

  private ObjectMapper mapper = DefaultEsContext.instance().getObjectMapper();

  @Value(value = "${agorha2.batch.importCsvDir}")
  private String importCsvDir;

  @Value(value = "${agorha2.batch.importMedia.pathJson}")
  private String importMediaPathToJson;

  @Value(value = "${agorha2.batch.importMedia.pathResources}")
  private String importMediaPathToResources;

  @Autowired
  private NoticeService noticeService;

  @Autowired
  private MediaService mediaService;

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private MetaModelService metaModelService;

  @Override
  public void importCsv() throws ServiceException {

    Collection<File> csvFiles = org.apache.commons.io.FileUtils.listFiles(Path.of(importCsvDir).toFile(),
        new String[] { "csv" }, false);

    for (File csvFile : csvFiles) {
      String filename = csvFile.getName();

      MDC.put("importFileName",
          filename.replace(".csv", "") + "_" + DateUtils.formatDate(LocalDateTime.now(), LOGFILE_DATE_PATTERN));

      logImportCsv.info("Traitement du fichier {}", filename);
      try (CSVParser parser = CSVParser.parse(csvFile, StandardCharsets.UTF_8,
          CSVFormat.EXCEL.withDelimiter(';').withFirstRecordAsHeader());) {

        NoticeType noticeType = NoticeType.fromFileName(filename.replace(".csv", ""));
        if (noticeType == null) {
          logImportCsv.error("Nom de fichier incorrect");
          throw new BadRequestException("Nom de fichier incorrect, valeurs autorisées : " + Stream
              .of(NoticeType.values()).map(type -> type.getExportFileName() + ".csv").collect(Collectors.toList()));
        }

        JsonNode schema = mapper.readTree(metaModelService.get(null, noticeType));
        Map<String, Integer> headerMap = parser.getHeaderMap();
        for (CSVRecord csvRecord : parser) {
          logImportCsv.info("Traitement de la ligne {}", csvRecord.getRecordNumber());
          if (!csvRecord.isConsistent()) {
            logImportCsv.warn("Les informations de la ligne {} ne correspondent pas aux informations de l’en-tête",
                csvRecord.getRecordNumber());
            continue;
          }

          ObjectNode noticeNode = createNoticeFromRecord(csvRecord, headerMap, schema);
          if (noticeNode != null) {
            JsonUtils.addValueToNodeFromPath("internal.noticeType", noticeNode, noticeType.name());
            JsonUtils.addValueToNodeFromPath("internal.originImportFile", noticeNode, filename);
            JsonUtils.addValueToNodeFromPath("internal.originImportFileLine", noticeNode,
                csvRecord.getRecordNumber() + "");

            String uuid = noticeNode.at("/internal/uuid").asText();
            String noticeAsString = mapper.writeValueAsString(noticeNode);

            createOrUpdateNotice(csvRecord.getRecordNumber(), uuid, noticeAsString);
          }
        }
      } catch (IOException e) {
        logImportCsv.error("Erreur lors de la lecture du fichier", e);
        FileUtils.moveFile(csvFile, importCsvDir, "errors", filename.replace(".csv", "") + "_"
            + DateUtils.formatDate(LocalDateTime.now(), LOGFILE_DATE_PATTERN) + ".csv");
      }
      FileUtils.moveFile(csvFile, importCsvDir, SUCCESS_PATH, filename.replace(".csv", "") + "_"
          + DateUtils.formatDate(LocalDateTime.now(), LOGFILE_DATE_PATTERN) + ".csv");
    }
  }

  @Override
  public void importMedia() throws ServiceException {

    Collection<File> mediaFIles = org.apache.commons.io.FileUtils.listFiles(Path.of(importMediaPathToJson).toFile(),
        new String[] { "json" }, false);

    for (File file : mediaFIles) {

      String filename = file.getName();
      MDC.put("importFileName",
          filename.replace(JSON_EXTENSION, "") + "_" + DateUtils.formatDate(LocalDateTime.now(), LOGFILE_DATE_PATTERN));
      logImportMedia.info("Traitement du fichier {}", filename);

      int nbMediaOk = 0;
      int nbMediaKo = 0;

      try {
        JsonNode mediaNode = mapper.readTree(file);
        int index = 0;
        for (Iterator<JsonNode> iterator = mediaNode.get("medias").elements(); iterator.hasNext();) {
          index++;
          boolean created = createMediaFromBatchImport(iterator.next(), index);
          if (created) {
            nbMediaOk++;
          } else {
            nbMediaKo++;
          }
        }
        logImportMedia.info("{} fichiers traités, {} media importés, {} en erreur", mediaFIles.size(), nbMediaOk,
            nbMediaKo);
        // Déplacement du fichier dans success
        fr.inha.agorha2.utils.FileUtils.moveFile(file, importMediaPathToJson, SUCCESS_PATH,
            filename.replace(JSON_EXTENSION, "") + "_" + DateUtils.formatDate(LocalDateTime.now(), LOGFILE_DATE_PATTERN)
                + JSON_EXTENSION);
      } catch (Exception e) {
        // Déplacement du fichier dans errors
        fr.inha.agorha2.utils.FileUtils.moveFile(file, importMediaPathToJson, "errors",
            filename.replace(JSON_EXTENSION, "") + "_" + DateUtils.formatDate(LocalDateTime.now(), LOGFILE_DATE_PATTERN)
                + JSON_EXTENSION);
      }
    }

  }

  private void createOrUpdateNotice(long lineNumber, String uuid, String noticeAsString) {
    try {
      if (StringUtils.isNotBlank(uuid)) {
        logImportCsv.info("Mise à jour de la notice {}", noticeAsString);
        noticeService.update(uuid, noticeAsString, NoticeDataSourceMode.IMPORT);
      } else {
        logImportCsv.info("Création de la notice {}", noticeAsString);
        noticeService.create(noticeAsString, NoticeDataSourceMode.IMPORT);
      }
    } catch (Exception e) {
      logImportCsv.warn("Erreur lors du traitement de la ligne {}", lineNumber, e);
    }
  }

  ObjectNode createNoticeFromRecord(CSVRecord csvRecord, Map<String, Integer> headerMap, JsonNode schema) {
    ObjectNode notice = mapper.createObjectNode();
    ObjectNode contentNode = notice.putObject("content");
    for (Entry<String, Integer> headerEntry : headerMap.entrySet()) {
      String columnName = headerEntry.getKey();
      String value = csvRecord.get(columnName);

      if ("uuid".equals(columnName)) {
        notice.putObject("internal").put("uuid", value);
      } else if ("status".equals(columnName)) {
        NoticeStatus status = NoticeStatus.fromValue(value);
        if (status == null) {
          logImportCsv.warn("Ligne {}, status {} inconnu", csvRecord.getRecordNumber(), value);
          return null;
        }
        JsonUtils.addValueToNodeFromPath("internal.status", notice, status.getValue(), null);
      } else if (StringUtils.isNotEmpty(value)) {
        readRecordValue(contentNode, columnName, value, schema);
      }
    }
    return notice;
  }

  private void readRecordValue(ObjectNode contentNode, String columnName, String value, JsonNode schema) {
    String[] blockProperties = columnName.split("§");
    String[] blockValues = value.split("§", -1);
    String blockPath = blockProperties[0];
    for (int i = 1; i < blockProperties.length; i++) {
      if (StringUtils.isNotEmpty(blockValues[i])) {
        String blockProperty = blockProperties[i];
        int valueIndex = 0;
        for (String blockValue : blockValues[i].split("¤")) {
          String blockPropertyWithIndex = blockProperty.replace("[]", valueIndex + "");
          String blockPropertyWithoutIndex = blockProperty.replace("[].", "");
          if (blockProperty.endsWith("date.display")) {
            continue;
          } else {
            JsonNode associatedFieldFromSchema = JsonUtils.getAssociatedFieldFromSchema(schema,
                "content." + blockPath.replaceFirst("[0-9]+", "") + "." + blockPropertyWithoutIndex);
            String valueType = associatedFieldFromSchema.get("type").asText();
            JsonUtils.addValueToNodeFromPath(blockPath + "." + blockPropertyWithIndex, contentNode, blockValue,
                valueType);
          }
          valueIndex++;
        }
      }
    }
  }

  private boolean createMediaFromBatchImport(JsonNode mediaNode, int index)
      throws ServiceException, FunctionnalException, IOException {
    try {
      String uuid = mediaNode.at("/uuid").asText();
      Media media = null;
      if (StringUtils.isNotBlank(uuid)) {
        media = mediaService.get(uuid);
      } else {
        media = new Media();
        media.setId(UUID.randomUUID().toString());
      }

      String source = mediaNode.at("/fileProvenance").asText();
      if (StringUtils.isNotBlank(source)) {
        media.setFileProvenance(source);
      }
      String licence = mediaNode.at("/licence").asText();
      if (StringUtils.isNotBlank(licence)) {
        media.setLicence(licence);
      }
      String comment = mediaNode.at("/comment").asText();
      if (StringUtils.isNotBlank(comment)) {
        media.setComment(comment);
      }
      String caption = mediaNode.at("/caption").asText();
      if (StringUtils.isBlank(caption)) {
        throw new FunctionnalException(messageSource.getMessage("media.title.mandatory", new Object[] {}, null));
      }
      media.setCaption(caption);

      List<MediaItem> mediaItems = new ArrayList<>();
      for (Iterator<JsonNode> iterator = mediaNode.get("files").elements(); iterator.hasNext();) {
        createMediaItemFromJson(mediaItems, iterator.next());
      }
      if (StringUtils.isNoneBlank(uuid)) {
        mediaService.updateMedia(uuid, mediaItems, media, MediaDataSourceMode.IMPORT);
      } else {
        mediaService.createMedia(mediaItems, media, MediaDataSourceMode.IMPORT);
      }

      for (MediaItem mediaItem : mediaItems) {
        mediaItem.getInputStream().close();
        if (MediaType.LOCAL.equals(mediaItem.getMediaType()) || MediaType.IIIF.equals(mediaItem.getMediaType())) {
          File file = Path.of(importMediaPathToResources, mediaItem.getName()).toFile();
          String extension = FilenameUtils.getExtension(file.getName());
          fr.inha.agorha2.utils.FileUtils.moveFile(file, importMediaPathToResources, SUCCESS_PATH,
              file.getName().replace("." + extension, "")
                  + DateUtils.formatDate(LocalDateTime.now(), LOGFILE_DATE_PATTERN) + "." + extension);
        }
      }
    } catch (IOException | FunctionnalException e) {
      logImportMedia.error("Erreur lors du traitement du {} media", index, e);
      return false;
    }
    return true;
  }

  private void createMediaItemFromJson(List<MediaItem> mediaItems, JsonNode fileNode)
      throws IOException, FunctionnalException {
    String mediaTypeAsText = fileNode.get("mediaType").asText();
    MediaType mediaType = MediaType.valueOf(mediaTypeAsText);
    if (MediaType.LOCAL.equals(mediaType) || MediaType.IIIF.equals(mediaType)) {
      MediaItem mediaItem = new MediaItem();
      mediaItem.setName(fileNode.get("originFile").asText());
      mediaItem.setMediaType(mediaType);
      mediaItem.setInputStream(org.apache.commons.io.FileUtils
          .openInputStream(Path.of(importMediaPathToResources, fileNode.get("originFile").asText()).toFile()));
      mediaItems.add(mediaItem);
    } else if (MediaType.YOUTUBE.equals(mediaType) || MediaType.FLICKR.equals(mediaType)
        || MediaType.SKETCHFAB.equals(mediaType)) {
      MediaItem mediaItem = new MediaItem();
      mediaItem.setMediaType(mediaType);
      mediaItem.setInputStream(IOUtils.toInputStream(fileNode.get("url").asText(), StandardCharsets.UTF_8));
      mediaItems.add(mediaItem);
    } else {
      throw new FunctionnalException("Type de media non supproté pour l'import en masse");
    }

  }
}
