package fr.inha.agorha2.service;

import fr.inha.agorha2.api.dto.request.FindDatabaseRequest;
import fr.inha.agorha2.api.dto.response.DatabaseListResponse;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.model.Database;
import java.util.List;

public interface DatabaseService {

  Database create(Database database) throws ServiceException;

  DatabaseListResponse list(List<String> noticeTypes) throws ServiceException;

  Database get(Integer id) throws ServiceException;

  Database find(FindDatabaseRequest request) throws ServiceException;

  List<Database> autocomplete(String terms) throws ServiceException;

}
