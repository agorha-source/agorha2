package fr.inha.agorha2.service;

public enum NoticeGetMode {
  DEFAULT,
  CONSULTATION,
  MOSAIC_CARDS
}
