package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.search.EsSearchParameters;
import fr.inha.agorha2.api.dto.Aggregation;
import fr.inha.agorha2.api.dto.GraphLink;
import fr.inha.agorha2.api.dto.GraphNode;
import fr.inha.agorha2.api.dto.MediaRetrieveMode;
import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import fr.inha.agorha2.api.dto.request.MediaSearchRequest;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest;
import fr.inha.agorha2.api.dto.request.UserSearchRequest;
import fr.inha.agorha2.api.dto.response.GraphApiResponse;
import fr.inha.agorha2.api.dto.response.MediaSearchResponse;
import fr.inha.agorha2.api.dto.response.NoticeSearchResponse;
import fr.inha.agorha2.api.dto.response.TimelineSearchResponse;
import fr.inha.agorha2.api.dto.response.UserSearchResponse;
import fr.inha.agorha2.api.dto.result.FacetResult;
import fr.inha.agorha2.api.dto.result.MediaSearchResult;
import fr.inha.agorha2.api.dto.result.NoticeSearchResult;
import fr.inha.agorha2.api.dto.result.TimelineSearchResult;
import fr.inha.agorha2.api.dto.result.UserSearchResult;
import fr.inha.agorha2.bean.CustomFrenchAnalyzer;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.service.MediaService;
import fr.inha.agorha2.service.NoticeService;
import fr.inha.agorha2.service.SearchService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.agorha2.utils.LuceneUtils;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.config.EsIndexEnum;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.NoticeType;
import fr.inha.elastic.model.Selection;
import fr.inha.elastic.model.UserStatus;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.ws.rs.POST;
import javax.ws.rs.WebApplicationException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.search.MultiSearchResponse.Item;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
@ConfigurationProperties(prefix = "agorha2")
public class SearchServiceImpl implements SearchService {

  private static final String INTERNAL_DIGEST_DISPLAY_LABEL_LINK_PATH = "/internal/digest/displayLabelLink";
  private static final String INTERNAL_UUID_PATH = "/internal/uuid";
  private static final String EXCEPTION_ERROR_SEARCH_NOTICE = "Erreur lors de la recherche de notices";
  private static final List<String> SEARCH_SOURCE_INCLUDE = Arrays.asList("internal.digest.*",
      EsQueryService.INTERNAL_UUID_FIELD, "content.mediaInformation.prefPicture",
      "content.mediaInformation.associatedMedia.ref");
  private static final List<String> TIMELINE_SEARCH_SOURCE_INCLUDE = ListUtils.union(SEARCH_SOURCE_INCLUDE,
      Arrays.asList("content.descriptionInformation.description.note.value",
          "content.identificationInformation.description.descriptiveNote.value",
          "content.identificationInformation.eventType.comment", "content.activityInformation.biography.note",
          "content.identificationInformation.reference.comment", "content.mediaInformation.associatedMedia.digest"));
  private static final List<String> MEDIA_SEARCH_SOURCE_INCLUDE = Arrays.asList("caption",
      EsQueryService.INTERNAL_UUID_FIELD, "files.thumbnail");
  private static final List<String> USER_SEARCH_SOURCE_INCLUDE = Arrays.asList(EsQueryService.INTERNAL_UUID_FIELD,
      "internal.createdDate", "internal.firstname", "internal.lastname", "internal.status", "authorizations.role");

  @Autowired
  private EsClient esClient;

  @Autowired
  private ObjectMapper mapper;

  @Autowired
  private EsQueryService esQueryService;

  @Autowired
  private CustomFrenchAnalyzer analyzer;

  @Autowired
  private UserInfoService userInfoService;

  @Autowired
  private MessageSource messageSource;

  @Autowired
  private MediaService mediaService;

  @Autowired
  private NoticeService noticeService;

  @Value(value = "${resources.fullBaseUrl}")
  private String fullBaseUrl;

  private Map<String, List<String>> timelineDateFields;
  private Map<String, List<String>> mapGeoPointFields;

  @Override
  @POST
  public NoticeSearchResponse search(NoticeSearchRequest request, NoticeType noticeType) throws ServiceException {
    try {
      Selection selection = null;
      if (StringUtils.isNotEmpty(request.getSelectionId())) {
        selection = esClient.getServiceSelection().get(request.getSelectionId());
        // Si on filtre sur une selection sans notices associées, on renvoi un résultat
        // vide
        if (selection != null && selection.getNotices().isEmpty()) {
          return new NoticeSearchResponse();
        }
      }
      EsSearchParameters searchParameters = esQueryService.createSearchParameters(request, noticeType, selection, true);

      List<String> geoPointFields = new ArrayList<>();
      // Si pas de champ précisé dans la requête, on prend ceux définis dans la
      // configuration
      if (request.getFields().isEmpty()) {
        for (List<String> fields : mapGeoPointFields.values()) {
          for (String field : fields) {
            geoPointFields.add(field);
          }
        }
      } else {
        // Sinon on utilise les champs de la requête
        geoPointFields.addAll(request.getFields());
      }

      List<String> fieldsWithoutLabel = geoPointFields.stream().map(field -> field.replaceFirst("\\s\\(.*\\)", ""))
          .collect(Collectors.toList());

      searchParameters.setSourceInclude(ListUtils.union(SEARCH_SOURCE_INCLUDE, fieldsWithoutLabel));
      searchParameters.setPage(0);
      searchParameters.setSize(0);

      BoolQueryBuilder queryWithoutFacets = (BoolQueryBuilder) searchParameters.getQuery();

      List<QueryBuilder> originalFilters = new ArrayList<>();
      originalFilters.addAll(queryWithoutFacets.filter());
      final List<FacetResult> aggs = buildAndRetrieveAggs(request.getAggregations(), searchParameters,
          queryWithoutFacets, originalFilters);

      // Filtre sur les valeurs cochées dans les facettes
      queryWithoutFacets.filter().clear();
      queryWithoutFacets.filter().addAll(originalFilters);
      searchParameters.setQuery(queryWithoutFacets);
      esQueryService.addFacetsFilters(request.getAggregations(), queryWithoutFacets);
      searchParameters.setPage(request.getPage() - 1);
      searchParameters.setSize(request.getPageSize());

      SearchResponse searchResponse = esClient.getServiceGeneral().search(searchParameters);

      NoticeSearchResponse buildResponse = buildResponse(request.getPage(), request.getPageSize(), searchResponse);
      buildResponse.getFacets().addAll(aggs);

      if (selection != null && userInfoService.isAuthenticated()) {
        String currentUserId = userInfoService.getPrincipal().getUser().getInternal().getUuid();
        buildResponse.setSelectionBelongToUser(selection.getUserId().equals(currentUserId));
      }
      return buildResponse;

    } catch (EsRequestException | IOException e) {
      throw new ServiceException(EXCEPTION_ERROR_SEARCH_NOTICE, e);
    }
  }

  @Override
  public TimelineSearchResponse searchTimeline(NoticeSearchRequest request, NoticeType noticeType)
      throws ServiceException {
    try {
      Selection selection = null;
      if (StringUtils.isNotEmpty(request.getSelectionId())) {
        selection = esClient.getServiceSelection().get(request.getSelectionId());
        // Si on filtre sur une selection sans notices associées, on renvoi un résultat
        // vide
        if (selection.getNotices().isEmpty()) {
          return new TimelineSearchResponse();
        }
      }
      EsSearchParameters searchParameters;
      if (request instanceof AdvancedSearchRequest) {
        searchParameters = esQueryService.createAdvancedSearchParameters((AdvancedSearchRequest) request, noticeType,
            selection);
      } else {
        searchParameters = esQueryService.createSearchParameters(request, noticeType, selection);
      }

      List<String> dateFields = new ArrayList<>();
      // Si pas de champ précisé dans la requête, on prend ceux définis dans la
      // configuration
      if (request.getFields().isEmpty()) {
        for (List<String> fields : timelineDateFields.values()) {
          for (String field : fields) {
            dateFields.add(field);
          }
        }
      } else {
        // Sinon on utilise les champs de la requête
        dateFields.addAll(request.getFields());
      }

      List<String> fieldsWithoutLabel = dateFields.stream().map(field -> field.replaceFirst("\\s\\(.*\\)", ""))
          .collect(Collectors.toList());
      searchParameters.setSourceInclude(ListUtils.union(TIMELINE_SEARCH_SOURCE_INCLUDE, fieldsWithoutLabel));
      searchParameters.setPage(0);
      searchParameters.setSize(0);

      BoolQueryBuilder queryWithoutFacets = (BoolQueryBuilder) searchParameters.getQuery();

      List<QueryBuilder> originalFilters = new ArrayList<>();
      originalFilters.addAll(queryWithoutFacets.filter());
      final List<FacetResult> aggs = buildAndRetrieveAggs(request.getAggregations(), searchParameters,
          queryWithoutFacets, originalFilters);

      // Filtre sur les valeurs cochées dans les facettes
      queryWithoutFacets.filter().clear();
      queryWithoutFacets.filter().addAll(originalFilters);
      searchParameters.setQuery(queryWithoutFacets);
      esQueryService.addFacetsFilters(request.getAggregations(), queryWithoutFacets);
      searchParameters.setPage(request.getPage() - 1);
      searchParameters.setSize(request.getPageSize());

      SearchResponse searchResponse = esClient.getServiceGeneral().search(searchParameters);

      TimelineSearchResponse buildResponse = buildTimelineResponse(request.getPage(), request.getPageSize(),
          searchResponse, dateFields);
      buildResponse.getFacets().addAll(aggs);

      return buildResponse;

    } catch (EsRequestException | IOException e) {
      throw new ServiceException(EXCEPTION_ERROR_SEARCH_NOTICE, e);
    }
  }

  @Override
  public GraphApiResponse searchGraph(String selectionId, String noticeId) throws ServiceException {
    try {
      Selection selection = null;
      if (StringUtils.isNotEmpty(selectionId)) {
        selection = esClient.getServiceSelection().get(selectionId);
        // Si on filtre sur une selection sans notices associées, on renvoi un résultat
        // vide
        if (selection.getNotices().isEmpty()) {
          return new GraphApiResponse();
        }
      }
      EsSearchParameters searchParameters = esQueryService.createGraphFirstStepSearchParameters(selection, noticeId);
      SearchResponse searchResponse = esClient.getServiceGeneral().search(searchParameters);

      // Liste des uuids pour lequels on veux les notices liées
      // Notices liées à la notice source (noticeId) dans la selection + toutes les
      // notices liées de ces notices
      // Notice source + toute les notices liées à la notice source
      Set<String> uuids = new HashSet<>();
      uuids.add(noticeId);

      // Récupération des notices liées dans la selection
      for (SearchHit hit : searchResponse.getHits()) {
        JsonNode source = mapper.readTree(hit.getSourceAsString());
        uuids.add(source.at(INTERNAL_UUID_PATH).asText());
      }

      searchParameters = esQueryService.createGraphSecondStepSearchParameters(uuids);
      searchResponse = esClient.getServiceGeneral().search(searchParameters);

      return buildGraphResponse(searchResponse);
    } catch (EsRequestException | IOException e) {
      throw new ServiceException(EXCEPTION_ERROR_SEARCH_NOTICE, e);
    }
  }

  @Override
  public NoticeSearchResponse advancedSearch(AdvancedSearchRequest request, NoticeType noticeType)
      throws ServiceException {
    try {
      if (StringUtils.isEmpty(request.getRequest()) && CollectionUtils.isEmpty(request.getFilters())) {
        return new NoticeSearchResponse();
      }
      EsSearchParameters searchParameters = esQueryService.createAdvancedSearchParameters(request, noticeType);

      searchParameters.setSourceInclude(ListUtils.union(SEARCH_SOURCE_INCLUDE, request.getFields()));
      searchParameters.setPage(0);
      searchParameters.setSize(0);

      BoolQueryBuilder queryWithoutFacets = (BoolQueryBuilder) searchParameters.getQuery();
      List<QueryBuilder> originalFilters = new ArrayList<>();
      originalFilters.addAll(queryWithoutFacets.filter());
      final List<FacetResult> aggs = buildAndRetrieveAggs(request.getAggregations(), searchParameters,
          queryWithoutFacets, originalFilters);

      // Filtre sur les valeurs cochées dans les facettes
      searchParameters.setQuery(queryWithoutFacets);
      esQueryService.addFacetsFilters(request.getAggregations(), queryWithoutFacets);
      searchParameters.setPage(request.getPage() - 1);
      searchParameters.setSize(request.getPageSize());

      searchParameters.setAggregations(null);
      SearchResponse searchResponse = esClient.getServiceGeneral().search(searchParameters);

      NoticeSearchResponse buildResponse = buildResponse(request.getPage(), request.getPageSize(), searchResponse);
      buildResponse.getFacets().addAll(aggs);
      return buildResponse;
    } catch (EsRequestException | IOException e) {
      throw new ServiceException(EXCEPTION_ERROR_SEARCH_NOTICE, e);
    } catch (ElasticsearchStatusException e) {
      throw new WebApplicationException(messageSource.getMessage("search.fields.incorrect", new Object[] {}, null),
          e.status().getStatus());
    }
  }

  @Override
  public List<NoticeSearchResult> autocomplete(String terms, NoticeType noticeType, String lang, Integer noticePageSize)
      throws ServiceException {
    try {
      List<NoticeSearchResult> results = new ArrayList<>();
      if (NoticeType.NEWS.equals(noticeType)) {
        // L'autocompletino est désactivé pour les articles et actualités
        return results;
      }

      List<EsSearchParameters> searchParameters = esQueryService.createAutocompleteParameters(terms, noticeType,
          noticePageSize);

      Item[] items = esClient.getServiceGeneral().multiSearch(searchParameters);

      for (Item item : items) {
        SearchResponse response = item.getResponse();
        for (SearchHit hit : response.getHits()) {

          NoticeSearchResult searchResult = new NoticeSearchResult();
          JsonNode source = mapper.readTree(hit.getSourceAsString());
          JsonNode digest = source.at("/internal/digest");
          for (Entry<String, HighlightField> entry : hit.getHighlightFields().entrySet()) {
            String path = "/" + entry.getKey().replace(".autocomplete", "").replace(".", "/");
            JsonNode highlighted = source.at(path);
            if (!highlighted.isMissingNode()) {
              ((ObjectNode) digest).put(path.substring(path.lastIndexOf("/") + 1, path.length()),
                  entry.getValue().fragments()[0].toString());
            }

          }
          searchResult.getDigest().putAll(mapper.convertValue(digest, new TypeReference<Map<String, Object>>() {
          }));
          searchResult.setId(source.at(INTERNAL_UUID_PATH).asText());
          results.add(searchResult);
        }
      }
      return results;
    } catch (EsRequestException | IOException e) {
      throw new ServiceException(EXCEPTION_ERROR_SEARCH_NOTICE, e);
    }
  }

  @Override
  public Set<String> advAutocomplete(String field, String query, NoticeType noticeType) throws ServiceException {
    try {
      EsSearchParameters searchParameters = new EsSearchParameters();
      String autocompleteField = field + ".*" + EsQueryService.AUTOCOMPLETE_SUB_FIELD;
      // Pour un thesaurus, il faut chercher dans tout les sous champs text
      // (prefLabels, altLabels, etc)
      String autocompleteField2 = field + EsQueryService.AUTOCOMPLETE_SUB_FIELD;
      searchParameters
          .setQuery(QueryBuilders.simpleQueryStringQuery(query).field(autocompleteField).field(autocompleteField2));

      searchParameters.setSourceInclude(Arrays.asList(field, field + ".*"));

      searchParameters.setSize(10);
      searchParameters.setIndexes(Arrays.asList(EsIndexEnum.valueOf(noticeType.name())));
      SearchResponse search = esClient.getServiceGeneral().search(searchParameters);

      // Utilisation d'un set pour ne pas avoir de doublons dans les résultats
      Set<String> results = new TreeSet<>();
      for (SearchHit hit : search.getHits()) {
        JsonNode source = mapper.readTree(hit.getSourceAsString());
        results.addAll(getValues(source, field));
      }
      removeNonExpectedValues(results, query);
      return results;
    } catch (EsRequestException | IOException e) {
      throw new ServiceException(EXCEPTION_ERROR_SEARCH_NOTICE, e);
    }
  }

  @Override
  public List<Map<String, String>> autocompleteNoticeTitle(String terms, NoticeType noticeType)
      throws ServiceException {
    EsSearchParameters parameters = esQueryService.createAutocompleteSearchParameters(terms, noticeType,
        EsIndexEnum.valueOf(noticeType.name()), 10);

    SearchResponse search;
    List<Map<String, String>> results = new ArrayList<>();
    try {
      search = esClient.getServiceGeneral().search(parameters);
      for (SearchHit hit : search.getHits()) {
        HashMap<String, String> map = new HashMap<>();
        JsonNode source = mapper.readTree(hit.getSourceAsString());
        map.put("id", source.at(INTERNAL_UUID_PATH).asText());
        map.put("title", source.at(INTERNAL_DIGEST_DISPLAY_LABEL_LINK_PATH).asText());
        results.add(map);
      }
    } catch (EsRequestException | IOException e) {
      throw new ServiceException(e);
    }
    return results;
  }

  @Override
  public MediaSearchResponse searchMedia(MediaSearchRequest request) throws ServiceException {
    try {
      EsSearchParameters searchParameters = esQueryService.createMediaSearchParameters(request);

      searchParameters.setSourceInclude(MEDIA_SEARCH_SOURCE_INCLUDE);
      searchParameters.setPage(0);
      searchParameters.setSize(0);

      BoolQueryBuilder queryWithoutFacets = (BoolQueryBuilder) searchParameters.getQuery();

      List<QueryBuilder> originalFilters = new ArrayList<>();
      originalFilters.addAll(queryWithoutFacets.filter());
      final List<FacetResult> aggs = buildAndRetrieveAggs(request.getAggregations(), searchParameters,
          queryWithoutFacets, originalFilters);

      // Filtre sur les valeurs cochées dans les facettes
      queryWithoutFacets.filter().clear();
      queryWithoutFacets.filter().addAll(originalFilters);
      searchParameters.setQuery(queryWithoutFacets);
      esQueryService.addFacetsFilters(request.getAggregations(), queryWithoutFacets);
      searchParameters.setPage(request.getPage() - 1);
      searchParameters.setSize(request.getPageSize());

      SearchResponse searchResponse = esClient.getServiceGeneral().search(searchParameters);

      MediaSearchResponse buildResponse = buildMediaResponse(request.getPage(), request.getPageSize(), searchResponse);

      buildResponse.getFacets().addAll(aggs);

      return buildResponse;

    } catch (EsRequestException | IOException e) {
      throw new ServiceException(EXCEPTION_ERROR_SEARCH_NOTICE, e);
    }
  }

  @Override
  public UserSearchResponse searchUser(UserSearchRequest request) throws ServiceException {
    try {
      EsSearchParameters searchParameters = esQueryService.createUserSearchParameters(request);

      searchParameters.setSourceInclude(USER_SEARCH_SOURCE_INCLUDE);

      SearchResponse searchResponse = esClient.getServiceGeneral().search(searchParameters);

      return buildUserResponse(request.getPage(), request.getPageSize(), searchResponse);

    } catch (EsRequestException | IOException e) {
      throw new ServiceException(EXCEPTION_ERROR_SEARCH_NOTICE, e);
    }
  }

  @Override
  public Map<String, List<String>> getGeoPointFields() {
    return mapGeoPointFields;
  }

  private Set<String> getValues(JsonNode source, String fieldPath) {
    Set<String> values = new TreeSet<>();

    String[] subPaths = fieldPath.split("\\.");
    StringBuilder path = new StringBuilder();
    JsonNode subLevel = source;
    for (int i = 0; i < subPaths.length; i++) {
      path.append(subPaths[i]).append(".");
      String subPath = subPaths[i];
      subLevel = subLevel.at("/" + subPath);
      if (subLevel.isArray()) {
        for (JsonNode element : subLevel) {
          if (subPath.equals("thesaurus")) {
            addThesaurusValues(values, element);
          } else {
            values.addAll(getValues(element, fieldPath.replace(path.toString(), "")));
          }
        }
      } else if (i == subPaths.length - 1 && !subLevel.isMissingNode()) {
        addValues(values, subLevel);
      } else if (subPath.equals("thesaurus")) {
        addThesaurusValues(values, subLevel);
      }
    }

    return values;
  }

  /**
   * Suppression des valeurs non attendues pour l'autocompletion. Dans le cas de
   * champs multivalué, si une des champs match, les autres sont aussi remontés et
   * entrainent du bruit.<br>
   * Utilisation de {@link Analyzer} pour extraire les valeurs à comparer.
   * 
   * @param values valeur à vérifier
   * @param query  texte recherché
   * @throws IOException si une erreur se produit lors de l'analyze des valeurs
   */
  private void removeNonExpectedValues(Set<String> values, String query) throws IOException {
    for (Iterator<String> iterator = values.iterator(); iterator.hasNext();) {
      String value = iterator.next();
      boolean isExpected = false;
      List<String> analyze = LuceneUtils.analyze(value, analyzer);
      for (String analyzedValue : analyze) {
        if (analyzedValue.startsWith(query.toLowerCase())) {
          isExpected = true;
        }
      }
      if (!isExpected) {
        iterator.remove();
      }
    }

  }

  private void addValues(Set<String> values, JsonNode subLevel) {
    if (subLevel.isObject()) {
      for (JsonNode value : subLevel) {
        values.add(value.asText());
      }
    } else {
      values.add(subLevel.asText());
    }
  }

  private void addThesaurusValues(Set<String> values, JsonNode subLevel) {
    JsonNode prefLabels = subLevel.get("prefLabels");
    if (prefLabels != null) {
      for (JsonNode prefLabel : prefLabels) {
        values.add(prefLabel.get("value").asText());
      }
    }
    JsonNode altLabels = subLevel.get("/altLabels");
    if (altLabels != null) {
      for (JsonNode altLabel : altLabels) {
        values.add(altLabel.get("value").asText());
      }
    }
    JsonNode hiddenLabels = subLevel.get("/hiddenLabels");
    if (hiddenLabels != null) {
      for (JsonNode hiddenLabel : hiddenLabels) {
        values.add(hiddenLabel.get("value").asText());
      }
    }
  }

  private List<FacetResult> buildAndRetrieveAggs(Map<String, Aggregation> aggregations,
      EsSearchParameters searchParameters, QueryBuilder queryWithoutFacets, List<QueryBuilder> originalFilters)
      throws EsRequestException {
    List<FacetResult> aggs = new ArrayList<>();
    if (MapUtils.isNotEmpty(aggregations)) {
      for (Entry<String, Aggregation> agg : aggregations.entrySet()) {
        // La facette sur les status ne doit pas être affichée si l'on est pas connecté
        // avec des droits suffisants
        if (!agg.getValue().getField().equals("internal.status") || userInfoService.hasAnyRole(
            Arrays.asList(Authority.ADMIN.name(), Authority.CONTRIB.name(), Authority.DATABASE_MANAGER.name()))) {
          searchParameters.setAggregations(new ArrayList<>());
          ((BoolQueryBuilder) queryWithoutFacets).filter().clear();
          ((BoolQueryBuilder) queryWithoutFacets).filter().addAll(originalFilters);
          searchParameters.setQuery(queryWithoutFacets);
          esQueryService.addFacetsFilters(aggregations, (BoolQueryBuilder) queryWithoutFacets, agg.getKey());

          // Aggregation sur les termes
          searchParameters.getAggregations().add(esQueryService.constructAggregation(agg));

          // Aggregations pour les documents sans le champ
          searchParameters.getAggregations().add(esQueryService.constructMissingAggregation(agg));

          SearchResponse searchResponse = esClient.getServiceGeneral().search(searchParameters);

          aggs.add(esQueryService.buildFacetResults(searchResponse.getAggregations(), agg));
        }
      }
    }
    aggs.sort((o1, o2) -> {
      if (o1.getFacetScope() == null && o2.getFacetScope() == null || o1.getFacetScope().equals(o2.getFacetScope())) {
        return o1.getFacetOrder().compareTo(o2.getFacetOrder());
      } else if (o1.getFacetScope().equals("general")) {
        return -1;
      } else {
        return 1;
      }
    });
    return aggs;
  }

  private NoticeSearchResponse buildResponse(Integer page, Integer pageSize, SearchResponse searchResponse)
      throws IOException, ServiceException {
    NoticeSearchResponse response = new NoticeSearchResponse();
    for (SearchHit hit : searchResponse.getHits()) {

      JsonNode source = mapper.readTree(hit.getSourceAsString());
      NoticeSearchResult searchResult = new NoticeSearchResult();
      searchResult.getDigest()
          .putAll(mapper.convertValue(source.at("/internal/digest"), new TypeReference<Map<String, Object>>() {
          }));

      JsonNode content = source.at("/content");
      if (!content.isMissingNode()) {
        searchResult.getContent().putAll(mapper.convertValue(content, new TypeReference<Map<String, Object>>() {
        }));
      }
      searchResult.setId(source.at(INTERNAL_UUID_PATH).asText());

      List<String> mediaIds = JsonUtils.getMediaIds(source, "/content/mediaInformation/associatedMedia");
      searchResult.getMedias().addAll(noticeService.getMedia(mediaIds,
          source.at("/content/mediaInformation/prefPicture"), MediaRetrieveMode.THUMBNAILS));
      response.getResults().add(searchResult);
    }

    response.setCount((int) searchResponse.getHits().getTotalHits().value);
    response.setOffset(page);
    response.setLimit((response.getCount() + pageSize - 1) / pageSize);

    return response;
  }

  private TimelineSearchResponse buildTimelineResponse(Integer page, Integer pageSize, SearchResponse searchResponse,
      List<String> fields) throws IOException, ServiceException {
    ObjectNode data = mapper.createObjectNode();

    ArrayNode events = mapper.createArrayNode();
    Pattern patternLabel = Pattern.compile("(\\w+\\.)+\\w+\\s\\((.+)\\)");
    for (SearchHit hit : searchResponse.getHits()) {
      JsonNode source = mapper.readTree(hit.getSourceAsString());

      ObjectNode mediaNode = mapper.createObjectNode();
      JsonNode prefPicture = source.at("/content/mediaInformation/prefPicture");
      if (!prefPicture.isMissingNode()) {
        String idMedia = prefPicture.get("idMedia").asText();
        List<Media> media = mediaService.getMedia(Arrays.asList(idMedia), MediaRetrieveMode.FULL);
        if (!media.isEmpty()) {
          mediaNode.put("url", fullBaseUrl + idMedia + "/doc/" + prefPicture.get("idFile").asText() + "/medium."
              + prefPicture.get("extension").asText());
          mediaNode.put("credit",
              source.at(JsonUtils.getPathToField(source, "/content/mediaInformation/associatedMedia", "/digest", ""))
                  .asText());
        }
      }
      List<String> fieldsToProcess;
      if (fields.isEmpty()) {
        String noticeTypeAsText = source.at("/internal/digest/noticeType").asText();
        fieldsToProcess = timelineDateFields.get(noticeTypeAsText);
      } else {
        fieldsToProcess = fields;
      }
      for (String field : fieldsToProcess) {
        addEventsToTimeline(events, patternLabel, source, mediaNode, field);
      }
    }

    data.set("events", events);
    TimelineSearchResult result = new TimelineSearchResult();
    result.getData().putAll(mapper.convertValue(data, new TypeReference<Map<String, Object>>() {
    }));

    TimelineSearchResponse response = new TimelineSearchResponse();
    response.getResults().add(result);

    response.setCount((int) searchResponse.getHits().getTotalHits().value);
    response.setOffset(page);
    response.setLimit((response.getCount() + pageSize - 1) / pageSize);

    return response;
  }

  private void addEventsToTimeline(ArrayNode events, Pattern patternLabel, JsonNode source, ObjectNode mediaNode,
      String field) {
    List<String> effectiveSubPaths = JsonUtils.getAllSubPath(field.replaceFirst("\\s\\(.*\\)", ""), source, "");

    for (String subPath : effectiveSubPaths) {

      JsonNode dateNode = source.at(subPath);
      if (!dateNode.isMissingNode() && dateNode.get("startDateComputed") != null) {
        Matcher matcher = patternLabel.matcher(field);
        addEventToTimeline(events, source, mediaNode, dateNode, matcher.matches() ? matcher.group(2) : "");
      }
    }
  }

  private GraphApiResponse buildGraphResponse(SearchResponse searchResponse) throws IOException {
    Map<String, Set<String>> noticeLinks = new HashMap<>();
    Map<String, String> noticeTypes = new HashMap<>();
    Map<String, String> noticeTitles = new HashMap<>();

    for (SearchHit hit : searchResponse.getHits()) {
      JsonNode source = mapper.readTree(hit.getSourceAsString());
      Set<String> references = new HashSet<>();
      source.at("/internal/reference").elements().forEachRemaining(reference -> references.add(reference.asText()));
      noticeLinks.put(source.at(INTERNAL_UUID_PATH).asText(), references);
      noticeTypes.put(source.at(INTERNAL_UUID_PATH).asText(), source.at("/internal/noticeType").asText());
      noticeTitles.put(source.at(INTERNAL_UUID_PATH).asText(),
          source.at(INTERNAL_DIGEST_DISPLAY_LABEL_LINK_PATH).asText());
    }

    GraphApiResponse response = new GraphApiResponse();
    for (Entry<String, Set<String>> entry : noticeLinks.entrySet()) {
      GraphNode node = new GraphNode();
      node.setId(entry.getKey());
      node.setGroup(noticeTypes.get(entry.getKey()));
      node.setName(noticeTitles.get(entry.getKey()));
      response.getNodes().add(node);
      for (String reference : entry.getValue()) {
        if (noticeLinks.get(reference) != null) {
          GraphLink link = new GraphLink();
          link.setSource(entry.getKey());
          link.setTarget(reference);
          link.setValue(1);
          response.getLinks().add(link);
        }
      }
    }

    return response;
  }

  private void addEventToTimeline(ArrayNode events, JsonNode source, ObjectNode mediaNode, JsonNode dateNode,
      String dateLabel) {
    ObjectNode startDate = mapper.createObjectNode();
    ObjectNode endDate = mapper.createObjectNode();
    LocalDate startDateComputed = DateUtils.parseEraDate(dateNode.get("startDateComputed").asText());
    startDate.put("month", "" + startDateComputed.getMonthValue());
    startDate.put("day", "" + startDateComputed.getDayOfMonth());
    startDate.put("year", "" + startDateComputed.getYear());

    String startDisplayDate = "" + startDateComputed.getYear();
    String endDisplayDate = "";
    LocalDate endDateComputed = DateUtils.parseEraDate(dateNode.get("endDateComputed").asText());
    if (endDateComputed.compareTo(startDateComputed) != 0) {
      endDate.put("month", "" + endDateComputed.getMonthValue());
      endDate.put("day", "" + endDateComputed.getDayOfMonth());
      endDate.put("year", "" + endDateComputed.getYear());
      endDisplayDate = "" + endDateComputed.getYear();
    }

    JsonNode start = dateNode.at("/start");
    if (!start.isMissingNode()) {
      JsonNode siecle = start.at("/siecle");
      if (!siecle.isMissingNode()) {
        startDisplayDate = siecle.at("/thesaurus/prefLabels/0/value").asText();
      }
    }

    JsonNode end = dateNode.at("/end");
    if (!end.isMissingNode()) {
      JsonNode siecle = end.at("/siecle");
      if (!siecle.isMissingNode()) {
        endDisplayDate = siecle.at("/thesaurus/prefLabels/0/value").asText();
      }
    } else {
      endDisplayDate = "";
    }

    ObjectNode event = mapper.createObjectNode();
    event.set("start_date", startDate);
    event.set("end_date", endDate);
    event.put("display_date",
        (StringUtils.isEmpty(endDisplayDate) || startDisplayDate.equals(endDisplayDate) ? startDisplayDate
            : startDisplayDate + " / " + endDisplayDate) + " (" + dateLabel + ")");
    String noticeTypeAsText = source.at("/internal/digest/noticeType").asText();
    NoticeType noticeType = NoticeType.valueOf(noticeTypeAsText);
    event.put("group", noticeType.getLabel());
    ObjectNode eventText = mapper.createObjectNode();
    String otherInfos = getInfoForTimeLine(noticeType, source);
    if (StringUtils.isNotBlank(otherInfos)) {
      eventText.put("text", otherInfos);
    }

    ObjectNode headline = mapper.createObjectNode();
    headline.put("url", fullBaseUrl + source.at(INTERNAL_UUID_PATH).asText());
    headline.put("displayLabelLink", source.at(INTERNAL_DIGEST_DISPLAY_LABEL_LINK_PATH).asText());

    eventText.set("headline", headline);

    event.set("text", eventText);
    event.set("media", mediaNode);
    events.add(event);
  }

  private String getInfoForTimeLine(NoticeType noticeType, JsonNode source) {
    switch (noticeType) {
      case ARTWORK:
        return source
            .at(JsonUtils.getPathToField(source, "/content/descriptionInformation/description", "/note/value", ""))
            .asText();
      case COLLECTION:
        return source.at(JsonUtils.getPathToField(source, "/content/identificationInformation/description",
            "/descriptiveNote/value", "")).asText();
      case EVENT:
        return source.at("/content/identificationInformation/eventType/comment").asText();
      case PERSON:
        return source.at(JsonUtils.getPathToField(source, "/content/activityInformation/biography", "/note", ""))
            .asText();
      case REFERENCE:
        return source.at("/content/identificationInformation/reference/comment").asText();
      default:
        return null;
    }
  }

  private MediaSearchResponse buildMediaResponse(Integer page, Integer pageSize, SearchResponse searchResponse)
      throws IOException {
    MediaSearchResponse response = new MediaSearchResponse();
    for (SearchHit hit : searchResponse.getHits()) {

      JsonNode source = mapper.readTree(hit.getSourceAsString());
      MediaSearchResult searchResult = new MediaSearchResult();
      searchResult.setCaption(source.at("/caption").asText());
      JsonNode files = source.at("/files");
      String firstThumbnail = null;
      for (JsonNode file : files) {
        String thumbnail = file.get("thumbnail").asText();
        if (StringUtils.isNotEmpty(thumbnail)) {
          if (firstThumbnail == null) {
            firstThumbnail = thumbnail;
          }
          searchResult.getUrls().add(thumbnail);
        }
      }
      searchResult.setThumbnail(firstThumbnail);
      searchResult.setId(source.at(INTERNAL_UUID_PATH).asText());

      response.getResults().add(searchResult);
    }
    response.setCount((int) searchResponse.getHits().getTotalHits().value);
    response.setOffset(page);
    response.setLimit((response.getCount() + pageSize - 1) / pageSize);

    return response;
  }

  private UserSearchResponse buildUserResponse(Integer page, Integer pageSize, SearchResponse searchResponse)
      throws IOException {
    UserSearchResponse response = new UserSearchResponse();
    for (SearchHit hit : searchResponse.getHits()) {

      JsonNode source = mapper.readTree(hit.getSourceAsString());
      UserSearchResult searchResult = new UserSearchResult();
      searchResult.setId(source.at(INTERNAL_UUID_PATH).asText());

      searchResult.setCreationDate(source.at("/internal/createdDate").asText());
      searchResult.setFirstname(source.at("/internal/firstname").asText());
      searchResult.setLastname(source.at("/internal/lastname").asText());
      searchResult.setRole(Authority.valueOf(source.at("/authorizations/role").asText()).getValue());
      searchResult.setStatus(UserStatus.valueOf(source.at("/internal/status").asText()).getValue());

      response.getResults().add(searchResult);
    }
    response.setCount((int) searchResponse.getHits().getTotalHits().value);
    response.setOffset(page);
    response.setLimit((response.getCount() + pageSize - 1) / pageSize);

    return response;
  }

  public Map<String, List<String>> getTimelineDateFields() {
    return timelineDateFields;
  }

  public void setTimelineDateFields(Map<String, List<String>> timelineDateFields) {
    this.timelineDateFields = timelineDateFields;
  }

  public void setMapGeoPointFields(Map<String, List<String>> mapGeoPointFields) {
    this.mapGeoPointFields = mapGeoPointFields;
  }

}
