package fr.inha.agorha2.service.impl;

import fr.inha.agorha2.service.ESHealthService;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.config.EsIndexEnum;
import java.io.IOException;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthRequest;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
import org.elasticsearch.client.RequestOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ESHealthServiceImpl implements ESHealthService {

  @Autowired
  EsClient esClient;

  @Override
  public ClusterHealthResponse clusterHealth() throws IOException {
    return esClient.getEsRestClient().cluster().health(new ClusterHealthRequest(EsIndexEnum.COLLECTION.getName()),
        RequestOptions.DEFAULT);
  }

}
