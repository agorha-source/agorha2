package fr.inha.agorha2.service;

import fr.inha.elastic.model.NoticeType;

public interface MetaModelService {

  public String get(String version, NoticeType noticeType);

}
