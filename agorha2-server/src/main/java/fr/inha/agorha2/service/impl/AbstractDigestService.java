package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.NoticeService;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.model.Notice;
import java.io.IOException;
import java.io.StringReader;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.parser.ParserDelegator;
import javax.ws.rs.WebApplicationException;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractDigestService {

  private static final String EARLIEST_FIELD = "earliest";
  private static final Logger log = LoggerFactory.getLogger(AbstractDigestService.class);

  protected ObjectMapper mapper = DefaultEsContext.instance().getObjectMapper();
  private ParserDelegator delegator = new ParserDelegator();

  @Autowired
  private NoticeService noticeService;

  protected ObjectNode getDigestObject(JsonNode notice) {
    JsonNode digest = notice.at("/internal/digest");
    ObjectNode digestObject = null;
    if (digest.isMissingNode()) {
      digestObject = mapper.createObjectNode();
      ((ObjectNode) notice.get("internal")).set("digest", digestObject);
    } else {
      digestObject = (ObjectNode) digest;
    }
    digestObject.removeAll();
    return digestObject;
  }

  protected void buildCommonFields(JsonNode notice, ObjectNode digestObject) {
    digestObject.put("updatedDate", notice.at("/internal/updatedDate").asText());
    buildDatabases(notice, digestObject);
    digestObject.put("status", notice.at("/internal/status").asText());
    digestObject.put("createdBy", notice.at("/internal/createdBy").asText());
  }

  protected void buildDatabases(JsonNode notice, ObjectNode digestObject) {
    JsonNode databases = notice.at("/content/recordManagementInformation/databaseLabel");
    ArrayNode databasesNode = mapper.createArrayNode();
    for (JsonNode database : databases) {
      databasesNode.add(database.at("/value").asText());
    }
    digestObject.set("databases", databasesNode);
  }

  protected PersonInfo buildPersonDataFromUuid(String uuid) throws ServiceException {
    Notice person = null;
    try {
      person = noticeService.get(uuid);
    } catch (WebApplicationException e) {
      return null;
    }

    JsonNode personContent;
    try {
      personContent = mapper.readTree(new JSONObject(person.getContent()).toJSONString());
    } catch (IOException e) {
      throw new ServiceException("Erreur lors de création du digest", e);
    }
    return buildPersonDataFromContent(personContent);
  }

  protected PersonInfo buildPersonDataFromContent(JsonNode personContent) {
    PersonInfo personInfo = new PersonInfo();

    personInfo.setName(getObjectNode(personContent.at("/identificationInformation/name/0/label/value")));
    personInfo.setFirstName(getObjectNode(personContent.at("/identificationInformation/name/0/firstname/value")));

    String birthDateStartPath = JsonUtils.getPathToField(personContent, "/identificationInformation/life",
        "/birth/date/start", "");
    JsonNode start = constructDate(birthDateStartPath, personContent);
    if (start.get(EARLIEST_FIELD).asText().isEmpty()) {
      String institutionDateStartPath = JsonUtils.getPathToField(personContent,
          "/identificationInformation/institution", "/date/start", "");
      start = constructDate(institutionDateStartPath, personContent);
      if (start.get(EARLIEST_FIELD).asText().isEmpty()) {
        String activityDateStartPath = JsonUtils.getPathToField(personContent, "/activityInformation/activity",
            "/date/start", "");
        start = constructDate(activityDateStartPath, personContent);
      }
    }

    personInfo.setStart(start);

    String deathDateStartPath = JsonUtils.getPathToField(personContent, "/identificationInformation/life",
        "/death/date/start", "");
    JsonNode end = constructDate(deathDateStartPath, personContent);
    if (end.get(EARLIEST_FIELD).asText().isEmpty()) {
      String institutionDateEndPath = JsonUtils.getPathToField(personContent, "/identificationInformation/institution",
          "/date/end", "");
      end = constructDate(institutionDateEndPath, personContent);
      if (end.get(EARLIEST_FIELD).asText().isEmpty()) {
        String activityDateEndPath = JsonUtils.getPathToField(personContent, "/activityInformation/activity",
            "/date/end", "");
        end = constructDate(activityDateEndPath, personContent);
      }
    }

    personInfo.setEnd(end);
    return personInfo;
  }

  protected String constructPersonTitle(JsonNode usualName, JsonNode usualFirstName) {
    String titleAsText = "";
    if (StringUtils.isNoneEmpty(usualName.asText())) {
      titleAsText += usualName.asText();
    }
    if (StringUtils.isNoneEmpty(usualFirstName.asText())) {
      titleAsText += ", " + usualFirstName.asText();
    }
    return titleAsText;
  }

  protected JsonNode constructDate(String path, JsonNode jsonContent) {
    ObjectNode date = mapper.createObjectNode();
    date.set("prefix", getObjectNode(jsonContent.at(path + "/prefix/thesaurus/prefLabels/0/value")));
    JsonNode siecle = jsonContent.at(path + "/siecle");
    if (siecle.isMissingNode()) {
      date.put(EARLIEST_FIELD, jsonContent.at(path + "/earliest/date").asText());
      date.put("earliestFormat", jsonContent.at(path + "/earliest/format").asText());
      date.put("latest", jsonContent.at(path + "/latest/date").asText());
      date.put("latestFormat", jsonContent.at(path + "/latest/format").asText());
    } else {
      date.put(EARLIEST_FIELD, siecle.at("/thesaurus/prefLabels/0/value").asText());
    }
    return date;
  }

  protected void addValueToDisplayLabel(JsonNode nodeValue, StringBuilder sb) {
    if (nodeValue != null && !nodeValue.isMissingNode()) {
      if (sb.length() != 0 && StringUtils.isNotBlank(nodeValue.asText())) {
        sb.append(", ");
      }
      sb.append(nodeValue.asText());
    }
  }

  /**
   * Return a new ObjectNode if the given node is a missingNode, the given node
   * otherwise.
   * 
   * @param node json node
   * @return
   */
  protected JsonNode getObjectNode(JsonNode node) {
    if (node.isMissingNode()) {
      return mapper.createObjectNode();
    }
    return node;
  }

  protected String removeHtmlTags(String value) {

    Html2Text parser = new Html2Text();
    try {
      delegator.parse(new StringReader(value), parser, false);
    } catch (IOException e) {
      log.warn(
          "Impossible de parser le texte suivant {} pour supprimer les balises HTML, utilisation tel quel dans le digest",
          value);
      return value;
    }
    return parser.getText();
  }

  protected class Html2Text extends HTMLEditorKit.ParserCallback {
    private StringBuilder sb = new StringBuilder();

    @Override
    public void handleText(char[] text, int pos) {
      sb.append(text);
    }

    public String getText() {
      return sb.toString();
    }
  }

  protected class PersonInfo {
    private JsonNode name;
    private JsonNode firstName;
    private JsonNode start;
    private JsonNode end;

    public JsonNode getName() {
      return name;
    }

    public void setName(JsonNode name) {
      this.name = name;
    }

    public JsonNode getFirstName() {
      return firstName;
    }

    public void setFirstName(JsonNode firstName) {
      this.firstName = firstName;
    }

    public JsonNode getStart() {
      return start;
    }

    public void setStart(JsonNode jsonNode) {
      this.start = jsonNode;
    }

    public JsonNode getEnd() {
      return end;
    }

    public void setEnd(JsonNode end) {
      this.end = end;
    }

  }
}
