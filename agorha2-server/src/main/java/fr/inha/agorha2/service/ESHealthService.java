package fr.inha.agorha2.service;

import java.io.IOException;
import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;

public interface ESHealthService {

  public ClusterHealthResponse clusterHealth() throws IOException;
}
