package fr.inha.agorha2.service;

import com.fasterxml.jackson.databind.JsonNode;
import fr.inha.agorha2.api.dto.MediaRetrieveMode;
import fr.inha.agorha2.api.dto.OldNoticeType;
import fr.inha.agorha2.api.dto.response.MosaicCardResponse;
import fr.inha.agorha2.exception.FunctionnalException;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.Notice;
import fr.inha.elastic.model.NoticeStatus;
import java.util.List;
import java.util.Map;

public interface NoticeService {

  /**
   * Create a new notice from the given json.
   * 
   * @param noticeAsJson json representation of the notice
   * @return the newly created notice
   * @throws ServiceException thrown if input json is not parsable
   */
  public Notice create(String noticeAsJson) throws ServiceException;

  public Notice create(String noticeAsJson, NoticeDataSourceMode creationMode) throws ServiceException;

  public void delete(String uuid) throws ServiceException;

  public List<Map<String, String>> upddateStatus(List<String> uuids, NoticeStatus targetStatus) throws ServiceException;

  public Notice update(String uuid, String noticeAsJson) throws ServiceException;

  public Notice update(String uuid, String noticeAsJson, NoticeDataSourceMode dataSourceMode) throws ServiceException;

  public Notice duplicate(String noticeId, String databaseId) throws ServiceException;

  /**
   * Get a notice from its id.
   * 
   * @param noticeId id of the notice to retrieve
   * @return notice with id noticeId
   * @throws ServiceException thrown if an error occurs during ES request
   */
  public Notice get(String noticeId) throws ServiceException;

  public Notice get(String noticeId, NoticeGetMode mode) throws ServiceException;

  public List<MosaicCardResponse> getMosaicCards(List<String> ids, NoticeGetMode mode) throws ServiceException;

  public String getJsonLd(String noticeId) throws ServiceException;

  public String getRdf(String noticeId, String format) throws ServiceException;

  public Map<String, String> getPrefPicture(String noticeId) throws ServiceException;

  public List<Media> getMedia(String noticeId, MediaRetrieveMode mode) throws ServiceException;

  public List<Media> getMedia(List<String> mediaIds, JsonNode prefPictureNode, MediaRetrieveMode mode)
      throws ServiceException;

  public List<String> getReferenceBy(String noticeId) throws ServiceException;

  void addNews(String selectionId, String newsUrl, String newsTitle) throws ServiceException;

  void removeNews(String selctionId, String newsUrl) throws ServiceException;

  public String getUuid(String uniqueKey, OldNoticeType oldNoticeType) throws FunctionnalException, ServiceException;

}
