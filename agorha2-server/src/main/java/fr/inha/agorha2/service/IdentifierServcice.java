package fr.inha.agorha2.service;

public interface IdentifierServcice {

  public String getArkIdentifier();

  public String getPermalink(String uuid);
}
