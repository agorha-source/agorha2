package fr.inha.agorha2.service.impl;

import fr.inha.agorha2.security.UserPrincipal;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.elastic.model.DatabaseAuthorization;
import java.util.Collections;
import java.util.List;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Implémentation de {@link UserInfoService} dans un context spring security.
 *
 * @see SecurityContextHolder#getContext()
 * @author broussot
 *
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {

  @Override
  public String getCurrentUsername() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null) {
      return authentication.getName();
    }
    return null;
  }

  @Override
  public boolean hasAnyRole(List<String> authorities) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null) {
      for (GrantedAuthority authority : authentication.getAuthorities()) {
        if (authorities.contains(authority.getAuthority())) {
          return true;
        }
      }
    }
    return false;
  }

  @Override
  public List<DatabaseAuthorization> getAuthorizations() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null) {
      return ((UserPrincipal) authentication.getPrincipal()).getUser().getAuthorizations().getDatabase();
    }
    return Collections.emptyList();
  }

  @Override
  public boolean isAuthenticated() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null
        && !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken)) {
      return authentication.isAuthenticated();
    }
    return false;
  }

  @Override
  public UserPrincipal getPrincipal() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null && !authentication.getPrincipal().equals("anonymousUser")) {
      return ((UserPrincipal) authentication.getPrincipal());
    }
    return null;
  }

}
