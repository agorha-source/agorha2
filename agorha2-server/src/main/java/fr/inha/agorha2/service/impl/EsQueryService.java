package fr.inha.agorha2.service.impl;

import com.sword.utils.elasticsearch.search.EsSearchParameters;
import fr.inha.agorha2.api.dto.Aggregation;
import fr.inha.agorha2.api.dto.AggregationTermsOrder;
import fr.inha.agorha2.api.dto.DateRange;
import fr.inha.agorha2.api.dto.DateRangeFilter;
import fr.inha.agorha2.api.dto.LongRangeFilter;
import fr.inha.agorha2.api.dto.MultiValueSearchFilter;
import fr.inha.agorha2.api.dto.Range;
import fr.inha.agorha2.api.dto.RangeFilter;
import fr.inha.agorha2.api.dto.SearchFilter;
import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import fr.inha.agorha2.api.dto.request.MediaSearchRequest;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest.ContentTarget;
import fr.inha.agorha2.api.dto.request.UserSearchRequest;
import fr.inha.agorha2.api.dto.result.FacetResult;
import fr.inha.agorha2.api.dto.result.FacetResultEntry;
import fr.inha.agorha2.api.dto.sort.MediaSortType;
import fr.inha.agorha2.api.dto.sort.NoticeSortType;
import fr.inha.agorha2.api.dto.sort.UserSortType;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.service.MetaModelService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.config.EsIndexEnum;
import fr.inha.elastic.model.NoticeStatus;
import fr.inha.elastic.model.NoticeType;
import fr.inha.elastic.model.Selection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ws.rs.BadRequestException;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.SimpleQueryStringBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation.Bucket;
import org.elasticsearch.search.aggregations.bucket.SingleBucketAggregation;
import org.elasticsearch.search.aggregations.bucket.filter.InternalFilter;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class EsQueryService {

  public static final String AUTOCOMPLETE_SUB_FIELD = ".autocomplete";
  public static final String FACET_MISSING_FIELD_PREFIX = "missing_";
  public static final String INTERNAL_UUID_FIELD = "internal.uuid";

  private static final String QUOTE_FIELD_SUFFIXE = ".exact";
  private static final String INTERNAL_STATUS_FIELD = "internal.status";
  private static final String INTERNAL_BDD_STATUS_FIELD = "internal.agorhaStatus";
  private static final String INTERNAL_DIGEST_TITLE_FIELD = "internal.digest.title";
  private static final String INTERNAL_DISPLAY_LABEL_LINK_FIELD = "internal.digest.displayLabelLink";
  private static final String INTERNAL_NOTICE_TYPE_FIELD = "internal.noticeType";
  private static final String END_DATE_COMPUTED = ".endDateComputed";
  private static final String START_DATE_COMPUTED = ".startDateComputed";
  private static final String AGG_SUB_FIELD = ".keyword";

  private SimpleDateFormat dateEraFormatter = new SimpleDateFormat("yyyy-MM-dd GG", Locale.ENGLISH);

  @Value(value = "${agorha2.systemUsers.admin}")
  private String agorha2AdminUser;

  @Value(value = "${agorha2.systemUsers.batch}")
  private String agorha2BatchUser;

  @Autowired
  private MetaModelService metaModelService;

  @Autowired
  private UserInfoService userInfoService;

  @Autowired
  private MessageSource messageSource;

  public static FieldSortBuilder createFieldSortBuilder(String sort, String field) {
    if (StringUtils.isNotEmpty(sort)) {
      return createFieldSortBuilder(SortOrder.fromString(sort), field);
    } else {
      return createFieldSortBuilder(SortOrder.ASC, field);
    }
  }

  public static FieldSortBuilder createFieldSortBuilder(SortOrder sortOrder, String field) {
    return SortBuilders.fieldSort(field).order(sortOrder);
  }

  public EsSearchParameters createSearchParameters(NoticeSearchRequest request, NoticeType noticeType) {
    return createSearchParameters(request, noticeType, null, false);
  }

  public EsSearchParameters createSearchParameters(NoticeSearchRequest request, NoticeType noticeType,
      Selection selection) {
    return createSearchParameters(request, noticeType, selection, false);
  }

  public EsSearchParameters createSearchParameters(NoticeSearchRequest request, NoticeType noticeType,
      Selection selection, boolean withArticle) {
    EsSearchParameters searchParameters = new EsSearchParameters();
    BoolQueryBuilder query = constructQueryFromSchemas(request.getRequest(), noticeType, request.getContentTarget());

    if (selection != null && !selection.getNotices().isEmpty()) {
      query.filter(
          QueryBuilders.termsQuery(INTERNAL_UUID_FIELD, selection.getNotices()));
    }

    searchParameters.setQuery(query);

    addSort(request.getSort(), request.getSortType(), searchParameters);

    if (noticeType == null) {
      if (withArticle) {
        searchParameters.setIndexes(Arrays.asList(EsIndexEnum.CONTENT));
      } else {
        searchParameters.setIndexes(Arrays.asList(EsIndexEnum.NOTICE));
      }
    } else {
      searchParameters.setIndexes(Arrays.asList(EsIndexEnum.valueOf(noticeType.name())));
    }
    return searchParameters;
  }

  public EsSearchParameters createAdvancedSearchParameters(AdvancedSearchRequest request, NoticeType noticeType) {
    return createAdvancedSearchParameters(request, noticeType, null);
  }

  public EsSearchParameters createAdvancedSearchParameters(AdvancedSearchRequest request, NoticeType noticeType,
      Selection selection) {
    EsSearchParameters searchParameters = new EsSearchParameters();
    BoolQueryBuilder query;
    if (request.getContentTarget() == null || request.getContentTarget().equals(ContentTarget.NOTICE)) {
      query = constructQuery(request);
    } else {
      query = constructQueryWithAttachment(request);
    }

    if (selection != null && !selection.getNotices().isEmpty()) {
      query.filter(
          QueryBuilders.simpleQueryStringQuery(String.join(" ", selection.getNotices())).field(INTERNAL_UUID_FIELD));
    }

    searchParameters.setQuery(query);

    addSort(request.getSort(), request.getSortType(), searchParameters);

    if (noticeType == null) {
      searchParameters.setIndexes(Arrays.asList(EsIndexEnum.NOTICE));
    } else {
      searchParameters.setIndexes(Arrays.asList(EsIndexEnum.valueOf(noticeType.name())));
    }
    return searchParameters;
  }

  public EsSearchParameters createGraphFirstStepSearchParameters(Selection selection, String noticeId) {
    EsSearchParameters searchParameters = new EsSearchParameters();
    BoolQueryBuilder query = QueryBuilders.boolQuery();
    if (selection != null && !selection.getNotices().isEmpty()) {
      query.filter(QueryBuilders.simpleQueryStringQuery(noticeId).field("internal.reference"));
      query.filter(
          QueryBuilders.simpleQueryStringQuery(String.join(" ", selection.getNotices())).field(INTERNAL_UUID_FIELD));
    }
    searchParameters.setSourceInclude(Arrays.asList(INTERNAL_UUID_FIELD));
    searchParameters.setQuery(query);
    searchParameters.setSize(1000);
    searchParameters.setIndexes(Arrays.asList(EsIndexEnum.NOTICE));
    return searchParameters;
  }

  public EsSearchParameters createGraphSecondStepSearchParameters(Set<String> uuids) {
    EsSearchParameters searchParameters = new EsSearchParameters();
    BoolQueryBuilder query = QueryBuilders.boolQuery();
    query.should(QueryBuilders.simpleQueryStringQuery(String.join(" ", uuids)).field("internal.reference"));
    query.should(QueryBuilders.simpleQueryStringQuery(String.join(" ", uuids)).field(INTERNAL_UUID_FIELD));
    searchParameters.setQuery(query);
    searchParameters.setSourceInclude(Arrays.asList("internal.reference", INTERNAL_UUID_FIELD,
        INTERNAL_NOTICE_TYPE_FIELD, INTERNAL_DISPLAY_LABEL_LINK_FIELD));
    searchParameters.setSize(1000);
    searchParameters.setIndexes(Arrays.asList(EsIndexEnum.NOTICE));
    return searchParameters;
  }

  public EsSearchParameters createMediaSearchParameters(MediaSearchRequest request) {
    SimpleQueryStringBuilder simpleQueryString = QueryBuilders.simpleQueryStringQuery("*");
    if (!StringUtils.isEmpty(request.getRequest())) {
      simpleQueryString = QueryBuilders.simpleQueryStringQuery(request.getRequest());
    }
    simpleQueryString.field("database").field("internal.createdBy").field("comment").field("credit").field("caption")
        .field("licence").field("fileProvenance").field("communicationRight.thesaurus.prefLabels.value")
        .field("reproductionRight.thesaurus.prefLabels.value").field("files.alt").field("files.ocr");
    simpleQueryString.quoteFieldSuffix(QUOTE_FIELD_SUFFIXE);

    EsSearchParameters searchParameters = new EsSearchParameters();
    searchParameters.setQuery(QueryBuilders.boolQuery().must(simpleQueryString));

    addMediaSort(request.getSort(), request.getSortType(), searchParameters);

    searchParameters.setIndexes(Arrays.asList(EsIndexEnum.MEDIA));
    return searchParameters;
  }

  public EsSearchParameters createUserSearchParameters(UserSearchRequest request) {
    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

    // Filtre sur les utilisateurs system BATCH et ADMIN, qui ne peuvent et ne
    // doivent pas être modifiés
    boolQuery.filter(QueryBuilders.boolQuery().mustNot(QueryBuilders.matchQuery("internal.login", agorha2BatchUser))
        .mustNot(QueryBuilders.matchQuery("internal.login", agorha2AdminUser)));

    SimpleQueryStringBuilder simpleQueryString = QueryBuilders.simpleQueryStringQuery("*");
    if (!StringUtils.isEmpty(request.getRequest())) {
      simpleQueryString = QueryBuilders.simpleQueryStringQuery(request.getRequest() + "*");
    }
    simpleQueryString.field("internal.firstname");
    simpleQueryString.field("internal.lastname");

    EsSearchParameters searchParameters = new EsSearchParameters();
    searchParameters.setQuery(boolQuery.must(simpleQueryString));

    addUserSort(request.getSort(), request.getSortType(), searchParameters);

    searchParameters.setPage(request.getPage() - 1);
    searchParameters.setSize(request.getPageSize());

    searchParameters.setIndexes(Arrays.asList(EsIndexEnum.USER));
    return searchParameters;
  }

  public List<EsSearchParameters> createAutocompleteParameters(String terms, NoticeType noticeType,
      int noticePageSize) {
    List<EsSearchParameters> searchParameters = new ArrayList<>();

    if (noticeType == null) {
      searchParameters
          .add(createAutocompleteSearchParameters(terms, NoticeType.ARTWORK, EsIndexEnum.ARTWORK, noticePageSize));
      searchParameters.add(
          createAutocompleteSearchParameters(terms, NoticeType.COLLECTION, EsIndexEnum.COLLECTION, noticePageSize));
      searchParameters
          .add(createAutocompleteSearchParameters(terms, NoticeType.EVENT, EsIndexEnum.EVENT, noticePageSize));
      searchParameters
          .add(createAutocompleteSearchParameters(terms, NoticeType.PERSON, EsIndexEnum.PERSON, noticePageSize));
      searchParameters
          .add(createAutocompleteSearchParameters(terms, NoticeType.REFERENCE, EsIndexEnum.REFERENCE, noticePageSize));
    } else {
      searchParameters.add(createAutocompleteSearchParameters(terms, noticeType, EsIndexEnum.valueOf(noticeType.name()),
          noticePageSize));
    }

    return searchParameters;
  }

  public EsSearchParameters createAutocompleteSearchParameters(String terms, NoticeType noticeType, EsIndexEnum index,
      int noticePageSize) {
    EsSearchParameters parameter = new EsSearchParameters();
    parameter.setSourceInclude(Arrays.asList("internal.digest.*", INTERNAL_UUID_FIELD));
    parameter.setSize(noticePageSize);
    parameter.setQuery(constructAutocompleteQueryForNoticeType(terms, noticeType));
    parameter.setIndexes(Arrays.asList(index));
    parameter.setHighlightedFields(Arrays.asList("*"));
    parameter.setHighlightTag("highlight");
    return parameter;
  }

  public EsSearchParameters createAutocompleteTitleParameters(String request, NoticeType noticeType) {
    EsSearchParameters parameter = new EsSearchParameters();
    parameter.setSourceInclude(Arrays.asList(INTERNAL_DISPLAY_LABEL_LINK_FIELD, INTERNAL_UUID_FIELD));
    parameter.setSize(5);

    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
    SimpleQueryStringBuilder filterType = QueryBuilders.simpleQueryStringQuery(noticeType.name());
    filterType.field(INTERNAL_NOTICE_TYPE_FIELD);

    BoolQueryBuilder filterQuery = QueryBuilders.boolQuery().must(filterType);

    SimpleQueryStringBuilder filterNotArchived = QueryBuilders.simpleQueryStringQuery(NoticeStatus.ARCHIVED.getValue());
    filterNotArchived.field(INTERNAL_STATUS_FIELD);
    filterQuery.mustNot(filterNotArchived);

    SimpleQueryStringBuilder simpleQueryString = QueryBuilders.simpleQueryStringQuery(request)
        .field(INTERNAL_DIGEST_TITLE_FIELD + AUTOCOMPLETE_SUB_FIELD);
    simpleQueryString.quoteFieldSuffix(QUOTE_FIELD_SUFFIXE);

    boolQuery.filter(filterQuery);
    boolQuery.should(simpleQueryString);
    boolQuery.minimumShouldMatch(1);

    parameter.setQuery(boolQuery);
    parameter.setIndexes(Arrays.asList(EsIndexEnum.valueOf(noticeType.name())));

    return parameter;
  }

  public EsSearchParameters createSearchParametersForSelection(Set<String> noticeIds) {
    EsSearchParameters searchParameters = new EsSearchParameters();
    BoolQueryBuilder query = QueryBuilders.boolQuery()
        .must(QueryBuilders.simpleQueryStringQuery(String.join(" ", noticeIds)).field(INTERNAL_UUID_FIELD));

    searchParameters.setQuery(query);

    searchParameters.setIndexes(Arrays.asList(EsIndexEnum.NOTICE));
    return searchParameters;
  }

  public EsSearchParameters createSearchParametersForUniqueKey(int uniqueKey, NoticeType noticeType) {
    EsSearchParameters searchParameters = new EsSearchParameters();
    searchParameters.setIndexes(Arrays.asList(EsIndexEnum.valueOf(noticeType.name())));
    searchParameters.setQuery(QueryBuilders.termQuery("internal.uniqueKey", uniqueKey));
    searchParameters.setSourceInclude(Arrays.asList(INTERNAL_UUID_FIELD));
    return searchParameters;
  }

  public EsSearchParameters createExportJsonSearchParameters(NoticeType noticeType, String databaseId) {
    EsSearchParameters searchParameters = new EsSearchParameters();

    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery().must(QueryBuilders.multiMatchQuery(databaseId,
        "content.recordManagementInformation.databaseLabel.ref", "content.*.sourcing.database.ref"));

    if (noticeType != null) {
      boolQuery.filter(QueryBuilders.matchQuery(INTERNAL_NOTICE_TYPE_FIELD, noticeType.name()));
    }
    filterStatus(boolQuery);
    searchParameters.setQuery(boolQuery);

    searchParameters.setIndexes(Arrays.asList(EsIndexEnum.NOTICE));
    return searchParameters;
  }

  public void addFacetsFilters(Map<String, Aggregation> aggregations, BoolQueryBuilder query) {
    addFacetsFilters(aggregations, query, "");
  }

  public void addFacetsFilters(Map<String, Aggregation> aggregations, BoolQueryBuilder query, String aggName) {
    if (aggregations != null) {
      for (Map.Entry<String, Aggregation> aggregationEntry : aggregations.entrySet()) {
        if (StringUtils.isEmpty(aggName) || !aggName.equals(aggregationEntry.getKey())) {
          Aggregation agg = aggregationEntry.getValue();
          if (!agg.getFilters().isEmpty()) {
            BoolQueryBuilder filterQuery = QueryBuilders.boolQuery();
            for (String filterValue : agg.getFilters()) {
              if (filterValue.equals(messageSource.getMessage("search.facet.not.present", new Object[] {}, null))) {
                filterQuery.should(
                    QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery(agg.getField() + AGG_SUB_FIELD)));
              } else {
                filterQuery.should(QueryBuilders.termsQuery(agg.getField() + AGG_SUB_FIELD, filterValue));
              }
            }
            query.filter(filterQuery.minimumShouldMatch(1));
          }
        }
      }
    }
  }

  public void filterStatus(BoolQueryBuilder boolQuery) {
    if (!userInfoService.hasAnyRole(
        Arrays.asList(Authority.ADMIN.name(), Authority.CONTRIB.name(), Authority.DATABASE_MANAGER.name()))) {
      SimpleQueryStringBuilder filterPublished = QueryBuilders
          .simpleQueryStringQuery(NoticeStatus.PUBLISHED.getValue());
      filterPublished.field(INTERNAL_STATUS_FIELD);
      boolQuery.filter(filterPublished);
    }
  }

  public void filterBddStatus(BoolQueryBuilder boolQuery) {
    if (!userInfoService.hasAnyRole(
        Arrays.asList(Authority.ADMIN.name(), Authority.CONTRIB.name(), Authority.DATABASE_MANAGER.name()))) {
      SimpleQueryStringBuilder filterPublished = QueryBuilders
          .simpleQueryStringQuery(NoticeStatus.PUBLISHED.getId() + "");
      filterPublished.field(INTERNAL_BDD_STATUS_FIELD);
      boolQuery.filter(filterPublished);
    }
  }

  /**
   * Construit les resultats de facette à partir des {@link Aggregation}
   * retournées par ES.
   * 
   * @param resultAggs resultat renvoyer par ES
   * @param agg        agrégation demandées
   * @return
   */
  public FacetResult buildFacetResults(Aggregations resultAggs, Entry<String, Aggregation> agg) {
    FacetResult facet = null;
    if (resultAggs != null) {
      String facetId = agg.getKey();

      org.elasticsearch.search.aggregations.Aggregation aggregation = resultAggs.getAsMap().get(facetId);
      Aggregation requestAgg = agg.getValue();
      facet = buildFacetResult(aggregation, facetId, requestAgg);

      // Dans le cas d'une facette sur des dates, il faut trier les résultats dans
      // l'ordre chronologique des siècles
      if (requestAgg.getField().startsWith("content.computedFacetDate")
          || requestAgg.getField().contains("DatePathComputed")) {
        sortFacetDateResults(facet);
      }

      // Ajout de la valeur "non renseigné" des les résultats de la facette
      org.elasticsearch.search.aggregations.Aggregation missingAggregation = resultAggs.getAsMap()
          .get(EsQueryService.FACET_MISSING_FIELD_PREFIX + facetId);
      long missingCount = ((SingleBucketAggregation) missingAggregation).getDocCount();
      if (missingCount > 0) {
        FacetResultEntry missingEntry = new FacetResultEntry();
        missingEntry.setCount(missingCount);
        missingEntry.setValue(messageSource.getMessage("search.facet.not.present", new Object[] {}, null));
        facet.getEntries().add(0, missingEntry);
      }

    }
    return facet;
  }

  private void sortFacetDateResults(FacetResult facet) {
    Pattern pattern = Pattern.compile("([\\d]+).*");
    facet.getEntries().sort((FacetResultEntry o1, FacetResultEntry o2) -> {
      String o1value = o1.getValue().replaceFirst("/", "");
      String o2value = o2.getValue().replaceFirst("/", "");

      Matcher matcher1 = pattern.matcher(o1value);
      Matcher matcher2 = pattern.matcher(o2value);

      boolean matches1 = matcher1.matches();
      boolean matches2 = matcher2.matches();

      if (matches1 && matches2) {
        String group1 = matcher1.group(1);
        String group2 = matcher2.group(1);

        if (group1.length() == 1) {
          o1value = "0" + o1value;
        }
        if (group2.length() == 1) {
          o2value = "0" + o2value;
        }

        String before = "avant";
        if (o1value.contains(before) && o2value.contains(before)) {
          return o2value.compareTo(o1value);
        } else if (o1value.contains(before)) {
          return -1;
        } else if (o2value.contains(before)) {
          return 1;
        } else {
          return o1value.compareTo(o2value);
        }
      }
      return -1;
    });

  }

  private FacetResult buildFacetResult(org.elasticsearch.search.aggregations.Aggregation aggregation, String facetId,
      Aggregation requestedAgg) {

    FacetResult facet = new FacetResult();
    facet.setId(facetId);
    facet.setFacetScope(requestedAgg.getFacetScope());
    facet.setFacetOrder(requestedAgg.getFacetOrder());
    facet.setPathHierarchical(requestedAgg.isPathHierarchical());

    if (aggregation instanceof SingleBucketAggregation) {
      for (org.elasticsearch.search.aggregations.Aggregation agg : ((SingleBucketAggregation) aggregation)
          .getAggregations()) {
        if (agg instanceof MultiBucketsAggregation) {
          addFacetEntries(((MultiBucketsAggregation) agg).getBuckets(), facet, requestedAgg.isPathHierarchical());
        } else if (agg instanceof InternalFilter) {
          for (org.elasticsearch.search.aggregations.Aggregation agg2 : ((InternalFilter) agg).getAggregations()) {
            if (agg2 instanceof MultiBucketsAggregation) {
              addFacetEntries(((MultiBucketsAggregation) agg2).getBuckets(), facet, requestedAgg.isPathHierarchical());
            } else {
              throw new RuntimeException("Unimplemented facet sub-aggregation : " + agg2.getClass());
            }
          }
        } else {
          throw new RuntimeException("Unimplemented facet sub-aggregation : " + agg.getClass());
        }
      }
    } else if (aggregation instanceof MultiBucketsAggregation) {
      addFacetEntries(((MultiBucketsAggregation) aggregation).getBuckets(), facet, requestedAgg.isPathHierarchical());
    } else {
      throw new RuntimeException("Unimplemented facet type aggregation : " + aggregation.getClass());
    }
    return facet;
  }

  private void addFacetEntries(Collection<? extends Bucket> buckets, FacetResult facet, boolean isPathHierarchical) {
    if (isPathHierarchical) {
      List<FacetResultEntry> topLevelEntries = new ArrayList<>();
      Map<String, FacetResultEntry> entries = new HashMap<>();
      for (Bucket bucket : buckets) {
        String facetEntryValue = bucket.getKeyAsString();
        if (facetEntryValue.startsWith("/")) {
          FacetResultEntry facetEntry = new FacetResultEntry();
          facetEntry.setCount(bucket.getDocCount());
          facetEntry.setValue(facetEntryValue);
          FacetResultEntry parentEntry = entries.get(facetEntryValue.substring(0, facetEntryValue.lastIndexOf('/')));
          if (parentEntry != null) {
            parentEntry.getSubEntries().add(facetEntry);
          } else {
            topLevelEntries.add(facetEntry);
          }
          entries.put(facetEntryValue, facetEntry);
        }
      }

      facet.getEntries().addAll(topLevelEntries);
    } else {
      for (Bucket bucket : buckets) {
        FacetResultEntry facetEntry = new FacetResultEntry();
        facetEntry.setCount(bucket.getDocCount());
        String facetEntryValue = bucket.getKeyAsString();
        facetEntry.setValue(facetEntryValue);

        facet.getEntries().add(facetEntry);
      }
    }

  }

  private BoolQueryBuilder constructQuery(AdvancedSearchRequest searchRequest) {
    List<QueryBuilder> queries = new LinkedList<>();

    // Filters
    if (searchRequest.getFilters() != null && !searchRequest.getFilters().isEmpty()) {
      for (SearchFilter searchFilter : searchRequest.getFilters()) {
        if (searchFilter instanceof MultiValueSearchFilter) {
          queries.add(getMultiValueSearchFilterBoolQuery(searchFilter));
        } else if (searchFilter instanceof DateRangeFilter) {
          queries.add(getRangeFilterBoolQuery(searchFilter));
        } else if (searchFilter instanceof LongRangeFilter) {
          queries.add(getRangeFilterBoolQuery(searchFilter));
        }
      }
    }

    // Construction de la query globale
    BoolQueryBuilder query = QueryBuilders.boolQuery();
    for (QueryBuilder subQuery : queries) {
      query.must(subQuery);
    }
    // Filtre sur le status publié pour les utilisateurs non admin
    if (!userInfoService.hasAnyRole(
        Arrays.asList(Authority.ADMIN.name(), Authority.CONTRIB.name(), Authority.DATABASE_MANAGER.name()))) {
      SimpleQueryStringBuilder filterPublished = QueryBuilders
          .simpleQueryStringQuery(NoticeStatus.PUBLISHED.getValue());
      filterPublished.field(INTERNAL_STATUS_FIELD);
      query.filter(filterPublished);
    }
    return query;
  }

  private BoolQueryBuilder constructQueryWithAttachment(AdvancedSearchRequest searchRequest) {
    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
    if (!searchRequest.getFilters().isEmpty() && searchRequest.getFilters().get(0) instanceof MultiValueSearchFilter) {
      if (searchRequest.getContentTarget().equals(ContentTarget.ATTACHMENT)) {
        boolQuery.must(QueryBuilders
            .queryStringQuery(((MultiValueSearchFilter) searchRequest.getFilters().get(0)).getValues().get(0))
            .field("content.mediaInformation.associatedMedia.ocr"));
      } else if (searchRequest.getContentTarget().equals(ContentTarget.ALL)) {
        boolQuery.must(QueryBuilders
            .queryStringQuery(((MultiValueSearchFilter) searchRequest.getFilters().get(0)).getValues().get(0))
            .field("*"));
      }
    } else {
      throw new BadRequestException("Mauvaise construction de la requête");
    }
    return boolQuery;
  }

  AggregationBuilder constructAggregation(Entry<String, Aggregation> agg) {

    String aggregationName = agg.getKey();
    Aggregation aggregation = agg.getValue();
    String aggregationField = aggregation.getField();

    aggregationField += AGG_SUB_FIELD;

    TermsAggregationBuilder aggregationBuilder = AggregationBuilders.terms(aggregationName).field(aggregationField)
        .size(aggregation.getSize()).minDocCount(aggregation.getMinDocCount());

    AggregationTermsOrder order = aggregation.getOrder();
    if (order == null) {
      order = AggregationTermsOrder.DEFAULT;
    }
    BucketOrder aggregationTermsOrder = getAggregationTermsOrder(order);
    if (aggregationTermsOrder != null) {
      aggregationBuilder.order(aggregationTermsOrder);
    }

    return aggregationBuilder;
  }

  AggregationBuilder constructMissingAggregation(Entry<String, Aggregation> agg) {

    String aggregationField = agg.getValue().getField();
    aggregationField += AGG_SUB_FIELD;

    return AggregationBuilders.missing(FACET_MISSING_FIELD_PREFIX + agg.getKey()).field(aggregationField);
  }

  private QueryBuilder constructAutocompleteQueryForNoticeType(String request, NoticeType noticeType) {

    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
    SimpleQueryStringBuilder filterType = QueryBuilders.simpleQueryStringQuery(noticeType.name());
    filterType.field(INTERNAL_NOTICE_TYPE_FIELD);

    BoolQueryBuilder filterQuery = QueryBuilders.boolQuery().must(filterType);
    // Filtre sur le status publié pour les utilisateurs non admin
    filterStatus(filterQuery);

    boolQuery.filter(filterQuery);

    SimpleQueryStringBuilder simpleQueryString = QueryBuilders.simpleQueryStringQuery(request);
    Map<String, Float> autocompleteFields = JsonUtils.getAutocompleteFields(metaModelService.get(null, noticeType));
    for (Entry<String, Float> entry : autocompleteFields.entrySet()) {
      simpleQueryString.field(entry.getKey() + AUTOCOMPLETE_SUB_FIELD, entry.getValue());
    }

    boolQuery.should(simpleQueryString);
    simpleQueryString.quoteFieldSuffix(QUOTE_FIELD_SUFFIXE);
    boolQuery.minimumShouldMatch(1);

    return boolQuery;

  }

  /**
   * Retourne l'objet de tri pour les TermsAggregation.
   *
   * @return
   */
  private BucketOrder getAggregationTermsOrder(AggregationTermsOrder order) {
    BucketOrder bucketOrder = null;
    switch (order.getType()) {
      case COUNT:
        bucketOrder = BucketOrder.count(order.getDirection().toBool());
        break;
      case KEY:
        bucketOrder = BucketOrder.key(order.getDirection().toBool());
        break;
      default:
        break;
    }
    return bucketOrder;
  }

  /**
   * Retourne la boolQuery construite à partir d'un MultiValueSearchFilter.
   *
   * @return
   */
  private BoolQueryBuilder getMultiValueSearchFilterBoolQuery(SearchFilter searchFilter) {
    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
    MultiValueSearchFilter multiValueSearchFilter = (MultiValueSearchFilter) searchFilter;

    String field = multiValueSearchFilter.getField();
    boolean isThesaurus = field.endsWith("thesaurus");
    String field2;
    if (isThesaurus) {
      field2 = field + ".*Labels.value";
    } else {
      field2 = field + ".*";
    }
    SearchFilter.Operator operator = multiValueSearchFilter.getOp();
    List<String> values = multiValueSearchFilter.getValues();
    Float boost = multiValueSearchFilter.getBoost();

    // all, any, none, not_all
    switch (operator) {
      case ANY:
        values.forEach(value -> {
          if ("*".equals(value)) {
            // Dans le cas d'un * on remplace par une requête exists pour avoir un
            // comportement similaire au QueryString sur ce cas précis (on ne veut pas
            // permettre l'utilisation des autres fonctionnalités du QueryString)
            // https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html#query-string-wildcard
            boolQuery.should(QueryBuilders.existsQuery(field).boost(boost));
          } else {
            boolQuery.should(QueryBuilders.simpleQueryStringQuery(value).field(field).field(field2).boost(boost));
          }
        });
        boolQuery.minimumShouldMatch(1);
        break;
      case NONE:
        BoolQueryBuilder subBoolQuery = QueryBuilders.boolQuery();
        values.forEach(value -> {
          if ("*".equals(value)) {
            // Dans le cas d'un * on remplace par une requête exists pour avoir un
            // comportement similaire au QueryString sur ce cas précis (on ne veut pas
            // permettre l'utilisation des autres fonctionnalités du QueryString)
            // https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html#query-string-wildcard
            subBoolQuery.must(QueryBuilders.existsQuery(field).boost(boost));
          } else {
            subBoolQuery.must(QueryBuilders.simpleQueryStringQuery(value).field(field).field(field2).boost(boost));
          }
        });
        boolQuery.mustNot(subBoolQuery);
        break;
      default:
        break;
    }

    return boolQuery;
  }

  /**
   * Retourne la boolQuery construite à partir d'un RangeFilter.
   *
   * @return
   */
  @SuppressWarnings("unchecked")
  private <T extends Comparable<T>, Y extends Range<T>> QueryBuilder getRangeFilterBoolQuery(
      SearchFilter searchFilter) {
    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
    BoolQueryBuilder subBoolQuery = QueryBuilders.boolQuery();
    RangeFilter<T, Y> rangeFilter = (RangeFilter<T, Y>) searchFilter;

    String field = rangeFilter.getField();
    SearchFilter.Operator operator = rangeFilter.getOp();
    List<Y> values = rangeFilter.getValues();
    Float boost = rangeFilter.getBoost();

    // all, any, none, not_all
    switch (operator) {
      case ANY:
        values.forEach(el -> boolQuery.should(buildRangeQuery(el, field).boost(boost)));
        boolQuery.minimumShouldMatch(1);
        break;
      case NONE:
        values.forEach(el -> subBoolQuery.must(buildRangeQuery(el, field).boost(boost)));
        boolQuery.mustNot(subBoolQuery);
        break;
      default:
        break;
    }

    return boolQuery;

  }

  private <T extends Comparable<T>> QueryBuilder buildRangeQuery(Range<T> range, String field) {
    if (range instanceof DateRange) {
      BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

      boolQuery.should(buildOverLaps((DateRange) range, field));
      if (range.getMin() != null && range.getMax() != null) {
        boolQuery.should(buildContainedIn((DateRange) range, field));
      }
      return boolQuery.minimumShouldMatch(1);
    } else {
      RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery(field);
      if (range.getMin() != null) {
        rangeQuery.gte(range.getMin());
      }
      if (range.getMax() != null) {
        rangeQuery.lte(range.getMax());
      }
      return rangeQuery;
    }
  }

  private QueryBuilder buildContainedIn(DateRange range, String field) {
    BoolQueryBuilder boolQueryContainedIn = QueryBuilders.boolQuery();

    RangeQueryBuilder rangeQueryStart = QueryBuilders.rangeQuery(field + START_DATE_COMPUTED);
    RangeQueryBuilder rangeQueryEnd = QueryBuilders.rangeQuery(field + END_DATE_COMPUTED);

    rangeQueryStart.lte(dateEraFormatter.format(range.getMin()));
    rangeQueryEnd.gte(dateEraFormatter.format(range.getMax()));
    return boolQueryContainedIn.must(rangeQueryStart).must(rangeQueryEnd);
  }

  private QueryBuilder buildOverLaps(DateRange range, String field) {
    BoolQueryBuilder boolQueryOverlaps = QueryBuilders.boolQuery();

    RangeQueryBuilder rangeQueryStart = QueryBuilders.rangeQuery(field + START_DATE_COMPUTED);
    RangeQueryBuilder rangeQueryEnd = QueryBuilders.rangeQuery(field + END_DATE_COMPUTED);

    if (range.getMin() != null) {
      String dateMin = dateEraFormatter.format(range.getMin());
      rangeQueryStart.gte(dateMin);
      rangeQueryEnd.gte(dateMin);
    }
    if (range.getMax() != null) {
      String dateMax = dateEraFormatter.format(range.getMax());
      rangeQueryStart.lte(dateMax);
      rangeQueryEnd.lte(dateMax);
    }
    return boolQueryOverlaps.should(rangeQueryStart).should(rangeQueryEnd).minimumShouldMatch(1);
  }

  private BoolQueryBuilder constructQueryFromSchemas(String request, NoticeType noticeType,
      ContentTarget contentTarget) {
    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
    if (noticeType != null) {
      if (NoticeType.NEWS.equals(noticeType)) {
        boolQuery.should(constructQueryForNews(request));
      } else {
        boolQuery.should(constructQueryForNoticeType(request, noticeType, contentTarget));
      }
    } else {
      boolQuery.should(constructQueryForNews(request));
      boolQuery.should(constructQueryForNoticeType(request, NoticeType.ARTWORK, contentTarget));
      boolQuery.should(constructQueryForNoticeType(request, NoticeType.COLLECTION, contentTarget));
      boolQuery.should(constructQueryForNoticeType(request, NoticeType.EVENT, contentTarget));
      boolQuery.should(constructQueryForNoticeType(request, NoticeType.PERSON, contentTarget));
      boolQuery.should(constructQueryForNoticeType(request, NoticeType.REFERENCE, contentTarget));
    }
    // Filtre sur le status publié pour les utilisateurs non admin
    filterStatus(boolQuery);
    boolQuery.minimumShouldMatch(1);
    return boolQuery;
  }

  private QueryBuilder constructQueryForNews(String request) {
    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
    SimpleQueryStringBuilder filterType = QueryBuilders.simpleQueryStringQuery(NoticeType.NEWS.name());
    filterType.field(INTERNAL_NOTICE_TYPE_FIELD);

    boolQuery.filter(QueryBuilders.boolQuery().must(filterType));
    SimpleQueryStringBuilder simpleQueryStringExact = QueryBuilders.simpleQueryStringQuery("*");
    SimpleQueryStringBuilder simpleQueryString = QueryBuilders.simpleQueryStringQuery("*");
    if (!StringUtils.isEmpty(request)) {
      simpleQueryStringExact = QueryBuilders.simpleQueryStringQuery(request);
      simpleQueryString = QueryBuilders.simpleQueryStringQuery(request);
    }
    Map<String, Float> fieldsExact = new HashMap<>();
    Map<String, Float> fields = new HashMap<>();
    fields.put("content.title", 1f);
    fields.put("content.contentElements", 1f);
    fieldsExact.putAll(fields);
    fieldsExact.put("internal.createdBy.exact", 1f);
    fields.put("internal.createdBy.text", 1f);
    simpleQueryStringExact.fields(fieldsExact);
    simpleQueryString.fields(fields);
    simpleQueryStringExact.quoteFieldSuffix(QUOTE_FIELD_SUFFIXE);
    simpleQueryString.quoteFieldSuffix(QUOTE_FIELD_SUFFIXE);

    simpleQueryStringExact.defaultOperator(Operator.AND).boost(1000f);

    boolQuery.should(simpleQueryString);
    boolQuery.should(simpleQueryStringExact);
    boolQuery.minimumShouldMatch(1);

    return boolQuery;
  }

  private QueryBuilder constructQueryForNoticeType(String request, NoticeType noticeType, ContentTarget contentTarget) {

    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
    SimpleQueryStringBuilder filterType = QueryBuilders.simpleQueryStringQuery(noticeType.name());
    filterType.field(INTERNAL_NOTICE_TYPE_FIELD);

    boolQuery.filter(QueryBuilders.boolQuery().must(filterType));

    String jsonSchema = metaModelService.get(null, noticeType);
    Map<String, Float> fields = JsonUtils.getSearchableFields(jsonSchema);

    // On ajoute le champ internal.reference pour les recherches sur les notices
    // liées
    fields.put("internal.reference", 1f);

    // Recherche dans les pièces jointe
    if (ContentTarget.ALL.equals(contentTarget)) {
      fields.put("content.mediaInformation.associatedMedia.ocr", 1f);
    }

    SimpleQueryStringBuilder simpleQueryStringExact = QueryBuilders.simpleQueryStringQuery("*");
    SimpleQueryStringBuilder simpleQueryString = QueryBuilders.simpleQueryStringQuery("*");
    if (!StringUtils.isEmpty(request)) {
      simpleQueryStringExact = QueryBuilders.simpleQueryStringQuery(request);
      simpleQueryString = QueryBuilders.simpleQueryStringQuery(request);
    }
    simpleQueryStringExact.fields(fields);
    simpleQueryString.fields(fields);
    simpleQueryStringExact.quoteFieldSuffix(QUOTE_FIELD_SUFFIXE);
    simpleQueryString.quoteFieldSuffix(QUOTE_FIELD_SUFFIXE);

    simpleQueryStringExact.defaultOperator(Operator.AND).boost(1000f);

    boolQuery.should(simpleQueryString);
    boolQuery.should(simpleQueryStringExact);
    boolQuery.minimumShouldMatch(1);

    return boolQuery;
  }

  private void addSort(String sort, NoticeSortType sortType, EsSearchParameters searchParameters) {
    if (sortType != null) {
      String field = sortType.getField();
      searchParameters.setSortBuilders(Arrays.asList(createFieldSortBuilder(sort, field)));
    } else {
      searchParameters.setSortBuilders(Arrays.asList(SortBuilders.scoreSort()));
    }
  }

  private void addMediaSort(String sort, MediaSortType sortType, EsSearchParameters searchParameters) {
    if (sortType != null && sortType.getField() != null) {
      searchParameters.setSortBuilders(Arrays.asList(createFieldSortBuilder(sort, sortType.getField())));
    } else {
      searchParameters.setSortBuilders(Arrays.asList(SortBuilders.scoreSort()));
    }
  }

  private void addUserSort(String sort, UserSortType sortType, EsSearchParameters searchParameters) {
    if (sortType != null && sortType.getField() != null) {
      searchParameters.setSortBuilders(Arrays.asList(createFieldSortBuilder(sort, sortType.getField())));
    } else {
      searchParameters
          .setSortBuilders(Arrays.asList(createFieldSortBuilder("DESC", UserSortType.CREATION_DATE.getField())));
    }
  }
}
