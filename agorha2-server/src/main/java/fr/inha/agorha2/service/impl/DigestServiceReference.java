package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.DigestService;
import fr.inha.agorha2.utils.DateUtils;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.model.NoticeType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class DigestServiceReference extends AbstractDigestService implements DigestService {

  private static final String DIGEST_JOURNAL_TITLE_FIELD = "journalTitle";
  private static final String DIGEST_TITLE_FIELD = "title";
  private static final String EDITION_FIELD_PATH = "/content/identificationInformation/edition";

  @Override
  public NoticeType getType() {
    return NoticeType.REFERENCE;
  }

  @Override
  public void build(JsonNode notice) throws ServiceException {
    ObjectNode digestObject = getDigestObject(notice);

    buildCommonFields(notice, digestObject);

    String title = removeHtmlTags(notice
        .at(JsonUtils.getPathToField(notice, "/content/identificationInformation/journal", "/articleTitle/value", ""))
        .asText());
    if (StringUtils.isBlank(title)) {
      // Affichage en italique pour le titre de l'ouvrage (APDT-640)
      String workTitle = removeHtmlTags(notice
          .at(JsonUtils.getPathToField(notice, "/content/identificationInformation/workTitle", "/label/value", ""))
          .asText());
      if (StringUtils.isNotBlank(workTitle)) {
        title = "<i>" + workTitle + "</i>";
      }
      if (StringUtils.isBlank(title)) {
        title = removeHtmlTags(notice.at("/content/identificationInformation/reference/description/value").asText());
        int length = title.length();
        // On tronque la description des références à 100 caractèrescar trop longues
        // pour être affichées correctement sur le front
        if (length > 100) {
          title = title.substring(0, 100) + " [...]";
        }
      }
    }
    digestObject.put(DIGEST_TITLE_FIELD, title);

    digestObject.set("referenceType",
        getObjectNode(notice.at("/content/identificationInformation/reference/type/thesaurus/0/prefLabels/0/value")));
    // Affichage en italique pour le titre de la revue (APDT-640)
    String journalTitle = removeHtmlTags(
        notice.at(JsonUtils.getPathToField(notice, "/content/identificationInformation/journal", "/title/value", ""))
            .asText());
    if (StringUtils.isNoneBlank(journalTitle)) {
      digestObject.set(DIGEST_JOURNAL_TITLE_FIELD, new TextNode("<i>" + journalTitle + "</i>"));
    }

    JsonNode authorName = notice
        .at(JsonUtils.getPathToField(notice, "/content/identificationInformation/contributor", "/name/value", ""));
    if (authorName.isMissingNode()) {
      authorName = notice
          .at(JsonUtils.getPathToField(notice, "/content/identificationInformation/author", "/name/value", ""));
    }
    digestObject.set("authorName", getObjectNode(authorName));

    String pathToDate = JsonUtils.getPathToField(notice, EDITION_FIELD_PATH, "/date", "");
    if (StringUtils.isNotEmpty(pathToDate)) {
      digestObject.set("startDate", constructDate(pathToDate + "/start", notice));
      digestObject.set("endDate", constructDate(pathToDate + "/end", notice));
    }

    digestObject.set("editionPlace", getObjectNode(notice
        .at(JsonUtils.getPathToField(notice, EDITION_FIELD_PATH, "/editingPlace/thesaurus", "/0/prefLabels/0/value"))));
    digestObject.set("editor", getObjectNode(
        notice.at(JsonUtils.getPathToField(notice, EDITION_FIELD_PATH, "/editingInstitution/value", ""))));

    digestObject.set("institution", getObjectNode(notice.at(
        JsonUtils.getPathToField(notice, "/content/localizationInformation/localization", "/institution/value", ""))));
    digestObject.set("shelfMark", getObjectNode(notice.at(
        JsonUtils.getPathToField(notice, "/content/localizationInformation/localization", "/shelfMark/value", ""))));

    digestObject.put("noticeType", getType().toString());

    digestObject.put("displayLabelLink", buildDisplayLabelLink(digestObject));

    // On supprimer les tag html <i> ajoutés pour journalTitle et title (ne servent
    // qu'à calculer le displayLabelLink)
    digestObject.put(DIGEST_TITLE_FIELD, removeHtmlTags(digestObject.at("/title").asText()));
    digestObject.put(DIGEST_JOURNAL_TITLE_FIELD, removeHtmlTags(digestObject.at("/journalTitle").asText()));
  }

  private String buildDisplayLabelLink(JsonNode digestObject) {
    StringBuilder sb = new StringBuilder();
    addValueToDisplayLabel(digestObject.get("authorName"), sb);

    addValueToDisplayLabel(digestObject.get(DIGEST_TITLE_FIELD), sb);
    addValueToDisplayLabel(digestObject.get(DIGEST_JOURNAL_TITLE_FIELD), sb);
    addValueToDisplayLabel(digestObject.get("editionPlace"), sb);

    String datesAsString = DateUtils.buildDatesFormattedFromDigest(digestObject.get("startDate"),
        digestObject.get("endDate"));
    if (StringUtils.isNoneBlank(datesAsString)) {
      sb.append(" ");
      sb.append(datesAsString);
    }
    addValueToDisplayLabel(digestObject.get("institution"), sb);
    addValueToDisplayLabel(digestObject.get("shelfMark"), sb);

    return sb.toString();
  }

}
