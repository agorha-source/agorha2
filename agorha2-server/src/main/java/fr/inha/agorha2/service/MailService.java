package fr.inha.agorha2.service;

public interface MailService {

  public void sendCorrectionProposal(String noticeId, String text, String userMail);

  public void sendAccountCreation(String mail, String validationLink);

  public void sendResetPassword(String mail, String resetPasswordLink);

  public void sendUpdateAuthorizations(String mail, String role);

}
