package fr.inha.agorha2.service.impl;

import static java.util.Map.entry;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.intf.IServiceDedicated;
import com.sword.utils.elasticsearch.search.EsSearchParameters;
import fr.inha.agorha2.api.dto.ImageFormat;
import fr.inha.agorha2.api.dto.MediaItem;
import fr.inha.agorha2.api.dto.MediaRetrieveMode;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.service.MediaDataSourceMode;
import fr.inha.agorha2.service.MediaService;
import fr.inha.agorha2.service.ThesaurusService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.config.EsIndexEnum;
import fr.inha.elastic.model.Database;
import fr.inha.elastic.model.InternationalLabel;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.MediaFile;
import fr.inha.elastic.model.MediaType;
import fr.inha.elastic.model.Thesaurus;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.ws.rs.NotFoundException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.common.util.UrlUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.SimpleQueryStringBuilder;
import org.elasticsearch.search.SearchHit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

@Service
public class MediaServiceImpl implements MediaService {

  public static final String YOUTUBE_OEMBED_URL = "https://www.youtube.com/oembed";
  public static final String FLICKR_OEMBED_URL = "https://www.flickr.com/services/oembed";
  public static final String SKETCHFAB_OEMBED_URL = "https://sketchfab.com/oembed";

  public static final Pattern EXTENSION_PATTERN = Pattern.compile(".*\\.([\\w]+)");

  static final Map<MediaType, String> OEMBED_URL_PER_MEDIA_TYPE = Map.ofEntries(
      entry(MediaType.YOUTUBE, YOUTUBE_OEMBED_URL), entry(MediaType.FLICKR, FLICKR_OEMBED_URL),
      entry(MediaType.SKETCHFAB, SKETCHFAB_OEMBED_URL));

  private static final List<String> VIDEO_CONTENT_TYPE = Arrays.asList("application/mp4", "application/mov",
      "video/mp4", "video/x-flv", "video/quicktime", "video/x-msvideo");
  private static final List<String> TIF_EXTENSIONS = Arrays.asList("tif", "tiff", "TIF", "TIFF");
  private static final String PDF_CONTENT_TYPE = "application/pdf";
  private static final String FILES_FIELD = "files";
  private static final String CONTENT_TYPE_FIELD = "contentType";
  private static final String IMAGE_CONTENT_TYPE = "image";
  private static final String CAPTION_FIELD = "caption";
  private static final String FILES_CONTENT_TYPE_FIELD = "files.contentType";
  private static final String FILES_FILE_UUID_FIELD = "files.uuid";
  private static final String THUMBNAILS_FIELD = "thumbnail";
  private static final String ALT_FIELD = "alt";
  private static final String INTERNAL_UUID_FIELD = "internal.uuid";
  private static final String INTERNAL_UNIQUE_KEY_FIELD = "internal.uniqueKey";
  private static final String TITLE_FIELD = "title";

  private static final Logger log = LoggerFactory.getLogger(MediaServiceImpl.class);

  private ObjectMapper mapper = DefaultEsContext.instance().getObjectMapper();

  @Value("${media.store.path}")
  private String mediaStorePathAsName;

  @Value(value = "${media.iiif.manifests.baseUrl}")
  private String iiifManifestsBaseUrl;

  @Value(value = "${resources.fullBaseUrl}")
  private String fullBaseUrl;

  @Autowired
  private IServiceDedicated<Media> mediaService;

  @Autowired
  private EsClient esClient;

  @Autowired
  private UserInfoService userInfoService;

  @Autowired
  private ThesaurusService thesaurusService;

  @Autowired
  private MessageSource messageSource;

  private Path mediaStorePath;

  @PostConstruct
  private void init() {
    mediaStorePath = Paths.get(mediaStorePathAsName);
  }

  @Override
  public Media get(String mediaId) throws ServiceException {
    try {
      List<Media> list = mediaService.list(QueryBuilders.matchQuery(INTERNAL_UUID_FIELD, mediaId), 1);
      if (list.isEmpty()) {
        throw new NotFoundException("Pas de media associé à l'id : " + mediaId);
      }
      return list.get(0);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de l'interrogation d'ES, récupération du media " + mediaId, e);
    }
  }

  @Override
  public Map<String, Object> createMedia(List<MediaItem> mediaItems, Media draft) throws ServiceException {
    if (userInfoService.hasAnyRole(Arrays.asList(Authority.BATCH.name()))) {
      return createMedia(mediaItems, draft, MediaDataSourceMode.BATCH);
    } else {
      return createMedia(mediaItems, draft, MediaDataSourceMode.CONTRIBUTION);
    }
  }

  @Override
  public Map<String, Object> createMedia(List<MediaItem> mediaItems, Media draft, MediaDataSourceMode dataSourceMode)
      throws ServiceException {

    draft.getInternal().setUuid(UUID.randomUUID().toString());

    // Mise à jour des champs d'audit
    if (!MediaDataSourceMode.BATCH.equals(dataSourceMode)) {
      updateInternal(draft, true);
    }
    draft.getInternal().setLinkedWithNotices("Non");

    updateDatabase(draft);

    for (MediaItem mediaItem : mediaItems) {
      createMediaFile(draft, mediaItem);
    }

    updateThesaurus(draft);

    // Indexation dans ES
    Media createdMedia;
    try {
      createdMedia = mediaService.createOrUpdateAndGet(draft, true);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de l'indexation du media " + draft.getCaption(), e);
    }

    log.info("Media {} créé", createdMedia.getInternal().getUuid());

    Map<String, Object> result = new HashMap<>();
    result.put("id", createdMedia.getInternal().getUuid());
    result.put(TITLE_FIELD, draft.getCaption());
    Optional<MediaFile> findFirst = draft.getFiles().stream().filter(file -> file.getThumbnail() != null).findFirst();
    if (findFirst.isPresent()) {
      result.put(THUMBNAILS_FIELD, findFirst.get().getThumbnail());
    }
    result.put(FILES_FIELD, createdMedia.getFiles());
    return result;
  }

  @Override
  public Map<String, String> updateMedia(String uuid, List<MediaItem> mediaItems, Media draft) throws ServiceException {
    if (userInfoService.hasAnyRole(Arrays.asList(Authority.BATCH.name()))) {
      return updateMedia(uuid, mediaItems, draft, MediaDataSourceMode.BATCH);
    } else {
      return updateMedia(uuid, mediaItems, draft, MediaDataSourceMode.CONTRIBUTION);
    }
  }

  @Override
  public Map<String, String> updateMedia(String uuid, List<MediaItem> mediaItems, Media draft,
      MediaDataSourceMode dataSourceMode) throws ServiceException {
    Media media = get(uuid);
    Map<String, MediaFile> existingFiles = new HashMap<>();

    for (MediaFile mediaFile : media.getFiles()) {
      existingFiles.put(mediaFile.getUuid(), mediaFile);
    }

    // Mise à jour des champs d'audit
    if (!MediaDataSourceMode.BATCH.equals(dataSourceMode)) {
      draft.setInternal(media.getInternal());
      updateInternal(draft, false);
    }
    updateDatabase(draft);

    draft.setId(media.getId());

    Map<String, MediaFile> filesToDelete = updateDraftFiles(mediaItems, draft, existingFiles);

    for (Entry<String, MediaFile> entry : filesToDelete.entrySet()) {
      if (entry.getValue().getMediaType().equals(MediaType.LOCAL)) {
        deleteLocalFile(media.getInternal().getUuid(), entry.getKey());
      }
    }

    updateThesaurus(draft);

    // Indexation dans ES
    Media createdMedia;
    try {
      createdMedia = mediaService.createOrUpdateAndGet(draft, true);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de l'indexation du media " + draft.getCaption(), e);
    }

    Map<String, String> result = new HashMap<>();
    result.put("id", createdMedia.getInternal().getUuid());
    result.put(TITLE_FIELD, draft.getCaption());
    Optional<MediaFile> findFirst = draft.getFiles().stream().filter(file -> file.getThumbnail() != null).findFirst();
    if (findFirst.isPresent()) {
      result.put(THUMBNAILS_FIELD, findFirst.get().getThumbnail());
    }
    return result;
  }

  @Override
  public Map<String, String> getFile(String idMedia, String idFile, ImageFormat format) throws ServiceException {
    EsSearchParameters searchParameters = new EsSearchParameters();
    MatchQueryBuilder matchQuery = QueryBuilders.matchQuery(INTERNAL_UUID_FIELD, idMedia);
    searchParameters.setQuery(matchQuery);
    searchParameters.setIndexes(Arrays.asList(EsIndexEnum.MEDIA));

    String fileContentField = "";
    switch (format) {
      case L:
        fileContentField = "originFile";
        break;
      case M:
        fileContentField = "mediumSizePicture";
        break;
      case S:
        fileContentField = THUMBNAILS_FIELD;
        break;
      default:
        break;
    }
    searchParameters.setSourceInclude(Arrays.asList("files.alt", "files." + fileContentField, FILES_CONTENT_TYPE_FIELD,
        CAPTION_FIELD, FILES_FILE_UUID_FIELD));

    try {
      SearchResponse search = esClient.getServiceGeneral().search(searchParameters);

      if (search.getHits().getHits().length == 0) {
        throw new NotFoundException("Identifiant de media inconnu : " + idMedia);
      } else if (search.getHits().getHits().length != 1) {
        throw new NotFoundException("Plusieurs media trouvé pour l'identifiant : " + idMedia);
      }

      SearchHit hit = search.getHits().getAt(0);
      Map<String, Object> source = hit.getSourceAsMap();
      List<Map<String, Object>> files = (List<Map<String, Object>>) source.get(FILES_FIELD);
      for (Map<String, Object> file : files) {
        if (idFile.equals(file.get("uuid"))) {
          return createResult((String) file.get(fileContentField), (String) source.get(CAPTION_FIELD), file);
        }
      }
    } catch (EsRequestException e) {
      throw new ServiceException(
          "Erreur lors de l'interrogation d'ES, récupération du media " + idMedia + ", fichier " + idFile, e);
    }
    return null;
  }

  @Override
  public List<Media> getMedia(List<String> mediaIds, MediaRetrieveMode mode) throws ServiceException {
    SimpleQueryStringBuilder queryString = QueryBuilders.simpleQueryStringQuery(String.join(" ", mediaIds));
    BoolQueryBuilder query = QueryBuilders.boolQuery();
    query.must(queryString.field(INTERNAL_UUID_FIELD));

    List<String> sourceExclude = null;
    List<String> sourceInclude = null;
    switch (mode) {
      case FULL:
        sourceExclude = Arrays.asList("files.internalMetadata.*");
        break;
      case THUMBNAILS:
        sourceExclude = Arrays.asList("files.internalMetadata.*", "files.originFile", "files.mediumSizePicture",
            "files.embeddedContent");
        sourceInclude = Arrays.asList("files.*", INTERNAL_UUID_FIELD);
        break;
      default:
        break;
    }
    List<Media> medias = new ArrayList<>();
    try {
      medias = esClient.getServiceMedia().listAll(query, sourceInclude, sourceExclude);
    } catch (EsRequestException e) {
      throw new ServiceException(
          "Erreur lors de l'interrogation d'ES, recherche des thumbnails des media : " + mediaIds, e);
    }
    // Tri des medias récupérés pour les avoir dans le même ordre que demandé dans
    // la liste mediaIds
    medias
        .sort((o1, o2) -> mediaIds.indexOf(o1.getInternal().getUuid()) - mediaIds.indexOf(o2.getInternal().getUuid()));
    return medias;
  }

  @Override
  public long delete(List<String> uuids) throws ServiceException {
    List<String> toDelete = new ArrayList<>();
    for (String uuid : uuids) {
      Media media = get(uuid);
      if (media.getInternal().getNotice().isEmpty()) {
        toDelete.add(uuid);
        deleteLocalMedia(media.getInternal().getUuid());
      }
    }

    try {
      return mediaService.deleteByQuery(
          QueryBuilders.simpleQueryStringQuery(String.join(" ", toDelete)).field(INTERNAL_UUID_FIELD), true);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de la suppression des notices " + uuids, e);
    }
  }

  private void deleteLocalMedia(String mediaUuid) {
    try {
      FileUtils.deleteDirectory(mediaStorePath.resolve(mediaUuid).toFile());
    } catch (IOException e) {
      log.error("Erreur lors de la suppression du media {}", mediaUuid, e);
    }

  }

  private void deleteLocalFile(String mediaUuid, String fileUuid) {
    try {
      FileUtils.deleteDirectory(mediaStorePath.resolve(mediaUuid).resolve(fileUuid).toFile());
    } catch (IOException e) {
      log.error("Erreur lors de la suppression du media {},fichier {}", mediaUuid, fileUuid, e);
    }
  }

  private Map<String, MediaFile> updateDraftFiles(List<MediaItem> mediaItems, Media draft,
      Map<String, MediaFile> existingFiles) throws ServiceException {
    for (MediaItem mediaItem : mediaItems) {
      MediaFile mediaFile = existingFiles.get(mediaItem.getUuid());
      // Si le fichier existe déjà dans ES
      if (mediaFile != null) {
        try {
          // Si le fichier est un fichier uploadé et qu'il n'a pas de contenu
          // Ou si c'est une url externe et que c'est la même que celle dans ES
          // On ne re traite pas le fichier, on reprend celui dans ES
          if (((mediaItem.getMediaType().equals(MediaType.LOCAL) || mediaItem.getMediaType().equals(MediaType.IIIF))
              && mediaItem.getInputStream().available() == 0)
              || (!mediaItem.getMediaType().equals(MediaType.LOCAL) && !mediaItem.getMediaType().equals(MediaType.IIIF)
                  && getMediaContentAndRewriteIt(mediaItem).equals(mediaFile.getOriginFile()))) {
            draft.getFiles().add(mediaFile);
          } else {
            createMediaFile(draft, mediaItem);
          }
          existingFiles.remove(mediaItem.getUuid());
        } catch (IOException e) {
          throw new ServiceException(
              "Erreur lors de la lecture de la pièce jointe de type " + mediaItem.getMediaType().name()
                  + ", nom du fichier : " + mediaItem.getName() + ", uniqueKey : " + mediaItem.getUniqueKey(),
              e);
        }
      } else {
        createMediaFile(draft, mediaItem);
      }
    }
    return existingFiles;
  }

  private String getMediaContentAndRewriteIt(MediaItem mediaItem) throws ServiceException {
    try {
      String content = IOUtils.toString(mediaItem.getInputStream(), StandardCharsets.UTF_8);
      mediaItem.setInputStream(IOUtils.toInputStream(content, StandardCharsets.UTF_8));
      return content;
    } catch (IOException e) {
      throw new ServiceException(
          "Erreur lors de la lecture de la pièce jointe de type " + mediaItem.getMediaType().name()
              + ", nom du fichier : " + mediaItem.getName() + ", uniqueKey : " + mediaItem.getUniqueKey(),
          e);
    }
  }

  private void createMediaFile(Media draft, MediaItem mediaItem) throws ServiceException {
    try {
      MediaFile file = null;
      switch (mediaItem.getMediaType()) {
        case LOCAL:
        case LOCAL_IMAGE:
        case LOCAL_VIDEO:
          file = createLocalMedia(draft.getInternal().getUuid(), mediaItem);
          break;
        case IIIF:
          file = createMediaFromManifest(mediaItem, draft.getInternal().getUuid());
          break;
        case RMN:
          file = createMediaFromRMN(mediaItem);
          break;
        default:
          // Dans tous les autres cas, il s'agit d'une url
          file = createMediaFromUrl(mediaItem);
          break;
      }
      updateMediaType(mediaItem, file);
      addFacetMediaType(file);
      file.setUniqueKey(mediaItem.getUniqueKey());
      draft.getFiles().add(file);
    } catch (IOException e) {
      throw new ServiceException("Erreur lors de la lecture de la pièce jointe de type "
          + mediaItem.getMediaType().name() + ", nom du fichier : " + mediaItem.getName() + ", uniqueKey : "
          + mediaItem.getUniqueKey() + ", titre : " + draft.getCaption(), e);
    }
  }

  private void updateMediaType(MediaItem mediaItem, MediaFile file) {
    if (mediaItem.getMediaType().equals(MediaType.LOCAL)) {
      if (VIDEO_CONTENT_TYPE.contains(file.getContentType())) {
        file.setMediaType(MediaType.LOCAL_VIDEO);
      } else if (file.getContentType().startsWith(IMAGE_CONTENT_TYPE)) {
        file.setMediaType(MediaType.LOCAL_IMAGE);
      } else {
        file.setMediaType(MediaType.LOCAL);
      }
    } else {
      file.setMediaType(mediaItem.getMediaType());
    }
  }

  private void addFacetMediaType(MediaFile file) {
    String computedMediaType;
    switch (file.getMediaType()) {
      case LOCAL_IMAGE:
        computedMediaType = "/Média interne/image";
        break;
      case LOCAL_VIDEO:
        computedMediaType = "/Média interne/vidéo";
        break;
      case LOCAL:
        if (file.getContentType().startsWith("application")) {
          computedMediaType = "/Média interne/fichier bureautique";
        } else {
          computedMediaType = "/Média interne/autre type de média";
        }
        break;
      case IIIF:
        computedMediaType = "/Média externe/manifest IIIF";
        break;
      case RMN:
        computedMediaType = "/Média externe/API Rmn-Grand Palais";
        break;
      case SKETCHFAB:
        computedMediaType = "/Média externe/modèle 3D";
        break;
      default:
        computedMediaType = "/Média externe/autres média";
        break;
    }
    file.setComputedFacetMediaType(computedMediaType);
  }

  private MediaFile createLocalMedia(String mediaUUID, MediaItem mediaItem) throws ServiceException {
    String name = mediaItem.getName();

    MediaFile file;
    // Pour chaque fichier, on créer un nouveau MediaFile
    try (InputStream inputStream = mediaItem.getInputStream()) {
      file = createLocalMediaFile(mediaUUID, inputStream, name, mediaItem.getUuid(), mediaItem.getThumbnail());
    } catch (IOException | SAXException | TikaException e) {
      throw new ServiceException("Erreur lors de la lecture du contenu du fichier : " + name, e);
    }
    return file;
  }

  private MediaFile createMediaFromManifest(MediaItem mediaItem, String mediaUUID) throws IOException {
    MediaFile mediaFile = new MediaFile();
    if (StringUtils.isNotEmpty(mediaItem.getUuid())) {
      mediaFile.setUuid(mediaItem.getUuid());
    } else {
      mediaFile.setUuid(UUID.randomUUID().toString());
    }
    URL manifestUrl = new URL(IOUtils.toString(mediaItem.getInputStream(), StandardCharsets.UTF_8));
	String jsonContent = IOUtils.toString(manifestUrl.openStream(), StandardCharsets.UTF_8);
    
    
    Path originFile = Paths.get(mediaUUID, mediaFile.getUuid(), "manifest.json");
    FileUtils.writeByteArrayToFile(mediaStorePath.resolve(originFile).toFile(), jsonContent.getBytes());
    JsonNode manifest = mapper.readTree(jsonContent);
    mediaFile.setOriginFile(manifest.at("/@id").asText());
    JsonNode imageUrl = manifest.at("/sequences/0/canvases/0/images/0/resource/service/@id");
    mediaFile.setMediumSizePicture(imageUrl.asText() + "/full/!400,400/0/default.jpg");
    mediaFile.setThumbnail(imageUrl.asText() + "/full/!200,200/0/default.jpg");
    return mediaFile;
  }

  private MediaFile createMediaFromRMN(MediaItem mediaItem) throws IOException {
    String jsonAsString = IOUtils.toString(mediaItem.getInputStream(), StandardCharsets.UTF_8);

    JsonNode jsonNode = mapper.readTree(jsonAsString);
    MediaFile mediaFile = new MediaFile();
    mediaFile.setThumbnail(jsonNode.get(THUMBNAILS_FIELD).asText());
    mediaFile.setOriginFile(jsonNode.get("permalink").asText());
    mediaFile.setMediumSizePicture(jsonNode.get("medium").asText());

    if (StringUtils.isNotEmpty(mediaItem.getUuid())) {
      mediaFile.setUuid(mediaItem.getUuid());
    } else {
      mediaFile.setUuid(UUID.randomUUID().toString());
    }
    return mediaFile;
  }

  private MediaFile createMediaFromUrl(MediaItem mediaItem) throws IOException {
    String mediaUrl = IOUtils.toString(mediaItem.getInputStream(), StandardCharsets.UTF_8);
    MediaFile media = new MediaFile();
    media.setOriginFile(mediaUrl);
    if (StringUtils.isNotEmpty(mediaItem.getUuid())) {
      media.setUuid(mediaItem.getUuid());
    } else {
      media.setUuid(UUID.randomUUID().toString());
    }

    String encodedMediaUrl = URLEncoder.encode(mediaUrl, StandardCharsets.UTF_8);
    URL url = new URL(OEMBED_URL_PER_MEDIA_TYPE.get(mediaItem.getMediaType()) + "?format=json&url=" + encodedMediaUrl);

    log.info("Appel de contenu externe, url : {}", url);

    JsonNode jsonNode = getOembedContent(url);
    media.setEmbeddedContent(jsonNode.get("html").asText());
    media.setThumbnail(jsonNode.get("thumbnail_url").asText());
    String title = jsonNode.get(TITLE_FIELD).asText();
    media.setAlt(title);
    media.getInternalMetadata().put("provider_name", jsonNode.get("provider_name").asText());
    media.getInternalMetadata().put("author_name", jsonNode.get("author_name").asText());
    media.getInternalMetadata().put(TITLE_FIELD, title);
    media.setContentType("oembed");
    return media;

  }

  private void updateDatabase(Media draft) throws ServiceException {
    List<Map<String, String>> newRefs = new ArrayList<>();
    for (Map<String, String> database : draft.getDatabase()) {
      String ref = database.get("ref");

      if (ref.matches("[A-Z]+/[\\d]+")) {
        String uniqueKey = ref.replaceFirst("[A-Z]+/", "");
        updateDatabase(newRefs, ref, uniqueKey, INTERNAL_UNIQUE_KEY_FIELD);
      } else {
        updateDatabase(newRefs, ref, ref, INTERNAL_UUID_FIELD);
      }
    }
    draft.getDatabase().clear();
    draft.getDatabase().addAll(newRefs);
  }

  private void updateDatabase(List<Map<String, String>> newRefs, String ref, String uniqueKey, String field)
      throws ServiceException {
    Map<String, String> newDatabase = new HashMap<>();
    List<Database> databases = null;
    try {
      databases = esClient.getServiceDatabase().list(QueryBuilders.termQuery(field, uniqueKey), 2);
      if (CollectionUtils.isEmpty(databases)) {
        newDatabase.put("ref", ref);
        newRefs.add(newDatabase);
        log.warn("Cle unique inconnu : {}", uniqueKey);
      } else if (databases.size() != 1) {
        newDatabase.put("ref", ref);
        newRefs.add(newDatabase);
        log.warn("Plusieurs database trouvées pour la clé unique : {}", uniqueKey);
      } else {
        newDatabase.put("ref", databases.get(0).getInternal().getUuid());
        newDatabase.put("value", databases.get(0).getContent().getTitle());
        newRefs.add(newDatabase);
      }
    } catch (EsRequestException e) {
      throw new ServiceException(messageSource.getMessage("elasticsearch.internal.error", new Object[] {}, null), e);
    }
  }

  private void updateThesaurus(Media draft) {
    if (draft.getCommunicationRight() != null) {
      Thesaurus communicationRight = draft.getCommunicationRight().getThesaurus();
      communicationRight.getPreflabels()
          .add(new InternationalLabel(thesaurusService.getPrefLabel(communicationRight.getRef())));
    }

    if (draft.getReproductionRight() != null) {
      Thesaurus reproductionRight = draft.getReproductionRight().getThesaurus();
      reproductionRight.getPreflabels()
          .add(new InternationalLabel(thesaurusService.getPrefLabel(reproductionRight.getRef())));
    }
  }

  JsonNode getOembedContent(URL url) throws IOException {
    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    conn.setRequestMethod("GET");
    conn.setRequestProperty("Accept", "application/json");

    StringBuilder sb = new StringBuilder();
    try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {

      String output;
      while ((output = br.readLine()) != null) {
        sb.append(output);
      }
    } finally {
      conn.disconnect();
    }
    String string = sb.toString();

    return mapper.readTree(string);
  }

  private Map<String, String> createResult(String content, String title, Map<String, Object> file) {
    Map<String, String> result = new HashMap<>();
    result.put(ALT_FIELD, (String) file.get(ALT_FIELD));
    result.put(CONTENT_TYPE_FIELD, (String) file.get(CONTENT_TYPE_FIELD));
    result.put(TITLE_FIELD, title);
    result.put("file", content);
    return result;
  }

  private void updateInternal(Media draft, boolean isCreation) {
    if (isCreation) {
      draft.getInternal().setCreatedBy(userInfoService.getCurrentUsername());
      draft.getInternal().setCreatedDate(LocalDateTime.now());
    }
    draft.getInternal().setUpdatedBy(userInfoService.getCurrentUsername());
    draft.getInternal().setUpdatedDate(LocalDateTime.now());
  }

  private MediaFile createLocalMediaFile(String mediaUUID, InputStream inputStream, String fileName, String fileUUID,
      InputStream videoThumbnail) throws IOException, SAXException, TikaException {
    MediaFile file = new MediaFile();
    file.setAlt(fileName);
    if (fileUUID == null) {
      fileUUID = UUID.randomUUID().toString();
    }
    file.setUuid(fileUUID);

    // Pour récupérer le contenu du fichier
    BodyContentHandler handler = new BodyContentHandler(-1);
    // Pour récupérer les metadata du fichier
    Metadata metadata = new Metadata();

    ParseContext context = new ParseContext();

    AutoDetectParser parser = new AutoDetectParser();

    // Création d'un nouvel inputstream pour pouvoir le réutiliser plusieurs fois
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
      inputStream.transferTo(baos);

      try (ByteArrayInputStream tikaInputStream = new ByteArrayInputStream(baos.toByteArray())) {
        parser.parse(tikaInputStream, handler, metadata, context);
      }
      file.setOcr(handler.toString());

      // On ajoute tout les metadata
      for (String name : metadata.names()) {
        file.getInternalMetadata().put(name.replace('.', '_'), metadata.get(name));
      }

      String contentType = metadata.get("Content-Type");
      file.setContentType(contentType);

      byte[] originalBytes = baos.toByteArray();
      file.setFileSize(originalBytes.length);

      String extension = FilenameUtils.getExtension(fileName);
      Path originfilePath = Paths.get(mediaUUID, fileUUID, "original." + extension);
      file.setOriginFile(fullBaseUrl + String.join("/", mediaUUID, "doc", fileUUID, "original." + extension));

      if (!contentType.startsWith(IMAGE_CONTENT_TYPE)) {
        FileUtils.writeByteArrayToFile(mediaStorePath.resolve(originfilePath).toFile(), originalBytes);
      }

      // Pour les pdfs, on génère un vignette de la première page
      if (contentType.equals(PDF_CONTENT_TYPE)) {
        BufferedImage original = generatePreview(originalBytes);
        createMediumAndThumbnail(mediaUUID, file, fileUUID, original, "png");
      } else if (contentType.startsWith(IMAGE_CONTENT_TYPE)) {
        // Dans le cas d'une image on créé le thumbnails et le medium
        try (InputStream imageInputStream = new ByteArrayInputStream(baos.toByteArray())) {
          BufferedImage original = ImageIO.read(imageInputStream);
          // Transformation de l'image en fonction de l'orientation dans les métadata
          original = transformImage(original, file.getInternalMetadata().get("tiff:Orientation"));
          file.getInternalMetadata().put("Height", original.getHeight() + "");
          file.getInternalMetadata().put("Width", original.getWidth() + "");

          if (extension.equals("png")) {
            // Les PNG ne sont pas compatibles avec hymir/mirador, on créer des jpg à la
            // place, on garde une copie du fichier original
            log.debug("Création de l'original PNG {}", fileUUID);
            FileUtils.writeByteArrayToFile(mediaStorePath.resolve(originfilePath).toFile(),
                getBytesFromImage(original, extension));

            BufferedImage jpgImage = new BufferedImage(original.getWidth(), original.getHeight(),
                BufferedImage.TYPE_INT_RGB);
            // correspond au background de mirador (#f5f5f5)
            float[] rgBtoHSB = Color.RGBtoHSB(245, 245, 245, null);
            jpgImage.createGraphics().drawImage(original, 0, 0,
                Color.getHSBColor(rgBtoHSB[0], rgBtoHSB[1], rgBtoHSB[2]), null);
            original = jpgImage;

            extension = "jpg";
            originfilePath = Paths.get(mediaUUID, fileUUID, "original." + extension);
          }

          if (TIF_EXTENSIONS.contains(extension)) {
            createMediumAndThumbnail(mediaUUID, file, fileUUID, original, "png");
          } else {
            createMediumAndThumbnail(mediaUUID, file, fileUUID, original, extension);
          }
          log.debug("Création de l'original {}", fileUUID);
          FileUtils.writeByteArrayToFile(mediaStorePath.resolve(originfilePath).toFile(),
              getBytesFromImage(original, extension));
        }
      } else if (VIDEO_CONTENT_TYPE.contains(contentType)) {
        // Dans le cas d'une vidéo on utilise le thumbnail fournit pour créer thumbnail
        // et medium
        BufferedImage original = ImageIO.read(videoThumbnail);
        createMediumAndThumbnail(mediaUUID, file, fileUUID, original, "png");
      }
    }
    return file;
  }

  private BufferedImage transformImage(BufferedImage original, String orientation) {

    AffineTransform transform = getExifTransformation(original.getWidth(), original.getHeight(), orientation);
    if (transform == null) {
      return original;
    }

    AffineTransformOp op = new AffineTransformOp(transform, AffineTransformOp.TYPE_BICUBIC);
    BufferedImage orientedImage = new BufferedImage(original.getHeight(), original.getWidth(), original.getType());
    orientedImage = op.filter(original, orientedImage);
    return orientedImage;
  }

  private AffineTransform getExifTransformation(int width, int height, String orientation) {
    if (StringUtils.isEmpty(orientation)) {
      return null;
    }
    AffineTransform t = new AffineTransform();

    switch (orientation) {
      case "1":
        return null;
      case "2": // Flip X
        t.scale(-1.0, 1.0);
        t.translate(-width, 0);
        break;
      case "3": // PI rotation
        t.translate(width, height);
        t.rotate(Math.PI);
        break;
      case "4": // Flip Y
        t.scale(1.0, -1.0);
        t.translate(0, -height);
        break;
      case "5": // - PI/2 and Flip X
        t.rotate(-Math.PI / 2);
        t.scale(-1.0, 1.0);
        break;
      case "6": // -PI/2 and -width
        t.translate(height, 0);
        t.rotate(Math.PI / 2);
        break;
      case "7": // PI/2 and Flip
        t.scale(-1.0, 1.0);
        t.translate(-height, 0);
        t.translate(0, width);
        t.rotate(3 * Math.PI / 2);
        break;
      case "8": // PI / 2
        t.translate(0, width);
        t.rotate(3 * Math.PI / 2);
        break;
      default:
        return null;
    }

    return t;
  }

  private void createMediumAndThumbnail(String mediaUUID, MediaFile file, String fileUUID, BufferedImage original,
      String format) throws IOException {
    log.debug("Création des versions medium et thumbnail pour le fichier {}", fileUUID);
    byte[] medium = reSize(original, 1200, 400, format);
    byte[] thumbnail = reSize(original, 200, 200, format);

    Path mediumFilePath = Paths.get(mediaUUID, fileUUID, "medium." + format);
    Path smallFilePath = Paths.get(mediaUUID, fileUUID, "thumbnail." + format);

    FileUtils.writeByteArrayToFile(mediaStorePath.resolve(mediumFilePath).toFile(), medium);
    FileUtils.writeByteArrayToFile(mediaStorePath.resolve(smallFilePath).toFile(), thumbnail);

    file.setMediumSizePicture(fullBaseUrl + String.join("/", mediaUUID, "doc", fileUUID, "medium." + format));
    file.setThumbnail(fullBaseUrl + String.join("/", mediaUUID, "doc", fileUUID, "thumbnail." + format));
  }

  /**
   * Génére une image de la première page d'un pdf.
   * 
   * @param pdfFile fichier pdf sous forme de tableau de byte
   * @return a BufferedImage
   * @throws IOException si une erreur technique survient lors du traitement du
   *                     fichier
   */
  private BufferedImage generatePreview(byte[] pdfFile) throws IOException {
    ByteArrayInputStream inputStream = new ByteArrayInputStream(pdfFile);
    PDDocument document = PDDocument.load(inputStream);
    PDFRenderer renderer = new PDFRenderer(document);
    return renderer.renderImage(0);
  }

  /**
   * Re size the given image with a new width. The height stay proportional to the
   * original image.
   * 
   * @param original original image
   * @param width    new width
   * @param format   image format (ex : png, jpg)
   * @return the newly created image as bytes
   * @throws IOException thrown if anything goes wrong while resizing
   */
  private byte[] reSize(BufferedImage original, int width, int height, String format) throws IOException {
    int type = original.getType();
    float heightRatio = (float) original.getHeight() / height;
    float widthRatio = (float) original.getWidth() / width;
    Image scaled;
    if (heightRatio > widthRatio) {
      scaled = original.getScaledInstance(-1, height, Image.SCALE_SMOOTH);
    } else {
      scaled = original.getScaledInstance(width, -1, Image.SCALE_SMOOTH);
    }
    // Pour les format tiff, le type est égal à zéro et n'est pas utilisable pour la
    // suite
    if (type == 0) {
      type = BufferedImage.TYPE_INT_RGB;
    }
    BufferedImage image;
    if (heightRatio > widthRatio) {
      image = new BufferedImage(scaled.getWidth(null), height, type);
    } else {
      image = new BufferedImage(width, scaled.getHeight(null), type);
    }
    Graphics2D g2d = image.createGraphics();
    if (heightRatio > widthRatio) {
      g2d.drawImage(scaled, 0, 0, scaled.getWidth(null), height, null);
    } else {
      g2d.drawImage(scaled, 0, 0, width, scaled.getHeight(null), null);
    }
    g2d.dispose();

    return getBytesFromImage(image, format);
  }

  private byte[] getBytesFromImage(BufferedImage image, String format) throws IOException {
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    boolean write = ImageIO.write(image, format, output);
    log.debug("Succès de la récupération des bytes de l'image {}", write);
    output.flush();
    byte[] byteArray = output.toByteArray();
    output.close();
    return byteArray;
  }

}
