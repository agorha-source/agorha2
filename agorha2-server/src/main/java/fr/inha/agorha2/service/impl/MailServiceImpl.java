package fr.inha.agorha2.service.impl;

import fr.inha.agorha2.service.MailService;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MailServiceImpl implements MailService {

  private static final String LOG_ERROR_NOT_SENT_MAIL = "Can't send email to ";

  private static final String LOG_INFO_SENT_MAIL = "Message envoyé à l'adresse:{}";

  private static final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);

  @Autowired
  private Session mailSession;

  @Value(value = "${mail.agorha2.from}")
  private String from;

  @Value(value = "${mail.noticeCorrection.recipient}")
  private String correctionProposalRecipient;

  @Value(value = "${mail.noticeCorrection.subjectTemplate}")
  private String noticeCorrectionSubjectTemplate;

  @Value(value = "${mail.noticeCorrection.bodyTemplate}")
  private String noticeCorrectionBodyTemplate;

  @Value(value = "${mail.accountCreation.subject}")
  private String accountCreationSubject;

  @Value(value = "${mail.accountCreation.bodyTemplate}")
  private String accountCreationBody;

  @Value(value = "${mail.resetPassword.subject}")
  private String resetPasswordSubject;

  @Value(value = "${mail.resetPassword.bodyTemplate}")
  private String resetPasswordBody;

  @Value(value = "${mail.updateAuthorizations.subject}")
  private String updateAuthorizationsSubject;

  @Value(value = "${mail.updateAuthorizations.bodyTemplate}")
  private String updateAuthorizationsBody;

  @Override
  public void sendCorrectionProposal(String noticeId, String text, String userMail) {
    sendMail(from, correctionProposalRecipient, noticeCorrectionSubjectTemplate.replace("{noticeId}", noticeId),
        noticeCorrectionBodyTemplate.replace("{userMail}", userMail).replace("{propositionText}", text));
  }

  @Override
  public void sendAccountCreation(String mail, String validationLink) {
    sendMail(from, mail, accountCreationSubject, accountCreationBody.replace("{validationLink}", validationLink));
  }

  @Override
  public void sendResetPassword(String mail, String resetPasswordLink) {
    sendMail(from, mail, resetPasswordSubject, resetPasswordBody.replace("{resetPasswordLink}", resetPasswordLink));
  }

  @Override
  public void sendUpdateAuthorizations(String mail, String role) {
    sendMail(from, mail, updateAuthorizationsSubject, updateAuthorizationsBody.replace("{newRole}", role));
  }

  private void sendMail(String from, String recipient, String subject, String text) {
    try {
      Message message = new MimeMessage(mailSession);
      message.setFrom(new InternetAddress(from));
      message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
      message.setSubject(subject);
      message.setText(text);
      Transport.send(message);
      logger.info(LOG_INFO_SENT_MAIL, recipient);
    } catch (MessagingException e) {
      logger.error(LOG_ERROR_NOT_SENT_MAIL + recipient, e);
    }
  }

}
