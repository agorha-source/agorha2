package fr.inha.agorha2.service;

import com.fasterxml.jackson.databind.JsonNode;
import fr.inha.agorha2.bean.ThesaurusConcept;
import fr.inha.agorha2.bean.ThesaurusSiecle;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.utils.DateUtils.PrefixType;
import java.util.List;
import java.util.Map;

public interface ThesaurusService extends PrefLabelService {

  /**
   * Récupération du concepatPath d'un concept. Le conceptPath est constitué des
   * prefLabels des parents et du concept lui-même, concaténer par des "/".
   * 
   * Exemple : pour le concept "4e quart du 19e siècle", son conceptPath sera
   * "/19e siècle/2e moitié du 19e siècle/4e quart du 19e siècle".
   * 
   * @param conceptId id du concept
   * @return le conceptPath
   * @throws ServiceException si un problème se produit lors de l'appel aux
   *                          services Ginco.
   */
  public String getConceptPath(JsonNode concept);

  public String getAltLabel(String conceptId);

  /**
   * Récupère le thésaurus siècle complet sous forme arborescente.
   * 
   * @return {@link ThesaurusSiecle}
   */
  public ThesaurusSiecle getThesaurusSiecle();

  public Map<String, PrefixType> getThesaurusPrefixe();

  public List<ThesaurusConcept> autocomplete(String terms, String thesaurusKey, String branchLabel, Integer limit);

  public List<ThesaurusConcept> list(String thesaurusKey);

  public Map<String, ThesaurusConcept> thesaurusByLabel(String thesaurusKey);

  public JsonNode getConcept(String conceptId);

  public String getGeoPoint(String conceptId);

  public String getThesaurusId(String thesaurusKey);

  public String getUrlFromUuid(String uuid);

}
