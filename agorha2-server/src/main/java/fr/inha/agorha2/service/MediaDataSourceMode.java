package fr.inha.agorha2.service;

public enum MediaDataSourceMode {
  BATCH,
  CONTRIBUTION,
  IMPORT;
}
