package fr.inha.agorha2.service.impl;

import fr.inha.agorha2.service.MetaModelService;
import fr.inha.elastic.model.NoticeType;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public class MetaModelServiceImpl implements MetaModelService {

  private static final Logger log = LoggerFactory.getLogger(MetaModelServiceImpl.class);

  @Value("${agorha2.metamodel.path}")
  Resource metaModelPath;

  @Override
  public String get(String version, NoticeType noticeType) {
    String jsonSchema = "";
    try {
      InputStream resourceAsStream = null;
      switch (noticeType) {
        case ARTWORK:
          resourceAsStream = metaModelPath.createRelative("schemaArtwork.json").getInputStream();
          break;
        case COLLECTION:
          resourceAsStream = metaModelPath.createRelative("schemaCollection.json").getInputStream();
          break;
        case EVENT:
          resourceAsStream = metaModelPath.createRelative("schemaEvent.json").getInputStream();
          break;
        case PERSON:
          resourceAsStream = metaModelPath.createRelative("schemaPerson.json").getInputStream();
          break;
        case REFERENCE:
          resourceAsStream = metaModelPath.createRelative("schemaReference.json").getInputStream();
          break;
        default:
          break;
      }
      jsonSchema = IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8);
    } catch (IOException e) {
      log.error("erreur", e);
    }
    return jsonSchema;
  }

}
