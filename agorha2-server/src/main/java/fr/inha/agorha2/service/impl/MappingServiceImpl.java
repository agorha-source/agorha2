package fr.inha.agorha2.service.impl;

import com.sword.utils.elasticsearch.config.EsCustomIndex;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;

import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.MappingService;
import fr.inha.agorha2.service.MetaModelService;
import fr.inha.agorha2.utils.JsonUtils;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.config.EsFilePath;
import fr.inha.elastic.config.EsIndexEnum;
import fr.inha.elastic.model.NoticeType;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.cluster.node.tasks.list.ListTasksRequest;
import org.elasticsearch.action.admin.cluster.node.tasks.list.ListTasksResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.tasks.TaskSubmissionResponse;
import org.elasticsearch.cluster.metadata.AliasMetadata;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.ReindexRequest;
import org.elasticsearch.tasks.TaskId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MappingServiceImpl implements MappingService {

  @Autowired
  EsClient esClient;

  @Autowired
  private MetaModelService metaModelService;

  @Override
  public String generateFromJson(String jsonSchema) {
    return JsonUtils.generateMappingFromJson(jsonSchema);
  }

  @Override
  public void updateMetamodel(NoticeType noticeType) throws ServiceException {
    if (noticeType == null) {
      return;
    }

    EsIndexEnum alias = getAlias(noticeType);
    EsIndexEnum newIndex = getNewIndex(noticeType);
    // Récupération du méta modèle au format json
    String jsonSchema = getJsonSchema(noticeType);
    if (newIndex == null || alias == null || StringUtils.isBlank(jsonSchema)) {
      throw new ServiceException(String.format("La notice %1$s ne peut pas être mise à jour", noticeType.name()));
    }

    // Récupération de l'ancien index
    EsCustomIndex oldIndex = getOldIndex(alias);
    if (oldIndex == null) {
      throw new ServiceException(
          String.format("L'index source %1$s pour la notice %2$s n'existe pas", alias.getName(), noticeType.name()));
    }

    // Construction du mapping
    String mapping = JsonUtils.generateMappingFromJson(jsonSchema);

    // Récupération des settings
    String settings = getDefaultSettings();

    // Création du nouvel index
    buildNewMetamodelIndex(noticeType, newIndex, mapping, settings);

    // Reindexation de l'ancien index vers le nouvel index
    reindex(newIndex, oldIndex);

    // Déplacement de l'alias
    switchAlias(noticeType, newIndex, oldIndex, alias);
  }

  /**
   * Récupère un alias à partir d'une notice.
   * 
   * @param noticeType
   * @return alias ou null si la notice n'est pas gérée
   */
  private EsIndexEnum getAlias(NoticeType noticeType) {
    switch (noticeType) {
      case ARTWORK:
        return EsIndexEnum.ARTWORK;
      case COLLECTION:
        return EsIndexEnum.COLLECTION;
      case EVENT:
        return EsIndexEnum.EVENT;
      case PERSON:
        return EsIndexEnum.PERSON;
      case REFERENCE:
        return EsIndexEnum.REFERENCE;
      default:
        return null;
    }
  }

  /**
   * Récupère un index à partir d'une notice.
   * 
   * @param noticeType
   * @return index ou null si la notice n'est pas gérée
   */
  private EsIndexEnum getNewIndex(NoticeType noticeType) {
    switch (noticeType) {
      case ARTWORK:
        return EsIndexEnum.CREATE_ARTWORK;
      case COLLECTION:
        return EsIndexEnum.CREATE_COLLECTION;
      case EVENT:
        return EsIndexEnum.CREATE_EVENT;
      case PERSON:
        return EsIndexEnum.CREATE_PERSON;
      case REFERENCE:
        return EsIndexEnum.CREATE_REFERENCE;
      default:
        return null;
    }
  }

  /**
   * Récupère un méta modèle à partir d'une notice.
   * 
   * @param noticeType
   * @return méta model
   * @throws ServiceException
   */
  private String getJsonSchema(NoticeType noticeType) throws ServiceException {
    String jsonSchema = metaModelService.get(null, noticeType);

    if (jsonSchema.isBlank()) {
      // Récupération du fichier méta modèle associé
      String jsonSchemaFilename = getJsonSchemaFilename(noticeType);
      if (StringUtils.isBlank(jsonSchemaFilename)) {
        throw new ServiceException(String.format("La notice %1$s n'a pas de fichier JSON associé", noticeType.name()));
      }

      // Lecture du fichier méta modèle
      InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("schema/" + jsonSchemaFilename);
      try {
        return IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8);
      } catch (IOException e) {
        throw new ServiceException(
            String.format("La schéma JSON de la notice %1$s n'est pas accessible", noticeType.name()), e);
      }
    }
    return jsonSchema;
  }

  /**
   * Récupère le nom du fichier de méta modèle à partir d'une notice.
   * 
   * @param noticeType
   * @return nom du fichier de méta modèle
   */
  private String getJsonSchemaFilename(NoticeType noticeType) {
    switch (noticeType) {
      case ARTWORK:
        return "schemaArtwork.json";
      case COLLECTION:
        return "schemaCollection.json";
      case EVENT:
        return "schemaEvent.json";
      case PERSON:
        return "schemaPerson.json";
      case REFERENCE:
        return "schemaReference.json";
      default:
        return null;
    }
  }

  /**
   * Récupère l'ancien index à partir de l'alias à migrer.
   * 
   * @param alias
   * @return ancien index ou null si aucun index ne correspond à l'alias fournit
   * @throws ServiceException
   */
  private EsCustomIndex getOldIndex(EsIndexEnum alias) throws ServiceException {
    // Récupération des alias définis du cluster
    Map<String, Collection<String>> indicesByAlias = null;
    try {
      indicesByAlias = esClient.getAdminService().getIndicesByAlias();
    } catch (EsRequestException e) {
      throw new ServiceException("Impossible de récupérer la liste des alias Elasticsearch", e);
    }

    String aliasName = alias.getName();
    // Récupération de l'index si UN SEUL alias correspond dans le cluster
    if (indicesByAlias.containsKey(aliasName) && indicesByAlias.get(aliasName).size() == 1) {
      Optional<String> oldIndexName = indicesByAlias.get(alias.getName()).stream().findFirst();
      if (oldIndexName.isPresent() && StringUtils.isNotBlank(oldIndexName.get())) {
        return new EsCustomIndex(oldIndexName.get());
      }
    }
    return null;
  }

  /**
   * Récupération des settings ES.
   * 
   * @return settings ES
   * @throws ServiceException
   */
  private String getDefaultSettings() throws ServiceException {
    // Fichier avec les paramètres pour les indexes
    try {
      InputStream settingsDefaultInputStream = getClass().getResourceAsStream(EsFilePath.SETTINGS.getFileName());
      return IOUtils.toString(settingsDefaultInputStream, StandardCharsets.UTF_8.name());
    } catch (IOException ex) {
      throw new ServiceException("Impossible de lire le fichier de paramétrage des propriétés de l'index Elasticsearch",
          ex);
    }
  }

  /**
   * Création du nouvel index.
   * 
   * @param noticeType
   * @param index
   * @param mapping
   * @param settings
   * @throws ServiceException
   */
  private void buildNewMetamodelIndex(NoticeType noticeType, EsIndexEnum index, String mapping, String settings)
      throws ServiceException {
    // Création de l'index
    try {
      esClient.getAdminService().createIndexWithSetting(index, settings);
    } catch (EsRequestException e) {
      throw new ServiceException(String.format("L'index de la notice %1$s n'a pas pu être créé", noticeType.name()), e);
    }

    // Affectation du mapping
    try {
      esClient.getAdminService().putMapping(index, mapping);
    } catch (EsRequestException e) {
      throw new ServiceException(String.format("Le mapping de la notice %1$s n'a pas pu être créé", noticeType.name()),
          e);
    }
  }

  /**
   * Exécution et validation de la réindexation de l'ancien index dans le nouvel
   * index.
   * 
   * @param newIndex
   * @param oldIndex
   * @throws ServiceException
   */
  private void reindex(EsIndexEnum newIndex, EsCustomIndex oldIndex) throws ServiceException {
    // Création de la requête de réindexation
    ReindexRequest reindexRequest = new ReindexRequest();
    reindexRequest.setSourceIndices(oldIndex.getName());
    reindexRequest.setDestIndex(newIndex.getName());
    reindexRequest.setRefresh(true);

    // Réindexation
    TaskSubmissionResponse response = null;
    try {
      response = esClient.getEsRestClient().submitReindexTask(reindexRequest, RequestOptions.DEFAULT);
    } catch (IOException e) {
      throw new ServiceException(
          String.format("La réindexation de %1$s dans %2$s a échoué", oldIndex.getName(), newIndex.getName()), e);
    }

    // Attente de la finalisation de la réindexation
    ListTasksRequest listTasksRequest = new ListTasksRequest();
    TaskId taskId = new TaskId(response.getTask());
    ListTasksResponse list = null;
    do {
      try {
        list = esClient.getEsRestClient().tasks().list(listTasksRequest, RequestOptions.DEFAULT);
      } catch (IOException e) {
        throw new ServiceException(String.format("La récupération de la tâche %1$s a échoué", taskId), e);
      }
      try {
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        // NOP
        Thread.currentThread().interrupt();
      }
    } while (list.getTasks().stream()
        .anyMatch(taskInfo -> taskInfo.getTaskId().equals(taskId) || taskInfo.getParentTaskId().equals(taskId)));

    // Validations
    // - comparaison nombre de documents avant et après
    try {
      long newDataCount = esClient.getServiceGeneral().count(newIndex, QueryBuilders.matchAllQuery());
      long oldDataCount = esClient.getServiceGeneral().count(oldIndex, QueryBuilders.matchAllQuery());

      if (oldDataCount != newDataCount) {
        throw new ServiceException(
            "Erreur de validation : Le nombre de données réindexées est différent du nombre de données originales");
      }
    } catch (EsRequestException e) {
      throw new ServiceException("La récupération du nombre de données de l'index a échoué", e);
    }
  }

  /**
   * Déplacement des alias depuis l'ancien index vers le nouvel index et
   * validation.
   * 
   * @param noticeType
   * @param newIndex
   * @param oldIndex
   * @param alias
   * @throws ServiceException
   */
  private void switchAlias(NoticeType noticeType, EsIndexEnum newIndex, EsCustomIndex oldIndex, EsIndexEnum alias)
      throws ServiceException {
    EsIndexEnum[] indicesToAdd = new EsIndexEnum[] { newIndex };
    EsCustomIndex[] indicesToRemove = new EsCustomIndex[] { oldIndex };

    try {
      // Déplacement des index
      // - spécifique à la réindexation
      esClient.getAdminService().modifyAlias(alias.getName(), indicesToRemove, indicesToAdd);
      // - content
      esClient.getAdminService().modifyAlias(EsIndexEnum.CONTENT.getName(), indicesToRemove, indicesToAdd);
      // - notice
      esClient.getAdminService().modifyAlias(EsIndexEnum.NOTICE.getName(), indicesToRemove, indicesToAdd);
    } catch (EsRequestException e) {
      throw new ServiceException(
          String.format("Les alias de la notice %1$s n'ont pas pu être mis à jour", noticeType.name()), e);
    }

    // Validations
    try {
      Map<String, Set<AliasMetadata>> aliasesByIndex = esClient.getAdminService().getAliasesByIndex();
      // - Les anciens alias sont supprimés
      if (aliasesByIndex.containsKey(oldIndex.getName()) && !aliasesByIndex.get(oldIndex.getName()).isEmpty()) {
        throw new ServiceException("Erreur de validation : Les anciens alias n'ont pas été correctement supprimés");
      }
      // - Les nouveaux alias sont créés
      if (!aliasesByIndex.containsKey(newIndex.getName()) || aliasesByIndex.get(newIndex.getName()).size() != 3) {
        throw new ServiceException("Erreur de validation : Les nouveaux alias n'ont pas été correctement créés");
      }
    } catch (EsRequestException e) {
      throw new ServiceException("La récupération des alias a échoué", e);
    }
  }

}
