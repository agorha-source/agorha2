package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.search.EsSearchParameters;
import fr.inha.agorha2.service.PrefLabelService;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.config.EsIndexEnum;
import java.io.IOException;
import java.util.Arrays;
import javax.ws.rs.InternalServerErrorException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EsPrefLabelService implements PrefLabelService {

  private ObjectMapper mapper = DefaultEsContext.instance().getObjectMapper();

  @Autowired
  private EsClient esClient;

  @Override
  public String getPrefLabel(String conceptId) {
    EsSearchParameters parameters = new EsSearchParameters();
    parameters.setIndexes(Arrays.asList(EsIndexEnum.NOTICE));
    parameters.setQuery(QueryBuilders.simpleQueryStringQuery("\"" + conceptId + "\"").field("content.*.ref"));
    parameters.setFetchSource(true);
    parameters.setSize(1);
    SearchResponse searchResponse;
    StringBuilder response = new StringBuilder();
    try {
      searchResponse = esClient.getServiceGeneral().search(parameters);

      for (SearchHit hits : searchResponse.getHits()) {
        JsonNode jsonNode = mapper.readTree(hits.getSourceAsString());
        for (JsonNode node : jsonNode.findParents("ref")) {
          if (node.at("/ref").asText().endsWith(conceptId)) {
            JsonNode thesaurusValue = node.at("/prefLabels/0/value");
            if (thesaurusValue.isMissingNode()) {
              response.append(node.at("/value").asText());
            } else {
              response.append(thesaurusValue.asText());
            }
            break;
          }
        }
      }
    } catch (EsRequestException | IOException e) {
      throw new InternalServerErrorException(e);
    }
    return response.toString();
  }
}
