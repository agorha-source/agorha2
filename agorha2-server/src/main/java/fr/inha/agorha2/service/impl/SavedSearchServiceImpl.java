package fr.inha.agorha2.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.intf.IServiceDedicated;

import fr.inha.agorha2.api.dto.sort.SavedSearchSortType;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.SavedSearchService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.elastic.model.SavedSearch;

@Service
public class SavedSearchServiceImpl implements SavedSearchService {

  private static final Integer SAVED_SEARCH_DEFAULT_PAGE = 1;
  private static final Integer SAVED_SEARCH_DEFAULT_PAGE_SIZE = 5;

  @Autowired
  private IServiceDedicated<SavedSearch> savedSearchService;

  @Autowired
  private UserInfoService userInfoService;

  @Autowired
  private MessageSource messageSource;

  @Override
  public SavedSearch get(String id) throws ServiceException {
    try {
      SavedSearch savedSearch = savedSearchService.get(id);
      if (savedSearch == null) {
        throw new NotFoundException("Recherche enregistrée inconnue");
      }
      return savedSearch;
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de la récupération de la recherche enregistrée " + id, e);
    }
  }

  @Override
  public SavedSearch create(String name, List<String> resume, String url) throws ServiceException {
    String userId = userInfoService.getPrincipal().getUser().getInternal().getUuid();
    try {
      List<SavedSearch> list = savedSearchService.list(QueryBuilders.boolQuery()
          .must(QueryBuilders.matchQuery("userId", userId)).must(QueryBuilders.matchQuery("name", name)), 1);
      if (!list.isEmpty()) {
        throw new BadRequestException(
            messageSource.getMessage("savedSearch.name.already.exists", new Object[] {}, null));
      }

      SavedSearch savedSearch = new SavedSearch();
      savedSearch.setCreatedDate(LocalDateTime.now());
      savedSearch.setName(name);
      savedSearch.setUrl(url);
      savedSearch.setUserId(userId);
      savedSearch.getResume().addAll(resume);

      return savedSearchService.createOrUpdateAndGet(savedSearch, true);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de l'enregistrement de la recherche " + name + ", url : " + url, e);
    }
  }

  @Override
  public void delete(String id) throws ServiceException {
    SavedSearch savedSearch = get(id);
    if (!userInfoService.getPrincipal().getUser().getInternal().getUuid().equals(savedSearch.getUserId())) {
      throw new NotAuthorizedException(messageSource.getMessage("user.insufficient.rights", new Object[] {}, null));
    }
    try {
      savedSearchService.delete(id, RefreshPolicy.IMMEDIATE);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors de la suppression de la recherche " + id, e);
    }
  }

  @Override
  public List<SavedSearch> list(String terms, Integer page, Integer pageSize, SortOrder sort,
      SavedSearchSortType sortType) throws ServiceException {

    if (page == null) {
      page = SAVED_SEARCH_DEFAULT_PAGE;
    }
    if (pageSize == null) {
      pageSize = SAVED_SEARCH_DEFAULT_PAGE_SIZE;
    }
    if (page < 1 || pageSize > 10000) {
      throw new BadRequestException(messageSource.getMessage("search.paging.error", new Object[] {}, null));
    }
    String userUuid = userInfoService.getPrincipal().getUser().getInternal().getUuid();
    BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
    MatchQueryBuilder matchQuery = QueryBuilders.matchQuery("userId", userUuid);
    boolQuery.must(matchQuery);

    if (StringUtils.isNotEmpty(terms)) {
      boolQuery.must(QueryBuilders.simpleQueryStringQuery(terms).field("name.autocomplete"));
    }

    if (sortType == null) {
      sortType = SavedSearchSortType.CREATION_DATE;
    }
    if (sort == null) {
      sort = SortOrder.DESC;
    }
    List<SortBuilder<?>> sortBuilders = new ArrayList<>();
    sortBuilders.add(EsQueryService.createFieldSortBuilder(sort, sortType.getField()));
    try {
      return savedSearchService.list(boolQuery, pageSize, page - 1, sortBuilders);
    } catch (EsRequestException e) {
      throw new ServiceException("Erreur lors du listing des recherche enregistrées pour l'utilisateur" + userUuid, e);
    }
  }

}
