package fr.inha.agorha2.service;

import fr.inha.agorha2.api.dto.ImageFormat;
import fr.inha.agorha2.api.dto.MediaItem;
import fr.inha.agorha2.api.dto.MediaRetrieveMode;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.model.Media;
import java.util.List;
import java.util.Map;

public interface MediaService {

  /**
   * Create a media from a draft, and index it into ES. The draft is filled with
   * the content in the files inpustreams.
   * 
   * @param files list of pair filename/inputstream
   * @param draft the media draft to fill
   * @return Custom attributes
   * @throws ServiceException thrown if any error occurs during the media creation
   */
  public Map<String, Object> createMedia(List<MediaItem> mediaItems, Media draft) throws ServiceException;

  public Map<String, Object> createMedia(List<MediaItem> mediaItems, Media draft, MediaDataSourceMode dataSourceMode)
      throws ServiceException;

  /**
   * Update a media from a draft, and index it into ES. The draft is filled with
   * the content in the files inpustreams.
   * 
   * @param files list of pair filename/inputstream
   * @param draft the media draft to fill
   * @return Custom attributes
   * @throws ServiceException thrown if any error occurs during the media update
   */
  public Map<String, String> updateMedia(String uuid, List<MediaItem> mediaItems, Media draft) throws ServiceException;

  public Map<String, String> updateMedia(String uuid, List<MediaItem> mediaItems, Media draft,
      MediaDataSourceMode dataSourceMode) throws ServiceException;

  /**
   * Get a file from a mediaId and a fileId.
   * 
   * @param idMedia     parent mediaId of the file
   * @param fileId      fileId
   * @param imageFormat format
   * @return Pair of fileName and its data as inputstream
   * @throws ServiceException thrown if any error occurs during the get of the
   *                          file
   */
  public Map<String, String> getFile(String idMedia, String idFile, ImageFormat imageFormat) throws ServiceException;

  /**
   * Get a media from his id.
   * 
   * @param mediaId id du media
   * @return founded media
   * @throws ServiceException if anything goes wrong during ES interrogation
   */
  public Media get(String mediaId) throws ServiceException;

  /**
   * Get all the media from given ids.
   * 
   * @param mediaIds list of media ids
   * @param mode     retrieve mode
   * @return
   */
  public List<Media> getMedia(List<String> mediaIds, MediaRetrieveMode mode) throws ServiceException;

  public long delete(List<String> uuids) throws ServiceException;

}
