package fr.inha.agorha2.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.search.EsSearchParameters;
import fr.inha.agorha2.api.dto.Aggregation;
import fr.inha.agorha2.api.dto.request.FindDatabaseRequest;
import fr.inha.agorha2.api.dto.response.DatabaseListResponse;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.DatabaseService;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.config.EsIndexEnum;
import fr.inha.elastic.model.Database;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

@Service
public class DatabaseServiceImpl implements DatabaseService {

  private static final String CONTENT_TITLE_FIELD = "content.title";
  private static final String INTERNAL_RELATED_NOTICE_TYPE_FIELD = "internal.relatedNoticeType";

  @Autowired
  private EsClient esClient;

  @Autowired
  private EsQueryService esQueryService;

  @Autowired
  private ObjectMapper mapper;

  @Autowired
  private MessageSource messageSource;

  @Override
  public Database create(Database database) throws ServiceException {
    try {
      return esClient.getServiceDatabase().createOrUpdateAndGet(database, true);
    } catch (EsRequestException e) {
      throw new ServiceException(e);
    }
  }

  @Override
  public DatabaseListResponse list(List<String> noticeTypes) throws ServiceException {
    try {
      BoolQueryBuilder listQuery = QueryBuilders.boolQuery().must(QueryBuilders.matchAllQuery());
      if (CollectionUtils.isNotEmpty(noticeTypes)) {
        listQuery = QueryBuilders.boolQuery();
        listQuery.minimumShouldMatch(1);
        for (String noticeType : noticeTypes) {
          if ("UNKNOWN".equals(noticeType)) {
            // Cas de la valeur "Non renseignée" dans le facette
            listQuery.should(
                QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery(INTERNAL_RELATED_NOTICE_TYPE_FIELD)));
          } else {
            listQuery.should(QueryBuilders.termQuery(INTERNAL_RELATED_NOTICE_TYPE_FIELD, noticeType));
          }
        }
      }
      esQueryService.filterBddStatus(listQuery);

      EsSearchParameters searchParameters = new EsSearchParameters();
      searchParameters.setAggregations(new ArrayList<>());
      searchParameters.setIndexes(Arrays.asList(EsIndexEnum.DATABASE));
      searchParameters.setSize(0);
      BoolQueryBuilder facetQuery = QueryBuilders.boolQuery().must(QueryBuilders.matchAllQuery());
      esQueryService.filterBddStatus(facetQuery);
      searchParameters.setQuery(facetQuery);

      Aggregation aggregation = new Aggregation();
      aggregation.setField(INTERNAL_RELATED_NOTICE_TYPE_FIELD);
      aggregation.getFilters().addAll(noticeTypes);
      ImmutablePair<String, Aggregation> agg = ImmutablePair.of("Types de bases de données", aggregation);
      // Aggregation sur les termes
      searchParameters.getAggregations().add(esQueryService.constructAggregation(agg));

      // Aggregations pour les documents sans le champ
      searchParameters.getAggregations().add(esQueryService.constructMissingAggregation(agg));
      SearchResponse searchResponse = esClient.getServiceGeneral().search(searchParameters);

      DatabaseListResponse response = new DatabaseListResponse();
      response.getFacets().add(esQueryService.buildFacetResults(searchResponse.getAggregations(), agg));
      response.getDatabases().addAll(esClient.getServiceDatabase().list(listQuery, 200,
          Arrays.asList(SortBuilders.fieldSort(CONTENT_TITLE_FIELD).order(SortOrder.ASC))));
      return response;
    } catch (EsRequestException e) {
      throw new ServiceException(e);
    }
  }

  @Override
  public Database get(Integer id) throws ServiceException {
    try {
      List<Database> databases = esClient.getServiceDatabase().list(QueryBuilders.matchQuery("internal.uniqueKey", id),
          1);
      if (databases.size() != 1) {
        throw new NotFoundException(messageSource.getMessage("database.unknown", new Object[] { id }, null));
      }
      return databases.get(0);
    } catch (EsRequestException e) {
      throw new InternalServerErrorException("Erreur lors de l'intérogation des bases de données" + id, e);
    }
  }

  @Override
  public Database find(FindDatabaseRequest request) throws ServiceException {
    try {
      List<Database> databases = esClient.getServiceDatabase()
          .list(QueryBuilders.matchQuery(CONTENT_TITLE_FIELD, request.getTitle()), 1);
      if (databases.size() != 1) {
        throw new NotFoundException(
            messageSource.getMessage("database.unknown", new Object[] { request.getTitle() }, null));
      }
      return databases.get(0);
    } catch (EsRequestException e) {
      throw new InternalServerErrorException("Erreur lors de l'intérogation des bases de données " + request.getTitle(),
          e);
    }
  }

  @Override
  public List<Database> autocomplete(String terms) throws ServiceException {
    try {
      EsSearchParameters parameters = new EsSearchParameters();
      parameters.setIndexes(Arrays.asList(EsIndexEnum.DATABASE));
      parameters.setSourceInclude(Arrays.asList("internal.uuid", CONTENT_TITLE_FIELD));
      parameters.setQuery(QueryBuilders.simpleQueryStringQuery(terms).field("content.title.autocomplete"));
      parameters.setSize(100);
      SearchResponse response = esClient.getServiceGeneral().search(parameters);

      List<Database> databases = new ArrayList<>();
      for (SearchHit hit : response.getHits()) {
        databases.add(mapper.readValue(hit.getSourceAsString(), Database.class));
      }
      return databases;
    } catch (EsRequestException | IOException e) {
      throw new ServiceException(e);
    }
  }

}
