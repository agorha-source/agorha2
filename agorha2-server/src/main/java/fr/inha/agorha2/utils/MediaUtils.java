package fr.inha.agorha2.utils;

import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.MediaFile;
import fr.inha.elastic.model.MediaInternal;
import java.util.stream.Collectors;

public class MediaUtils {

  private MediaUtils() {

  }

  public static Media copy(Media media) {

    Media copy = new Media();
    copy.setCaption(media.getCaption());
    copy.setComment(media.getComment());
    copy.setCommunicationRight(media.getCommunicationRight());
    copy.setCredit(media.getCredit());
    copy.setFileProvenance(media.getFileProvenance());
    copy.setInternal(copy(media.getInternal()));
    copy.setLicence(media.getLicence());
    copy.setReproductionRight(media.getReproductionRight());
    copy.getFiles().addAll(media.getFiles().stream().map(MediaUtils::copy).collect(Collectors.toList()));

    return copy;
  }

  private static MediaInternal copy(MediaInternal internal) {
    MediaInternal copy = new MediaInternal();
    copy.setCreatedBy(internal.getCreatedBy());
    copy.setCreatedDate(internal.getCreatedDate());
    copy.setLinkedWithNotices(internal.getLinkedWithNotices());
    copy.setUniqueKey(internal.getUniqueKey());
    copy.setUpdatedBy(internal.getUpdatedBy());
    copy.setUpdatedDate(internal.getUpdatedDate());
    copy.setUuid(internal.getUuid());
    return copy;
  }

  private static MediaFile copy(MediaFile mediaFile) {
    MediaFile copy = new MediaFile();
    copy.setAlt(mediaFile.getAlt());
    copy.setComputedFacetMediaType(mediaFile.getComputedFacetMediaType());
    copy.setContentType(mediaFile.getContentType());
    copy.setEmbeddedContent(mediaFile.getEmbeddedContent());
    copy.setFileSize(mediaFile.getFileSize());
    copy.setMediaType(mediaFile.getMediaType());
    copy.setMediumSizePicture(mediaFile.getMediumSizePicture());
    copy.setOcr(mediaFile.getOcr());
    copy.setOriginFile(mediaFile.getOriginFile());
    copy.setThumbnail(mediaFile.getThumbnail());
    copy.setUniqueKey(mediaFile.getUniqueKey());
    copy.setUuid(mediaFile.getUuid());
    return copy;
  }
}
