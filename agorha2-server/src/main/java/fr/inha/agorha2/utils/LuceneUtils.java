package fr.inha.agorha2.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

public class LuceneUtils {

  private static final String FIELD_NAME = "dummy";

  public static List<String> analyze(String text, Analyzer analyzer) throws IOException {
    List<String> result = new ArrayList<>();
    try (TokenStream tokenStream = analyzer.tokenStream(FIELD_NAME, text)) {
      CharTermAttribute attr = tokenStream.addAttribute(CharTermAttribute.class);
      tokenStream.reset();
      while (tokenStream.incrementToken()) {
        result.add(attr.toString());
      }
      return result;
    }
  }
}
