package fr.inha.agorha2.utils;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;

/**
 * Processor that generates the Deep Zoom version of an image.
 * <p>
 * Heavily based on the DeepJZoom project (https://code.google.com/p/deepjzoom).
 * <p>
 * This implementation is thread-safe.
 * 
 * @author sword
 * @author Glenn Lawrence (https://code.google.com/p/deepjzoom)
 */
public class DeepZoomConverter {

  private static final String DESCRIPTOR_HEADER = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
  private static final String SCHEMA_URI = "http://schemas.microsoft.com/deepzoom/2009";
  private static final String IMAGE_XML_NAME = "image.xml";
  private static final int TILESIZE = 256;
  private static final int TILEOVERLAP = 1;

  private DeepZoomConverter() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Process the given image file, producing its Deep Zoom output files in a
   * subdirectory of the given output directory.
   * 
   * @param image     the original image
   * @param outputDir the output directory
   * 
   * @throws MediaVersionTransformationException Thrown if the media version
   *                                             transformation has failed due to
   *                                             an invalid context
   * @throws IOException                         Thrown if an I/O error occurred
   * @throws MediaReadingException               Thrown if the input media could
   *                                             not be read
   * @throws MediaAnalyzerException              Thrown if the image loader has
   *                                             failed
   * @throws MediaProcessorException             Thrown if the processor has
   *                                             failed due to an internal error
   */
  public static void processImageFile(BufferedImage image, File outputDir) throws IOException {

    final int originalWidth = image.getWidth();
    final int originalHeight = image.getHeight();

    final double maxDim = Math.max(originalWidth, originalHeight);
    final int nLevels = (int) Math.ceil(Math.log(maxDim) / Math.log(2));

    final File descriptor = new File(outputDir, IMAGE_XML_NAME);
    FileUtils.forceMkdirParent(descriptor);
    double width = originalWidth;
    double height = originalHeight;
    for (int level = nLevels; level >= 0; level--) {
      processLevel(image, outputDir, width, height, level);

      // Scale down image for the next level.
      width = Math.ceil(width / 2);
      height = Math.ceil(height / 2);

      if (width > 10 && height > 10) {
        // resize in stages to improve quality
        image = resizeImage(image, (int) (width * 1.66), (int) (height * 1.66));
        image = resizeImage(image, (int) (width * 1.33), (int) (height * 1.33));
      }
      image = resizeImage(image, (int) width, (int) height);
    }
    saveImageDescriptor(originalWidth, originalHeight, descriptor);
  }

  /**
   * Processes the current image level.
   * 
   * @param image            Current image
   * @param imgagesDirectory Output image levels directory
   * @param width            Current image width
   * @param height           Current image height
   * @param level            Current level
   * @throws IOException             Thrown if an I/O error occurred while saving
   *                                 the level's tiles
   * @throws MediaProcessorException Thrown if the processor has failed to create
   *                                 the new images due to an internal error
   */
  private static void processLevel(BufferedImage image, final File imgagesDirectory, double width, double height,
      int level) throws IOException {
    final int cols = (int) Math.ceil(width / TILESIZE);
    final int rows = (int) Math.ceil(height / TILESIZE);
    final File levelDirectory = new File(imgagesDirectory, Integer.toString(level));

    levelDirectory.mkdirs();

    for (int col = 0; col < cols; col++) {
      for (int row = 0; row < rows; row++) {
        final BufferedImage tile = getTile(image, row, col);
        final File tileFile = new File(levelDirectory, String.format("%d_%d.%s", col, row, "png"));
        ImageIO.write(tile, "png", tileFile);
      }
    }
  }

  /**
   * Gets an image containing the tile at the given row and column for the given
   * image.
   * 
   * @param image the input image from whihc the tile is taken
   * @param row   the tile's row (i.e. y) index
   * @param col   the tile's column (i.e. x) index
   * @return Requested tile image
   */
  private static BufferedImage getTile(BufferedImage image, int row, int col) {
    final int x = col * TILESIZE - (col == 0 ? 0 : TILEOVERLAP);
    final int y = row * TILESIZE - (row == 0 ? 0 : TILEOVERLAP);
    int w = TILESIZE + (col == 0 ? 1 : 2) * TILEOVERLAP;
    int h = TILESIZE + (row == 0 ? 1 : 2) * TILEOVERLAP;

    if (x + w > image.getWidth()) {
      w = image.getWidth() - x;
    }
    if (y + h > image.getHeight()) {
      h = image.getHeight() - y;
    }

    BufferedImage result = new BufferedImage(w, h, image.getType());
    Graphics2D g = result.createGraphics();
    try {
      g.drawImage(image, 0, 0, w, h, x, y, x + w, y + h, null);
    } finally {
      g.dispose();
    }

    return result;
  }

  /**
   * Returns resized image NB - useful reference on high quality image resizing
   * can be found here:
   * http://today.java.net/pub/a/today/2007/04/03/perils-of-image-
   * getscaledinstance.html
   * 
   * @param width      the required width
   * @param height     the required height
   * @param inputImage the image to be resized
   * @return The rescaled image
   */
  private static BufferedImage resizeImage(BufferedImage inputImage, int width, int height) {
    final BufferedImage resizedImage = new BufferedImage(width, height, inputImage.getType());

    Graphics2D graphics = resizedImage.createGraphics();
    try {
      graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
      graphics.drawImage(inputImage, 0, 0, width, height, 0, 0, inputImage.getWidth(), inputImage.getHeight(), null);
    } finally {
      graphics.dispose();
    }

    return resizedImage;
  }

  /**
   * Write image descriptor XML file.
   * 
   * @param width      image width
   * @param height     image height
   * @param outputFile the file to which it is saved
   * @throws IOException Thrown if writing the descriptor has failed
   */
  private static void saveImageDescriptor(int width, int height, File outputFile) throws IOException {
    final StringBuilder builder = new StringBuilder();

    builder.append(DESCRIPTOR_HEADER);
    builder.append("<Image TileSize=\"" + TILESIZE + "\" Overlap=\"" + TILEOVERLAP + "\" Format=\"" + "png"
        + "\" ServerFormat=\"Default\" xmnls=\"" + SCHEMA_URI + "\">");
    builder.append("<Size Width=\"" + width + "\" Height=\"" + height + "\" />");
    builder.append("</Image>");

    FileUtils.write(outputFile, builder.toString(), StandardCharsets.UTF_8);
  }

}
