package fr.inha.agorha2.utils;

import fr.inha.agorha2.exception.ServiceException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class FileUtils {

  private FileUtils() {

  }

  public static void moveFile(File file, String path, String dir) throws ServiceException {
    moveFile(file, path, dir, file.getName());
  }

  public static void moveFile(File file, String path, String dir, String filename) throws ServiceException {
    try {
      org.apache.commons.io.FileUtils.moveFile(file, Path.of(path, dir, filename).toFile());
    } catch (IOException e) {
      throw new ServiceException(e);
    }
  }
}
