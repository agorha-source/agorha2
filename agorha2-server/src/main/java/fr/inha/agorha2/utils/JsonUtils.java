package fr.inha.agorha2.utils;

import static java.util.Map.entry;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;

import fr.inha.agorha2.json.JsonDateTimeValidator;
import fr.inha.agorha2.json.JsonDateValidator;

public class JsonUtils {

  private static final String JSONLD_TYPE_ATTRIBUTE = "@type";
  private static final String PATH_DELIMITER = "/";
  private static final String NO_PATH_TO_THIS_FIELD = "/_no_path_to_this_field_";
  private static final String CONTENT_ATTRIBUTE = "content";
  private static final String CONCEPT_PATH_ATTRIBUTE = "conceptPath";
  private static final String FORMAT_ATTRIBUTE = "format";
  private static final String ITEMS_ATTRIBUTE = "items";
  private static final String PROPERTIES_ATTRIBUTE = "properties";
  private static final String AUTO_COMPLETION_ATTRIBUTE = "autoCompletion";
  private static final String ADV_AUTO_COMPLETION_ATTRIBUTE = "advAutoCompletion";
  private static final String ES_ANALYZER_PROPERTIE_NAME = "analyzer";
  private static final String ES_FIELDS_PROPERTIE_NAME = "fields";
  private static final String ES_TYPE_PROPERTIE_NAME = "type";
  private static final String ES_FORMAT_PROPERTIE_NAME = "format";
  private static final String KEYWORD_FIELD_NAME = "keyword";
  private static final String EXACT_FIELD_NAME = "exact";
  private static final String ES_KEYWORD_TYPE = "keyword";
  private static final String FACET_TYPE_HIERARCHICAL = "hierarchical";
  private static final String FACET_TYPE_SIMPLE = "simple";
  private static final List<String> KEYS_TO_IGNORE = new ArrayList<>(
      Arrays.asList("displayName", "_id", "autoCompleteInfoThesaurus"));
  private static final String DATE_ERA_FORMAT = "date-era";
  private static final List<String> DATE_FORMAT = new ArrayList<>(
      Arrays.asList("date-time", "date", "time", DATE_ERA_FORMAT));
  private static final Map<String, String> JSON_TYPE_TO_ESTYPE = Map.ofEntries(entry("string", "text"),
      entry("null", "text"), entry("integer", "integer"), entry("object", ""), entry("array", ""),
      entry("number", "float"));
  private static final List<String> JSON_SCHEMA_PRIMITIVE_TYPE = new ArrayList<>(
      Arrays.asList("string", "number", "integer"));

  private static final Pattern CSV_MULTIVALUE_FIELDNAME_PATTERN = Pattern.compile("[a-zA-Z]+([0-9]+)");

  private static ObjectMapper mapper = DefaultEsContext.instance().getObjectMapper();

  private JsonUtils() {

  }

  public static List<String> getKeysToIgnore() {
    return Collections.unmodifiableList(KEYS_TO_IGNORE);
  }

  public static List<String> getDateFormat() {
    return Collections.unmodifiableList(DATE_FORMAT);
  }

  public static String generateMappingFromJson(String jsonSchemaAsString) {
    JSONObject jsonSchema = new JSONObject(jsonSchemaAsString);
    JSONObject jsonObject = jsonSchema.getJSONObject(PROPERTIES_ATTRIBUTE);

    JSONObject processChilds = processChildsForMapping(jsonObject, jsonSchema, false, false);
    JSONObject result = new JSONObject();
    result.put(PROPERTIES_ATTRIBUTE, processChilds);
    return result.toString(2);
  }

  /**
   * Validate the given json against the provided schema<br>
   * Validation is done with {@link Schema} implementation.
   * 
   * @param schema schema to validate against
   * @param json   json to validate
   */
  public static void validateJson(String schema, String json) {
    JSONObject jsonSchemaObject = new JSONObject(schema);
    JSONObject jsonSubjectObject = new JSONObject(json);

    SchemaLoader schemaLoader = SchemaLoader.builder().schemaJson(jsonSchemaObject)
        .addFormatValidator(new JsonDateValidator()).addFormatValidator(new JsonDateTimeValidator())
        .enableOverrideOfBuiltInFormatValidators().build();
    Schema jsonSchema = schemaLoader.load().build();
    jsonSchema.validate(jsonSubjectObject);
  }

  public static Map<String, List<String>> getFieldPerBlock(String jsonSchemaAsString) throws IOException {
    Map<String, List<String>> blocks = new LinkedHashMap<>();

    JsonNode tree = mapper.readTree(jsonSchemaAsString);
    JsonNode content = tree.at("/properties/content/properties");
    content.fieldNames().forEachRemaining(fieldName -> {
      if (!fieldName.toLowerCase().contains("computed")) {
        JsonNode tab = content.get(fieldName);
        JsonNode properties = tab.get(PROPERTIES_ATTRIBUTE);
        if (properties != null) {
          properties.fieldNames().forEachRemaining(blockName -> {
            JsonNode block = properties.get(blockName);
            blocks.put(fieldName + "." + blockName, getFieldsFromObject(block, ""));
            if (blockName.equals("associatedMedia")) {
              // Pour les media, on n'exporte pas l'ocr qui contient le fulltext des documents
              // associés, ni les metadata
              blocks.get(fieldName + "." + blockName).remove("ocr");
              blocks.get(fieldName + "." + blockName).remove("internalMetadata");
            }
          });
        }
      }
    });
    return blocks;

  }

  public static void addValueToNodeFromPath(String path, ObjectNode noticeNode, String value) {
    addValueToNodeFromPath(path, noticeNode, value, null);

  }

  public static void addValueToNodeFromPath(String path, ObjectNode objectNode, String value, String valueType) {
    JsonNode currentNode = objectNode;
    String[] fieldNames = path.split("\\.");

    // index du block de données courant (-1 si non multivalué)
    int indexBlock = -1;

    StringBuilder currentPath = new StringBuilder();
    for (int i = 0; i < fieldNames.length; i++) {
      int currentIndex = -1;
      String fieldName = fieldNames[i];
      Matcher matcher = CSV_MULTIVALUE_FIELDNAME_PATTERN.matcher(fieldName);

      // Dans le cas au un block de données est multivalue (fieldName fini par un
      // nombre)
      if (matcher.matches()) {
        indexBlock = Integer.parseInt(matcher.group(1));
        currentIndex = indexBlock;
        fieldName = fieldName.replace(currentIndex + "", "");
      }

      if (StringUtils.isNumeric(fieldName)) {
        int indexValue = Integer.parseInt(fieldName);
        // Object multivalué
        if (currentNode.isArray()) {
          if (currentNode.get(indexValue) != null) {
            currentNode = currentNode.get(indexValue);
          } else {
            currentNode = ((ArrayNode) currentNode).addObject();
          }
        } else {
          String currentPathAsString = currentPath.toString();
          currentNode = ((ObjectNode) objectNode
              .at(currentPathAsString.replace(currentPath.substring(currentPathAsString.lastIndexOf('/')), "")))
                  .putArray(fieldNames[i - 1]);
        }
      } else if (currentNode.get(fieldName) != null) {
        // Le champ existe déjà dans l'object courant
        currentNode = currentNode.get(fieldName);
      } else {
        // On construit le nouveau champ
        if (currentNode.isArray()) {
          // Object multivalué
          if (currentNode.get(indexBlock - 1) != null) {
            // L'instance du block multivalué existe déjà
            if (currentNode.get(indexBlock - 1).get(fieldName) != null) {
              // Le champ existe déjà dans l'instance
              currentNode = currentNode.get(indexBlock - 1).get(fieldName);
            } else {
              // Le champ n'existe pas, création du champ
              currentNode = addValueToNode(value, valueType, ((ObjectNode) currentNode.get(indexBlock - 1)), fieldName,
                  i == fieldNames.length - 1);
            }
          } else {
            // L'instance du block n'existe pas, on la créée
            currentNode = addValueToNode(value, valueType, ((ArrayNode) currentNode).addObject(), fieldName,
                i == fieldNames.length - 1);
          }
        } else if (currentNode.isObject()) {
          // Object
          if (currentIndex > 0) {
            currentNode = ((ObjectNode) currentNode).putArray(fieldName);
          } else {
            currentNode = addValueToNode(value, valueType, ((ObjectNode) currentNode), fieldName,
                i == fieldNames.length - 1);
          }
        }
      }
      currentPath.append("/").append(fieldName);
      if (currentIndex > 0) {
        currentPath.append("/").append(currentIndex - 1);
      }
    }
  }

  private static JsonNode addValueToNode(String value, String valueType, ObjectNode objectNode, String fieldName,
      boolean leafValue) {
    JsonNode currentNode = null;
    if (leafValue) {
      if ("integer".equals(valueType)) {
        currentNode = objectNode.put(fieldName, Integer.parseInt(value));
      } else if ("number".equals(valueType)) {
        currentNode = objectNode.put(fieldName, Float.parseFloat(value));
      } else {
        currentNode = objectNode.put(fieldName, value);
      }
    } else {
      currentNode = objectNode.putObject(fieldName);
    }
    return currentNode;
  }

  public static List<String> getAllSubPath(String path, JsonNode source, String currentPath) {
    List<String> resultSubPaths = new ArrayList<>();
    String[] splitPath = path.split("\\.");
    String subPath = splitPath[0];
    JsonNode node = source.at("/" + subPath);
    if (splitPath.length == 1 && !node.isMissingNode()) {
      resultSubPaths.add(currentPath + "/" + subPath);
    } else if (node.isArray()) {
      for (int i = 0; i < node.size(); i++) {
        resultSubPaths.addAll(
            getAllSubPath(path.replaceFirst(subPath + "\\.", ""), node.get(i), currentPath + "/" + subPath + "/" + i));
      }
    } else if (node.isObject()) {
      resultSubPaths.addAll(
          getAllSubPath(path.replaceFirst(subPath + "\\.", ""), source.get(subPath), currentPath + "/" + subPath));
    }
    return resultSubPaths;
  }

  public static void removeEmptyNodes(JsonNode noticeNode) {
    Iterator<JsonNode> iterator = noticeNode.iterator();
    while (iterator.hasNext()) {
      removeEmptyNodes(iterator.next(), iterator);
    }
  }

  private static void removeEmptyNodes(JsonNode currentNode, Iterator<JsonNode> iterator) {
    if (currentNode.isObject() || currentNode.isArray()) {
      Iterator<JsonNode> subIterator = currentNode.iterator();
      while (subIterator.hasNext()) {
        removeEmptyNodes(subIterator.next(), subIterator);
      }
      if (currentNode.size() == 0) {
        iterator.remove();
      }
    }
  }

  public static String getPathToField(JsonNode notice, String pathToMultiBlock, String fieldRelativePath,
      String endFieldPath) {
    int i = 0;
    String fieldPath = NO_PATH_TO_THIS_FIELD;
    JsonNode multiBlockNode = notice.at(pathToMultiBlock);
    for (JsonNode jsonNode : multiBlockNode) {
      if (!jsonNode.at(fieldRelativePath).isMissingNode()) {
        fieldPath = pathToMultiBlock + PATH_DELIMITER + i + fieldRelativePath + endFieldPath;
        break;
      }
      i++;
    }
    return fieldPath;
  }

  public static String getFieldLabelFromSchema(String schemaAsText, String field) throws IOException {
    JsonNode nodeInSchema = getAssociatedFieldFromSchema(schemaAsText, field);
    if (nodeInSchema == null) {
      return null;
    }
    return nodeInSchema.at("/displayName/fre").asText();
  }

  public static JsonNode getAssociatedFieldFromSchema(JsonNode schemaNode, String field) {
    if (StringUtils.isBlank(field)) {
      return schemaNode;
    }
    JsonNode currentNode = schemaNode.get(PROPERTIES_ATTRIBUTE);
    String[] pathParts = field.split("\\.", -1);
    int i = 0;
    for (String pathPart : pathParts) {
      i++;
      currentNode = currentNode.at("/" + pathPart);
      if (currentNode.isMissingNode()) {
        return null;
      }

      if (i < pathParts.length) {
        if (!currentNode.at("/properties").isMissingNode()) {
          currentNode = currentNode.get(PROPERTIES_ATTRIBUTE);
        } else if (!currentNode.at("/items").isMissingNode()) {
          currentNode = currentNode.at("/items/properties");
        }
      }
    }
    return currentNode;
  }

  public static JsonNode getAssociatedFieldFromSchema(String schemaAsText, String field) throws IOException {
    JsonNode schemaNode = mapper.readTree(schemaAsText);
    return getAssociatedFieldFromSchema(schemaNode, field);
  }

  /**
   * Get the field that are used for autocomplete from the given jsonSchema.
   * 
   * @param jsonSchema jsonSchema to extract from
   * @param request    the text to search
   * @return
   */
  public static Map<String, Float> getAutocompleteFields(String jsonSchema) {
    Map<String, Float> fields = new HashMap<>();
    JSONObject jsonSchemaObject = new JSONObject(new JSONTokener(jsonSchema));
    JSONObject digestObject = jsonSchemaObject.getJSONObject(PROPERTIES_ATTRIBUTE).getJSONObject("internal")
        .getJSONObject(PROPERTIES_ATTRIBUTE).getJSONObject("digest");

    fields.putAll(addFieldsFromObject(digestObject, "internal.digest", 1f, AUTO_COMPLETION_ATTRIBUTE, false));
    return fields;
  }

  /**
   * Get the field that are searchable from the given jsonSchema.
   * 
   * @param jsonSchema jsonSchema to extract from
   * @param request    the text to search
   * @return
   */
  public static Map<String, Float> getSearchableFields(String jsonSchema) {
    Map<String, Float> fields = new HashMap<>();
    JSONObject jsonSchemaObject = new JSONObject(new JSONTokener(jsonSchema));
    JSONObject propertiesObject = jsonSchemaObject.getJSONObject(PROPERTIES_ATTRIBUTE);

    fields.putAll(addFieldsFromObject(propertiesObject.getJSONObject(CONTENT_ATTRIBUTE), CONTENT_ATTRIBUTE, 1f,
        "containsIntoSimpleSearch", true));
    return fields;
  }

  public static ObjectNode convertNoticeToJsonLd(JsonNode schemaNode, JsonNode noticeNode) throws IOException {

    ObjectNode jsonLdNode = mapper.createObjectNode();

    addNodeToJsonLd(schemaNode, noticeNode, jsonLdNode, "");

    return jsonLdNode;
  }

  public static List<String> getMediaIds(JsonNode noticeNode, String associatedMediaPath) {
    JsonNode associatedMedia = noticeNode.at(associatedMediaPath);
    List<String> mediaIds = new ArrayList<>();
    associatedMedia.elements().forEachRemaining(media -> {
      if (media.get("ref") != null) {
        mediaIds.add(media.get("ref").asText());
      }
    });
    return mediaIds;
  }

  private static void addNodeToJsonLd(JsonNode schemaNode, JsonNode contentNode, ObjectNode jsonLdNode,
      String parentPath) throws IOException {
    JsonNode currentJsonLdNode = jsonLdNode;
    String jsonLdTypeForArray = "";
    JsonNode associatedSchemaNode = getAssociatedFieldFromSchema(schemaNode, parentPath);
    String linkedDataFieldName = "";
    if (associatedSchemaNode != null && associatedSchemaNode.isObject()) {
      linkedDataFieldName = ((ObjectNode) associatedSchemaNode).at("/linkedDataFieldName").asText();
      String jsonLdType = ((ObjectNode) associatedSchemaNode).at("/jsonLdType").asText();

      if (StringUtils.isNotEmpty(linkedDataFieldName)) {
        if (contentNode.isValueNode()) {
          if (StringUtils.isNotEmpty(jsonLdType)) {
            currentJsonLdNode = mapper.createObjectNode();
            ((ObjectNode) currentJsonLdNode).put("@value", contentNode.asText());
            jsonLdNode.set(linkedDataFieldName, currentJsonLdNode);
          } else {
            ((ObjectNode) currentJsonLdNode).put(linkedDataFieldName, contentNode.asText());
          }
        } else if (contentNode.isObject()) {
          currentJsonLdNode = updateCurrentNodeFromObject(jsonLdNode, linkedDataFieldName);
        } else if (contentNode.isArray()) {
          currentJsonLdNode = updateCurrentNodeFromArray(jsonLdNode, linkedDataFieldName);
        }
        if (StringUtils.isNotEmpty(jsonLdType)) {
          if (currentJsonLdNode.isObject()) {
            ((ObjectNode) currentJsonLdNode).put(JSONLD_TYPE_ATTRIBUTE, jsonLdType);
          } else if (currentJsonLdNode.isArray()) {
            jsonLdTypeForArray = jsonLdType;
          }
        }
      } else if (StringUtils.isNotEmpty(jsonLdType)) {
        jsonLdNode.put(JSONLD_TYPE_ATTRIBUTE, jsonLdType);
      }
    }

    if (contentNode.isObject()) {
      addSubNodeToJsonLd(schemaNode, contentNode, parentPath, (ObjectNode) currentJsonLdNode);
    } else if (contentNode.isArray() && StringUtils.isNotEmpty(linkedDataFieldName)) {
      for (Iterator<JsonNode> elements = contentNode.elements(); elements.hasNext();) {
        JsonNode subContentNode = elements.next();
        ObjectNode jsonLdNodeForArray = mapper.createObjectNode();
        if (StringUtils.isNotBlank(jsonLdTypeForArray)) {
          jsonLdNodeForArray.put(JSONLD_TYPE_ATTRIBUTE, jsonLdTypeForArray);
        }
        ((ArrayNode) currentJsonLdNode).add(jsonLdNodeForArray);
        addSubNodeToJsonLd(schemaNode, subContentNode, parentPath, jsonLdNodeForArray);
      }
    }
  }

  private static JsonNode updateCurrentNodeFromArray(ObjectNode jsonLdNode, String linkedDataFieldName) {
    JsonNode currentJsonLdNode;
    JsonNode jsonNode = jsonLdNode.get(linkedDataFieldName);
    if (jsonNode != null) {
      ArrayNode array;
      if (jsonNode.isArray()) {
        array = (ArrayNode) jsonNode;
      } else {
        array = mapper.createArrayNode().add(jsonNode);
      }
      currentJsonLdNode = array;
    } else {
      currentJsonLdNode = mapper.createArrayNode();
    }
    jsonLdNode.set(linkedDataFieldName, currentJsonLdNode);
    return currentJsonLdNode;
  }

  private static JsonNode updateCurrentNodeFromObject(ObjectNode jsonLdNode, String linkedDataFieldName) {
    JsonNode jsonNode = jsonLdNode.get(linkedDataFieldName);
    JsonNode currentJsonLdNode = mapper.createObjectNode();
    if (jsonNode != null) {
      ArrayNode array;
      if (jsonNode.isArray()) {
        array = (ArrayNode) jsonNode;
      } else {
        array = mapper.createArrayNode().add(jsonNode);
      }
      jsonLdNode.set(linkedDataFieldName, array.add(currentJsonLdNode));
    } else {
      jsonLdNode.set(linkedDataFieldName, currentJsonLdNode);
    }
    return currentJsonLdNode;
  }

  private static void addSubNodeToJsonLd(JsonNode schemaNode, JsonNode contentNode, String parentPath,
      ObjectNode currentJsonLdNode) throws IOException {
    for (Iterator<Entry<String, JsonNode>> fields = contentNode.fields(); fields.hasNext();) {
      StringBuilder currentPath = new StringBuilder(parentPath);
      Entry<String, JsonNode> entry = fields.next();
      String property = entry.getKey();
      if (currentPath.length() != 0) {
        currentPath.append(".");
      }
      currentPath.append(property);
      JsonNode node = entry.getValue();
      if (node.isArray() && currentPath.toString().endsWith("prefLabels")) {
        node = ((ArrayNode) node).get(0);
      }
      addNodeToJsonLd(schemaNode, node, currentJsonLdNode, currentPath.toString());
    }
  }

  private static List<String> getFieldsFromObject(JsonNode block, String path) {
    List<String> fields = new ArrayList<>();

    if (block.get("type") != null && JSON_SCHEMA_PRIMITIVE_TYPE.contains(block.get("type").asText())
        && StringUtils.isNotEmpty(path) && !path.toLowerCase().contains("computed")) {
      fields.add(path);
    }

    Iterator<String> keys = block.fieldNames();
    while (keys.hasNext()) {
      String key = keys.next();
      JsonNode child = block.get(key);

      // Si la valeur est un objet JSON
      if (child != null && !JsonUtils.getKeysToIgnore().contains(key)) {
        String subPath = "";
        if (key.equals(PROPERTIES_ATTRIBUTE)) {
          // Si on est sur un objet properties, on garde le path existant
          subPath = path;
          // Cas d'une date
          if (child.get("startDateComputed") != null) {
            fields.add(subPath + ".display");
          }
        } else if (key.equals(ITEMS_ATTRIBUTE)) {
          // Si on est sur un objet items, on ajoute l'information que la champ est multi
          // valuée
          subPath = path + (path.length() == 0 ? "" : ".[]");
        } else {
          // Dans les autres cas, on ajoute la clé au path courant
          subPath = path + (path.length() == 0 ? "" : ".") + key;
        }
        fields.addAll(getFieldsFromObject(child, subPath));
      }

    }

    return fields;
  }

  private static JSONObject processChildsForMapping(JSONObject parentObject, JSONObject jsonSchema, boolean isContent,
      boolean autocomplete) {
    JSONObject object = new JSONObject();
    Set<String> keys = parentObject.keySet();

    for (String key : keys) {
      JSONObject child = parentObject.optJSONObject(key);

      // Si la valeur est un objet JSON
      if (child != null) {
        object = processObject(jsonSchema, isContent, autocomplete, object, key, child);
      } else if (key.equals("type")) {
        processTypeKey(parentObject, isContent, autocomplete, object, key);
      } else if (key.equals("facetType")) {
        processFacetTypeKey(parentObject, object, key, isContent);
      } else if (key.equals("facetCopyTo")) {
        processCopyTo(parentObject, object, key);
      } else if (key.equals("$ref")) {
        // Dans le cas d'une référence, on récupère l'objet correspondant dans les
        // définitions
        JSONObject refObject = getDefinitionFromRef(parentObject, jsonSchema, key);
        object = processChildsForMapping(refObject, jsonSchema, isContent, autocomplete);
      } else if (key.equals(FORMAT_ATTRIBUTE)) {
        // Si on a un format de type date-time, date ou time, c'est que le type cible
        // est une date
        String format = parentObject.getString(key);
        if (DATE_ERA_FORMAT.equals(format)) {
          object.put(ES_TYPE_PROPERTIE_NAME, "date");
          object.put(ES_FORMAT_PROPERTIE_NAME, "yyyy-MM-dd GG");
          object.put("locale", Locale.ENGLISH);
        } else if (DATE_FORMAT.contains(format)) {
          object.put(ES_TYPE_PROPERTIE_NAME, "date");
        }
      }
    }
    return object;
  }

  private static JSONObject processObject(JSONObject jsonSchema, boolean isContent, boolean autocomplete,
      JSONObject object, String key, JSONObject child) {
    if (key.equals(ITEMS_ATTRIBUTE)) {
      // Dans le cas de la clé items, on a un niveau de trop dans le JSON Schema, on
      // récupère le champ properties au lieu d'utiliser l'objet parent items
      JSONObject items = processChildsForMapping(child, jsonSchema, isContent, autocomplete);
      if (items.has(PROPERTIES_ATTRIBUTE)) {
        object.put(PROPERTIES_ATTRIBUTE, items.getJSONObject(PROPERTIES_ATTRIBUTE));
      } else if (object.length() == 0) {
        object = items;
      }

    } else if (key.equals("geoPoint")) {
      object.put(key, createGeoPointField());
    } else if (!KEYS_TO_IGNORE.contains(key)) {
      // On ajoute l'objet contruit à partir de l'objet courrant
      boolean autocompleteForChildren = child.optBoolean(AUTO_COMPLETION_ATTRIBUTE, false)
          || child.optBoolean(ADV_AUTO_COMPLETION_ATTRIBUTE, false) || autocomplete;
      if (key.equals(CONTENT_ATTRIBUTE)) {
        object.put(key, processChildsForMapping(child, jsonSchema, true, autocompleteForChildren));
      } else {
        // Si l'autocomplete est définit sur un objet parent, on ne l'applique pas aux
        // références ni aux conceptPath
        if (key.equals("ref") || key.equals(CONCEPT_PATH_ATTRIBUTE)) {
          autocompleteForChildren = false;
        }
        object.put(key, processChildsForMapping(child, jsonSchema, isContent, autocompleteForChildren));
      }
    }
    return object;
  }

  private static void processCopyTo(JSONObject parentObject, JSONObject object, String key) {
    String field = parentObject.getString(key);
    object.put("copy_to", field);

  }

  private static void processFacetTypeKey(JSONObject parentObject, JSONObject object, String key, boolean isContent) {
    String facetType = parentObject.getString(key);
    if (facetType.equals(FACET_TYPE_SIMPLE)) {
      if (isContent) {
        createFullTextField(object);
        createExactSubField(object);
      }
      createKeywordSubField(object);
    } else if (facetType.equals(FACET_TYPE_HIERARCHICAL)) {
      createPathField(object);
    } else {
      throw new UnsupportedOperationException("Type de facette inconnu : " + facetType);
    }

  }

  private static void processTypeKey(JSONObject parentObject, boolean isContent, boolean autocomplete,
      JSONObject object, String key) {
    // Récupération du type ES correspondant au type dans le JSONSchema
    String esTypeFromSchemaType = getEsTypeFromSchemaType(parentObject.getString(key));

    if (esTypeFromSchemaType.equals("text") && object.opt(key) == null) {
      if (isContent) {
        createFullTextField(object);
        createExactSubField(object);
      } else {
        object.put(key, ES_KEYWORD_TYPE);
      }
      if (parentObject.optBoolean(AUTO_COMPLETION_ATTRIBUTE, false)
          || parentObject.optBoolean(ADV_AUTO_COMPLETION_ATTRIBUTE, false) || autocomplete) {
        createAutoCompleteSubField(object);
      }
    } else if (StringUtils.isNotEmpty(esTypeFromSchemaType) && object.opt(key) == null) {
      // Dans le cas du type objet ou array, pas besoin de renseigné un type dans le
      // mappping
      object.put(key, esTypeFromSchemaType);
    }
  }

  private static void createPathField(JSONObject object) {
    JSONObject path = new JSONObject();
    path.put(ES_TYPE_PROPERTIE_NAME, "text");
    path.put(ES_ANALYZER_PROPERTIE_NAME, "path_analyzer");
    path.put("fielddata", "true");

    JSONObject fields = new JSONObject();
    fields.put(KEYWORD_FIELD_NAME, path);

    object.put(ES_TYPE_PROPERTIE_NAME, "text");
    object.put(ES_FIELDS_PROPERTIE_NAME, fields);
  }

  private static void createAutoCompleteSubField(JSONObject object) {
    JSONObject autocomplete = new JSONObject();
    autocomplete.put(ES_TYPE_PROPERTIE_NAME, "text");
    autocomplete.put(ES_ANALYZER_PROPERTIE_NAME, "autocomplete_ngram");
    autocomplete.put("search_analyzer", "autocomplete_search");
    autocomplete.put("fielddata", true);
    addToFields("autocomplete", object, autocomplete);
  }

  private static void createExactSubField(JSONObject object) {
    JSONObject exact = new JSONObject();
    exact.put(ES_TYPE_PROPERTIE_NAME, "text");
    exact.put(ES_ANALYZER_PROPERTIE_NAME, "exact");

    addToFields(EXACT_FIELD_NAME, object, exact);
  }

  private static void createKeywordSubField(JSONObject object) {
    JSONObject keyword = new JSONObject();
    keyword.put(ES_TYPE_PROPERTIE_NAME, ES_KEYWORD_TYPE);

    addToFields(KEYWORD_FIELD_NAME, object, keyword);
  }

  private static JSONObject createGeoPointField() {
    JSONObject fields = new JSONObject();
    fields.put(ES_TYPE_PROPERTIE_NAME, "geo_point");
    return fields;
  }

  private static void addToFields(String fieldName, JSONObject parent, JSONObject object) {
    JSONObject fields = parent.optJSONObject(ES_FIELDS_PROPERTIE_NAME);
    if (fields == null) {
      fields = new JSONObject();
      parent.put(ES_FIELDS_PROPERTIE_NAME, fields);
    }
    fields.put(fieldName, object);
  }

  private static void createFullTextField(JSONObject object) {
    object.put(ES_TYPE_PROPERTIE_NAME, "text");
    object.put(ES_ANALYZER_PROPERTIE_NAME, "fulltext");
  }

  private static JSONObject getDefinitionFromRef(JSONObject parentObject, JSONObject jsonSchema, String refKey) {
    String ref = parentObject.getString(refKey);
    ref = ref.replaceFirst("#/", "");
    String[] split = ref.split("/");

    JSONObject refObject = jsonSchema;
    for (String string : split) {
      refObject = refObject.getJSONObject(string);
    }
    return refObject;
  }

  private static String getEsTypeFromSchemaType(String type) {
    return JSON_TYPE_TO_ESTYPE.get(type);
  }

  private static Map<String, Float> addFieldsFromObject(JSONObject parentObject, String path, double parentBoost,
      String mandatoryAttribute, boolean defaultAttributevalue) {
    Map<String, Float> fields = new HashMap<>();
    double boost;
    // On ne recherche que sur les champ text
    if (parentObject.optString("type").equals("string") && StringUtils.isNotEmpty(parentObject.optString("type"))
        && !JsonUtils.getDateFormat().contains(parentObject.optString(FORMAT_ATTRIBUTE))) {
      // On ne prend pas les champ qui ont l'attribut demandé en paramètre
      if (parentObject.optBoolean(mandatoryAttribute, defaultAttributevalue)) {
        boost = parentObject.optDouble("simpleBoost");
        if (!Double.isNaN(boost)) {
          fields.put(path, (float) boost);
        } else {
          fields.put(path, (float) parentBoost);
        }
      }
    }

    Iterator<String> keys = parentObject.keys();
    while (keys.hasNext()) {
      String key = keys.next();
      JSONObject child = parentObject.optJSONObject(key);

      // Si la valeur est un objet JSON
      if (child != null && !JsonUtils.getKeysToIgnore().contains(key)) {
        String subPath = "";
        if (key.equals(PROPERTIES_ATTRIBUTE) || key.equals(ITEMS_ATTRIBUTE)) {
          // Si on est sur un objet properties ou items, ce ne sont pas des éléments
          // d'arboresence dans le données, on garde le path existant
          subPath = path;
        } else {
          // Dans les autres cas, on ajoute la clé au path courant
          subPath = path + "." + key;
        }
        boost = child.optDouble("simpleBoost");
        fields.putAll(addFieldsFromObject(child, subPath, Double.isNaN(boost) ? parentBoost : boost, mandatoryAttribute,
            defaultAttributevalue));
      }
    }
    return fields;
  }

}
