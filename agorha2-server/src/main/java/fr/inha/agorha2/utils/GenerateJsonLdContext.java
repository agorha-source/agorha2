package fr.inha.agorha2.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import fr.inha.elastic.model.NoticeType;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenerateJsonLdContext {
  private static final Logger log = LoggerFactory.getLogger(GenerateJsonLdContext.class);
  private static Map<NoticeType, String> fileNameByNoticeType = new HashMap<>();

  static {
    fileNameByNoticeType.put(NoticeType.ARTWORK, "artwork.json");
    fileNameByNoticeType.put(NoticeType.COLLECTION, "collection.json");
    fileNameByNoticeType.put(NoticeType.EVENT, "event.json");
    fileNameByNoticeType.put(NoticeType.PERSON, "person.json");
    fileNameByNoticeType.put(NoticeType.REFERENCE, "reference.json");
  }

  private JsonNode ldNode;
  private String basePath;

  public GenerateJsonLdContext(String basePath) {
    this.basePath = basePath;
    ObjectMapper mapper = new ObjectMapper();
    try {
      this.ldNode = mapper.readTree("{ \"@id\": \"\",\"@type\": \"\"}");
    } catch (IOException ex) {
      log.error("une erreur est survenue lors de la tranformation en JsonNode des éléments @id et @type", ex);
    }
  }

  public String build(NoticeType noticeType) throws IOException {

    byte[] jsonData = Files.readAllBytes(Paths.get(basePath, fileNameByNoticeType.get(noticeType)));

    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);

    //create JsonNode
    JsonNode rootNode = objectMapper.readTree(jsonData);

    // méthode récursive pour ajouter les eléments de définition du contexte json-ld
    processNode(rootNode, null, null);

    Object json = objectMapper.readValue(rootNode.toString(), Object.class);

    return objectMapper.writeValueAsString(json);
  }

  /**
   * Ajoute les @id et les @type du context ld sur les noeuds du json en paramètre.
   *
   * @param currentNode noeud json en cours de traitement
   * @param key         clef du noeud en court de traitement
   * @param parentNode  noeud parent du noeud courant
   */
  private void processNode(JsonNode currentNode, String key, JsonNode parentNode) {

    if (currentNode.isArray()) {
      currentNode.elements().forEachRemaining(element -> {
        if (element.isObject()) {
          processNode(element, null, null);
        } else {
          ((ObjectNode) parentNode).set(key, ldNode);
        }
      });
    } else {
      currentNode.fields().forEachRemaining(element -> {
        if (element.getValue().isObject()) {
          ((ObjectNode) element.getValue()).put("@id", "");
          ((ObjectNode) element.getValue()).put("@type", "");

          processNode(element.getValue(), null, null);
        } else if (element.getValue().isArray()) {
          processNode(element.getValue(), element.getKey(), currentNode);
        } else {
          // traitement des types primitifs
          if (!StringUtils.equalsAnyIgnoreCase(element.getKey(), "@id", "@type") && !StringUtils.equalsAnyIgnoreCase(element.getKey(), "@type")) {
            ((ObjectNode) currentNode).set(element.getKey(), ldNode);
          }
        }
      });

    }
  }
}
