package fr.inha.agorha2.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.inha.elastic.model.NoticeType;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe permettant de générer une liste de chaine de caractère contenant
 * toutes les propriétés du fichier json en paramètre. La liste obtenue permet
 * de générer un fichier CSV de la forme "CHAMPS;TERME ONTOLOGIE;@TYPE".
 */
public class GenerateJsonLdTemplate {

  private static final Logger log = LoggerFactory.getLogger(GenerateJsonLdTemplate.class);
  private static final String SEPARATOR = ".";
  private static final String DOUBLE_CSV_SEPARATOR = ";;";
  private static Map<NoticeType, String> fileNameByNoticeType = new HashMap<>();

  static {
    fileNameByNoticeType.put(NoticeType.ARTWORK, "artwork.json");
    fileNameByNoticeType.put(NoticeType.COLLECTION, "collection.json");
    fileNameByNoticeType.put(NoticeType.EVENT, "event.json");
    fileNameByNoticeType.put(NoticeType.PERSON, "person.json");
    fileNameByNoticeType.put(NoticeType.REFERENCE, "reference.json");
  }

  private String basePath;

  public GenerateJsonLdTemplate(String basePath) {
    this.basePath = basePath;
  }

  /**
   * Construit une liste de chaine de caractère contenant les noms des champs
   * contenu dans un fichier json.
   * 
   * @param noticeType type de notice à traiter
   * @return une liste de chaine de caractère
   * @throws IOException lance une exception en cas de d'erreur de lecture du
   *                     fichier ou de serialisation
   */
  public List<String> build(NoticeType noticeType) throws IOException {
    // sérialisation fichier json
    byte[] jsonData = Files.readAllBytes(Paths.get(basePath, fileNameByNoticeType.get(noticeType)));
    ObjectMapper objectMapper = new ObjectMapper();
    JsonNode rootNode = objectMapper.readTree(jsonData);

    List<String> jsonPaths = new ArrayList<>();
    jsonPaths.add("CHAMPS;TERME ONTOLOGIE;@TYPE");

    // méthode récursive pour extraire les json path
    processNode(rootNode, jsonPaths, "", null);

    return jsonPaths;
  }

  /**
   * Méthode récursive pour extraire le nom de chaque champ du jsonNode en
   * paramètre.
   *
   * @param currentNode noeud json en cours de traitement
   * @param jsonPaths   Liste de chaine de caractère contenant le nom des champs
   *                    du json
   * @param key         nom du champ en cours de traitement
   * @param parentNode  noeud parent du noeud courant
   */
  private void processNode(JsonNode currentNode, List<String> jsonPaths, String key, JsonNode parentNode) {

    if (currentNode.isArray()) {
      currentNode.elements().forEachRemaining(element -> {
        jsonPaths.add(key + DOUBLE_CSV_SEPARATOR);
        if (element.isObject()) {
          processNode(element, jsonPaths, key, currentNode);
        }
      });
    } else {
      currentNode.fields().forEachRemaining(element -> {
        if (element.getValue().isObject()) {
          String currentKey = (StringUtils.isNotEmpty(key)) ? key + SEPARATOR + element.getKey() : element.getKey();
          jsonPaths.add(currentKey + DOUBLE_CSV_SEPARATOR);
          processNode(element.getValue(), jsonPaths, currentKey, null);
        } else if (element.getValue().isArray()) {
          String currentKey = (StringUtils.isNotEmpty(key)) ? key + SEPARATOR + element.getKey() : element.getKey();
          processNode(element.getValue(), jsonPaths, currentKey, currentNode);
        } else {
          String currentKey = (StringUtils.isNotEmpty(key)) ? key + SEPARATOR + element.getKey() : element.getKey();
          jsonPaths.add(currentKey + DOUBLE_CSV_SEPARATOR);
        }
      });

    }
  }
}
