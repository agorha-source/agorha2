package fr.inha.agorha2.utils;

import fr.inha.agorha2.api.dto.AuthenticatedUser;
import fr.inha.agorha2.ldap.LdapInhaUser;
import fr.inha.agorha2.ldap.LdapUser;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.security.UserPrincipal;
import fr.inha.elastic.model.Authorizations;
import fr.inha.elastic.model.UserInternal;
import fr.inha.elastic.model.UserStatus;
import java.time.LocalDateTime;
import java.util.UUID;
import javax.annotation.Nonnull;

public class UserUtils {
  private UserUtils() {
    throw new IllegalStateException("Utility class");
  }

  public static Authorizations createAuthorizations() {
    Authorizations authorizations = new Authorizations();
    authorizations.setRole(Authority.DEFAULT.name());
    return authorizations;
  }

  public static UserInternal createInternal(LdapUser ldapUser, String login) {
    UserInternal internal = new UserInternal();
    internal.setLogin(login);
    internal.setLastname(ldapUser.getName());
    internal.setFirstname(ldapUser.getFirstName());
    internal.setUuid(UUID.randomUUID().toString());
    internal.setCreatedDate(LocalDateTime.now());
    internal.setUpdatedDate(LocalDateTime.now());
    String fullName = ldapUser.getName() + " " + ldapUser.getFirstName();
    if (ldapUser instanceof LdapInhaUser) {
      fullName = ((LdapInhaUser) ldapUser).getDisplayName();
    }
    internal.setCreatedBy(fullName);
    internal.setUpdatedBy(fullName);
    internal.setStatus(UserStatus.ACTIVE);
    return internal;
  }

  public static AuthenticatedUser getAuthenticatedUser(@Nonnull UserPrincipal principal) {
    AuthenticatedUser user = new AuthenticatedUser();
    user.setAuthorizations(principal.getUser().getAuthorizations());
    user.setFirstName(principal.getLdapUser().getFirstName());
    user.setMail(principal.getLdapUser().getMail());
    user.setName(principal.getLdapUser().getName());
    return user;
  }
}
