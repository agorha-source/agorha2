package fr.inha.agorha2.utils;

import com.fasterxml.jackson.databind.JsonNode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;
import java.util.Locale;
import org.apache.commons.lang3.StringUtils;

public class DateUtils {
  private static final String YEAR_FORMAT = "u";
  private static final String YEAR_MONTH_FORMAT = "MM/u";
  private static final String DEFAULT_FORMAT = "dd/MM/u";
  private static final String ERA_FORMAT = "yyyy-MM-dd GG";

  private static final DateTimeFormatter yearFormatter = new DateTimeFormatterBuilder().appendPattern(YEAR_FORMAT)
      .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1).parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter();
  private static final DateTimeFormatter isoDateFormatter = DateTimeFormatter.ISO_DATE_TIME;
  private static final DateTimeFormatter yearMonthDateFormatter = new DateTimeFormatterBuilder()
      .appendPattern(YEAR_MONTH_FORMAT).parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter();
  private static final DateTimeFormatter defaultDateFormatter = DateTimeFormatter.ofPattern(DEFAULT_FORMAT);
  private static final DateTimeFormatter eraDateFormatter = DateTimeFormatter.ofPattern(ERA_FORMAT, Locale.ENGLISH);

  public enum ConvertionMode {
    START,
    END;
  }

  public enum PrefixType {
    AFTER("Après"),
    BEFORE("Avant"),
    NEAR("Vers");

    private String value;

    private PrefixType(String value) {
      this.value = value;
    }

    public String getValue() {
      return value;
    }

    public static PrefixType fromValue(String value) {
      for (PrefixType prefixType : PrefixType.values()) {
        if (prefixType.getValue().equals(value)) {
          return prefixType;
        }
      }
      return null;
    }
  }

  public static LocalDate parseYear(String yearAsString) {
    return LocalDate.parse(yearAsString, yearFormatter);
  }

  public static String formatYear(LocalDate year) {
    return yearFormatter.format(year);
  }

  public static LocalDateTime parseIsoDate(String dateAsString) {
    return LocalDateTime.parse(dateAsString, isoDateFormatter);
  }

  public static String formatIsoDate(LocalDateTime date) {
    return isoDateFormatter.format(date);
  }

  public static String formatIsoDate(LocalDate date) {
    return isoDateFormatter.format(date);
  }

  /**
   * Parse the given date using
   * {@link DateUtils#parseDate(String, ConvertionMode)} with
   * {@link ConvertionMode#START}.
   * 
   * @see DateUtils#parseDate(String, ConvertionMode)
   * @param dateAsString date as string
   * @return
   */
  public static LocalDate parseDate(String dateAsString) {
    return parseDate(dateAsString, ConvertionMode.START);
  }

  /**
   * Convert the given string to a {@link LocalDate}. Supported format are : u,
   * MM/u, dd/MM/u.<br>
   * 
   * {@link ConvertionMode} is used to determine how to handle dates with uuuu or
   * MM/uuuu formats.<br>
   * 
   * Examples:<br>
   * 
   * parseDate("1950", {@link ConvertionMode#START}) will return "01/01/1950"<br>
   * parseDate("1950", {@link ConvertionMode#END}) will return "31/12/1950"<br>
   * parseDate("05/1950", {@link ConvertionMode#START}) will return
   * "01/05/1950"<br>
   * parseDate("05/1950", {@link ConvertionMode#END}) will return "31/05/1950"<br>
   * parseDate("01/05/1950", {@link ConvertionMode#START}) will return
   * "01/05/1950"<br>
   * parseDate("01/05/1950", {@link ConvertionMode#END}) will return
   * "01/05/1950"<br>
   * 
   * @param dateAsString   date to parse
   * @param convertionMode convertion mode for non complete dates (eg. only year
   *                       or month-year)
   * @return
   */
  public static LocalDate parseDate(String dateAsString, ConvertionMode convertionMode) {
    if (dateAsString.matches("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)-?\\d{1,4}$")) {
      return LocalDate.parse(dateAsString, defaultDateFormatter);
    } else if (dateAsString.matches("^-?\\d{1,4}$")) {
      if (convertionMode.equals(ConvertionMode.END)) {
        return LocalDate.parse(dateAsString, yearFormatter).with(TemporalAdjusters.lastDayOfYear());
      } else {
        return LocalDate.parse(dateAsString, yearFormatter);
      }
    } else if (dateAsString.matches("^(((0)[0-9])|((1)[0-2]))(\\/)-?\\d{1,4}$")) {
      if (convertionMode.equals(ConvertionMode.END)) {
        return LocalDate.parse(dateAsString, yearMonthDateFormatter).with(TemporalAdjusters.lastDayOfMonth());
      } else {
        return LocalDate.parse(dateAsString, yearMonthDateFormatter);
      }
    } else {
      throw new IllegalArgumentException("Invalid date format " + dateAsString);
    }
  }

  public static String formatDate(LocalDate date) {
    return defaultDateFormatter.format(date);
  }

  public static String formatDate(LocalDateTime date, String format) {
    return DateTimeFormatter.ofPattern(format).format(date);
  }

  public static String formatEraDate(LocalDate date) {
    return eraDateFormatter.format(date);
  }

  public static LocalDate parseEraDate(String dateAsString) {
    return LocalDate.from(eraDateFormatter.parse(dateAsString));
  }

  /**
   * Return a {@link LocalDate} corresponding to the given start date plus or
   * minus a certain amount of year. The amount is based on the given prefix and
   * if the date is from the siecle thesaurus or not.
   * 
   * @param prefixType type of the prefix
   * @param startDate  base date
   * @param isSiecle   the date is from the siecle thesaurus or not
   * @return
   */
  public static LocalDate updateYearsForEnd(PrefixType prefixType, LocalDate startDate, boolean isSiecle) {
    if (PrefixType.NEAR.equals(prefixType)) {
      return startDate.plusYears(5);
    } else if (PrefixType.BEFORE.equals(prefixType) && isSiecle) {
      return startDate.minusYears(100);
    } else if (PrefixType.AFTER.equals(prefixType)) {
      return startDate.plusYears(100);
    }
    return startDate;
  }

  /**
   * Return a {@link LocalDate} corresponding to the given end date plus or minus
   * a certain amount of year. The amount is based on the given prefix and if the
   * date is from the siecle thesaurus or not.
   * 
   * @param prefixType type of the prefix
   * @param endDate    base date
   * @param isSiecle   the date is from the siecle thesaurus or not
   * @return
   */
  public static LocalDate updateYearsForStart(PrefixType prefixType, LocalDate endDate, boolean isSiecle) {
    if (PrefixType.NEAR.equals(prefixType)) {
      return endDate.minusYears(5);
    } else if (PrefixType.BEFORE.equals(prefixType)) {
      return endDate.minusYears(100);
    } else if (PrefixType.AFTER.equals(prefixType) && isSiecle) {
      return endDate.plusYears(100);
    }
    return endDate;
  }

  public static String buildDatesFormattedFromDigest(JsonNode startDate, JsonNode endDate) {
    if (startDate.isMissingNode()) {
      return null;
    }
    StringBuilder dates = new StringBuilder();
    String startFormatted = buildDateFormattedFromDigest(startDate);
    dates.append(startFormatted);

    if (!endDate.isMissingNode()) {
      String endFormatted = buildDateFormattedFromDigest(endDate);
      if (!startFormatted.equals(endFormatted)) {
        dates.append(" - ");
        dates.append(endFormatted);
      }
    }
    return dates.toString();
  }

  public static String buildDatesFormattedFromContent(JsonNode startDate, JsonNode endDate, boolean toIsoDate) {
    return buildDatesFormattedFromContent(startDate, endDate, toIsoDate, true);
  }

  public static String buildDatesFormattedFromContent(JsonNode startDate, JsonNode endDate, boolean toIsoDate,
      boolean centuryAsLabel) {
    if (startDate.isMissingNode()) {
      return null;
    }
    StringBuilder dates = new StringBuilder();
    String startFormatted = buildDateFormattedFromContent(startDate, toIsoDate, centuryAsLabel);
    dates.append(startFormatted);

    if (!endDate.isMissingNode()) {
      String endFormatted = buildDateFormattedFromContent(endDate, toIsoDate, centuryAsLabel);
      if (!startFormatted.equals(endFormatted)) {
        dates.append(" - ");
        dates.append(endFormatted);
      }
    }
    return dates.toString();
  }

  private static String buildDateFormattedFromDigest(JsonNode date) {
    JsonNode prefix = date.at("/prefix");
    JsonNode earliest = date.at("/earliest");
    JsonNode latest = date.at("/latest");
    return buildDateFormatted(earliest.asText(), latest.asText(), prefix.asText(), false);
  }

  private static String buildDateFormattedFromContent(JsonNode date, boolean toIsoDate, boolean centuryAsLabel) {
    JsonNode prefix = date.at("/prefix/thesaurus/prefLabels/0/value");
    if (date.at("/siecle/thesaurus").isMissingNode()) {
      JsonNode earliest = date.at("/earliest/date");
      JsonNode latest = date.at("/latest/date");
      return buildDateFormatted(earliest.asText(), latest.asText(), prefix.asText(), toIsoDate);
    } else {
      if (StringUtils.isNoneBlank(prefix.asText())) {
        return prefix.asText() + " " + buildCenturyFormatted(date, centuryAsLabel);
      }
      return buildCenturyFormatted(date, centuryAsLabel);
    }
  }

  private static String buildCenturyFormatted(JsonNode date, boolean centuryAsLabel) {
    if (centuryAsLabel) {
      return date.at("/siecle/thesaurus/prefLabels/0/value").asText();
    } else {
      JsonNode siecleConceptPath = date.at("/siecle/thesaurus/conceptPath");
      return new StringBuilder().append(siecleConceptPath.asText()).append("(")
          .append(date.at("/siecle/thesaurus/ref").asText()).append(")").toString();
    }
  }

  private static String buildDateFormatted(String dateEarliest, String dateLatest, String prefix, boolean toIsoDate) {
    StringBuilder date = new StringBuilder();
    if (StringUtils.isNoneBlank(prefix)) {
      date.append(prefix).append(" ");
    }
    if (!dateEarliest.startsWith("/")) {
      if (toIsoDate) {
        date.append(DateUtils.parseDate(dateEarliest));
      } else {
        date.append(dateEarliest);
      }

      if (StringUtils.isNoneBlank(dateLatest)) {
        if (toIsoDate) {
          date.append(" / ").append(DateUtils.parseDate(dateLatest));
        } else {
          date.append(" / ").append(dateLatest);
        }
      }
    } else {
      date.append(dateEarliest);
    }
    return date.toString();
  }

}
