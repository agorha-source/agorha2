package fr.inha.agorha2.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.jsonldjava.core.JsonLdError;
import fr.inha.elastic.model.NoticeType;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.io.FileUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;

class JsonLdTest {
  private static ObjectMapper mapper = new ObjectMapper();

  @Test
  @Disabled
  void generateJsonLdTest() throws JsonLdError, IOException {
    Map<String, String> context = new HashMap<>();
    context.put("schema", "http://schema.org/");
    context.put("dc", "http://purl.org/dc/terms/");
    context.put("crm", "http://www.cidoc-crm.org/cidoc-crm/");
    context.put("rdfs", "https://www.w3.org/TR/rdf-schema/");
    context.put("prov", "http://www.w3.org/ns/prov#");
    InputStream resourceAsStream = JsonUtilsTest.class.getClassLoader()
        .getResourceAsStream("jsonld/input/internal.json");
    Model model = ModelFactory.createDefaultModel();
    model.read(resourceAsStream, null, "JSON-LD");
    StringWriter writer = new StringWriter();
    RDFDataMgr.write(writer, model, Lang.NTRIPLES);
    System.out.println(writer.toString());

    RDFDataMgr.write(writer, model, Lang.N3);
    System.out.println(writer.toString());

    RDFDataMgr.write(writer, model, Lang.RDFXML);
    System.out.println(writer.toString());
  }

  @Test
  @Disabled
  void updateSchemaFromTemplate() {
    Map<NoticeType, String> schemas = new HashMap<>();
    schemas.put(NoticeType.ARTWORK, "schemaArtwork.json");
    schemas.put(NoticeType.COLLECTION, "schemaCollection.json");
    schemas.put(NoticeType.EVENT, "schemaEvent.json");
    schemas.put(NoticeType.PERSON, "schemaPerson.json");
    schemas.put(NoticeType.REFERENCE, "schemaReference.json");
    InputStream resourceAsStream = JsonUtilsTest.class.getClassLoader()
        .getResourceAsStream("jsonld/JSONLD_Template.xlsx");
    try (Workbook workbook = new XSSFWorkbook(resourceAsStream)) {
      for (Entry<NoticeType, String> entry : schemas.entrySet()) {

        InputStream schemaAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream(entry.getValue());

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode schemaNode = objectMapper.readTree(schemaAsStream);
        Sheet datatypeSheet = workbook.getSheet(entry.getKey().name() + "_data");
        Iterator<Row> iterator = datatypeSheet.iterator();
        // On ne traite pas la ligne d'en-tête
        iterator.next();
        while (iterator.hasNext()) {

          Row currentRow = iterator.next();
          Cell champCell = currentRow.getCell(0);
          String champ = "";
          if (champCell != null) {
            champ = champCell.getStringCellValue();
          }
          Cell termOntoCell = currentRow.getCell(1);
          String termOnto = "";
          if (termOntoCell != null) {
            termOnto = termOntoCell.getStringCellValue();
          }
          Cell typeCell = currentRow.getCell(2);
          String type = "";
          if (typeCell != null) {
            type = typeCell.getStringCellValue();
          }

          if (StringUtils.isNotBlank(type) || StringUtils.isNotBlank(termOnto)) {
            JsonNode schemaField = JsonUtils.getAssociatedFieldFromSchema(schemaNode, champ);
            if (StringUtils.isBlank(champ)) {
              schemaField = schemaNode;
            }
            if (schemaField == null) {
              System.out.println(entry.getKey() + "  " + champ);
              break;
            }
            if (schemaField.isObject()) {
              ((ObjectNode) schemaField).remove("linkedDataFieldName");
              ((ObjectNode) schemaField).remove("jsonLdType");
              if (StringUtils.isNotBlank(termOnto)) {
                ((ObjectNode) schemaField).put("linkedDataFieldName", termOnto);
              }
              if (StringUtils.isNotBlank(type)) {
                ((ObjectNode) schemaField).put("jsonLdType", type);
              }
            }
          }
          FileUtils.write(new File("target/schemaJsonLd/" + entry.getValue()),
              objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(schemaNode), "UTF-8");
        }
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
