package fr.inha.agorha2.utils;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import fr.inha.elastic.model.NoticeType;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

class GenerateJsonLdTemplateTest {

  @Test public void build() {

    try {
      File inputPath = new File(this.getClass().getResource("/jsonld/input").getPath());

      generateByNoticeType(inputPath, NoticeType.ARTWORK);
      generateByNoticeType(inputPath, NoticeType.COLLECTION);
      generateByNoticeType(inputPath, NoticeType.EVENT);
      generateByNoticeType(inputPath, NoticeType.PERSON);
      generateByNoticeType(inputPath, NoticeType.REFERENCE);

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void generateByNoticeType(File inputPath, NoticeType noticeType) throws IOException {
    GenerateJsonLdTemplate builder = new GenerateJsonLdTemplate(inputPath.toString());
    final List<String> jsonPaths = builder.build(noticeType);
    assertNotNull(jsonPaths);
    File outputPath = new File(inputPath, noticeType.name() + "_data.csv");
    FileUtils.writeLines(outputPath, "UTF-8", jsonPaths);
  }
}