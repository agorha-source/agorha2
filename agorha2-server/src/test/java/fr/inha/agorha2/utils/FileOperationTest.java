package fr.inha.agorha2.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

public class FileOperationTest {

	@Test
	@Disabled
	void skosUuidGeneration() throws IOException {
		String inputFilePath = "C:\\Users\\broussot\\Downloads\\20230705-exemple-skos-thesaurus-materiaux.rdf";
		String outpuFilePath = "C:\\Users\\broussot\\Downloads\\20230705-exemple-skos-thesaurus-materiaux-updated.rdf";
		File skos = new File(inputFilePath);
		File skosDest = new File(outpuFilePath);

		List<String> allLines = Files.readAllLines(skos.toPath());
		List<String> newLines = new ArrayList<>();
		Map<Integer, String> ids = new HashMap<>();

		for (String line : allLines) {
			if (line.contains("concept_")) {
				Integer id = Integer.parseInt(line.substring(line.lastIndexOf("_") + 1, line.lastIndexOf("\"")));
				String uuid = ids.get(id) != null ? ids.get(id) : UUID.randomUUID().toString();
				ids.put(id, uuid);
				line = line.replaceFirst("concept_[0-9]+", "54721/" + uuid);
			}
			newLines.add(line);
		}

		Files.write(skosDest.toPath(), newLines);

	}
}
