package fr.inha.agorha2.utils;

import static org.junit.jupiter.api.Assertions.assertFalse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.everit.json.schema.ValidationException;
import org.junit.jupiter.api.Test;

class JsonUtilsTest {
  private static ObjectMapper mapper = new ObjectMapper();

  @Test
  void generateMappingFromJsonTest() throws IOException {
    InputStream resourceAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaCollection.json");
    String string = IOUtils.toString(resourceAsStream, "UTF-8");
    FileUtils.write(new File("target/mapping/mapping_collection.json"), JsonUtils.generateMappingFromJson(string),
        "UTF-8");

    resourceAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaArtwork.json");
    string = IOUtils.toString(resourceAsStream, "UTF-8");
    FileUtils.write(new File("target/mapping/mapping_artwork.json"), JsonUtils.generateMappingFromJson(string),
        "UTF-8");

    resourceAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaEvent.json");
    string = IOUtils.toString(resourceAsStream, "UTF-8");
    FileUtils.write(new File("target/mapping/mapping_event.json"), JsonUtils.generateMappingFromJson(string), "UTF-8");

    resourceAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaPerson.json");
    string = IOUtils.toString(resourceAsStream, "UTF-8");
    FileUtils.write(new File("target/mapping/mapping_person.json"), JsonUtils.generateMappingFromJson(string), "UTF-8");

    resourceAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaReference.json");
    string = IOUtils.toString(resourceAsStream, "UTF-8");
    FileUtils.write(new File("target/mapping/mapping_reference.json"), JsonUtils.generateMappingFromJson(string),
        "UTF-8");
  }

  @Test
  void validateCollectionTest() throws IOException {
    InputStream schemaAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaCollection.json");
    InputStream subjectAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("dataCollection.json");
    validateAndPrint(schemaAsStream, subjectAsStream);
  }

  @Test
  void validateEventTest() throws IOException {
    InputStream schemaAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaEvent.json");
    InputStream subjectAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("dataEvent.json");
    validateAndPrint(schemaAsStream, subjectAsStream);
  }

  @Test
  void validateReferenceTest() throws IOException {
    InputStream schemaAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaReference.json");
    InputStream subjectAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("dataReference.json");
    validateAndPrint(schemaAsStream, subjectAsStream);
  }

  @Test
  void validatePersonTest() throws IOException {
    InputStream schemaAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaPerson.json");
    InputStream subjectAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("dataPerson.json");
    validateAndPrint(schemaAsStream, subjectAsStream);
  }

  @Test
  void validateArtworkTest() throws IOException {
    InputStream schemaAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaArtwork.json");
    InputStream subjectAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("dataArtwork.json");
    validateAndPrint(schemaAsStream, subjectAsStream);
  }

  @Test
  void getFieldPerBlockTest() throws IOException {
    InputStream resourceAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaArtwork.json");
    String string = IOUtils.toString(resourceAsStream, "UTF-8");
    Map<String, List<String>> fieldPerBlock = JsonUtils.getFieldPerBlock(string);
    System.out.println(fieldPerBlock);
  }

  @Test
  void getAllSubPathTest() throws IOException {
    InputStream subjectAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("dataArtwork.json");
    for (String path : JsonUtils.getAllSubPath("content.creationInformation.creation.date",
        mapper.readTree(subjectAsStream), "")) {
      System.out.println(path);
    }

  }

  @Test
  void addValueToNodeFromPathTest() {
    ObjectNode contentNode = mapper.createObjectNode();
    String path = "identificationInformation.personType1.sourcing.0.biblioRef.ref";
    JsonUtils.addValueToNodeFromPath(path, contentNode, "ref biblioRef");
    System.out.println(path + "\n" + contentNode);

    path = "identificationInformation.personType1.sourcing.0.biblioRef.digest";
    JsonUtils.addValueToNodeFromPath(path, contentNode, "digest biblioRef");
    System.out.println(path + "\n" + contentNode);

    path = "identificationInformation.personType1.sourcing.0.url";
    JsonUtils.addValueToNodeFromPath(path, contentNode, "url");
    System.out.println(path + "\n" + contentNode);

    path = "identificationInformation.personType1.sourcing.0.database.ref";
    JsonUtils.addValueToNodeFromPath(path, contentNode, "ref database");
    System.out.println(path + "\n" + contentNode);

    path = "identificationInformation.personType1.sourcing.0.database.digest";
    JsonUtils.addValueToNodeFromPath(path, contentNode, "digest database");
    System.out.println(path + "\n" + contentNode);

    path = "identificationInformation.personType1.sourcing.0.comment";
    JsonUtils.addValueToNodeFromPath(path, contentNode, "comment");
    System.out.println(path + "\n" + contentNode);

    path = "identificationInformation.personType2.sourcing.0.biblioRef.ref";
    JsonUtils.addValueToNodeFromPath(path, contentNode, "ref biblioRef");
    System.out.println(path + "\n" + contentNode);

    path = "identificationInformation.personType2.sourcing.1.biblioRef.ref";
    JsonUtils.addValueToNodeFromPath(path, contentNode, "ref biblioRef 2");
    System.out.println(path + "\n" + contentNode);
  }

  @Test
  void getSearchableFieldsTest() throws IOException {
    InputStream resourceAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaCollection.json");
    String string = IOUtils.toString(resourceAsStream, "UTF-8");
    Map<String, Float> fields = JsonUtils.getSearchableFields(string);
    assertFalse(fields.isEmpty());
    System.out.println(fields);
  }

  @Test
  void getAutocompleteFieldsTest() throws IOException {
    InputStream resourceAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaCollection.json");
    String string = IOUtils.toString(resourceAsStream, "UTF-8");
    Map<String, Float> fields = JsonUtils.getAutocompleteFields(string);
    assertFalse(fields.isEmpty());
    System.out.println(fields);
  }

  @Test
  void convertNoticeToJsonLdTest() throws IOException {
    InputStream artworkNoticeAsStream = JsonUtilsTest.class.getClassLoader()
        .getResourceAsStream("jsonld/input/artwork.json");
    InputStream schemaArtworkAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaArtwork.json");

    ObjectNode jsonLdArtwork = JsonUtils.convertNoticeToJsonLd(mapper.readTree(schemaArtworkAsStream),
        mapper.readTree(artworkNoticeAsStream));

    InputStream collectionNoticeAsStream = JsonUtilsTest.class.getClassLoader()
        .getResourceAsStream("jsonld/input/collection.json");
    InputStream schemaCollectionAsStream = JsonUtilsTest.class.getClassLoader()
        .getResourceAsStream("schemaCollection.json");

    ObjectNode jsonLdCollection = JsonUtils.convertNoticeToJsonLd(mapper.readTree(schemaCollectionAsStream),
        mapper.readTree(collectionNoticeAsStream));

    InputStream eventNoticeAsStream = JsonUtilsTest.class.getClassLoader()
        .getResourceAsStream("jsonld/input/event.json");
    InputStream schemaEventAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaEvent.json");

    ObjectNode jsonLdEvent = JsonUtils.convertNoticeToJsonLd(mapper.readTree(schemaEventAsStream),
        mapper.readTree(eventNoticeAsStream));

    InputStream personNoticeAsStream = JsonUtilsTest.class.getClassLoader()
        .getResourceAsStream("jsonld/input/person.json");
    InputStream schemaPersonAsStream = JsonUtilsTest.class.getClassLoader().getResourceAsStream("schemaPerson.json");

    ObjectNode jsonLdPerson = JsonUtils.convertNoticeToJsonLd(mapper.readTree(schemaPersonAsStream),
        mapper.readTree(personNoticeAsStream));

    InputStream referenceNoticeAsStream = JsonUtilsTest.class.getClassLoader()
        .getResourceAsStream("jsonld/input/reference.json");
    InputStream schemaReferenceAsStream = JsonUtilsTest.class.getClassLoader()
        .getResourceAsStream("schemaReference.json");

    ObjectNode jsonLdReference = JsonUtils.convertNoticeToJsonLd(mapper.readTree(schemaReferenceAsStream),
        mapper.readTree(referenceNoticeAsStream));

  }

  private void validateAndPrint(InputStream schemaAsStream, InputStream subjectAsStream) throws IOException {
    try {
      JsonUtils.validateJson(IOUtils.toString(schemaAsStream, "UTF-8"), IOUtils.toString(subjectAsStream, "UTF-8"));
    } catch (ValidationException e) {
      System.out.println("Erreur lors de la validation de la notice");
      System.out.println(e.getMessage());
      for (ValidationException exception : e.getCausingExceptions()) {
        System.out.println(exception.getMessage());
      }
      throw e;
    }
  }
}
