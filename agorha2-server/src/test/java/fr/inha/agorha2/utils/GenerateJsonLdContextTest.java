package fr.inha.agorha2.utils;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import fr.inha.elastic.model.NoticeType;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;

class GenerateJsonLdContextTest {

  @Test
  void testGenerateCollectionContext() {

    try {
      File inputPath = new File(this.getClass().getResource("/jsonld/input").getPath());

      generateByNoticeType(inputPath, NoticeType.ARTWORK);
      generateByNoticeType(inputPath, NoticeType.COLLECTION);
      generateByNoticeType(inputPath, NoticeType.EVENT);
      generateByNoticeType(inputPath, NoticeType.PERSON);
      generateByNoticeType(inputPath, NoticeType.REFERENCE);

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  private void generateByNoticeType(File inputPath, NoticeType noticeType) throws IOException {
    GenerateJsonLdContext builder = new GenerateJsonLdContext(inputPath.toString());
    String artworkContext = builder.build(noticeType);
    assertNotNull(artworkContext);

    File outputPath = new File(inputPath, noticeType.name() + "_data.json");
    FileUtils.write(outputPath, artworkContext, "UTF-8");
  }
}
