package fr.inha.agorha2.service.impl;

import static org.junit.jupiter.api.Assertions.assertFalse;

import fr.inha.agorha2.utils.JsonUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;

class SearchServiceImplTest {

  @Test
  void addSearchableFieldsToRequestTest() throws IOException {
    InputStream resourceAsStream = SearchServiceImplTest.class.getClassLoader()
        .getResourceAsStream("schemaCollection.json");
    String string = IOUtils.toString(resourceAsStream, "UTF-8");
    Map<String, Float> fields = JsonUtils.getSearchableFields(string);
    assertFalse(fields.isEmpty());
  }

}
