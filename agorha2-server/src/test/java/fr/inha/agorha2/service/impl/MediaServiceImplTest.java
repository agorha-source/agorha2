package fr.inha.agorha2.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.services.EsVersionableServiceDedicated;
import fr.inha.agorha2.api.dto.MediaItem;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.elastic.config.EsMapperConfigurator;
import fr.inha.elastic.model.Media;
import fr.inha.elastic.model.MediaType;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

@TestInstance(Lifecycle.PER_CLASS)
public class MediaServiceImplTest {

  @InjectMocks
  @Spy
  private MediaServiceImpl mediaService = new MediaServiceImpl();

  @Mock
  private EsVersionableServiceDedicated<Media> mediaServiceDedicated;

  @Mock
  private UserInfoService userInfoService;

  private String uuidMedia = "uuidMedia";

  @BeforeAll
  public void init() throws IOException, ServiceException, EsRequestException {
    EsMapperConfigurator.getInstance().configureMapper(DefaultEsContext.instance().getObjectMapper());
    MockitoAnnotations.initMocks(this);

    Media media = new Media();
    media.getInternal().setUuid(uuidMedia);
    when(mediaServiceDedicated.createOrUpdateAndGet(Mockito.any(Media.class), Mockito.anyBoolean())).thenReturn(media);

    InputStream stream1 = NoticeServiceImplTest.class.getClassLoader().getResourceAsStream("media/oembedYoutube.json");
    Mockito.doReturn(DefaultEsContext.instance().getObjectMapper().readTree(stream1)).when(mediaService)
        .getOembedContent(Mockito.any(URL.class));

    when(userInfoService.hasAnyRole(Arrays.asList(Authority.BATCH.name()))).thenReturn(false);
  }

  @Test
  public void createMediaTest() throws ServiceException {
    List<MediaItem> mediaItems = new ArrayList<>();

    MediaItem mediaItemYoutube = new MediaItem();
    mediaItemYoutube.setMediaType(MediaType.YOUTUBE);
    mediaItemYoutube
        .setInputStream(IOUtils.toInputStream("https://www.youtube.com/watch?v=BomwH3z1mOA", StandardCharsets.UTF_8));
    mediaItems.add(mediaItemYoutube);

    Media draft = new Media();
    draft.setCaption("legend");

    Map<String, Object> media = mediaService.createMedia(mediaItems, draft);

    assertEquals(uuidMedia, media.get("id"));
    assertEquals("legend", media.get("title"));
    assertEquals("https://i.ytimg.com/vi/BomwH3z1mOA/hqdefault.jpg", media.get("thumbnail"));

    mediaItems.clear();

    MediaItem mediaItemRMN = new MediaItem();
    mediaItemRMN.setMediaType(MediaType.RMN);
    mediaItemRMN.setInputStream(IOUtils.toInputStream(
        "{\"permalink\": \"https://www.photo.rmn.fr/archive/-2C6NU0AOKFPY2.html\",\"thumbnail\": \"https://api.art.rmngp.fr/v1/images/502/919022/t?t=rHCpsD_br_XV6vYzZiK_dQ\",\"medium\": \"https://api.art.rmngp.fr/v1/images/502/919022/m?t=rHCpsD_br_XV6vYzZiK_dQ\"}",
        StandardCharsets.UTF_8));
    mediaItems.add(mediaItemRMN);

    draft = new Media();
    draft.setCaption("legend");
    media = mediaService.createMedia(mediaItems, draft);
    assertEquals(uuidMedia, media.get("id"));
    assertEquals("legend", media.get("title"));
    assertEquals("https://api.art.rmngp.fr/v1/images/502/919022/t?t=rHCpsD_br_XV6vYzZiK_dQ", media.get("thumbnail"));
  }

}
