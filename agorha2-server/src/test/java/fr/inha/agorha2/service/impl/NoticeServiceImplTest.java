package fr.inha.agorha2.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.JsonNode;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import fr.inha.agorha2.bean.ThesaurusSiecle;
import fr.inha.agorha2.exception.FunctionnalException;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.service.DigestService;
import fr.inha.agorha2.service.IdentifierServcice;
import fr.inha.agorha2.service.NoticeDataSourceMode;
import fr.inha.agorha2.service.ThesaurusService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.agorha2.service.ValidationService;
import fr.inha.agorha2.utils.DateUtils.PrefixType;
import fr.inha.elastic.EsClient;
import fr.inha.elastic.config.EsMapperConfigurator;
import fr.inha.elastic.model.Notice;
import fr.inha.elastic.model.NoticeStatus;
import fr.inha.elastic.model.NoticeType;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.context.MessageSource;

@TestInstance(Lifecycle.PER_CLASS)
class NoticeServiceImplTest {

  @InjectMocks
  @Spy
  private NoticeServiceImpl noticeService = new NoticeServiceImpl();

  @Mock
  private ValidationService validationService;

  @Mock
  private UserInfoService userInfoService;

  @Mock
  private EsClient esClient;

  @Mock
  private IdentifierServcice identifierService;

  @Mock
  private MessageSource messageSource;

  @Mock
  private ThesaurusService thesaurusService;

  @Spy
  private List<DigestService> digestServices = new ArrayList<>();

  private String uuidArchived = "uuidArchived";
  private String uuidPublished = "uuidPublished";
  private String uuidToPublish = "uuidToPublish";
  private String uuidWIP = "uuidWIP";

  @BeforeAll
  public void init() throws IOException, ServiceException {
    EsMapperConfigurator.getInstance().configureMapper(DefaultEsContext.instance().getObjectMapper());
    MockitoAnnotations.initMocks(this);

    InputStream stream1 = NoticeServiceImplTest.class.getClassLoader()
        .getResourceAsStream("status/personARCHIVED.json");
    InputStream stream2 = NoticeServiceImplTest.class.getClassLoader()
        .getResourceAsStream("status/personPUBLISHED.json");
    InputStream stream3 = NoticeServiceImplTest.class.getClassLoader()
        .getResourceAsStream("status/personTO_PUBLISH.json");
    InputStream stream4 = NoticeServiceImplTest.class.getClassLoader().getResourceAsStream("status/personWIP.json");

    Mockito.doReturn(DefaultEsContext.instance().getObjectMapper().readValue(stream1, Notice.class)).when(noticeService)
        .get(uuidArchived);
    Mockito.doReturn(DefaultEsContext.instance().getObjectMapper().readValue(stream2, Notice.class)).when(noticeService)
        .get(uuidPublished);
    Mockito.doReturn(DefaultEsContext.instance().getObjectMapper().readValue(stream3, Notice.class)).when(noticeService)
        .get(uuidToPublish);
    Mockito.doReturn(DefaultEsContext.instance().getObjectMapper().readValue(stream4, Notice.class)).when(noticeService)
        .get(uuidWIP);

    Mockito.doReturn(null).when(noticeService).save(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
        Mockito.any(NoticeDataSourceMode.class));
    
    Mockito.doNothing().when(noticeService).deleteNoticeInMedia(Mockito.any());

    DigestServicePerson mockDigestServicePerson = mock(DigestServicePerson.class);
    when(mockDigestServicePerson.getType()).thenReturn(NoticeType.PERSON);
    digestServices.add(mockDigestServicePerson);

    when(userInfoService.hasAnyRole(Arrays.asList(Authority.BATCH.name()))).thenReturn(false);

    when(identifierService.getArkIdentifier()).thenReturn(uuidArchived);

    when(messageSource.getMessage("notice.status.update.impossible",
        new Object[] { "En cours", NoticeStatus.PUBLISHED }, null)).thenReturn("Erreur status");

    ThesaurusSiecle thesaurusSiecle = new ThesaurusSiecle();
    thesaurusSiecle.getMapSiecles().put(
        "http://data.culture.fr/thesaurus/resource/ark:/92287c1d-010b-49bd-bb0e-3e82798c9966",
        new ImmutablePair<LocalDate, LocalDate>(LocalDate.of(1801, 1, 1), LocalDate.of(1900, 1, 1)));
    thesaurusSiecle.getMapSiecles().put(
        "http://data.culture.fr/thesaurus/resource/ark:/e04418b2-7f12-4ee4-a1f4-f6473e58d1ca",
        new ImmutablePair<LocalDate, LocalDate>(LocalDate.of(1601, 1, 1), LocalDate.of(1700, 1, 1)));
    thesaurusSiecle.getMapSiecles().put(
        "http://data.culture.fr/thesaurus/resource/ark:/bcca693b-1801-4556-a395-007f7a287b2f",
        new ImmutablePair<LocalDate, LocalDate>(LocalDate.of(-1900, 1, 1), LocalDate.of(-1801, 1, 1)));
    when(thesaurusService.getThesaurusSiecle()).thenReturn(thesaurusSiecle);

    Map<String, PrefixType> thesaurusPrefix = new HashMap<>();
    thesaurusPrefix.put("AVANT", PrefixType.BEFORE);
    thesaurusPrefix.put("APRES", PrefixType.AFTER);
    thesaurusPrefix.put("VERS", PrefixType.NEAR);
    when(thesaurusService.getThesaurusPrefixe()).thenReturn(thesaurusPrefix);
  }

  @Test
  void testCreate() throws ServiceException {
    assertNull(noticeService.create("{\"internal\":{\"noticeType\":\"PERSON\"},\"content\":{\"blablab\":\"blabla\"}}",
        NoticeDataSourceMode.CONTRIBUTION));
  }

  @Test
  void testUpdateStatusAsAdmin() throws ServiceException, EsRequestException, IOException, FunctionnalException {
    when(userInfoService.hasAnyRole(Arrays.asList(Authority.ADMIN.name()))).thenReturn(true);
    List<Map<String, String>> errors = noticeService.upddateStatus(Arrays.asList(uuidWIP), NoticeStatus.PUBLISHED);
    assertTrue(errors.isEmpty());
  }

  @Test
  void testVerifyStatusAsAdmin() throws ServiceException, EsRequestException, IOException, FunctionnalException {
    boolean isAdmin = true;
    boolean isDatabaseManager = false;

    // Test des changements interdits
    List<Map<String, String>> errors = new ArrayList<>();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidArchived,
        NoticeStatus.ARCHIVED.getValue(), NoticeStatus.PUBLISHED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidPublished,
        NoticeStatus.PUBLISHED.getValue(), NoticeStatus.PUBLISHED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidToPublish,
        NoticeStatus.TO_PUBLISH.getValue(), NoticeStatus.ARCHIVED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidToPublish,
        NoticeStatus.TO_PUBLISH.getValue(), NoticeStatus.TO_PUBLISH, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class,
        () -> noticeService.verifyNoticeStatus(uuidWIP, NoticeStatus.CREATION_IN_PROGRESS.getValue(),
            NoticeStatus.CREATION_IN_PROGRESS, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidWIP,
        NoticeStatus.CREATION_IN_PROGRESS.getValue(), NoticeStatus.ARCHIVED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    // Test des changements autorisés
    errors.clear();
    noticeService.verifyNoticeStatus(uuidPublished, NoticeStatus.PUBLISHED.getValue(), NoticeStatus.TO_PUBLISH, errors,
        isAdmin, isDatabaseManager);
    assertTrue(errors.isEmpty());
    noticeService.verifyNoticeStatus(uuidPublished, NoticeStatus.PUBLISHED.getValue(),
        NoticeStatus.CREATION_IN_PROGRESS, errors, isAdmin, isDatabaseManager);
    assertTrue(errors.isEmpty());
    noticeService.verifyNoticeStatus(uuidPublished, NoticeStatus.PUBLISHED.getValue(), NoticeStatus.ARCHIVED, errors,
        isAdmin, isDatabaseManager);
    assertTrue(errors.isEmpty());
    noticeService.verifyNoticeStatus(uuidWIP, NoticeStatus.CREATION_IN_PROGRESS.getValue(), NoticeStatus.TO_PUBLISH,
        errors, isAdmin, isDatabaseManager);
    assertTrue(errors.isEmpty());
    noticeService.verifyNoticeStatus(uuidWIP, NoticeStatus.CREATION_IN_PROGRESS.getValue(), NoticeStatus.PUBLISHED,
        errors, isAdmin, isDatabaseManager);
    assertTrue(errors.isEmpty());
    noticeService.verifyNoticeStatus(uuidToPublish, NoticeStatus.TO_PUBLISH.getValue(), NoticeStatus.PUBLISHED, errors,
        isAdmin, isDatabaseManager);
    assertTrue(errors.isEmpty());
    noticeService.verifyNoticeStatus(uuidToPublish, NoticeStatus.TO_PUBLISH.getValue(),
        NoticeStatus.CREATION_IN_PROGRESS, errors, isAdmin, isDatabaseManager);
    assertTrue(errors.isEmpty());

  }

  @Test
  void testVerifyStatusAsDatabaseManager()
      throws ServiceException, EsRequestException, IOException, FunctionnalException {
    boolean isAdmin = false;
    boolean isDatabaseManager = true;

    // Test des changments interdits
    List<Map<String, String>> errors = new ArrayList<>();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidArchived,
        NoticeStatus.ARCHIVED.getValue(), NoticeStatus.PUBLISHED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidPublished,
        NoticeStatus.PUBLISHED.getValue(), NoticeStatus.PUBLISHED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidPublished,
        NoticeStatus.PUBLISHED.getValue(), NoticeStatus.CREATION_IN_PROGRESS, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidPublished,
        NoticeStatus.PUBLISHED.getValue(), NoticeStatus.ARCHIVED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidPublished,
        NoticeStatus.PUBLISHED.getValue(), NoticeStatus.TO_PUBLISH, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidToPublish,
        NoticeStatus.TO_PUBLISH.getValue(), NoticeStatus.ARCHIVED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidToPublish,
        NoticeStatus.TO_PUBLISH.getValue(), NoticeStatus.TO_PUBLISH, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidToPublish,
        NoticeStatus.TO_PUBLISH.getValue(), NoticeStatus.PUBLISHED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class,
        () -> noticeService.verifyNoticeStatus(uuidWIP, NoticeStatus.CREATION_IN_PROGRESS.getValue(),
            NoticeStatus.CREATION_IN_PROGRESS, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidWIP,
        NoticeStatus.CREATION_IN_PROGRESS.getValue(), NoticeStatus.ARCHIVED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    errors.clear();
    assertThrows(FunctionnalException.class, () -> noticeService.verifyNoticeStatus(uuidWIP,
        NoticeStatus.CREATION_IN_PROGRESS.getValue(), NoticeStatus.PUBLISHED, errors, isAdmin, isDatabaseManager));
    assertFalse(errors.isEmpty());

    // Test des changements autorisés
    errors.clear();
    noticeService.verifyNoticeStatus(uuidWIP, NoticeStatus.CREATION_IN_PROGRESS.getValue(), NoticeStatus.TO_PUBLISH,
        errors, isAdmin, isDatabaseManager);
    assertTrue(errors.isEmpty());
    noticeService.verifyNoticeStatus(uuidToPublish, NoticeStatus.TO_PUBLISH.getValue(),
        NoticeStatus.CREATION_IN_PROGRESS, errors, isAdmin, isDatabaseManager);
    assertTrue(errors.isEmpty());

  }

  @Test
  void testDatesComputed() throws IOException {
    InputStream resourceAsStream = NoticeServiceImplTest.class.getClassLoader()
        .getResourceAsStream("date/dateSimple.json");

    JsonNode readTree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    readTree.at("/data").forEach(node -> {
      noticeService.updateDateComputed(node.at("/date"));
      assertEquals(node.at("/date/startExpected").asText(), node.at("/date/startDateComputed").asText());
      assertEquals(node.at("/date/endExpected").asText(), node.at("/date/endDateComputed").asText());
    });

    resourceAsStream = NoticeServiceImplTest.class.getClassLoader().getResourceAsStream("date/datePeriod.json");

    readTree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    readTree.at("/data").forEach(node -> {
      noticeService.updateDateComputed(node.at("/date"));
      assertEquals(node.at("/date/startExpected").asText(), node.at("/date/startDateComputed").asText());
      assertEquals(node.at("/date/endExpected").asText(), node.at("/date/endDateComputed").asText());
    });
  }

}
