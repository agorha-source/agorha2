package fr.inha.agorha2.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.JsonNode;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.elastic.config.EsMapperConfigurator;
import java.io.InputStream;
import javax.ws.rs.WebApplicationException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

@TestInstance(Lifecycle.PER_CLASS)
class DigestServiceTest {

  @InjectMocks
  @Spy
  private DigestServicePerson digestServicePerson = new DigestServicePerson();

  @InjectMocks
  @Spy
  private DigestServiceArtwork digestServiceArtwork = new DigestServiceArtwork();

  @InjectMocks
  @Spy
  private DigestServiceCollection digestServiceCollection = new DigestServiceCollection();

  @InjectMocks
  @Spy
  private DigestServiceEvent digestServiceEvent = new DigestServiceEvent();

  @InjectMocks
  @Spy
  private DigestServiceReference digestServiceReference = new DigestServiceReference();

  @Mock
  private NoticeServiceImpl noticeService;

  @BeforeAll
  public void init() throws ServiceException {
    EsMapperConfigurator.getInstance().configureMapper(DefaultEsContext.instance().getObjectMapper());
    MockitoAnnotations.initMocks(this);
    Mockito.doThrow(WebApplicationException.class).when(noticeService).get(Mockito.any(String.class));
  }

  @Test
  void buildAndUpdateCollectionTest() throws Exception {
    InputStream resourceAsStream = DigestServiceTest.class.getClassLoader().getResourceAsStream("dataCollection.json");
    JsonNode tree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    digestServiceCollection.build(tree);

    JsonNode digest = tree.at("/internal/digest");
    assertEquals("2009-09-03T18:07:54Z", digest.get("updatedDate").asText());
    assertEquals("titre", digest.get("title").asText());
    assertEquals("type", digest.get("collectionType").asText());
    assertEquals("21/07/1864", digest.get("startDate").get("earliest").asText());
    assertEquals("yyyy", digest.get("startDate").get("earliestFormat").asText());
    assertEquals("21/07/1864", digest.get("startDate").get("latest").asText());
    assertEquals("yyyy", digest.get("startDate").get("latestFormat").asText());
    assertEquals("prefix start", digest.get("startDate").get("prefix").asText());
    assertEquals("21/07/1864", digest.get("endDate").get("earliest").asText());
    assertEquals("yyyy", digest.get("endDate").get("earliestFormat").asText());
    assertEquals("21/07/1864", digest.get("endDate").get("latest").asText());
    assertEquals("yyyy", digest.get("endDate").get("latestFormat").asText());
    assertEquals("prefix end", digest.get("endDate").get("prefix").asText());
    assertEquals("COLLECTION", digest.get("noticeType").asText());
    assertEquals("contet", digest.get("databases").get(0).asText());
    assertEquals("titre (prefix start 21/07/1864 / 21/07/1864 - prefix end 21/07/1864 / 21/07/1864)",
        digest.get("displayLabelLink").asText());

  }

  @Test
  void buildAndUpdateArtworkTest() throws Exception {
    InputStream resourceAsStream = DigestServiceTest.class.getClassLoader().getResourceAsStream("dataArtwork.json");
    JsonNode tree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    digestServiceArtwork.build(tree);

    JsonNode digest = tree.at("/internal/digest");
    assertEquals("2009-09-03T18:07:54Z", digest.get("updatedDate").asText());
    assertEquals("le titre", digest.get("title").asText());
    assertEquals("artworkType", digest.get("artworkType").asText());
    assertEquals("authorRole", digest.get("authorRole").asText());
    assertEquals("21/07/1864", digest.get("datationExecutionStart").get("earliest").asText());
    assertEquals("yyyy", digest.get("datationExecutionStart").get("earliestFormat").asText());
    assertEquals("21/07/1864", digest.get("datationExecutionStart").get("latest").asText());
    assertEquals("yyyy", digest.get("datationExecutionStart").get("latestFormat").asText());
    assertEquals("prefix start", digest.get("datationExecutionStart").get("prefix").asText());
    assertEquals("21/07/1864", digest.get("datationExecutionEnd").get("earliest").asText());
    assertEquals("yyyy", digest.get("datationExecutionEnd").get("earliestFormat").asText());
    assertEquals("21/07/1864", digest.get("datationExecutionEnd").get("latest").asText());
    assertEquals("yyyy", digest.get("datationExecutionEnd").get("latestFormat").asText());
    assertEquals("prefix end", digest.get("datationExecutionEnd").get("prefix").asText());
    assertEquals("type", digest.get("place").asText());
    assertEquals("contenu", digest.get("shelfMark").asText());
    assertEquals("type", digest.get("shelfMarkType").asText());
    assertEquals("ARTWORK", digest.get("noticeType").asText());
    assertEquals("contet", digest.get("databases").get(0).asText());
    assertEquals("le titre, artworkType (prefix start 21/07/1864 / 21/07/1864 - prefix end 21/07/1864 / 21/07/1864)",
        digest.get("displayLabelLink").asText());
  }

  @Test
  void buildAndUpdateEventTest() throws Exception {
    InputStream resourceAsStream = DigestServiceTest.class.getClassLoader().getResourceAsStream("dataEvent.json");
    JsonNode tree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    digestServiceEvent.build(tree);

    JsonNode digest = tree.at("/internal/digest");
    assertEquals("2009-09-03T18:07:54Z", digest.get("updatedDate").asText());
    assertEquals("titre", digest.get("title").asText());
    assertEquals("type", digest.get("eventType").asText());
    assertEquals("21/07/1864", digest.get("startDate").get("earliest").asText());
    assertEquals("yyyy", digest.get("startDate").get("earliestFormat").asText());
    assertEquals("21/07/1864", digest.get("startDate").get("latest").asText());
    assertEquals("yyyy", digest.get("startDate").get("latestFormat").asText());
    assertEquals("prefix start", digest.get("startDate").get("prefix").asText());
    assertEquals("21/07/1864", digest.get("endDate").get("earliest").asText());
    assertEquals("yyyy", digest.get("endDate").get("earliestFormat").asText());
    assertEquals("21/07/1864", digest.get("endDate").get("latest").asText());
    assertEquals("yyyy", digest.get("endDate").get("latestFormat").asText());
    assertEquals("prefix end", digest.get("endDate").get("prefix").asText());
    assertEquals("place", digest.get("place").asText());
    assertEquals("EVENT", digest.get("noticeType").asText());
    assertEquals("contet", digest.get("databases").get(0).asText());
    assertEquals("titre (prefix start 21/07/1864 / 21/07/1864 - prefix end 21/07/1864 / 21/07/1864), place",
        digest.get("displayLabelLink").asText());
  }

  @Test
  void buildAndUpdatePersonTest() throws Exception {
    InputStream resourceAsStream = DigestServiceTest.class.getClassLoader().getResourceAsStream("dataPerson.json");
    JsonNode tree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    digestServicePerson.build(tree);

    JsonNode digest = tree.at("/internal/digest");
    assertEquals("2009-09-03T18:07:54Z", digest.get("updatedDate").asText());
    assertEquals("collectivité", digest.get("personType").asText());
    assertEquals("usualName, usualFirstName", digest.get("title").asText());
    assertEquals("usualName", digest.get("usualName").asText());
    assertEquals("usualFirstName", digest.get("usualFirstName").asText());
    assertEquals("21/07/1864", digest.get("startDate").get("earliest").asText());
    assertEquals("yyyy", digest.get("startDate").get("earliestFormat").asText());
    assertEquals("21/07/1864", digest.get("startDate").get("latest").asText());
    assertEquals("yyyy", digest.get("startDate").get("latestFormat").asText());
    assertEquals("prefix start", digest.get("startDate").get("prefix").asText());
    assertEquals("21/07/1864", digest.get("endDate").get("earliest").asText());
    assertEquals("yyyy", digest.get("endDate").get("earliestFormat").asText());
    assertEquals("21/07/1864", digest.get("endDate").get("latest").asText());
    assertEquals("yyyy", digest.get("endDate").get("latestFormat").asText());
    assertEquals("prefix end", digest.get("endDate").get("prefix").asText());
    assertEquals("type", digest.get("activityType").asText());
    assertEquals("type", digest.get("place").asText());
    assertEquals("PERSON", digest.get("noticeType").asText());
    assertEquals("city", digest.get("city").asText());
    assertEquals("contet", digest.get("databases").get(0).asText());
    assertEquals(
        "usualName, usualFirstName (prefix start 21/07/1864 / 21/07/1864 - prefix end 21/07/1864 / 21/07/1864), city",
        digest.get("displayLabelLink").asText());
  }

  @Test
  void buildAndUpdatePersonSiecleTest() throws Exception {
    InputStream resourceAsStream = DigestServiceTest.class.getClassLoader()
        .getResourceAsStream("digest/personCharreEugene.json");
    JsonNode tree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    digestServicePerson.build(tree);

    JsonNode digest = tree.at("/internal/digest");
    assertEquals("19e siècle", digest.get("startDate").get("earliest").asText());
    assertEquals("19e siècle", digest.get("endDate").get("earliest").asText());
    assertEquals("Charre, Eugène (19e siècle)", digest.get("displayLabelLink").asText());
  }

  @Test
  void buildAndUpdatePersonDateEtablissementTest() throws Exception {
    InputStream resourceAsStream = DigestServiceTest.class.getClassLoader()
        .getResourceAsStream("digest/personDateEtablissement.json");
    JsonNode tree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    digestServicePerson.build(tree);

    JsonNode digest = tree.at("/internal/digest");
    assertEquals("1891", digest.get("startDate").get("earliest").asText());
    assertEquals("1901", digest.get("endDate").get("earliest").asText());
    assertEquals("Société des amis des arts de Cognac [fiche incomplète] (1891 - 1901)",
        digest.get("displayLabelLink").asText());
  }

  @Test
  void buildAndUpdatePersonDateActiviteTest() throws Exception {
    InputStream resourceAsStream = DigestServiceTest.class.getClassLoader()
        .getResourceAsStream("digest/personDateActivite.json");
    JsonNode tree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    digestServicePerson.build(tree);

    JsonNode digest = tree.at("/internal/digest");
    assertEquals("2e moitié du 6e siècle avant notre ère", digest.get("startDate").get("earliest").asText());
    assertEquals("1ère moitié du 5e siècle avant notre ère", digest.get("endDate").get("earliest").asText());
    assertEquals(" (2e moitié du 6e siècle avant notre ère - 1ère moitié du 5e siècle avant notre ère)",
        digest.get("displayLabelLink").asText());
  }

  @Test
  void buildAndUpdateReferenceTest() throws Exception {
    InputStream resourceAsStream = DigestServiceTest.class.getClassLoader().getResourceAsStream("dataReference.json");
    JsonNode tree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    digestServiceReference.build(tree);

    JsonNode digest = tree.at("/internal/digest");
    assertEquals("2009-09-03T18:07:54Z", digest.get("updatedDate").asText());
    assertEquals("titre", digest.get("title").asText());
    assertEquals("type", digest.get("referenceType").asText());
    assertEquals("journalTitle", digest.get("journalTitle").asText());
    assertEquals("authorName", digest.get("authorName").asText());
    assertEquals("21/07/1864", digest.get("startDate").get("earliest").asText());
    assertEquals("yyyy", digest.get("startDate").get("earliestFormat").asText());
    assertEquals("21/07/1864", digest.get("startDate").get("latest").asText());
    assertEquals("yyyy", digest.get("startDate").get("latestFormat").asText());
    assertEquals("prefix start", digest.get("startDate").get("prefix").asText());
    assertEquals("21/07/1864", digest.get("endDate").get("earliest").asText());
    assertEquals("yyyy", digest.get("endDate").get("earliestFormat").asText());
    assertEquals("21/07/1864", digest.get("endDate").get("latest").asText());
    assertEquals("yyyy", digest.get("endDate").get("latestFormat").asText());
    assertEquals("prefix end", digest.get("endDate").get("prefix").asText());
    assertEquals("editionPlace", digest.get("editionPlace").asText());
    assertEquals("contenu", digest.get("editor").asText());
    assertEquals("institution", digest.get("institution").asText());
    assertEquals("shelfMark", digest.get("shelfMark").asText());
    assertEquals("REFERENCE", digest.get("noticeType").asText());
    assertEquals("contet", digest.get("databases").get(0).asText());
    assertEquals(
        "authorName, titre, <i>journalTitle</i>, editionPlace prefix start 21/07/1864 / 21/07/1864 - prefix end 21/07/1864 / 21/07/1864, institution, shelfMark",
        digest.get("displayLabelLink").asText());
  }

  @Test
  void buildAndUpdateReferenceTitreOuvrageTest() throws Exception {
    InputStream resourceAsStream = DigestServiceTest.class.getClassLoader()
        .getResourceAsStream("digest/referenceTitreOuvrage.json");
    JsonNode tree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    digestServiceReference.build(tree);

    JsonNode digest = tree.at("/internal/digest");
    assertEquals(
        "La correspondance des membres de l'Académie des Sciences, Belles-Lettres et Arts de Rouen de 1744 à 1793 ",
        digest.get("title").asText());
    assertEquals(
        "<i>La correspondance des membres de l'Académie des Sciences, Belles-Lettres et Arts de Rouen de 1744 à 1793 </i>",
        digest.get("displayLabelLink").asText());
  }

  @Test
  void buildAndUpdateReferenceDescriptionLongueTest() throws Exception {
    InputStream resourceAsStream = DigestServiceTest.class.getClassLoader()
        .getResourceAsStream("digest/referenceDescriptionLongue.json");
    JsonNode tree = DefaultEsContext.instance().getObjectMapper().readTree(resourceAsStream);
    digestServiceReference.build(tree);

    JsonNode digest = tree.at("/internal/digest");
    assertEquals(
        "TEST GYD La Cour des comptes est l’institution supérieure de contrôle chargée de vérifier l’emploi d [...]",
        digest.get("title").asText());
    assertEquals(
        "TEST GYD La Cour des comptes est l’institution supérieure de contrôle chargée de vérifier l’emploi d [...]",
        digest.get("displayLabelLink").asText());
  }
}
