package fr.inha.agorha2.service.impl;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;

import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.ThesaurusService;
import fr.inha.agorha2.utils.DateUtils.PrefixType;
import fr.inha.elastic.config.EsMapperConfigurator;

@TestInstance(Lifecycle.PER_CLASS)
class ImportServiceImplTest {

  @InjectMocks
  @Spy
  private ImportServiceImpl importService = new ImportServiceImpl();

  @Mock
  private ThesaurusService thesaurusService;

  @BeforeAll
  public void init() throws IOException, ServiceException, EsRequestException {
    EsMapperConfigurator.getInstance().configureMapper(DefaultEsContext.instance().getObjectMapper());
    MockitoAnnotations.initMocks(this);

    Map<String, PrefixType> thesaurusPrefix = new HashMap<>();
    thesaurusPrefix.put("AVANT", PrefixType.BEFORE);
    thesaurusPrefix.put("APRES", PrefixType.AFTER);
    thesaurusPrefix.put("VERS", PrefixType.NEAR);
    when(thesaurusService.getThesaurusPrefixe()).thenReturn(thesaurusPrefix);
  }

  @Test
  void importCsvPersonTest() throws ServiceException, IOException {
    InputStream stream1 = ImportServiceImplTest.class.getClassLoader().getResourceAsStream("import/personnes.csv");
    CSVParser parser = CSVParser.parse(stream1, StandardCharsets.UTF_8,
        CSVFormat.EXCEL.withDelimiter(';').withFirstRecordAsHeader());
    JsonNode schema = new ObjectMapper()
        .readTree(ImportServiceImplTest.class.getClassLoader().getResourceAsStream("schemaPerson.json"));
    Map<String, Integer> headerMap = parser.getHeaderMap();
    for (CSVRecord csvRecord : parser) {
      ObjectNode noticeNode = importService.createNoticeFromRecord(csvRecord, headerMap, schema);
      assertEquals("22/10/1943",
          noticeNode.at("/content/identificationInformation/life/0/birth/date/start/earliest/date").asText());
    }
  }

  @Test
  void importCsvArtworkTest() throws ServiceException, IOException {
    InputStream stream1 = ImportServiceImplTest.class.getClassLoader().getResourceAsStream("import/oeuvres.csv");
    CSVParser parser = CSVParser.parse(stream1, StandardCharsets.UTF_8,
        CSVFormat.EXCEL.withDelimiter(';').withFirstRecordAsHeader());
    JsonNode schema = new ObjectMapper()
        .readTree(ImportServiceImplTest.class.getClassLoader().getResourceAsStream("schemaArtwork.json"));
    Map<String, Integer> headerMap = parser.getHeaderMap();
    for (CSVRecord csvRecord : parser) {
      ObjectNode noticeNode = importService.createNoticeFromRecord(csvRecord, headerMap, schema);
      assertThat(noticeNode.at("/content/descriptionInformation/dimension/0/height").isFloat()).isTrue();
      assertThat(noticeNode.at("/content/descriptionInformation/dimension/0/width").isFloat()).isTrue();
    }
  }
}
