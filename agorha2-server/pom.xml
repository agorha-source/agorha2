<?xml version="1.0"?>
<project
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd"
	xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>fr.inha</groupId>
		<artifactId>agorha2</artifactId>
		<version>1.9.6</version>
	</parent>

	<artifactId>agorha2-server</artifactId>
	<name>agorha2-server</name>
	<packaging>jar</packaging>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
	</properties>

	<dependencies>
		<dependency>
			<groupId>fr.inha</groupId>
			<artifactId>agorha2-elastic</artifactId>
			<version>${project.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-spring-boot-starter-jaxrs</artifactId>
		</dependency>

		<dependency>
			<groupId>org.webjars</groupId>
			<artifactId>swagger-ui</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-rs-service-description-openapi-v3</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-features-logging</artifactId>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
		</dependency>
		<dependency>
			<groupId>com.github.everit-org.json-schema</groupId>
			<artifactId>org.everit.json.schema</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.tika</groupId>
			<artifactId>tika-parsers</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.pdfbox</groupId>
			<artifactId>pdfbox</artifactId>
		</dependency>
		<dependency>
			<groupId>com.github.jai-imageio</groupId>
			<artifactId>jai-imageio-jpeg2000</artifactId>
		</dependency>

		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
		</dependency>
		<dependency>
			<groupId>commons-codec</groupId>
			<artifactId>commons-codec</artifactId>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-text</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-ldap</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.data</groupId>
			<artifactId>spring-data-ldap</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-aop</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-cache</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>javax.cache</groupId>
			<artifactId>cache-api</artifactId>
		</dependency>
		<dependency>
			<groupId>org.ehcache</groupId>
			<artifactId>ehcache</artifactId>
		</dependency>

		<dependency>
			<groupId>org.apache.cxf</groupId>
			<artifactId>cxf-rt-transports-http-jetty</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
			<optional>true</optional>
		</dependency>

		<dependency>
			<groupId>javax.xml.ws</groupId>
			<artifactId>jaxws-api</artifactId>
			<version>2.3.0</version>
			<optional>true</optional>
		</dependency>

		<dependency>
			<groupId>javax.mail</groupId>
			<artifactId>javax.mail-api</artifactId>
		</dependency>

		<dependency>
			<groupId>com.sun.mail</groupId>
			<artifactId>javax.mail</artifactId>
		</dependency>

		<dependency>
			<groupId>de.digitalcollections.iiif</groupId>
			<artifactId>iiif-apis</artifactId>
		</dependency>

		<dependency>
			<groupId>org.jdom</groupId>
			<artifactId>jdom2</artifactId>
		</dependency>

		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<scope>test</scope>
		</dependency>

		<dependency>
			<groupId>com.github.jsonld-java</groupId>
			<artifactId>jsonld-java</artifactId>
		</dependency>

		<dependency>
			<groupId>org.apache.jena</groupId>
			<artifactId>apache-jena-libs</artifactId>
			<type>pom</type>
		</dependency>

		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi</artifactId>
		</dependency>

		<!-- <dependency> -->
		<!-- <groupId>org.apache.poi</groupId> -->
		<!-- <artifactId>poi-ooxml</artifactId> -->
		<!-- </dependency> -->
	</dependencies>

	<profiles>
		<!-- Package as an executable jar, run it with : mvn clean install -Pjar 
			-DskipTests (within a profile because it won't be a Maven-compatible jar 
			anymore, because regular classes and MANIFEST are replaced by a specific 
			loader so that the jar can embed ("shade") all dependency jars) -->
		<profile>
			<id>jar</id>
			<build>
				<!-- Create jar named without -version.jar else cumbersome for .conf 
					when full executable, so rather putting version along with buildNumber in 
					MANIFEST : -->
				<finalName>${project.artifactId}</finalName>
				<plugins>
					<plugin>
						<groupId>org.springframework.boot</groupId>
						<artifactId>spring-boot-maven-plugin</artifactId>
						<configuration>
							<!-- Not using JVM arguments, because only works when starting using 
								mvn, (and both args don't work together because of quoting problems) rather 
								set env props in jarfilename.conf : see https://github.com/spring-projects/spring-boot/issues/5505#issuecomment-202239406 -->
							<!-- jvmArguments> -Dloader.path=".,BOOT-INF/lib/" -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8002 
								</jvmArguments -->
							<!-- Not using embeddedLaunchScriptProperties, because can't conf 
								JAVA_OPTS, see http://mrhaki.blogspot.fr/2016/06/grails-goodness-creating-fully.html -->
							<!-- embeddedLaunchScriptProperties> <initInfoDescription>myDesc</initInfoDescription> 
								</embeddedLaunchScriptProperties -->
							<!-- profiles> <profile>default</profile> </profiles -->
							<executable>true</executable><!-- creates a "fully executable" jar 
								http://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html -->
							<layout>ZIP</layout><!-- similar to the JAR layout using PropertiesLauncher 
								http://docs.spring.io/spring-boot/docs/current/maven-plugin/usage.html PropertiesLauncher 
								behaves the same as JarLauncher when no additional configuration is provided 
								http://docs.spring.io/spring-boot/docs/current/reference/html/executable-jar.html -->
						</configuration>
						<executions>
							<execution>
								<goals>
									<goal>repackage</goal><!-- create a jar or war file that is auto-executable 
										The plugin rewrites your manifest, and in particular it manages the Main-Class 
										and Start-Class entries http://docs.spring.io/spring-boot/docs/current/maven-plugin/usage.html -->
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
				<resources>
					<resource>
						<directory>${basedir}/src/main/resources</directory>
					</resource>
					<resource>
						<directory>${basedir}/config</directory>
					</resource>
				</resources>
			</build>

		</profile>
		<profile>
			<!-- Définition d'une conf pour le profile docker pour empêcher le comportement 
				par défault du docker-maven-plugin qui lance le dockfile à la racine du projet 
				si il ne trouve pas de conf dans le pom -->
			<id>docker</id>
			<build>
				<plugins>
					<plugin>
						<groupId>io.fabric8</groupId>
						<artifactId>docker-maven-plugin</artifactId>
						<configuration>
							<images>
								<image>
									<name>agorha2/server</name>
									<alias>server</alias>
									<build>
										<skip>true</skip>
									</build>
									<run>
										<skip>true</skip>
									</run>
								</image>
							</images>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		<profile>
			<id>server</id>
			<build>
				<plugins>
					<plugin>
						<artifactId>maven-resources-plugin</artifactId>
						<executions>
							<execution>
								<id>copy-client-content</id>
								<phase>generate-resources</phase>
								<goals>
									<goal>copy-resources</goal>
								</goals>
								<configuration>
									<outputDirectory>${project.build.directory}/classes/public</outputDirectory>
									<overwrite>true</overwrite>
									<resources>
										<resource>
											<directory>../client/target/dist</directory>
										</resource>
									</resources>
								</configuration>
							</execution>
						</executions>
					</plugin>
					<plugin>
						<groupId>io.fabric8</groupId>
						<artifactId>docker-maven-plugin</artifactId>
						<configuration>
							<images>
								<image>
									<name>agorha2/api</name>
									<alias>api</alias>
									<build>
										<dockerFileDir>${project.basedir}</dockerFileDir>
										<tags>
											<tag>0.0.1</tag>
										</tags>
									</build>
									<run>
										<skip>true</skip>
									</run>
								</image>
							</images>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>${maven.surefire.version}</version>
				<dependencies>
					<dependency>
						<groupId>org.junit.jupiter</groupId>
						<artifactId>junit-jupiter-engine</artifactId>
						<version>${junit.version}</version>
					</dependency>
				</dependencies>
			</plugin>
		</plugins>
	</build>
</project>
