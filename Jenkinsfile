pipeline {
  agent none
  stages {
    stage('Build') {
      agent {
        docker {
          image 'maven:3.6.1-jdk-11-slim'
          args '-v $PWD:/src/mymaven/ -v /root/.m2:/root/.m2'
        }
      }
      steps { sh 'mvn clean package -DskipTests=true' }
    }

    stage('Build and integration tests') {
      agent {
        docker {
          image 'maven:3.6.1-jdk-11-slim'
          args '-v $PWD:/src/mymaven/ -v /root/.m2:/root/.m2'
        }
      }
      steps { 
        script {     
          if (env.BRANCH_NAME == 'develop') {
            sh 'mvn docker:stop -Pdocker,int'
            sh 'mvn docker:stop -Pserver,int'
            sh 'mvn docker:volume-remove docker:build docker:start -Pdocker,int'
            sh 'mvn clean install -Pit,int'
          }
          else {
            sh 'mvn clean package'
          }
        }
      }
    }

    stage('Sonar Analysis') {
      agent {
        docker {
          image 'maven:3.6.1-jdk-11-slim'
          args '-v $PWD:/src/mymaven/ -v /root/.m2:/root/.m2'
        }
      }
      when { expression { branch 'develop' } }
      steps {
        withSonarQubeEnv('SonarQube SSL') { sh 'mvn compile sonar:sonar -Dsonar.pitest.mode=reuseReport -Dsonar.branch=$BRANCH_NAME' }
      }
    }
    
    stage('CheckStyle') {
      agent {
        docker {
          image 'maven:3.6.1-jdk-11-slim'
          args '-v $PWD:/src/mymaven/ -v /root/.m2:/root/.m2'
        }
      }
      steps { sh 'mvn -T 1C checkstyle:check' }
    }

    stage('Quality Gate') {
      agent none
      steps {
        script {
          timeout(time: 1, unit: 'HOURS') {
            def qg = waitForQualityGate()
            if (qg.status != 'OK') {
              error "Pipeline aborted due to quality gate failure: ${qg.status}"
            }
          }
        }
      }
    }
    
    stage('Deploy on integration server') {
      agent {
        docker {
          image 'maven:3.6.1-jdk-11-slim'
          args '-v $PWD:/src/mymaven/ -v /root/.m2:/root/.m2'
        }
      }
      steps { 
        script {     
          if (env.BRANCH_NAME == 'develop') {
            sh 'mvn package'
            sh 'mvn package -Pjar'
            sh 'mvn docker:build docker:start -Pserver,int'
          }
        }
      }
    }
  }
  post {
    changed {
      mail to: 'MLFRLYOSSLAGORHAINHA@sword-group.com',
      from: 'admin@jenkins-demo-ssl.lan',
      subject: "Job '${env.JOB_NAME}' - (${env.BUILD_NUMBER}) " + currentBuild.currentResult,
      body: "URL is ${env.BUILD_URL}"
    }
  }
}
