#  Agorha2

## Sword developer quickstart

Build Requirement : 
```
Java 11
Maven 3.5.4
```

Create a project folder : 
```bash
mkdir agorha2
cd agorha2
```

Clone dependencies into the folder :

```bash
git clone https://ssl-gitlab.sword-group.com/inha/agorha2/agorha2.git

#git checkout develop (if needed)
mvn clean install -DskipTests
```

Build and test :

```bash
mvn clean install
```

Change directory to be in server project :

```bash
cd server
```

Run it in development environment :

```bash
mvn spring-boot:run
```

or build an executable jar and run it :

```bash
mvn clean install -DskipTests
mvn clean install -DskipTests -Pjar
```

or run "fully executable" jar (linux only) :

```bash
cp target/agorha2-server.jar . && ./agorha2-server.jar
```

or on windows as a regular jar :

```bash
java -Dloader.path=.\,BOOT-INF/lib/ -jar target/agorha2-server.jar
```

## Configuration :

Default :
- app : [application.yml](agorha2-server/src/main/resources/application.yml)
- and its logging : [logback.xml](agorha2-server/src/main/resources/logback.xml)

# Developers

## Configuration eclipse

### Checkstyle

Installer le plugin checkstyle dans eclipse et utiliser le fichier checkstyle.xml présent à la racine du projet.
* Aller dans preference => checkstyle => new 
	* Choisir "Projet Relative Configuration" et choisir l'emplacement du fichier checkstyle.xml

### Formatter

Utiliser le fichier agorha2-formatter.xml présent à la racine du projet.
* Aller dans preference => Java => Code Style => Formatter => Import...
	* Choisir le fichier agorha2-formatter.xml

### Import

Utiliser le fichier agorha2.importorder présent à la racine du projet.
* Aller dans preference => Java => Code Style => Organize Imports => Import...
	* Choisir le fichier agorha2.importorder

## Docker
### Utilisation des volumes
* Le volume utilisé pour ES est configuré dans le pom (<b>docker.host.volume</b>), le repértoire de la machine hôte doit appartenir au même user (uuid:guid) que celui utilisé dans le conteneur.
* Le user du conteneur est elasticsearch:elasticsearch (<b>1000:1000</b>), utiliser la commande ```chown -R 1000:1000 {volume_dir}``` sur la machine hôte pour donner les droits
* Dans le cas d'un volume nommé, il n'est pas nécessaire de changer les droits

### Build et déploiement du server
* Build du jar executable : ```mvn clean package -Pjar``` sur le projet agorha2-server
* Construction et démarrage de l'image docker : ```mvn docker:build docker:start -Pserver,XXX``` sur le projet parent, avec XXX = dev,int,rec ou rec2
* Si besoin avant de démarrer l'image, arrêter et supprimer le conteneur déjà existant : ```mvn docker:stop -Pserver,XXX```

## Release

In order to assemble a binary release, clone and build as described in Quickstart and zip server/config/ dir and server/target/linkextraction-server.jar file.

In order to be able to do it with a non-SNAPSHOT version, first do as follow :

Remove -SNAPSHOT from all modules, tag (-DtagNameFormat=@{project.version}), upgrade project version number :
```bash
mvn release:prepare -DautoVersionSubmodules=true (-Dresume=false) (-DdryRun=true)
```
To test it, add -DdryRun=true. To retry, add -Dresume=false.

Then (checkout said tag and) assembly binary release as said previously.

## Faster build

To achieve a faster build, you can skip tests and stay offline (provided all dependencies have already been installed in a previous build) :
```bash
mvn clean install -o -DskipTests
```

## remote debugging of maven unit tests :

```bash
mvn -Dmaven.surefire.debug="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=8002 -Xnoagent -Djava.compiler=NONE" -Dtest=**Test** test
```

as said as http://maven.apache.org/surefire/maven-surefire-plugin/examples/debugging.html

