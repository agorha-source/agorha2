package fr.inha.agorha2.service.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.intf.IVersionableServiceDedicated;
import fr.inha.agorha2.ServerConfiguration;
import fr.inha.agorha2.exception.FunctionnalException;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.ExportService;
import fr.inha.agorha2.service.NoticeDataSourceMode;
import fr.inha.agorha2.service.NoticeService;
import fr.inha.elastic.model.Notice;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = ServerConfiguration.class)
@ActiveProfiles("test")
@TestInstance(Lifecycle.PER_CLASS)
class ExportServiceImplIT {

  @Autowired
  private ExportService exportService;
  @Autowired
  private NoticeService noticeService;
  @Autowired
  @Qualifier(value = "esPersonService")
  private IVersionableServiceDedicated<Notice> servicePerson;
  @Autowired
  @Qualifier(value = "esArtworkService")
  private IVersionableServiceDedicated<Notice> serviceArtwork;
  @Autowired
  @Qualifier(value = "esCollectionService")
  private IVersionableServiceDedicated<Notice> serviceCollection;
  @Autowired
  @Qualifier(value = "esEventService")
  private IVersionableServiceDedicated<Notice> serviceEvent;
  @Autowired
  @Qualifier(value = "esReferenceService")
  private IVersionableServiceDedicated<Notice> serviceReference;

  private List<Notice> notices = new ArrayList<>();

  @BeforeAll
  public void initData() throws FunctionnalException, ServiceException, IOException, URISyntaxException {
    try (Stream<Path> paths = Files.walk(Paths.get(ClassLoader.getSystemResource("export").toURI()))) {
      paths.filter(Files::isRegularFile).forEach(file -> {
        try {
          notices.add(noticeService.create(FileUtils.readFileToString(file.toFile(), "UTF-8"),
              NoticeDataSourceMode.CONTRIBUTION));
        } catch (ServiceException | IOException e) {
          e.printStackTrace();
        }
      });
    }
  }

  @AfterAll
  public void deleteData() throws EsRequestException {
    for (Notice notice : notices) {
      switch (notice.getInternal().getNoticeType()) {
        case ARTWORK:
          serviceArtwork.delete(notice.getId(), RefreshPolicy.IMMEDIATE);
          break;
        case COLLECTION:
          serviceCollection.delete(notice.getId(), RefreshPolicy.IMMEDIATE);
          break;
        case EVENT:
          serviceEvent.delete(notice.getId(), RefreshPolicy.IMMEDIATE);
          break;
        case PERSON:
          servicePerson.delete(notice.getId(), RefreshPolicy.IMMEDIATE);
          break;
        case REFERENCE:
          serviceReference.delete(notice.getId(), RefreshPolicy.IMMEDIATE);
          break;
        default:
          break;
      }
    }
  }

  @Test
  void exportCsvTest() throws ServiceException, EsRequestException, IOException {
    List<String> ids = notices.stream().map(notice -> notice.getInternal().getUuid()).collect(Collectors.toList());
    File zip = exportService.exportCsv(ids);
    assertTrue(zip.exists());
    Files.delete(zip.toPath());
  }
}
