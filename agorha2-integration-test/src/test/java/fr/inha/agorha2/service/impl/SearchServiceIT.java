package fr.inha.agorha2.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.intf.IVersionableServiceDedicated;
import fr.inha.agorha2.ServerConfiguration;
import fr.inha.agorha2.api.dto.Aggregation;
import fr.inha.agorha2.api.dto.DateRange;
import fr.inha.agorha2.api.dto.DateRangeFilter;
import fr.inha.agorha2.api.dto.SearchFilter.Operator;
import fr.inha.agorha2.api.dto.request.AdvancedSearchRequest;
import fr.inha.agorha2.api.dto.request.ApiSearchRequest;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest;
import fr.inha.agorha2.api.dto.response.NoticeSearchResponse;
import fr.inha.agorha2.exception.FunctionnalException;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.service.NoticeDataSourceMode;
import fr.inha.agorha2.service.NoticeService;
import fr.inha.agorha2.service.SearchService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.elastic.model.Notice;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = ServerConfiguration.class)
@ActiveProfiles("test")
@TestInstance(Lifecycle.PER_CLASS)
class SearchServiceIT {

  @Autowired
  private SearchService searchService;

  @Autowired
  private NoticeService noticeService;

  @Autowired
  @Qualifier(value = "esPersonService")
  private IVersionableServiceDedicated<Notice> servicePerson;

  @MockBean
  private UserInfoService userInfoService;

  private String id1;
  private String id2;
  private String id3;

  private String uuid1;
  private String uuid2;
  private String uuid3;

  @BeforeAll
  public void initData() throws FunctionnalException, ServiceException, IOException {
    MockitoAnnotations.initMocks(this);
    InputStream stream1 = SearchServiceIT.class.getClassLoader().getResourceAsStream("search/person1.json");
    Notice person1 = noticeService.create(IOUtils.toString(stream1, "UTF-8"), NoticeDataSourceMode.CONTRIBUTION);
    id1 = person1.getId();
    uuid1 = person1.getInternal().getUuid();
    InputStream stream2 = SearchServiceIT.class.getClassLoader().getResourceAsStream("search/person2.json");
    Notice person2 = noticeService.create(IOUtils.toString(stream2, "UTF-8"), NoticeDataSourceMode.CONTRIBUTION);
    id2 = person2.getId();
    uuid2 = person2.getInternal().getUuid();
    InputStream stream3 = SearchServiceIT.class.getClassLoader().getResourceAsStream("search/person3.json");
    Notice person3 = noticeService.create(IOUtils.toString(stream3, "UTF-8"), NoticeDataSourceMode.CONTRIBUTION);
    id3 = person3.getId();
    uuid3 = person3.getInternal().getUuid();
  }

  @AfterAll
  public void deleteData() throws EsRequestException {
    servicePerson.delete(id1, RefreshPolicy.IMMEDIATE);
    servicePerson.delete(id2, RefreshPolicy.IMMEDIATE);
    servicePerson.delete(id3, RefreshPolicy.IMMEDIATE);
  }

  @Test
  void advancedSearchFacetTest() throws ServiceException {
    Mockito.doReturn(true).when(userInfoService)
        .hasAnyRole(Arrays.asList(Authority.ADMIN.name(), Authority.CONTRIB.name(), Authority.DATABASE_MANAGER.name()));

    Aggregation agg = new Aggregation();
    agg.setField("content.computedFacetDateStart");
    agg.setPathHierarchical(true);
    agg.getFilters().addAll(Arrays.asList("/19e siècle"));

    AdvancedSearchRequest request = new AdvancedSearchRequest();
    request.getAggregations().put("dates", agg);
    // Une recherche avancée sans request et sans filters ne renvoie rien
    request.setRequest("blabla");
    testResults(request, Arrays.asList(uuid1, uuid2));
  }

  @Test
  void advancedSearchDateFilterTest() throws ServiceException, ParseException {
    Mockito.doReturn(true).when(userInfoService)
        .hasAnyRole(Arrays.asList(Authority.ADMIN.name(), Authority.CONTRIB.name(), Authority.DATABASE_MANAGER.name()));

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd GG", Locale.ENGLISH);

    DateRangeFilter dateRangeFilter = new DateRangeFilter(Operator.ANY);
    dateRangeFilter.setField("content.identificationInformation.institution.date");

    DateRange dateRange = new DateRange();
    dateRange.setMin(simpleDateFormat.parse("1863-01-01 AD"));
    dateRange.setMax(simpleDateFormat.parse("1890-01-01 AD"));

    dateRangeFilter.getValues().add(dateRange);

    AdvancedSearchRequest request = new AdvancedSearchRequest();
    request.getFilters().add(dateRangeFilter);
    testResults(request, Arrays.asList(uuid1));

    dateRange.setMin(simpleDateFormat.parse("1865-01-01 AD"));
    dateRange.setMax(simpleDateFormat.parse("1865-01-01 AD"));
    testResults(request, Arrays.asList(uuid1));

    dateRange.setMin(simpleDateFormat.parse("1865-01-01 AD"));
    dateRange.setMax(simpleDateFormat.parse("1868-01-01 AD"));
    testResults(request, Arrays.asList(uuid1));

    dateRange.setMin(null);
    dateRange.setMax(simpleDateFormat.parse("1865-01-01 AD"));
    testResults(request, Arrays.asList(uuid1));

    dateRange.setMin(simpleDateFormat.parse("1860-01-01 AD"));
    dateRange.setMax(simpleDateFormat.parse("1865-01-01 AD"));
    testResults(request, Arrays.asList(uuid1));

    dateRange.setMin(simpleDateFormat.parse("1890-01-01 AD"));
    dateRange.setMax(null);
    testResults(request, Arrays.asList(uuid2, uuid3));

    dateRange.setMin(simpleDateFormat.parse("1865-01-01 AD"));
    dateRange.setMax(simpleDateFormat.parse("1890-01-01 AD"));
    testResults(request, Arrays.asList(uuid1));

    dateRange.setMin(simpleDateFormat.parse("1888-01-01 AD"));
    dateRange.setMax(simpleDateFormat.parse("1894-01-01 AD"));
    testResults(request, Arrays.asList(uuid1, uuid2));
  }

  @Test
  void searchTest() {
    Mockito.doReturn(true).when(userInfoService)
        .hasAnyRole(Arrays.asList(Authority.ADMIN.name(), Authority.CONTRIB.name(), Authority.DATABASE_MANAGER.name()));

    NoticeSearchRequest request = new NoticeSearchRequest(1, 10, null, "1950", null, null);
    testResults(request, Arrays.asList(uuid3));

  }

  @Test
  void searchExactTest() {
    Mockito.doReturn(true).when(userInfoService)
        .hasAnyRole(Arrays.asList(Authority.ADMIN.name(), Authority.CONTRIB.name(), Authority.DATABASE_MANAGER.name()));

    NoticeSearchRequest request = new NoticeSearchRequest(1, 10, null, "\"peintre de Dutuit\"", null, null);
    testResults(request, Arrays.asList(uuid1));

    request = new NoticeSearchRequest(1, 10, null, "\"Peintre des Dutuit\"", null, null);
    testResults(request, Arrays.asList());

  }

  private void testResults(ApiSearchRequest request, List<String> uuidsTotest) {
    NoticeSearchResponse response = null;
    try {
      if (request instanceof AdvancedSearchRequest) {
        response = searchService.advancedSearch((AdvancedSearchRequest) request, null);
      } else if (request instanceof NoticeSearchRequest) {
        response = searchService.search((NoticeSearchRequest) request, null);
      }
      List<String> uuids = response.getResults().stream().map(res -> res.getId()).collect(Collectors.toList());
      assertEquals(uuidsTotest.size(), uuids.size());
      for (String uuid : uuidsTotest) {
        assertTrue(uuids.contains(uuid), "Expected uuid : " + uuid + " in the result, but not found");
      }
    } catch (Exception e) {
      Assertions.fail(e);
    }
  }
}
