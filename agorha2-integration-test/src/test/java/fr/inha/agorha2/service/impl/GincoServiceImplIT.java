package fr.inha.agorha2.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.databind.JsonNode;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.service.ThesaurusService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
public class GincoServiceImplIT {

  @Autowired
  private ThesaurusService gincoService;

  @Test
  public void getConceptPathTest() throws ServiceException {
    JsonNode concept = gincoService.getConcept(
        "https://ssl-agorha2.coexya.eu/thesaurus/resource/ark:/54721/0de3f246-0694-4274-8e28-fe0d2e757b83");
    String conceptPath = gincoService.getConceptPath(concept);

    assertEquals("/10e siècle/1ère moitié du 10e siècle/1er quart du 10e siècle", conceptPath);
  }

}
