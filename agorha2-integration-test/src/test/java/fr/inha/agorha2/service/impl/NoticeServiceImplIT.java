package fr.inha.agorha2.service.impl;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sword.utils.elasticsearch.contexts.DefaultEsContext;
import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.intf.IVersionableServiceDedicated;
import fr.inha.agorha2.ServerConfiguration;
import fr.inha.agorha2.exception.FunctionnalException;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.service.NoticeDataSourceMode;
import fr.inha.agorha2.service.NoticeService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.elastic.config.EsMapperConfigurator;
import fr.inha.elastic.model.Notice;
import fr.inha.elastic.model.NoticeStatus;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import javax.ws.rs.BadRequestException;
import org.apache.commons.io.IOUtils;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = ServerConfiguration.class)
@ActiveProfiles("test")
@TestInstance(Lifecycle.PER_CLASS)
class NoticeServiceImplIT {

  @Autowired
  private NoticeService noticeService;

  @Autowired
  @Qualifier(value = "esPersonService")
  private IVersionableServiceDedicated<Notice> servicePerson;

  @Autowired
  @Qualifier(value = "esArtworkService")
  private IVersionableServiceDedicated<Notice> serviceArtwork;

  @MockBean
  private UserInfoService userInfoService;

  private String idArtwork;
  private String idNewArtwork;
  private String idPerson;

  private String uuidArtwork;
  private String uuidNewArtwork;
  private String uuidPerson;

  @BeforeAll
  public void initData() throws FunctionnalException, ServiceException, IOException {
    EsMapperConfigurator.getInstance().configureMapper(DefaultEsContext.instance().getObjectMapper());
    MockitoAnnotations.initMocks(this);
    InputStream stream1 = SearchServiceIT.class.getClassLoader().getResourceAsStream("notice/person.json");
    InputStream stream2 = SearchServiceIT.class.getClassLoader().getResourceAsStream("notice/artwork.json");
    Notice person = noticeService.create(IOUtils.toString(stream1, "UTF-8"), NoticeDataSourceMode.CONTRIBUTION);
    idPerson = person.getId();
    uuidPerson = person.getInternal().getUuid();
    Notice artwork = noticeService.create(IOUtils.toString(stream2, "UTF-8"), NoticeDataSourceMode.CONTRIBUTION);
    idArtwork = artwork.getId();
    uuidArtwork = artwork.getInternal().getUuid();
  }

  @AfterAll
  public void deleteData() throws EsRequestException {
    try {
      serviceArtwork.delete(idArtwork, RefreshPolicy.IMMEDIATE);
      serviceArtwork.delete(idNewArtwork, RefreshPolicy.IMMEDIATE);
    } catch (Exception e) {
      System.out.println(e);
    }
    servicePerson.delete(idPerson, RefreshPolicy.IMMEDIATE);
  }

  @Test
  void updateNoticeTest() throws ServiceException, JsonProcessingException, EsRequestException {
    Mockito.doReturn(true).when(userInfoService).hasAnyRole(Arrays.asList(Authority.ADMIN.name()));

    Notice person = noticeService.get(uuidPerson);
    assertEquals(NoticeStatus.CREATION_IN_PROGRESS.getValue(), person.getInternal().getStatus());
    assertEquals(0, person.getInternal().getReference().size());

    Notice originalArtwork = noticeService.get(uuidArtwork);
    assertEquals(NoticeStatus.CREATION_IN_PROGRESS.getValue(), originalArtwork.getInternal().getStatus());
    assertArrayEquals(Arrays.asList(uuidPerson).toArray(), originalArtwork.getInternal().getReference().toArray());

    originalArtwork = noticeService.update(uuidArtwork,
        DefaultEsContext.instance().getObjectMapper().writeValueAsString(originalArtwork));

    assertEquals(NoticeStatus.CREATION_IN_PROGRESS.getValue(), originalArtwork.getInternal().getStatus());

    noticeService.upddateStatus(Arrays.asList(uuidPerson), NoticeStatus.PUBLISHED);

    person = noticeService.get(uuidPerson);
    assertEquals(NoticeStatus.PUBLISHED.getValue(), person.getInternal().getStatus());

    noticeService.upddateStatus(Arrays.asList(uuidArtwork), NoticeStatus.PUBLISHED);

    originalArtwork = noticeService.get(uuidArtwork);
    assertEquals(NoticeStatus.PUBLISHED.getValue(), originalArtwork.getInternal().getStatus());

    person = noticeService.get(uuidPerson);
    assertArrayEquals(Arrays.asList(uuidArtwork).toArray(), person.getInternal().getReference().toArray());

    Notice newArtwork = noticeService.update(uuidArtwork,
        DefaultEsContext.instance().getObjectMapper().writeValueAsString(originalArtwork));
    uuidNewArtwork = newArtwork.getInternal().getUuid();
    idNewArtwork = newArtwork.getId();
    assertEquals(NoticeStatus.CREATION_IN_PROGRESS.getValue(), newArtwork.getInternal().getStatus());
    assertNotEquals(uuidNewArtwork, uuidArtwork);
    assertEquals(uuidArtwork, newArtwork.getInternal().getSourceNoticeUuid());

    originalArtwork = noticeService.get(uuidArtwork);
    assertEquals(NoticeStatus.PUBLISHED.getValue(), originalArtwork.getInternal().getStatus());
    assertEquals(uuidArtwork, originalArtwork.getInternal().getUuid());

    final String noticeAsJson = DefaultEsContext.instance().getObjectMapper().writeValueAsString(originalArtwork);
    assertThrows(BadRequestException.class, () -> {
      noticeService.update(uuidArtwork, noticeAsJson);
    });

    noticeService.upddateStatus(Arrays.asList(uuidNewArtwork), NoticeStatus.PUBLISHED);

    newArtwork = noticeService.get(uuidArtwork);
    assertEquals(NoticeStatus.PUBLISHED.getValue(), newArtwork.getInternal().getStatus());
    assertEquals(uuidArtwork, newArtwork.getInternal().getUuid());

    assertNull(serviceArtwork.get(idArtwork));
    assertNotNull(serviceArtwork.get(idNewArtwork));
  }
}
