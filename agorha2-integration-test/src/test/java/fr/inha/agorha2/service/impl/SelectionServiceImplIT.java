package fr.inha.agorha2.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import com.sword.utils.elasticsearch.exceptions.EsRequestException;
import com.sword.utils.elasticsearch.intf.IServiceDedicated;
import com.sword.utils.elasticsearch.intf.IVersionableServiceDedicated;
import fr.inha.agorha2.ServerConfiguration;
import fr.inha.agorha2.api.dto.request.NoticeSearchRequest;
import fr.inha.agorha2.api.dto.response.NoticeSearchResponse;
import fr.inha.agorha2.api.dto.sort.SelectionSortType;
import fr.inha.agorha2.exception.FunctionnalException;
import fr.inha.agorha2.exception.ServiceException;
import fr.inha.agorha2.security.Authority;
import fr.inha.agorha2.security.UserPrincipal;
import fr.inha.agorha2.service.ExportService;
import fr.inha.agorha2.service.NoticeDataSourceMode;
import fr.inha.agorha2.service.NoticeService;
import fr.inha.agorha2.service.SearchService;
import fr.inha.agorha2.service.SelectionService;
import fr.inha.agorha2.service.UserInfoService;
import fr.inha.elastic.model.Notice;
import fr.inha.elastic.model.Selection;
import fr.inha.elastic.model.User;
import fr.inha.elastic.model.UserInternal;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.apache.commons.io.IOUtils;
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = { ServerConfiguration.class })
@ActiveProfiles("test")
@TestInstance(Lifecycle.PER_CLASS)
class SelectionServiceImplIT {

  private static final String SELECTION_TITLE = "selection 1";

  @Autowired
  private SearchService searchService;

  @Autowired
  private ExportService exportService;

  @Autowired
  private SelectionService selectionService;

  @Autowired
  private NoticeService noticeService;

  @MockBean
  private UserInfoService userInfoService;

  @Autowired
  @Qualifier(value = "esPersonService")
  private IVersionableServiceDedicated<Notice> servicePerson;

  @Autowired
  @Qualifier(value = "esSelectionService")
  private IServiceDedicated<Selection> serviceSelection;

  private String id1;
  private String id2;
  private String id3;

  private String uuid1;
  private String uuid2;
  private String uuid3;

  private String uuidUser = UUID.randomUUID().toString();

  private String selectionId;

  @BeforeAll
  public void initData() throws FunctionnalException, ServiceException, IOException, EsRequestException {
    MockitoAnnotations.initMocks(this);

    when(userInfoService.getCurrentUsername()).thenReturn("user1");
    User user = new User();
    UserInternal internal = new UserInternal();
    internal.setLogin("user1");
    internal.setUuid(uuidUser);
    user.setInternal(internal);
    when(userInfoService.getPrincipal()).thenReturn(new UserPrincipal(user, null));
    Mockito.doReturn(true).when(userInfoService)
        .hasAnyRole(Arrays.asList(Authority.ADMIN.name(), Authority.CONTRIB.name(), Authority.DATABASE_MANAGER.name()));

    InputStream stream1 = SelectionServiceImplIT.class.getClassLoader().getResourceAsStream("search/person1.json");
    Notice person1 = noticeService.create(IOUtils.toString(stream1, "UTF-8"), NoticeDataSourceMode.CONTRIBUTION);
    id1 = person1.getId();
    uuid1 = person1.getInternal().getUuid();
    InputStream stream2 = SelectionServiceImplIT.class.getClassLoader().getResourceAsStream("search/person2.json");
    Notice person2 = noticeService.create(IOUtils.toString(stream2, "UTF-8"), NoticeDataSourceMode.CONTRIBUTION);
    id2 = person2.getId();
    uuid2 = person2.getInternal().getUuid();
    InputStream stream3 = SelectionServiceImplIT.class.getClassLoader().getResourceAsStream("search/person3.json");
    Notice person3 = noticeService.create(IOUtils.toString(stream3, "UTF-8"), NoticeDataSourceMode.CONTRIBUTION);
    id3 = person3.getId();
    uuid3 = person3.getInternal().getUuid();
  }

  @AfterAll
  public void deleteData() throws EsRequestException, ServiceException {
    servicePerson.delete(id1, RefreshPolicy.IMMEDIATE);
    servicePerson.delete(id2, RefreshPolicy.IMMEDIATE);
    servicePerson.delete(id3, RefreshPolicy.IMMEDIATE);
    serviceSelection.delete(SELECTION_TITLE, RefreshPolicy.IMMEDIATE);
  }

  @Test
  void testAll() throws ServiceException, IOException {
    Selection selection = selectionService.create(SELECTION_TITLE, Arrays.asList(uuid1, uuid2));
    selectionId = selection.getId();

    selection = selectionService.get(selectionId);
    assertTrue(selection.getNotices().containsAll(Arrays.asList(uuid1, uuid2)));
    assertTrue(Arrays.asList(uuid1, uuid2).containsAll(selection.getNotices()));

    selection = selectionService.remove(selectionId, Arrays.asList(uuid1));
    assertTrue(selection.getNotices().containsAll(Arrays.asList(uuid2)));
    assertTrue(Arrays.asList(uuid2).containsAll(selection.getNotices()));

    selection = selectionService.add(selectionId, Arrays.asList(uuid3));
    assertTrue(selection.getNotices().containsAll(Arrays.asList(uuid2, uuid3)));
    assertTrue(Arrays.asList(uuid2, uuid3).containsAll(selection.getNotices()));

    List<Selection> list = selectionService.list("sel", 1, 10, "asc", SelectionSortType.SIZE);
    assertEquals(1, list.size());

    NoticeSearchResponse response = searchService
        .search(new NoticeSearchRequest(1, 20, null, "", null, selectionId, null), null);
    List<String> ids = new ArrayList<>();
    response.getResults().forEach(result -> ids.add(result.getId()));
    assertTrue(ids.containsAll(Arrays.asList(uuid2, uuid3)));
    assertTrue(Arrays.asList(uuid2, uuid3).containsAll(ids));

    File zip = exportService.exportSelection(selectionId);
    assertTrue(zip.exists());
    Files.delete(zip.toPath());
    Files.delete(zip.toPath().getParent());

    selectionService.delete(selectionId);
  }
}
